`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:28:30 06/16/2023 
// Design Name: 
// Module Name:    TFF 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//				TFF is asynchronous with clear signal
//				Made using behavioural model
// Dependencies: 
//				None
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//			classwork
//////////////////////////////////////////////////////////////////////////////////
module TFF(
	output reg q,
	input T,clk,R
    );
	
always@(posedge clk or posedge R)
begin
	if(R)
		q <= 1'b0;
	else if (T)
		q <= ~q;	
	else if(!T)
		q <= q;
end

endmodule
