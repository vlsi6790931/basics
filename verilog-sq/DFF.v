`timescale 1ns / 1ps

// DFF with reset signal - Behaviour Modelling

module DFF(
	output reg q,
	input din, clk,R
    );
	 
always@(posedge clk or negedge R)
begin
	
		if(R)
			q <= 1'b0;
		else
			q <= din;
end

endmodule