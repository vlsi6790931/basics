`timescale 1ns / 1ps

// Gate level modelling 

module SISO_DFF(
output qout,
input clk, clear, Din
 );

wire w1,w2,w3,w4;

DFF N1 (w1, Din, clk, clear);
DFF N2 (w2, w1,clk, clear);
DFF N3 (w3, w2, clk, clear);
DFF N4 (qout, w3, clk, clear);


endmodule
