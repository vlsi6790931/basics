`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:42:45 06/16/2023 
// Design Name: 
// Module Name:    counter_TFF 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//						Counter using TFF.
//						Gate level modelling.
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//				classwork
//////////////////////////////////////////////////////////////////////////////////
module counter_TFF(
	output Q0,Q1,Q2,Q3,
	input T,clk, clear
 );
	 
//wire w1,w2,w3;
//assign Q1 = w3;
//assign Q2 = w2;
//assign Q3 = w1;

TFF N1 (Q3,T,clk,clear);
//assign Q3= w3;
TFF N2 (Q2,T,Q3,clear);
//assign Q2 = w2;
TFF N3 (Q1,T,Q2,clear);
//assign Q1 = w1;
TFF N4 (Q0,T,Q1,clear);

endmodule
