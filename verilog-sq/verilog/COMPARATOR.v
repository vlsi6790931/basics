`timescale 1ns / 1ps

module COMPARATOR(
    output	reg	O_great, O_eq, O_less,
	 input	[7:0] A, B
    );

// give logical error in (A&&B) not in (A or B)
always@(*) begin
    {O_great, O_eq, O_less} = 0; 
    if(A > B) begin
			O_great = 1'b1;
		end
    else if(A<B) begin
			O_less = 1'b1;
		end
    else begin
			O_eq = 1'b1;
		end
	end
endmodule