`timescale 1ns / 1ps

module TWOsCOMPLIMENT(
	output	reg	O[7:0],
   input 			A[7:0]
   );

reg temp[7:0];
integer index;
always@(*)

begin

	temp = 8'b0;
	
	for(index = 0; index < 8; index = index + 1)
	begin
		O[index]= A[index] ~^ temp[index];
	end
	
end

endmodule
