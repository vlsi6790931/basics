`timescale 1ns / 1ps

// 1:8 DEMUX

module DEMUX(
    output reg [7:0]	O,
    input				A,
	 input		[2:0]	S
    );

integer index ;

always@(*)
begin
// O needs to be initialised with zero with each triggering of always block
	O=0;
	index = S;
	O[index] = A;
end

endmodule
