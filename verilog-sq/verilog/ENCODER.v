`timescale 1ns / 1ps
// 8:3 ENCODER
module ENCODER(
	output	reg	[2:0]		O,
	input				[7:0]		A
    );

integer reqidx;
reg [7:0] temp; // initialising here gives error "A(input) is not a constant"

always@(*)
	begin
		reqidx = -1; // for A=0000001 O=000
		temp = A;
		
		if(A === 1'd0)
			O = 3'bx;
		else 
			begin
				while(temp)
					begin// there will be only one set bit
						temp = temp >> 1;
						reqidx = reqidx + 1;
					end
				O = reqidx;
			end
	end
endmodule
