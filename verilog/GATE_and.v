`timescale 1ns / 1ps

`define X 1'bx
`define Z 1'bz

module GATE_and(
	 output reg O,
    input A,
    input B
    );
	 
always@(*)
	 
// ! - logical - negation
	 if (~A || ~B)
		O = 1'b0;
	else if ( A=== `X || B=== `X || A === `Z|| B === `Z)
		O = 1'bx;
	else if (A && B)
		O = 1'b1;

endmodule
