`timescale 1ns / 1ps

`define X 1'bx
`define Z 1'bz

module GATE_xor(
	 output reg O,
	 input A,
    input B
    );

always@(*)

if (A===`X||A===`Z||B===`X||B===`Z)
	O=1'bx;
	
// if both input is same, i.e. even one count whether 0 or 2
else if (A==B)
	O=1'b0;
	
else
	O=1'b1;

endmodule
