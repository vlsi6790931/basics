`timescale 1ns / 1ps
`define X 1'bx
`define Z 1'bz

module GATE_not(
    output reg O,
	 input A
    );

always@(*)

if ( A===`X || A === `Z)
	O=`X;
else
	O=~A;

endmodule
