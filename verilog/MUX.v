`timescale 1ns / 1ps

// 8:1 MUX

module MUX(
    output reg	O,
    input	[7:0]	A,
	 input	[2:0]	S
    );

//automatic for re-entrant
// verilog: default functions are static
//				should be defined as automatic for simultaneous calls	
//function automatic [7:0] mux;
//	try recursion
//endfunction
integer index ;

always@(*)
begin
	index = S;
	O = A[index];

end

endmodule
