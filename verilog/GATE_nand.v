`timescale 1ns / 1ps

`define X 1'bx
`define Z 1'bz

module GATE_nand(
    output reg O,
	 input A,
    input B
    );

always@(*)

if (~A || ~B)
	O=1'b1;
else if ( A===`X || A===`Z || B===`X || B===`Z)
	O=`X;
else if (A && B)
	O=1'b0;

endmodule
