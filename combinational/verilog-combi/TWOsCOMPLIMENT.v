`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:03:16 07/31/2023 
// Design Name: 
// Module Name:    TWOsCOMPLIMENT 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module TWOsCOMPLIMENT(
	output	reg	[7:0]		O,
   input 			[7:0]		A
   );

reg temp[7:0] = 8'b00000000;
integer index;
always@(*)

begin

	//temp = 8'b0;
	
	for(index = 0; index < 8; index = index + 1)
	begin
		O[index]= A[index] ~^ temp[index];
	end
	
end

endmodule
