`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:55:44 07/31/2023 
// Design Name: 
// Module Name:    GateNand 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


`define X 1'bx
`define Z 1'bz

module GateNand(
    output reg O,
	 input A,
    input B
    );

always@(*)

if (~A || ~B)
	O=1'b1;
else if ( A===`X || A===`Z || B===`X || B===`Z)
	O=`X;
else if (A && B)
	O=1'b0;

endmodule
