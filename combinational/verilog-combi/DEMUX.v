`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:51:36 07/31/2023 
// Design Name: 
// Module Name:    DEMUX 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


// 1:8 DEMUX

module DEMUX(
    output reg [7:0]	O,
    input				A,
	 input		[2:0]	S
    );

integer index ;

always@(*)
begin
// O needs to be initialised with zero with each triggering of always block
	O=0;
	index = S;
	O[index] = A;
end

endmodule
