`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:50:18 07/31/2023 
// Design Name: 
// Module Name:    DECODER 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// 3:8 DECODER

module DECODER(
	output	reg	[7:0]		O,
	input				[2:0]		A
	);

integer index;
reg [7:0] temp;

always@(*)
	begin
		O=8'b0; //reinitialisation is required with each trigger
		O[A] = 1'b1;	// encoder A=1, O=00
							// decoder A=0, O=00...1 <- Oth index, same as input
	end
endmodule
