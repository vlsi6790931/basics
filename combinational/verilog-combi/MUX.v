`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:01:20 07/31/2023 
// Design Name: 
// Module Name:    MUX 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

// 8:1 MUX

module MUX(
    output reg	O,
    input	[7:0]	A,
	 input	[2:0]	S
    );

//automatic for re-entrant
// verilog: default functions are static
//				should be defined as automatic for simultaneous calls	
//function automatic [7:0] mux;
//	try recursion
//endfunction
integer index ;

always@(*)
begin
	index = S;
	O = A[index];

end

endmodule
