`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:53:27 07/31/2023 
// Design Name: 
// Module Name:    GateAnd 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`define X 1'bx
`define Z 1'bz

module GATE_and(
	 output reg O,
    input A,
    input B
    );
	 
always@(*)
	 
// ! - logical - negation
	 if (~A || ~B)
		O = 1'b0;
	else if ( A=== `X || B=== `X || A === `Z|| B === `Z)
		O = 1'bx;
	else if (A && B)
		O = 1'b1;

endmodule
