<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Y(3)" />
        <signal name="X(2)" />
        <signal name="X(1)" />
        <signal name="XLXN_416" />
        <signal name="X(3)" />
        <signal name="XLXN_180" />
        <signal name="XLXN_189" />
        <signal name="XLXN_184" />
        <signal name="XLXN_187" />
        <signal name="XLXN_186" />
        <signal name="XLXN_209" />
        <signal name="XLXN_188" />
        <signal name="XLXN_210" />
        <signal name="X(0)" />
        <signal name="Y(2)" />
        <signal name="XLXN_190" />
        <signal name="Z(1)" />
        <signal name="XLXN_212" />
        <signal name="Z(0)" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="RESET" />
        <signal name="XLXN_175" />
        <signal name="XLXN_173" />
        <signal name="XLXN_166" />
        <signal name="XLXN_161" />
        <signal name="XLXN_118" />
        <signal name="XLXN_113" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <signal name="Y(1)" />
        <signal name="Y(0)" />
        <signal name="XLXN_680" />
        <signal name="Z(2)" />
        <signal name="XLXN_423" />
        <signal name="XLXN_497" />
        <signal name="XLXN_498" />
        <signal name="XLXN_691" />
        <signal name="XLXN_400" />
        <signal name="XLXN_395" />
        <signal name="XLXN_397" />
        <signal name="XLXN_1525" />
        <signal name="XLXN_1526" />
        <signal name="XLXN_1527" />
        <signal name="XLXN_1497" />
        <signal name="Z(7)" />
        <signal name="XLXN_1537" />
        <signal name="Z(4)" />
        <signal name="Z(5)" />
        <signal name="Z(6)" />
        <signal name="XLXN_1550" />
        <signal name="Z(3)" />
        <signal name="XLXN_1552" />
        <signal name="XLXN_1553" />
        <signal name="XLXN_1554" />
        <signal name="XLXN_1556" />
        <signal name="XLXN_1558" />
        <signal name="XLXN_1559" />
        <signal name="XLXN_1567" />
        <signal name="XLXN_1571" />
        <signal name="Z(7:0)" />
        <signal name="Y(3:0)" />
        <signal name="X(3:0)" />
        <port polarity="Input" name="RESET" />
        <port polarity="Output" name="Z(7:0)" />
        <port polarity="Input" name="Y(3:0)" />
        <port polarity="Input" name="X(3:0)" />
        <blockdef name="FA">
            <timestamp>2023-8-11T19:21:31</timestamp>
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
            <line x2="192" y1="-64" y2="0" x1="192" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="HA">
            <timestamp>2023-8-10T21:23:14</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="176" x="64" y="-156" height="156" />
            <line x2="304" y1="-128" y2="-128" x1="240" />
            <line x2="304" y1="-32" y2="-32" x1="240" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
        </blockdef>
        <block symbolname="and2" name="XLXI_184">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_1527" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_183">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_1525" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_197">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_416" name="I1" />
            <blockpin signalname="Z(2)" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_95">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin signalname="XLXN_680" name="Bin" />
            <blockpin signalname="XLXN_180" name="Cin" />
            <blockpin signalname="XLXN_691" name="Cout" />
            <blockpin signalname="XLXN_498" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_101">
            <blockpin signalname="XLXN_212" name="B" />
            <blockpin signalname="XLXN_190" name="A" />
            <blockpin signalname="XLXN_189" name="C" />
            <blockpin signalname="XLXN_416" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_100">
            <blockpin signalname="XLXN_188" name="Ain" />
            <blockpin signalname="XLXN_210" name="Bin" />
            <blockpin signalname="XLXN_189" name="Cin" />
            <blockpin signalname="XLXN_187" name="Cout" />
            <blockpin signalname="XLXN_423" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_104">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_184" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_99">
            <blockpin signalname="XLXN_186" name="Ain" />
            <blockpin signalname="XLXN_209" name="Bin" />
            <blockpin signalname="XLXN_187" name="Cin" />
            <blockpin signalname="XLXN_180" name="Cout" />
            <blockpin signalname="XLXN_497" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_105">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_186" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_106">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_188" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_108">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_190" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_93">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_173" name="I1" />
            <blockpin signalname="Z(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_94">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_175" name="I1" />
            <blockpin signalname="Z(1)" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_16">
            <blockpin signalname="XLXN_166" name="B" />
            <blockpin signalname="XLXN_118" name="A" />
            <blockpin signalname="XLXN_113" name="C" />
            <blockpin signalname="XLXN_175" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_53">
            <blockpin signalname="XLXN_43" name="Ain" />
            <blockpin signalname="XLXN_161" name="Bin" />
            <blockpin signalname="XLXN_113" name="Cin" />
            <blockpin signalname="XLXN_44" name="Cout" />
            <blockpin signalname="XLXN_212" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_51">
            <blockpin signalname="XLXN_45" name="Ain" />
            <blockpin signalname="XLXN_1552" name="Bin" />
            <blockpin signalname="XLXN_44" name="Cin" />
            <blockpin signalname="XLXN_46" name="Cout" />
            <blockpin signalname="XLXN_210" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_20">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_1553" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_55">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_45" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_56">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_65">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_118" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_92">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_173" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_166" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_161" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_1552" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_186">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_1526" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_191">
            <blockpin signalname="XLXN_1526" name="Ain" />
            <blockpin signalname="XLXN_691" name="Bin" />
            <blockpin signalname="XLXN_400" name="Cin" />
            <blockpin signalname="Z(7)" name="Cout" />
            <blockpin signalname="Z(6)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_185">
            <blockpin signalname="XLXN_1527" name="Ain" />
            <blockpin signalname="XLXN_498" name="Bin" />
            <blockpin signalname="XLXN_395" name="Cin" />
            <blockpin signalname="XLXN_400" name="Cout" />
            <blockpin signalname="Z(5)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_187">
            <blockpin signalname="XLXN_1525" name="Ain" />
            <blockpin signalname="XLXN_497" name="Bin" />
            <blockpin signalname="XLXN_397" name="Cin" />
            <blockpin signalname="XLXN_395" name="Cout" />
            <blockpin signalname="Z(4)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_182">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_1497" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_681">
            <blockpin signalname="XLXN_46" name="B" />
            <blockpin signalname="XLXN_1553" name="A" />
            <blockpin signalname="XLXN_680" name="C" />
            <blockpin signalname="XLXN_209" name="S" />
        </block>
        <block symbolname="HA" name="XLXI_682">
            <blockpin signalname="XLXN_423" name="B" />
            <blockpin signalname="XLXN_1497" name="A" />
            <blockpin signalname="XLXN_397" name="C" />
            <blockpin signalname="XLXN_1550" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_683">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_1550" name="I1" />
            <blockpin signalname="Z(3)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7609" height="5382">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <instance x="4864" y="3184" name="XLXI_184" orien="R90" />
        <instance x="5216" y="3184" name="XLXI_183" orien="R90" />
        <instance x="5952" y="2736" name="XLXI_197" orien="R90" />
        <instance x="5184" y="2272" name="XLXI_95" orien="M90">
        </instance>
        <instance x="6112" y="2288" name="XLXI_101" orien="M90">
        </instance>
        <instance x="4864" y="1952" name="XLXI_104" orien="R90" />
        <instance x="5536" y="2272" name="XLXI_99" orien="M90">
        </instance>
        <instance x="5216" y="1952" name="XLXI_105" orien="R90" />
        <instance x="5888" y="1952" name="XLXI_108" orien="R90" />
        <instance x="6832" y="1696" name="XLXI_93" orien="R90" />
        <instance x="6416" y="1696" name="XLXI_94" orien="R90" />
        <instance x="6576" y="1248" name="XLXI_16" orien="M90">
        </instance>
        <instance x="6240" y="1248" name="XLXI_53" orien="M90">
        </instance>
        <branch name="Y(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5872" y="3104" type="branch" />
            <wire x2="4928" y1="3104" y2="3104" x1="4608" />
            <wire x2="5280" y1="3104" y2="3104" x1="4928" />
            <wire x2="5280" y1="3104" y2="3184" x1="5280" />
            <wire x2="5616" y1="3104" y2="3104" x1="5280" />
            <wire x2="5616" y1="3104" y2="3184" x1="5616" />
            <wire x2="5872" y1="3104" y2="3104" x1="5616" />
            <wire x2="4928" y1="3104" y2="3184" x1="4928" />
            <wire x2="4608" y1="3104" y2="3168" x1="4608" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4992" y="3168" type="branch" />
            <wire x2="4992" y1="3168" y2="3184" x1="4992" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5344" y="3168" type="branch" />
            <wire x2="5344" y1="3168" y2="3184" x1="5344" />
        </branch>
        <branch name="XLXN_416">
            <wire x2="6080" y1="2592" y2="2736" x1="6080" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5344" y="1936" type="branch" />
            <wire x2="5344" y1="1936" y2="1952" x1="5344" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5712" y="1936" type="branch" />
            <wire x2="5712" y1="1936" y2="1952" x1="5712" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4992" y="1936" type="branch" />
            <wire x2="4992" y1="1936" y2="1952" x1="4992" />
        </branch>
        <branch name="XLXN_180">
            <wire x2="5248" y1="2256" y2="2256" x1="5088" />
            <wire x2="5248" y1="2256" y2="2608" x1="5248" />
            <wire x2="5312" y1="2608" y2="2608" x1="5248" />
            <wire x2="5088" y1="2256" y2="2272" x1="5088" />
            <wire x2="5312" y1="2592" y2="2608" x1="5312" />
        </branch>
        <branch name="XLXN_184">
            <wire x2="4960" y1="2208" y2="2272" x1="4960" />
        </branch>
        <branch name="XLXN_187">
            <wire x2="5440" y1="2256" y2="2272" x1="5440" />
            <wire x2="5600" y1="2256" y2="2256" x1="5440" />
            <wire x2="5600" y1="2256" y2="2688" x1="5600" />
            <wire x2="5680" y1="2688" y2="2688" x1="5600" />
            <wire x2="5680" y1="2608" y2="2688" x1="5680" />
        </branch>
        <branch name="XLXN_186">
            <wire x2="5312" y1="2208" y2="2272" x1="5312" />
        </branch>
        <branch name="XLXN_209">
            <wire x2="5376" y1="1568" y2="2272" x1="5376" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6016" y="1936" type="branch" />
            <wire x2="6016" y1="1936" y2="1952" x1="6016" />
        </branch>
        <branch name="Y(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="6224" y="1872" type="branch" />
            <wire x2="5280" y1="1872" y2="1872" x1="4928" />
            <wire x2="5280" y1="1872" y2="1952" x1="5280" />
            <wire x2="5648" y1="1872" y2="1872" x1="5280" />
            <wire x2="5952" y1="1872" y2="1872" x1="5648" />
            <wire x2="5952" y1="1872" y2="1952" x1="5952" />
            <wire x2="6224" y1="1872" y2="1872" x1="5952" />
            <wire x2="5648" y1="1872" y2="1952" x1="5648" />
            <wire x2="4928" y1="1872" y2="1952" x1="4928" />
        </branch>
        <branch name="XLXN_190">
            <wire x2="5984" y1="2208" y2="2288" x1="5984" />
        </branch>
        <branch name="Z(1)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="6512" y="2048" type="branch" />
            <wire x2="6512" y1="1952" y2="2048" x1="6512" />
        </branch>
        <branch name="XLXN_212">
            <wire x2="6080" y1="1568" y2="2288" x1="6080" />
        </branch>
        <branch name="Z(0)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="6928" y="1968" type="branch" />
            <wire x2="6928" y1="1952" y2="1968" x1="6928" />
        </branch>
        <branch name="XLXN_175">
            <wire x2="6544" y1="1552" y2="1696" x1="6544" />
        </branch>
        <branch name="XLXN_173">
            <wire x2="6960" y1="640" y2="1696" x1="6960" />
        </branch>
        <branch name="XLXN_166">
            <wire x2="6544" y1="640" y2="1248" x1="6544" />
        </branch>
        <branch name="XLXN_161">
            <wire x2="6080" y1="640" y2="1248" x1="6080" />
        </branch>
        <branch name="XLXN_118">
            <wire x2="6448" y1="1088" y2="1248" x1="6448" />
        </branch>
        <branch name="XLXN_113">
            <wire x2="6144" y1="1232" y2="1248" x1="6144" />
            <wire x2="6368" y1="1232" y2="1232" x1="6144" />
            <wire x2="6368" y1="1232" y2="1584" x1="6368" />
            <wire x2="6448" y1="1584" y2="1584" x1="6368" />
            <wire x2="6448" y1="1552" y2="1584" x1="6448" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="6016" y1="1088" y2="1248" x1="6016" />
        </branch>
        <branch name="XLXN_46">
            <wire x2="5376" y1="1248" y2="1264" x1="5376" />
            <wire x2="5472" y1="1248" y2="1248" x1="5376" />
            <wire x2="5472" y1="1248" y2="1648" x1="5472" />
            <wire x2="5680" y1="1648" y2="1648" x1="5472" />
            <wire x2="5680" y1="1568" y2="1648" x1="5680" />
        </branch>
        <instance x="5920" y="832" name="XLXI_56" orien="R90" />
        <instance x="6352" y="832" name="XLXI_65" orien="R90" />
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5312" y="816" type="branch" />
            <wire x2="5312" y1="816" y2="832" x1="5312" />
        </branch>
        <branch name="Y(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="6688" y="752" type="branch" />
            <wire x2="5248" y1="752" y2="832" x1="5248" />
            <wire x2="5648" y1="752" y2="752" x1="5248" />
            <wire x2="5984" y1="752" y2="752" x1="5648" />
            <wire x2="5984" y1="752" y2="832" x1="5984" />
            <wire x2="6416" y1="752" y2="752" x1="5984" />
            <wire x2="6416" y1="752" y2="832" x1="6416" />
            <wire x2="6688" y1="752" y2="752" x1="6416" />
            <wire x2="5648" y1="752" y2="832" x1="5648" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6480" y="816" type="branch" />
            <wire x2="6480" y1="816" y2="832" x1="6480" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6048" y="816" type="branch" />
            <wire x2="6048" y1="816" y2="832" x1="6048" />
        </branch>
        <instance x="6864" y="384" name="XLXI_92" orien="R90" />
        <instance x="6448" y="384" name="XLXI_7" orien="R90" />
        <instance x="5984" y="384" name="XLXI_6" orien="R90" />
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6112" y="368" type="branch" />
            <wire x2="6112" y1="368" y2="384" x1="6112" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6576" y="368" type="branch" />
            <wire x2="6576" y1="368" y2="384" x1="6576" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6992" y="368" type="branch" />
            <wire x2="6992" y1="368" y2="384" x1="6992" />
        </branch>
        <branch name="Y(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="7008" y="272" type="branch" />
            <wire x2="6048" y1="272" y2="272" x1="5712" />
            <wire x2="6512" y1="272" y2="272" x1="6048" />
            <wire x2="6928" y1="272" y2="272" x1="6512" />
            <wire x2="7008" y1="272" y2="272" x1="6928" />
            <wire x2="6928" y1="272" y2="384" x1="6928" />
            <wire x2="6512" y1="272" y2="384" x1="6512" />
            <wire x2="6048" y1="272" y2="384" x1="6048" />
            <wire x2="5712" y1="272" y2="368" x1="5712" />
        </branch>
        <branch name="XLXN_680">
            <wire x2="5024" y1="1648" y2="2272" x1="5024" />
            <wire x2="5280" y1="1648" y2="1648" x1="5024" />
            <wire x2="5280" y1="1568" y2="1648" x1="5280" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4672" y="3152" type="branch" />
            <wire x2="4672" y1="3152" y2="3168" x1="4672" />
        </branch>
        <instance x="4544" y="3168" name="XLXI_186" orien="R90" />
        <branch name="Z(2)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="6048" y="3024" type="branch" />
            <wire x2="6048" y1="2992" y2="3024" x1="6048" />
        </branch>
        <branch name="XLXN_497">
            <wire x2="5376" y1="2592" y2="3728" x1="5376" />
        </branch>
        <branch name="XLXN_498">
            <wire x2="5024" y1="2592" y2="3728" x1="5024" />
        </branch>
        <branch name="XLXN_691">
            <wire x2="4704" y1="2640" y2="3728" x1="4704" />
            <wire x2="4960" y1="2640" y2="2640" x1="4704" />
            <wire x2="4960" y1="2592" y2="2640" x1="4960" />
        </branch>
        <instance x="4864" y="3728" name="XLXI_191" orien="M90">
        </instance>
        <instance x="5184" y="3728" name="XLXI_185" orien="M90">
        </instance>
        <instance x="5536" y="3728" name="XLXI_187" orien="M90">
        </instance>
        <branch name="XLXN_400">
            <wire x2="4768" y1="3712" y2="3728" x1="4768" />
            <wire x2="4896" y1="3712" y2="3712" x1="4768" />
            <wire x2="4896" y1="3712" y2="4064" x1="4896" />
            <wire x2="4960" y1="4064" y2="4064" x1="4896" />
            <wire x2="4960" y1="4048" y2="4064" x1="4960" />
        </branch>
        <branch name="XLXN_395">
            <wire x2="5088" y1="3712" y2="3728" x1="5088" />
            <wire x2="5248" y1="3712" y2="3712" x1="5088" />
            <wire x2="5248" y1="3712" y2="4064" x1="5248" />
            <wire x2="5312" y1="4064" y2="4064" x1="5248" />
            <wire x2="5312" y1="4048" y2="4064" x1="5312" />
        </branch>
        <branch name="XLXN_397">
            <wire x2="5440" y1="3712" y2="3728" x1="5440" />
            <wire x2="5600" y1="3712" y2="3712" x1="5440" />
            <wire x2="5600" y1="3712" y2="4128" x1="5600" />
            <wire x2="5648" y1="4128" y2="4128" x1="5600" />
            <wire x2="5648" y1="4032" y2="4128" x1="5648" />
        </branch>
        <branch name="XLXN_1525">
            <wire x2="5312" y1="3440" y2="3728" x1="5312" />
        </branch>
        <branch name="XLXN_1526">
            <wire x2="4640" y1="3424" y2="3728" x1="4640" />
        </branch>
        <branch name="XLXN_1527">
            <wire x2="4960" y1="3440" y2="3728" x1="4960" />
        </branch>
        <branch name="XLXN_1497">
            <wire x2="5648" y1="3440" y2="3728" x1="5648" />
        </branch>
        <branch name="Z(7)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4384" y="4464" type="branch" />
            <wire x2="4576" y1="3712" y2="3712" x1="4384" />
            <wire x2="4576" y1="3712" y2="4064" x1="4576" />
            <wire x2="4640" y1="4064" y2="4064" x1="4576" />
            <wire x2="4384" y1="3712" y2="4464" x1="4384" />
            <wire x2="4640" y1="4048" y2="4064" x1="4640" />
        </branch>
        <branch name="Z(4)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5376" y="4480" type="branch" />
            <wire x2="5376" y1="4048" y2="4480" x1="5376" />
        </branch>
        <branch name="Z(5)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5024" y="4464" type="branch" />
            <wire x2="5024" y1="4048" y2="4464" x1="5024" />
        </branch>
        <branch name="Z(6)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4704" y="4464" type="branch" />
            <wire x2="4704" y1="4048" y2="4464" x1="4704" />
        </branch>
        <iomarker fontsize="28" x="2816" y="1680" name="RESET" orien="R180" />
        <instance x="5408" y="1264" name="XLXI_681" orien="M90">
        </instance>
        <branch name="Z(3)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5712" y="4496" type="branch" />
            <wire x2="5712" y1="4480" y2="4496" x1="5712" />
        </branch>
        <branch name="XLXN_1553">
            <wire x2="5280" y1="1088" y2="1264" x1="5280" />
        </branch>
        <instance x="5184" y="832" name="XLXI_20" orien="R90" />
        <instance x="5776" y="3728" name="XLXI_682" orien="M90">
        </instance>
        <branch name="XLXN_423">
            <wire x2="5744" y1="2608" y2="3728" x1="5744" />
        </branch>
        <instance x="5552" y="3184" name="XLXI_182" orien="R90" />
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5680" y="3168" type="branch" />
            <wire x2="5680" y1="3168" y2="3184" x1="5680" />
        </branch>
        <branch name="XLXN_1550">
            <wire x2="5744" y1="4032" y2="4224" x1="5744" />
        </branch>
        <branch name="RESET">
            <wire x2="2928" y1="1680" y2="1680" x1="2816" />
            <wire x2="2928" y1="1680" y2="2720" x1="2928" />
            <wire x2="2928" y1="2720" y2="4192" x1="2928" />
            <wire x2="4864" y1="4192" y2="4192" x1="2928" />
            <wire x2="5200" y1="4192" y2="4192" x1="4864" />
            <wire x2="5552" y1="4192" y2="4192" x1="5200" />
            <wire x2="5680" y1="4192" y2="4192" x1="5552" />
            <wire x2="5680" y1="4192" y2="4224" x1="5680" />
            <wire x2="5200" y1="2720" y2="2720" x1="2928" />
            <wire x2="5552" y1="2720" y2="2720" x1="5200" />
            <wire x2="5904" y1="2720" y2="2720" x1="5552" />
            <wire x2="6016" y1="2720" y2="2720" x1="5904" />
            <wire x2="6016" y1="2720" y2="2736" x1="6016" />
            <wire x2="5904" y1="1680" y2="1680" x1="2928" />
            <wire x2="6304" y1="1680" y2="1680" x1="5904" />
            <wire x2="6480" y1="1680" y2="1680" x1="6304" />
            <wire x2="6480" y1="1680" y2="1696" x1="6480" />
            <wire x2="6896" y1="1680" y2="1680" x1="6480" />
            <wire x2="6896" y1="1680" y2="1696" x1="6896" />
            <wire x2="4864" y1="3920" y2="4192" x1="4864" />
            <wire x2="5200" y1="2464" y2="2464" x1="5184" />
            <wire x2="5200" y1="2464" y2="2720" x1="5200" />
            <wire x2="5200" y1="3920" y2="3920" x1="5184" />
            <wire x2="5200" y1="3920" y2="4192" x1="5200" />
            <wire x2="5552" y1="2464" y2="2464" x1="5536" />
            <wire x2="5552" y1="2464" y2="2720" x1="5552" />
            <wire x2="5552" y1="3920" y2="3920" x1="5536" />
            <wire x2="5552" y1="3920" y2="4192" x1="5552" />
            <wire x2="5904" y1="1440" y2="1680" x1="5904" />
            <wire x2="5904" y1="2480" y2="2720" x1="5904" />
            <wire x2="6304" y1="1440" y2="1440" x1="6240" />
            <wire x2="6304" y1="1440" y2="1680" x1="6304" />
        </branch>
        <instance x="5616" y="4224" name="XLXI_683" orien="R90" />
        <instance x="5904" y="2288" name="XLXI_100" orien="M90">
        </instance>
        <branch name="XLXN_188">
            <wire x2="5680" y1="2208" y2="2288" x1="5680" />
        </branch>
        <branch name="XLXN_210">
            <wire x2="5744" y1="1568" y2="2288" x1="5744" />
        </branch>
        <branch name="XLXN_189">
            <wire x2="5808" y1="2224" y2="2288" x1="5808" />
            <wire x2="5936" y1="2224" y2="2224" x1="5808" />
            <wire x2="5936" y1="2224" y2="2608" x1="5936" />
            <wire x2="5984" y1="2608" y2="2608" x1="5936" />
            <wire x2="5984" y1="2592" y2="2608" x1="5984" />
        </branch>
        <instance x="5584" y="1952" name="XLXI_106" orien="R90" />
        <instance x="5904" y="1248" name="XLXI_51" orien="M90">
        </instance>
        <branch name="XLXN_45">
            <wire x2="5680" y1="1088" y2="1248" x1="5680" />
        </branch>
        <branch name="XLXN_1552">
            <wire x2="5744" y1="624" y2="1248" x1="5744" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="5952" y1="1248" y2="1248" x1="5808" />
            <wire x2="5952" y1="1248" y2="1648" x1="5952" />
            <wire x2="6016" y1="1648" y2="1648" x1="5952" />
            <wire x2="6016" y1="1568" y2="1648" x1="6016" />
        </branch>
        <instance x="5648" y="368" name="XLXI_5" orien="R90" />
        <instance x="5584" y="832" name="XLXI_55" orien="R90" />
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5712" y="816" type="branch" />
            <wire x2="5712" y1="816" y2="832" x1="5712" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5776" y="352" type="branch" />
            <wire x2="5776" y1="352" y2="368" x1="5776" />
        </branch>
        <iomarker fontsize="28" x="3072" y="544" name="Y(3:0)" orien="R180" />
        <iomarker fontsize="28" x="3072" y="400" name="X(3:0)" orien="R180" />
        <branch name="Y(3:0)">
            <wire x2="3600" y1="544" y2="544" x1="3072" />
        </branch>
        <branch name="X(3:0)">
            <wire x2="3600" y1="400" y2="400" x1="3072" />
        </branch>
        <iomarker fontsize="28" x="3488" y="752" name="Z(7:0)" orien="R0" />
        <branch name="Z(7:0)">
            <wire x2="3488" y1="752" y2="752" x1="2960" />
        </branch>
    </sheet>
</drawing>