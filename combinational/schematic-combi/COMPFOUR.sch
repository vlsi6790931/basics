<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="XLXN_50" />
        <signal name="XLXN_51" />
        <signal name="XLXN_58" />
        <signal name="XLXN_59" />
        <signal name="XLXN_60" />
        <signal name="XLXN_65" />
        <signal name="XLXN_69" />
        <signal name="XLXN_71" />
        <signal name="XLXN_78" />
        <signal name="XLXN_79" />
        <signal name="XLXN_80" />
        <signal name="XLXN_81" />
        <signal name="XLXN_82" />
        <signal name="XLXN_86" />
        <signal name="XLXN_87" />
        <signal name="XLXN_91" />
        <signal name="XLXN_47" />
        <signal name="XLXN_106" />
        <signal name="XLXN_112" />
        <signal name="XLXN_117" />
        <signal name="XLXN_120" />
        <signal name="XLXN_122" />
        <signal name="XLXN_124" />
        <signal name="XLXN_129" />
        <signal name="XLXN_132" />
        <signal name="XLXN_134" />
        <signal name="XLXN_145" />
        <signal name="XLXN_154" />
        <signal name="XLXN_155" />
        <signal name="XLXN_157" />
        <signal name="IA(3:0)" />
        <signal name="IB(3:0)" />
        <signal name="XLXN_46" />
        <signal name="IA(1:0)" />
        <signal name="IB(1:0)" />
        <signal name="IA(3:2)" />
        <signal name="IB(3:2)" />
        <signal name="XLXN_74" />
        <signal name="XLXN_56" />
        <signal name="XLXN_75" />
        <signal name="XLXN_169" />
        <signal name="XLXN_114" />
        <signal name="XLXN_115" />
        <signal name="XLXN_118" />
        <signal name="XLXN_119" />
        <signal name="XLXN_57" />
        <signal name="E" />
        <signal name="L" />
        <signal name="W" />
        <signal name="XLXN_178" />
        <signal name="XLXN_179" />
        <signal name="RESET" />
        <signal name="XLXN_181" />
        <signal name="XLXN_133" />
        <port polarity="Input" name="IA(3:0)" />
        <port polarity="Input" name="IB(3:0)" />
        <port polarity="Output" name="E" />
        <port polarity="Output" name="L" />
        <port polarity="Output" name="W" />
        <port polarity="Input" name="RESET" />
        <blockdef name="COMPTWO">
            <timestamp>2023-8-12T12:30:31</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="COMPTWO" name="XLXI_38">
            <blockpin signalname="XLXN_46" name="E" />
            <blockpin signalname="IA(1:0)" name="IA(1:0)" />
            <blockpin signalname="IB(1:0)" name="IB(1:0)" />
            <blockpin signalname="XLXN_119" name="L" />
            <blockpin signalname="RESET" name="reset" />
            <blockpin signalname="XLXN_118" name="W" />
        </block>
        <block symbolname="COMPTWO" name="XLXI_39">
            <blockpin signalname="XLXN_133" name="E" />
            <blockpin signalname="IA(3:2)" name="IA(1:0)" />
            <blockpin signalname="IB(3:2)" name="IB(1:0)" />
            <blockpin signalname="XLXN_115" name="L" />
            <blockpin signalname="RESET" name="reset" />
            <blockpin signalname="XLXN_114" name="W" />
        </block>
        <block symbolname="and3" name="XLXI_40">
            <blockpin signalname="XLXN_133" name="I0" />
            <blockpin signalname="RESET" name="I1" />
            <blockpin signalname="XLXN_46" name="I2" />
            <blockpin signalname="E" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_41">
            <blockpin signalname="XLXN_115" name="I0" />
            <blockpin signalname="XLXN_74" name="I1" />
            <blockpin signalname="XLXN_57" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_45">
            <blockpin signalname="XLXN_119" name="I0" />
            <blockpin signalname="XLXN_133" name="I1" />
            <blockpin signalname="XLXN_74" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_42">
            <blockpin signalname="XLXN_114" name="I0" />
            <blockpin signalname="XLXN_75" name="I1" />
            <blockpin signalname="XLXN_56" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_50">
            <blockpin signalname="XLXN_133" name="I0" />
            <blockpin signalname="XLXN_118" name="I1" />
            <blockpin signalname="XLXN_75" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_44">
            <blockpin signalname="XLXN_57" name="I0" />
            <blockpin signalname="RESET" name="I1" />
            <blockpin signalname="L" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_43">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_56" name="I1" />
            <blockpin signalname="W" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <text style="fontsize:150;fontname:Arial" x="464" y="172">FOUR BIT COMPARATOR</text>
        <branch name="IA(3:0)">
            <wire x2="960" y1="464" y2="464" x1="784" />
        </branch>
        <branch name="IB(3:0)">
            <wire x2="960" y1="576" y2="576" x1="784" />
        </branch>
        <instance x="528" y="1008" name="XLXI_38" orien="R0">
        </instance>
        <branch name="XLXN_46">
            <wire x2="1856" y1="912" y2="912" x1="912" />
            <wire x2="1856" y1="912" y2="976" x1="1856" />
        </branch>
        <instance x="528" y="1312" name="XLXI_39" orien="R0">
        </instance>
        <branch name="IA(1:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="848" type="branch" />
            <wire x2="528" y1="848" y2="848" x1="384" />
        </branch>
        <branch name="IB(1:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="912" type="branch" />
            <wire x2="528" y1="912" y2="912" x1="384" />
        </branch>
        <branch name="IA(3:2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="1152" type="branch" />
            <wire x2="528" y1="1152" y2="1152" x1="384" />
        </branch>
        <branch name="IB(3:2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="1216" type="branch" />
            <wire x2="528" y1="1216" y2="1216" x1="384" />
        </branch>
        <instance x="1856" y="1168" name="XLXI_40" orien="R0" />
        <instance x="1568" y="1696" name="XLXI_41" orien="R0" />
        <branch name="XLXN_74">
            <wire x2="1568" y1="1568" y2="1568" x1="1536" />
        </branch>
        <instance x="1280" y="1664" name="XLXI_45" orien="R0" />
        <branch name="XLXN_56">
            <wire x2="1808" y1="480" y2="480" x1="1760" />
        </branch>
        <instance x="1504" y="576" name="XLXI_42" orien="R0" />
        <branch name="XLXN_75">
            <wire x2="1504" y1="448" y2="448" x1="1472" />
        </branch>
        <instance x="1216" y="544" name="XLXI_50" orien="R0" />
        <branch name="XLXN_114">
            <wire x2="1472" y1="1152" y2="1152" x1="912" />
            <wire x2="1504" y1="512" y2="512" x1="1472" />
            <wire x2="1472" y1="512" y2="1152" x1="1472" />
        </branch>
        <branch name="XLXN_115">
            <wire x2="976" y1="1280" y2="1280" x1="912" />
            <wire x2="976" y1="1280" y2="1632" x1="976" />
            <wire x2="1568" y1="1632" y2="1632" x1="976" />
        </branch>
        <branch name="XLXN_118">
            <wire x2="1056" y1="848" y2="848" x1="912" />
            <wire x2="1056" y1="416" y2="848" x1="1056" />
            <wire x2="1216" y1="416" y2="416" x1="1056" />
        </branch>
        <branch name="XLXN_119">
            <wire x2="1088" y1="976" y2="976" x1="912" />
            <wire x2="1088" y1="976" y2="1600" x1="1088" />
            <wire x2="1280" y1="1600" y2="1600" x1="1088" />
        </branch>
        <branch name="XLXN_57">
            <wire x2="1856" y1="1600" y2="1600" x1="1824" />
        </branch>
        <instance x="1856" y="1664" name="XLXI_44" orien="R0" />
        <instance x="1808" y="608" name="XLXI_43" orien="R0" />
        <branch name="E">
            <wire x2="2144" y1="1040" y2="1040" x1="2112" />
        </branch>
        <branch name="L">
            <wire x2="2144" y1="1568" y2="1568" x1="2112" />
        </branch>
        <branch name="W">
            <wire x2="2080" y1="512" y2="512" x1="2064" />
            <wire x2="2112" y1="512" y2="512" x1="2080" />
        </branch>
        <branch name="RESET">
            <wire x2="512" y1="1280" y2="1280" x1="448" />
            <wire x2="528" y1="1280" y2="1280" x1="512" />
            <wire x2="528" y1="976" y2="976" x1="512" />
            <wire x2="512" y1="976" y2="1040" x1="512" />
            <wire x2="512" y1="1040" y2="1280" x1="512" />
            <wire x2="1792" y1="1040" y2="1040" x1="512" />
            <wire x2="1808" y1="1040" y2="1040" x1="1792" />
            <wire x2="1856" y1="1040" y2="1040" x1="1808" />
            <wire x2="1792" y1="1040" y2="1536" x1="1792" />
            <wire x2="1856" y1="1536" y2="1536" x1="1792" />
            <wire x2="1808" y1="544" y2="544" x1="1792" />
            <wire x2="1792" y1="544" y2="1040" x1="1792" />
        </branch>
        <branch name="XLXN_133">
            <wire x2="1200" y1="1216" y2="1216" x1="912" />
            <wire x2="1856" y1="1216" y2="1216" x1="1200" />
            <wire x2="1200" y1="1216" y2="1536" x1="1200" />
            <wire x2="1280" y1="1536" y2="1536" x1="1200" />
            <wire x2="1216" y1="480" y2="480" x1="1200" />
            <wire x2="1200" y1="480" y2="1216" x1="1200" />
            <wire x2="1856" y1="1104" y2="1216" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="784" y="464" name="IA(3:0)" orien="R180" />
        <iomarker fontsize="28" x="784" y="576" name="IB(3:0)" orien="R180" />
        <iomarker fontsize="28" x="2144" y="1040" name="E" orien="R0" />
        <iomarker fontsize="28" x="2144" y="1568" name="L" orien="R0" />
        <iomarker fontsize="28" x="2112" y="512" name="W" orien="R0" />
        <iomarker fontsize="28" x="448" y="1280" name="RESET" orien="R180" />
    </sheet>
</drawing>