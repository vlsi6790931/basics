<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="X(3:0)" />
        <signal name="Y(3:0)" />
        <signal name="Z(7:0)" />
        <signal name="X(3)" />
        <signal name="XLXN_566" />
        <signal name="XLXN_559" />
        <signal name="XLXN_492" />
        <signal name="XLXN_554" />
        <signal name="XLXN_553" />
        <signal name="XLXN_552" />
        <signal name="Y(0)" />
        <signal name="X(2)" />
        <signal name="X(1)" />
        <signal name="X(0)" />
        <signal name="XLXN_71" />
        <signal name="Y(1)" />
        <signal name="XLXN_73" />
        <signal name="XLXN_80" />
        <signal name="XLXN_82" />
        <signal name="XLXN_131" />
        <signal name="XLXN_135" />
        <signal name="XLXN_88" />
        <signal name="XLXN_83" />
        <signal name="XLXN_90" />
        <signal name="XLXN_91" />
        <signal name="XLXN_352" />
        <signal name="Y(2)" />
        <signal name="XLXN_136" />
        <signal name="XLXN_143" />
        <signal name="XLXN_145" />
        <signal name="XLXN_360" />
        <signal name="XLXN_364" />
        <signal name="XLXN_467" />
        <signal name="Y(3)" />
        <signal name="XLXN_474" />
        <signal name="XLXN_363" />
        <signal name="Z(4)" />
        <signal name="Z(5)" />
        <signal name="Z(6)" />
        <signal name="Z(7)" />
        <signal name="XLXN_486" />
        <signal name="XLXN_487" />
        <signal name="XLXN_488" />
        <signal name="XLXN_493" />
        <signal name="XLXN_495" />
        <signal name="XLXN_501" />
        <signal name="XLXN_502" />
        <signal name="XLXN_503" />
        <signal name="XLXN_491" />
        <signal name="XLXN_570" />
        <signal name="XLXN_571" />
        <signal name="XLXN_572" />
        <signal name="Z(3)" />
        <signal name="XLXN_574" />
        <signal name="Z(2)" />
        <signal name="Z(0)" />
        <signal name="XLXN_580" />
        <signal name="RESET" />
        <signal name="XLXN_583" />
        <signal name="XLXN_584" />
        <signal name="XLXN_586" />
        <signal name="Z(1)" />
        <port polarity="Input" name="X(3:0)" />
        <port polarity="Input" name="Y(3:0)" />
        <port polarity="Output" name="Z(7:0)" />
        <port polarity="Input" name="RESET" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="HA">
            <timestamp>2023-8-10T21:23:14</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="176" x="64" y="-156" height="156" />
            <line x2="304" y1="-128" y2="-128" x1="240" />
            <line x2="304" y1="-32" y2="-32" x1="240" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="FA">
            <timestamp>2023-8-11T19:21:31</timestamp>
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
            <line x2="192" y1="-64" y2="0" x1="192" />
        </blockdef>
        <block symbolname="and2" name="XLXI_14">
            <blockpin signalname="X(1)" name="I0" />
            <blockpin signalname="Y(1)" name="I1" />
            <blockpin signalname="XLXN_82" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_202">
            <blockpin signalname="XLXN_492" name="Ain" />
            <blockpin signalname="XLXN_360" name="Bin" />
            <blockpin signalname="XLXN_493" name="Cin" />
            <blockpin signalname="Z(7)" name="Cout" />
            <blockpin signalname="Z(6)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_200">
            <blockpin signalname="XLXN_364" name="B" />
            <blockpin signalname="XLXN_570" name="A" />
            <blockpin signalname="XLXN_559" name="C" />
            <blockpin signalname="XLXN_574" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_201">
            <blockpin signalname="X(0)" name="I0" />
            <blockpin signalname="Y(3)" name="I1" />
            <blockpin signalname="XLXN_364" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_82">
            <blockpin signalname="X(3)" name="I0" />
            <blockpin signalname="Y(2)" name="I1" />
            <blockpin signalname="XLXN_552" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="X(0)" name="I0" />
            <blockpin signalname="Y(0)" name="I1" />
            <blockpin signalname="XLXN_502" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="X(2)" name="I0" />
            <blockpin signalname="Y(0)" name="I1" />
            <blockpin signalname="XLXN_80" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="X(1)" name="I0" />
            <blockpin signalname="Y(0)" name="I1" />
            <blockpin signalname="XLXN_71" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_13">
            <blockpin signalname="X(0)" name="I0" />
            <blockpin signalname="Y(1)" name="I1" />
            <blockpin signalname="XLXN_73" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_61">
            <blockpin signalname="XLXN_83" name="Ain" />
            <blockpin signalname="XLXN_88" name="Bin" />
            <blockpin signalname="XLXN_495" name="Cin" />
            <blockpin signalname="XLXN_91" name="Cout" />
            <blockpin signalname="XLXN_135" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_15">
            <blockpin signalname="X(2)" name="I0" />
            <blockpin signalname="Y(1)" name="I1" />
            <blockpin signalname="XLXN_88" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_60">
            <blockpin signalname="XLXN_90" name="B" />
            <blockpin signalname="XLXN_91" name="A" />
            <blockpin signalname="XLXN_488" name="C" />
            <blockpin signalname="XLXN_352" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_16">
            <blockpin signalname="X(3)" name="I0" />
            <blockpin signalname="Y(1)" name="I1" />
            <blockpin signalname="XLXN_90" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_41">
            <blockpin signalname="XLXN_73" name="B" />
            <blockpin signalname="XLXN_71" name="A" />
            <blockpin signalname="XLXN_586" name="C" />
            <blockpin signalname="XLXN_501" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="X(3)" name="I0" />
            <blockpin signalname="Y(0)" name="I1" />
            <blockpin signalname="XLXN_83" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_80">
            <blockpin signalname="X(1)" name="I0" />
            <blockpin signalname="Y(2)" name="I1" />
            <blockpin signalname="XLXN_136" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_83">
            <blockpin signalname="X(2)" name="I0" />
            <blockpin signalname="Y(2)" name="I1" />
            <blockpin signalname="XLXN_143" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_84">
            <blockpin signalname="XLXN_352" name="Ain" />
            <blockpin signalname="XLXN_143" name="Bin" />
            <blockpin signalname="XLXN_487" name="Cin" />
            <blockpin signalname="XLXN_553" name="Cout" />
            <blockpin signalname="XLXN_467" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_86">
            <blockpin signalname="XLXN_145" name="B" />
            <blockpin signalname="XLXN_131" name="A" />
            <blockpin signalname="XLXN_486" name="C" />
            <blockpin signalname="XLXN_503" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_87">
            <blockpin signalname="X(0)" name="I0" />
            <blockpin signalname="Y(2)" name="I1" />
            <blockpin signalname="XLXN_145" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_194">
            <blockpin signalname="XLXN_488" name="Ain" />
            <blockpin signalname="XLXN_552" name="Bin" />
            <blockpin signalname="XLXN_553" name="Cin" />
            <blockpin signalname="XLXN_493" name="Cout" />
            <blockpin signalname="XLXN_474" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_196">
            <blockpin signalname="X(3)" name="I0" />
            <blockpin signalname="Y(3)" name="I1" />
            <blockpin signalname="XLXN_360" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_195">
            <blockpin signalname="X(1)" name="I0" />
            <blockpin signalname="Y(3)" name="I1" />
            <blockpin signalname="XLXN_554" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_199">
            <blockpin signalname="XLXN_467" name="Ain" />
            <blockpin signalname="XLXN_554" name="Bin" />
            <blockpin signalname="XLXN_559" name="Cin" />
            <blockpin signalname="XLXN_491" name="Cout" />
            <blockpin signalname="Z(4)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_198">
            <blockpin signalname="XLXN_474" name="Ain" />
            <blockpin signalname="XLXN_363" name="Bin" />
            <blockpin signalname="XLXN_491" name="Cin" />
            <blockpin signalname="XLXN_492" name="Cout" />
            <blockpin signalname="Z(5)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_197">
            <blockpin signalname="X(2)" name="I0" />
            <blockpin signalname="Y(3)" name="I1" />
            <blockpin signalname="XLXN_363" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_85">
            <blockpin signalname="XLXN_135" name="Ain" />
            <blockpin signalname="XLXN_136" name="Bin" />
            <blockpin signalname="XLXN_486" name="Cin" />
            <blockpin signalname="XLXN_487" name="Cout" />
            <blockpin signalname="XLXN_570" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_286">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_574" name="I1" />
            <blockpin signalname="Z(3)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_283">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_503" name="I1" />
            <blockpin signalname="Z(2)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_280">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_502" name="I1" />
            <blockpin signalname="Z(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_281">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_501" name="I1" />
            <blockpin signalname="Z(1)" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_62">
            <blockpin signalname="XLXN_80" name="Ain" />
            <blockpin signalname="XLXN_82" name="Bin" />
            <blockpin signalname="XLXN_586" name="Cin" />
            <blockpin signalname="XLXN_495" name="Cout" />
            <blockpin signalname="XLXN_131" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <text style="fontsize:150;fontname:Arial" x="264" y="236">COMBINATIONAL MULTIPLIER (UNSIGNED)</text>
        <branch name="X(3:0)">
            <wire x2="1648" y1="528" y2="528" x1="1424" />
        </branch>
        <branch name="Y(3:0)">
            <wire x2="1648" y1="592" y2="592" x1="1424" />
        </branch>
        <iomarker fontsize="28" x="1424" y="528" name="X(3:0)" orien="R180" />
        <iomarker fontsize="28" x="1424" y="592" name="Y(3:0)" orien="R180" />
        <branch name="Z(7:0)">
            <wire x2="1648" y1="672" y2="672" x1="1424" />
        </branch>
        <iomarker fontsize="28" x="1648" y="672" name="Z(7:0)" orien="R0" />
        <instance x="4160" y="944" name="XLXI_14" orien="R90" />
        <instance x="2448" y="2896" name="XLXI_202" orien="R90">
        </instance>
        <instance x="3632" y="2608" name="XLXI_201" orien="R90" />
        <instance x="2944" y="1728" name="XLXI_82" orien="R90" />
        <instance x="4896" y="496" name="XLXI_1" orien="R90" />
        <instance x="4224" y="512" name="XLXI_4" orien="R90" />
        <instance x="4592" y="496" name="XLXI_2" orien="R90" />
        <instance x="4496" y="944" name="XLXI_13" orien="R90" />
        <instance x="3728" y="1216" name="XLXI_61" orien="R90">
        </instance>
        <instance x="3792" y="928" name="XLXI_15" orien="R90" />
        <instance x="3472" y="1232" name="XLXI_60" orien="R90">
        </instance>
        <instance x="3408" y="944" name="XLXI_16" orien="R90" />
        <instance x="3856" y="496" name="XLXI_3" orien="R90" />
        <instance x="3728" y="1792" name="XLXI_80" orien="R90" />
        <instance x="3344" y="1776" name="XLXI_83" orien="R90" />
        <instance x="3280" y="2064" name="XLXI_84" orien="R90">
        </instance>
        <instance x="4128" y="2080" name="XLXI_86" orien="R90">
        </instance>
        <instance x="4064" y="1792" name="XLXI_87" orien="R90" />
        <instance x="2880" y="2080" name="XLXI_194" orien="R90">
        </instance>
        <instance x="2512" y="2624" name="XLXI_196" orien="R90" />
        <instance x="3280" y="2624" name="XLXI_195" orien="R90" />
        <instance x="3216" y="2912" name="XLXI_199" orien="R90">
        </instance>
        <instance x="2816" y="2912" name="XLXI_198" orien="R90">
        </instance>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3920" y="480" type="branch" />
            <wire x2="3920" y1="480" y2="496" x1="3920" />
        </branch>
        <branch name="XLXN_586">
            <wire x2="4192" y1="1184" y2="1184" x1="4032" />
            <wire x2="4192" y1="1184" y2="1232" x1="4192" />
            <wire x2="4032" y1="1184" y2="1648" x1="4032" />
            <wire x2="4688" y1="1648" y2="1648" x1="4032" />
            <wire x2="4688" y1="1536" y2="1648" x1="4688" />
        </branch>
        <branch name="XLXN_559">
            <wire x2="3312" y1="2896" y2="2912" x1="3312" />
            <wire x2="3520" y1="2896" y2="2896" x1="3312" />
            <wire x2="3520" y1="2896" y2="3264" x1="3520" />
            <wire x2="3824" y1="3264" y2="3264" x1="3520" />
            <wire x2="3824" y1="3184" y2="3264" x1="3824" />
        </branch>
        <branch name="XLXN_492">
            <wire x2="3152" y1="2544" y2="2544" x1="2672" />
            <wire x2="3152" y1="2544" y2="3248" x1="3152" />
            <wire x2="2672" y1="2544" y2="2896" x1="2672" />
            <wire x2="3040" y1="3232" y2="3248" x1="3040" />
            <wire x2="3152" y1="3248" y2="3248" x1="3040" />
        </branch>
        <branch name="XLXN_554">
            <wire x2="3376" y1="2880" y2="2912" x1="3376" />
        </branch>
        <branch name="XLXN_553">
            <wire x2="2976" y1="2000" y2="2080" x1="2976" />
            <wire x2="3200" y1="2000" y2="2000" x1="2976" />
            <wire x2="3200" y1="2000" y2="2416" x1="3200" />
            <wire x2="3504" y1="2416" y2="2416" x1="3200" />
            <wire x2="3504" y1="2384" y2="2416" x1="3504" />
        </branch>
        <branch name="XLXN_552">
            <wire x2="3040" y1="1984" y2="2080" x1="3040" />
        </branch>
        <branch name="Y(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5120" y="384" type="branch" />
            <wire x2="4352" y1="384" y2="384" x1="3984" />
            <wire x2="4352" y1="384" y2="512" x1="4352" />
            <wire x2="4720" y1="384" y2="384" x1="4352" />
            <wire x2="5024" y1="384" y2="384" x1="4720" />
            <wire x2="5120" y1="384" y2="384" x1="5024" />
            <wire x2="5024" y1="384" y2="496" x1="5024" />
            <wire x2="4720" y1="384" y2="496" x1="4720" />
            <wire x2="3984" y1="384" y2="496" x1="3984" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4288" y="480" type="branch" />
            <wire x2="4288" y1="480" y2="512" x1="4288" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4656" y="480" type="branch" />
            <wire x2="4656" y1="480" y2="496" x1="4656" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4960" y="480" type="branch" />
            <wire x2="4960" y1="480" y2="496" x1="4960" />
        </branch>
        <branch name="Y(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4752" y="832" type="branch" />
            <wire x2="3920" y1="832" y2="832" x1="3536" />
            <wire x2="4288" y1="832" y2="832" x1="3920" />
            <wire x2="4624" y1="832" y2="832" x1="4288" />
            <wire x2="4752" y1="832" y2="832" x1="4624" />
            <wire x2="4624" y1="832" y2="944" x1="4624" />
            <wire x2="4288" y1="832" y2="944" x1="4288" />
            <wire x2="3920" y1="832" y2="928" x1="3920" />
            <wire x2="3536" y1="832" y2="944" x1="3536" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4560" y="912" type="branch" />
            <wire x2="4560" y1="912" y2="944" x1="4560" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3472" y="912" type="branch" />
            <wire x2="3472" y1="912" y2="944" x1="3472" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3856" y="912" type="branch" />
            <wire x2="3856" y1="912" y2="928" x1="3856" />
        </branch>
        <branch name="XLXN_80">
            <wire x2="4320" y1="768" y2="1232" x1="4320" />
        </branch>
        <branch name="XLXN_82">
            <wire x2="4256" y1="1200" y2="1232" x1="4256" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4224" y="896" type="branch" />
            <wire x2="4224" y1="896" y2="944" x1="4224" />
        </branch>
        <branch name="XLXN_131">
            <wire x2="4256" y1="1552" y2="2080" x1="4256" />
        </branch>
        <branch name="XLXN_135">
            <wire x2="3888" y1="1536" y2="2080" x1="3888" />
        </branch>
        <branch name="XLXN_88">
            <wire x2="3888" y1="1184" y2="1216" x1="3888" />
        </branch>
        <branch name="XLXN_83">
            <wire x2="3952" y1="752" y2="1216" x1="3952" />
        </branch>
        <branch name="XLXN_90">
            <wire x2="3504" y1="1200" y2="1232" x1="3504" />
        </branch>
        <branch name="XLXN_91">
            <wire x2="3600" y1="1216" y2="1232" x1="3600" />
            <wire x2="3696" y1="1216" y2="1216" x1="3600" />
            <wire x2="3696" y1="1216" y2="1552" x1="3696" />
            <wire x2="3952" y1="1552" y2="1552" x1="3696" />
            <wire x2="3952" y1="1536" y2="1552" x1="3952" />
        </branch>
        <branch name="XLXN_352">
            <wire x2="3504" y1="1536" y2="2064" x1="3504" />
        </branch>
        <branch name="Y(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="1680" type="branch" />
            <wire x2="3472" y1="1680" y2="1680" x1="3072" />
            <wire x2="3856" y1="1680" y2="1680" x1="3472" />
            <wire x2="3856" y1="1680" y2="1792" x1="3856" />
            <wire x2="4192" y1="1680" y2="1680" x1="3856" />
            <wire x2="4320" y1="1680" y2="1680" x1="4192" />
            <wire x2="4192" y1="1680" y2="1792" x1="4192" />
            <wire x2="3472" y1="1680" y2="1776" x1="3472" />
            <wire x2="3072" y1="1680" y2="1728" x1="3072" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3792" y="1776" type="branch" />
            <wire x2="3792" y1="1776" y2="1792" x1="3792" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4128" y="1760" type="branch" />
            <wire x2="4128" y1="1760" y2="1792" x1="4128" />
        </branch>
        <branch name="XLXN_136">
            <wire x2="3824" y1="2048" y2="2080" x1="3824" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3008" y="1680" type="branch" />
            <wire x2="3008" y1="1680" y2="1728" x1="3008" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3408" y="1760" type="branch" />
            <wire x2="3408" y1="1760" y2="1776" x1="3408" />
        </branch>
        <branch name="XLXN_143">
            <wire x2="3440" y1="2032" y2="2064" x1="3440" />
        </branch>
        <branch name="XLXN_145">
            <wire x2="4160" y1="2048" y2="2080" x1="4160" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3696" y="2592" type="branch" />
            <wire x2="3696" y1="2592" y2="2608" x1="3696" />
        </branch>
        <branch name="XLXN_360">
            <wire x2="2608" y1="2880" y2="2896" x1="2608" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="2576" y="2592" type="branch" />
            <wire x2="2576" y1="2592" y2="2624" x1="2576" />
        </branch>
        <branch name="XLXN_364">
            <wire x2="3728" y1="2864" y2="2880" x1="3728" />
        </branch>
        <branch name="XLXN_467">
            <wire x2="3440" y1="2384" y2="2912" x1="3440" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3344" y="2608" type="branch" />
            <wire x2="3344" y1="2608" y2="2624" x1="3344" />
        </branch>
        <branch name="Y(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3888" y="2512" type="branch" />
            <wire x2="2640" y1="2512" y2="2624" x1="2640" />
            <wire x2="3008" y1="2512" y2="2512" x1="2640" />
            <wire x2="3408" y1="2512" y2="2512" x1="3008" />
            <wire x2="3408" y1="2512" y2="2624" x1="3408" />
            <wire x2="3760" y1="2512" y2="2512" x1="3408" />
            <wire x2="3888" y1="2512" y2="2512" x1="3760" />
            <wire x2="3760" y1="2512" y2="2608" x1="3760" />
            <wire x2="3008" y1="2512" y2="2624" x1="3008" />
        </branch>
        <branch name="XLXN_474">
            <wire x2="3040" y1="2400" y2="2912" x1="3040" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="2608" type="branch" />
            <wire x2="2944" y1="2608" y2="2624" x1="2944" />
        </branch>
        <branch name="XLXN_363">
            <wire x2="2976" y1="2880" y2="2912" x1="2976" />
        </branch>
        <branch name="Z(4)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3376" y="3248" type="branch" />
            <wire x2="3376" y1="3232" y2="3248" x1="3376" />
        </branch>
        <branch name="Z(5)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="3248" type="branch" />
            <wire x2="2976" y1="3232" y2="3248" x1="2976" />
        </branch>
        <branch name="Z(6)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2608" y="3264" type="branch" />
            <wire x2="2608" y1="3216" y2="3264" x1="2608" />
        </branch>
        <branch name="Z(7)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2432" y="3264" type="branch" />
            <wire x2="2432" y1="3248" y2="3264" x1="2432" />
            <wire x2="2672" y1="3248" y2="3248" x1="2432" />
            <wire x2="2672" y1="3216" y2="3248" x1="2672" />
        </branch>
        <branch name="XLXN_486">
            <wire x2="3760" y1="2016" y2="2080" x1="3760" />
            <wire x2="4032" y1="2016" y2="2016" x1="3760" />
            <wire x2="4032" y1="2016" y2="2496" x1="4032" />
            <wire x2="4256" y1="2496" y2="2496" x1="4032" />
            <wire x2="4256" y1="2384" y2="2496" x1="4256" />
        </branch>
        <branch name="XLXN_487">
            <wire x2="3376" y1="2048" y2="2064" x1="3376" />
            <wire x2="3600" y1="2048" y2="2048" x1="3376" />
            <wire x2="3600" y1="2048" y2="2416" x1="3600" />
            <wire x2="3888" y1="2416" y2="2416" x1="3600" />
            <wire x2="3888" y1="2400" y2="2416" x1="3888" />
        </branch>
        <branch name="XLXN_488">
            <wire x2="3104" y1="1744" y2="2080" x1="3104" />
            <wire x2="3600" y1="1744" y2="1744" x1="3104" />
            <wire x2="3600" y1="1536" y2="1744" x1="3600" />
        </branch>
        <branch name="XLXN_493">
            <wire x2="3104" y1="2416" y2="2416" x1="2544" />
            <wire x2="2544" y1="2416" y2="2896" x1="2544" />
            <wire x2="3104" y1="2400" y2="2416" x1="3104" />
        </branch>
        <branch name="XLXN_495">
            <wire x2="3824" y1="1200" y2="1216" x1="3824" />
            <wire x2="4048" y1="1200" y2="1200" x1="3824" />
            <wire x2="4048" y1="1200" y2="1616" x1="4048" />
            <wire x2="4320" y1="1616" y2="1616" x1="4048" />
            <wire x2="4320" y1="1552" y2="1616" x1="4320" />
        </branch>
        <branch name="XLXN_502">
            <wire x2="4992" y1="752" y2="848" x1="4992" />
        </branch>
        <branch name="XLXN_503">
            <wire x2="4160" y1="2384" y2="2544" x1="4160" />
        </branch>
        <instance x="3696" y="2880" name="XLXI_200" orien="R90">
        </instance>
        <instance x="2880" y="2624" name="XLXI_197" orien="R90" />
        <branch name="XLXN_491">
            <wire x2="2912" y1="2896" y2="2912" x1="2912" />
            <wire x2="3168" y1="2896" y2="2896" x1="2912" />
            <wire x2="3168" y1="2896" y2="3328" x1="3168" />
            <wire x2="3440" y1="3328" y2="3328" x1="3168" />
            <wire x2="3440" y1="3232" y2="3328" x1="3440" />
        </branch>
        <branch name="XLXN_73">
            <wire x2="4592" y1="1200" y2="1232" x1="4592" />
        </branch>
        <branch name="XLXN_71">
            <wire x2="4688" y1="752" y2="1232" x1="4688" />
        </branch>
        <instance x="3664" y="2080" name="XLXI_85" orien="R90">
        </instance>
        <branch name="XLXN_570">
            <wire x2="3824" y1="2400" y2="2880" x1="3824" />
        </branch>
        <branch name="Z(3)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3696" y="3632" type="branch" />
            <wire x2="3696" y1="3616" y2="3632" x1="3696" />
        </branch>
        <branch name="XLXN_574">
            <wire x2="3728" y1="3184" y2="3360" x1="3728" />
        </branch>
        <instance x="3600" y="3360" name="XLXI_286" orien="R90" />
        <instance x="4032" y="2544" name="XLXI_283" orien="R90" />
        <branch name="Z(2)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4128" y="2816" type="branch" />
            <wire x2="4128" y1="2800" y2="2816" x1="4128" />
        </branch>
        <instance x="4864" y="848" name="XLXI_280" orien="R90" />
        <branch name="Z(0)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4960" y="1120" type="branch" />
            <wire x2="4960" y1="1104" y2="1120" x1="4960" />
        </branch>
        <branch name="RESET">
            <wire x2="2368" y1="784" y2="784" x1="2256" />
            <wire x2="3664" y1="784" y2="784" x1="2368" />
            <wire x2="3664" y1="784" y2="1408" x1="3664" />
            <wire x2="3728" y1="1408" y2="1408" x1="3664" />
            <wire x2="4080" y1="784" y2="784" x1="3664" />
            <wire x2="4928" y1="784" y2="784" x1="4080" />
            <wire x2="4928" y1="784" y2="848" x1="4928" />
            <wire x2="4080" y1="784" y2="1424" x1="4080" />
            <wire x2="4096" y1="1424" y2="1424" x1="4080" />
            <wire x2="4080" y1="1424" y2="1568" x1="4080" />
            <wire x2="4528" y1="1568" y2="1568" x1="4080" />
            <wire x2="4528" y1="1568" y2="1680" x1="4528" />
            <wire x2="2368" y1="784" y2="2272" x1="2368" />
            <wire x2="2368" y1="2272" y2="3088" x1="2368" />
            <wire x2="2368" y1="3088" y2="3440" x1="2368" />
            <wire x2="2800" y1="3440" y2="3440" x1="2368" />
            <wire x2="3136" y1="3440" y2="3440" x1="2800" />
            <wire x2="3536" y1="3440" y2="3440" x1="3136" />
            <wire x2="2448" y1="3088" y2="3088" x1="2368" />
            <wire x2="2800" y1="2272" y2="2272" x1="2368" />
            <wire x2="2880" y1="2272" y2="2272" x1="2800" />
            <wire x2="2800" y1="2272" y2="2464" x1="2800" />
            <wire x2="3248" y1="2464" y2="2464" x1="2800" />
            <wire x2="3648" y1="2464" y2="2464" x1="3248" />
            <wire x2="4096" y1="2464" y2="2464" x1="3648" />
            <wire x2="4096" y1="2464" y2="2544" x1="4096" />
            <wire x2="2816" y1="3104" y2="3104" x1="2800" />
            <wire x2="2800" y1="3104" y2="3440" x1="2800" />
            <wire x2="3216" y1="3104" y2="3104" x1="3136" />
            <wire x2="3136" y1="3104" y2="3440" x1="3136" />
            <wire x2="3280" y1="2256" y2="2256" x1="3248" />
            <wire x2="3248" y1="2256" y2="2464" x1="3248" />
            <wire x2="3664" y1="3360" y2="3360" x1="3536" />
            <wire x2="3536" y1="3360" y2="3440" x1="3536" />
            <wire x2="3664" y1="2272" y2="2272" x1="3648" />
            <wire x2="3648" y1="2272" y2="2464" x1="3648" />
        </branch>
        <branch name="XLXN_501">
            <wire x2="4592" y1="1536" y2="1664" x1="4592" />
            <wire x2="4592" y1="1664" y2="1680" x1="4592" />
        </branch>
        <instance x="4464" y="1680" name="XLXI_281" orien="R90" />
        <branch name="Z(1)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4560" y="1968" type="branch" />
            <wire x2="4560" y1="1936" y2="1968" x1="4560" />
        </branch>
        <instance x="4560" y="1232" name="XLXI_41" orien="R90">
        </instance>
        <instance x="4096" y="1232" name="XLXI_62" orien="R90">
        </instance>
        <iomarker fontsize="28" x="2256" y="784" name="RESET" orien="R180" />
    </sheet>
</drawing>