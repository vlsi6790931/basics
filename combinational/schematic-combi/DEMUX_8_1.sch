<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_5" />
        <signal name="XLXN_13" />
        <signal name="S(1:0)" />
        <signal name="XLXN_23" />
        <signal name="RESET" />
        <signal name="O(7:4)" />
        <signal name="O(3:0)" />
        <signal name="I" />
        <signal name="S(2:0)" />
        <signal name="S(2)" />
        <signal name="O(7:0)" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <port polarity="Input" name="RESET" />
        <port polarity="Input" name="I" />
        <port polarity="Input" name="S(2:0)" />
        <port polarity="Output" name="O(7:0)" />
        <blockdef name="DEMUX_1_4">
            <timestamp>2023-8-11T7:7:24</timestamp>
            <rect width="208" x="64" y="-176" height="176" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="336" y1="-80" y2="-80" x1="272" />
            <rect width="64" x="272" y="-92" height="24" />
            <line x2="144" y1="-176" y2="-240" x1="144" />
        </blockdef>
        <blockdef name="DEMUX_1_2">
            <timestamp>2023-8-11T6:50:44</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="0" y1="-64" y2="-64" x1="64" />
        </blockdef>
        <block symbolname="DEMUX_1_4" name="XLXI_2">
            <blockpin signalname="RESET" name="reset" />
            <blockpin signalname="S(1:0)" name="S(1:0)" />
            <blockpin signalname="O(3:0)" name="O(3:0)" />
            <blockpin signalname="XLXN_32" name="I" />
        </block>
        <block symbolname="DEMUX_1_4" name="XLXI_1">
            <blockpin signalname="RESET" name="reset" />
            <blockpin signalname="S(1:0)" name="S(1:0)" />
            <blockpin signalname="O(7:4)" name="O(3:0)" />
            <blockpin signalname="XLXN_31" name="I" />
        </block>
        <block symbolname="DEMUX_1_2" name="XLXI_3">
            <blockpin signalname="XLXN_32" name="O0" />
            <blockpin signalname="XLXN_31" name="O1" />
            <blockpin signalname="RESET" name="R" />
            <blockpin signalname="I" name="I" />
            <blockpin signalname="S(2)" name="S" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="960" y="1136" name="XLXI_2" orien="R0">
        </instance>
        <branch name="S(1:0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="928" y="560" type="branch" />
            <wire x2="928" y1="560" y2="608" x1="928" />
            <wire x2="960" y1="608" y2="608" x1="928" />
            <wire x2="928" y1="608" y2="1008" x1="928" />
            <wire x2="960" y1="1008" y2="1008" x1="928" />
        </branch>
        <instance x="960" y="736" name="XLXI_1" orien="R0">
        </instance>
        <branch name="RESET">
            <wire x2="896" y1="1104" y2="1104" x1="480" />
            <wire x2="960" y1="1104" y2="1104" x1="896" />
            <wire x2="896" y1="944" y2="944" x1="832" />
            <wire x2="896" y1="944" y2="1104" x1="896" />
            <wire x2="960" y1="704" y2="704" x1="896" />
            <wire x2="896" y1="704" y2="944" x1="896" />
        </branch>
        <branch name="O(7:4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1344" y="656" type="branch" />
            <wire x2="1344" y1="656" y2="656" x1="1296" />
        </branch>
        <branch name="O(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1360" y="1056" type="branch" />
            <wire x2="1360" y1="1056" y2="1056" x1="1296" />
        </branch>
        <branch name="I">
            <wire x2="448" y1="848" y2="848" x1="416" />
        </branch>
        <branch name="S(2:0)">
            <wire x2="464" y1="608" y2="608" x1="304" />
        </branch>
        <branch name="S(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="432" y="912" type="branch" />
            <wire x2="448" y1="912" y2="912" x1="432" />
        </branch>
        <iomarker fontsize="28" x="416" y="848" name="I" orien="R180" />
        <iomarker fontsize="28" x="304" y="608" name="S(2:0)" orien="R180" />
        <branch name="O(7:0)">
            <wire x2="1552" y1="848" y2="848" x1="1408" />
        </branch>
        <iomarker fontsize="28" x="1552" y="848" name="O(7:0)" orien="R0" />
        <iomarker fontsize="28" x="480" y="1104" name="RESET" orien="R180" />
        <branch name="XLXN_31">
            <wire x2="864" y1="880" y2="880" x1="832" />
            <wire x2="864" y1="432" y2="880" x1="864" />
            <wire x2="1104" y1="432" y2="432" x1="864" />
            <wire x2="1104" y1="432" y2="496" x1="1104" />
        </branch>
        <branch name="XLXN_32">
            <wire x2="1104" y1="816" y2="816" x1="832" />
            <wire x2="1104" y1="816" y2="896" x1="1104" />
        </branch>
        <instance x="448" y="976" name="XLXI_3" orien="R0">
        </instance>
        <text style="fontsize:150;fontname:Arial" x="520" y="180">DEMUX 1X8</text>
    </sheet>
</drawing>