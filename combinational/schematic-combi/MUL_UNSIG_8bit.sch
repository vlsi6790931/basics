<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="X(3:0)" />
        <signal name="Y(3:0)" />
        <signal name="O(7:0)" />
        <signal name="XLXN_512" />
        <signal name="Y(0)" />
        <signal name="XLXN_721" />
        <signal name="XLXN_722" />
        <signal name="XLXN_723" />
        <signal name="XLXN_649" />
        <signal name="XLXN_514" />
        <signal name="Y(3)" />
        <signal name="XLXN_498" />
        <signal name="XLXN_497" />
        <signal name="X(2)" />
        <signal name="X(1)" />
        <signal name="X(3)" />
        <signal name="XLXN_400" />
        <signal name="XLXN_397" />
        <signal name="XLXN_396" />
        <signal name="XLXN_395" />
        <signal name="XLXN_394" />
        <signal name="XLXN_393" />
        <signal name="X(0)" />
        <signal name="XLXN_390" />
        <signal name="XLXN_423" />
        <signal name="XLXN_741" />
        <signal name="XLXN_742" />
        <signal name="XLXN_743" />
        <signal name="XLXN_421" />
        <signal name="XLXN_416" />
        <signal name="XLXN_180" />
        <signal name="XLXN_189" />
        <signal name="XLXN_184" />
        <signal name="XLXN_187" />
        <signal name="XLXN_186" />
        <signal name="XLXN_209" />
        <signal name="XLXN_188" />
        <signal name="XLXN_210" />
        <signal name="Y(2)" />
        <signal name="XLXN_190" />
        <signal name="Z(1)" />
        <signal name="XLXN_212" />
        <signal name="Z(0)" />
        <signal name="XLXN_763" />
        <signal name="XLXN_764" />
        <signal name="XLXN_765" />
        <signal name="XLXN_207" />
        <signal name="XLXN_175" />
        <signal name="XLXN_173" />
        <signal name="XLXN_166" />
        <signal name="Y(1)" />
        <signal name="XLXN_161" />
        <signal name="XLXN_155" />
        <signal name="XLXN_118" />
        <signal name="XLXN_113" />
        <signal name="XLXN_91" />
        <signal name="XLXN_69" />
        <signal name="XLXN_92" />
        <signal name="XLXN_90" />
        <signal name="XLXN_68" />
        <signal name="XLXN_788" />
        <port polarity="Input" name="X(3:0)" />
        <port polarity="Input" name="Y(3:0)" />
        <port polarity="Output" name="O(7:0)" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="HA">
            <timestamp>2023-8-10T21:23:14</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="176" x="64" y="-156" height="156" />
            <line x2="304" y1="-128" y2="-128" x1="240" />
            <line x2="304" y1="-32" y2="-32" x1="240" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="FA">
            <timestamp>2023-8-11T19:21:31</timestamp>
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
            <line x2="192" y1="-64" y2="0" x1="192" />
        </blockdef>
        <block symbolname="FA" name="XLXI_254">
            <blockpin name="Ain" />
            <blockpin name="Bin" />
            <blockpin name="Cin" />
            <blockpin name="Cout" />
            <blockpin name="sum" />
            <blockpin signalname="XLXN_207" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_253">
            <blockpin name="Ain" />
            <blockpin name="Bin" />
            <blockpin name="Cin" />
            <blockpin name="Cout" />
            <blockpin name="sum" />
            <blockpin signalname="XLXN_207" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_252">
            <blockpin name="Ain" />
            <blockpin name="Bin" />
            <blockpin name="Cin" />
            <blockpin name="Cout" />
            <blockpin name="sum" />
            <blockpin signalname="XLXN_207" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_251">
            <blockpin name="Ain" />
            <blockpin name="Bin" />
            <blockpin name="Cin" />
            <blockpin name="Cout" />
            <blockpin name="sum" />
            <blockpin signalname="XLXN_421" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_250">
            <blockpin name="Ain" />
            <blockpin name="Bin" />
            <blockpin name="Cin" />
            <blockpin name="Cout" />
            <blockpin name="sum" />
            <blockpin signalname="XLXN_421" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_249">
            <blockpin name="Ain" />
            <blockpin name="Bin" />
            <blockpin name="Cin" />
            <blockpin name="Cout" />
            <blockpin name="sum" />
            <blockpin name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_246">
            <blockpin signalname="XLXN_649" name="I0" />
            <blockpin signalname="XLXN_514" name="I1" />
            <blockpin name="O" />
        </block>
        <block symbolname="FA" name="XLXI_191">
            <blockpin signalname="XLXN_396" name="Ain" />
            <blockpin name="Bin" />
            <blockpin signalname="XLXN_400" name="Cin" />
            <blockpin name="Cout" />
            <blockpin name="sum" />
            <blockpin signalname="XLXN_649" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_188">
            <blockpin signalname="XLXN_423" name="B" />
            <blockpin signalname="XLXN_390" name="A" />
            <blockpin signalname="XLXN_397" name="C" />
            <blockpin signalname="XLXN_514" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_187">
            <blockpin signalname="XLXN_393" name="Ain" />
            <blockpin signalname="XLXN_497" name="Bin" />
            <blockpin signalname="XLXN_397" name="Cin" />
            <blockpin signalname="XLXN_395" name="Cout" />
            <blockpin name="sum" />
            <blockpin signalname="XLXN_649" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_186">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_396" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_185">
            <blockpin signalname="XLXN_394" name="Ain" />
            <blockpin signalname="XLXN_498" name="Bin" />
            <blockpin signalname="XLXN_395" name="Cin" />
            <blockpin signalname="XLXN_400" name="Cout" />
            <blockpin name="sum" />
            <blockpin signalname="XLXN_649" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_184">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_394" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_183">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_393" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_182">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_390" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_197">
            <blockpin signalname="XLXN_421" name="I0" />
            <blockpin signalname="XLXN_416" name="I1" />
            <blockpin name="O" />
        </block>
        <block symbolname="FA" name="XLXI_95">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin name="Bin" />
            <blockpin signalname="XLXN_180" name="Cin" />
            <blockpin name="Cout" />
            <blockpin signalname="XLXN_498" name="sum" />
            <blockpin signalname="XLXN_421" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_101">
            <blockpin signalname="XLXN_212" name="B" />
            <blockpin signalname="XLXN_190" name="A" />
            <blockpin signalname="XLXN_189" name="C" />
            <blockpin signalname="XLXN_416" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_100">
            <blockpin signalname="XLXN_188" name="Ain" />
            <blockpin signalname="XLXN_210" name="Bin" />
            <blockpin signalname="XLXN_189" name="Cin" />
            <blockpin signalname="XLXN_187" name="Cout" />
            <blockpin signalname="XLXN_423" name="sum" />
            <blockpin signalname="XLXN_421" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_104">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_184" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_99">
            <blockpin signalname="XLXN_186" name="Ain" />
            <blockpin signalname="XLXN_209" name="Bin" />
            <blockpin signalname="XLXN_187" name="Cin" />
            <blockpin signalname="XLXN_180" name="Cout" />
            <blockpin signalname="XLXN_497" name="sum" />
            <blockpin signalname="XLXN_421" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_105">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_186" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_106">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_188" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_108">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_190" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_93">
            <blockpin signalname="XLXN_207" name="I0" />
            <blockpin signalname="XLXN_173" name="I1" />
            <blockpin signalname="Z(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_94">
            <blockpin signalname="XLXN_207" name="I0" />
            <blockpin signalname="XLXN_175" name="I1" />
            <blockpin signalname="Z(1)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_92">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_173" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_166" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_161" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_155" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_65">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_118" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_36">
            <blockpin signalname="XLXN_166" name="B" />
            <blockpin signalname="XLXN_118" name="A" />
            <blockpin signalname="XLXN_113" name="C" />
            <blockpin signalname="XLXN_175" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_56">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_91" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_55">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_92" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_53">
            <blockpin signalname="XLXN_91" name="Ain" />
            <blockpin signalname="XLXN_161" name="Bin" />
            <blockpin signalname="XLXN_113" name="Cin" />
            <blockpin signalname="XLXN_69" name="Cout" />
            <blockpin signalname="XLXN_212" name="sum" />
            <blockpin signalname="XLXN_207" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_51">
            <blockpin signalname="XLXN_92" name="Ain" />
            <blockpin signalname="XLXN_155" name="Bin" />
            <blockpin signalname="XLXN_69" name="Cin" />
            <blockpin signalname="XLXN_68" name="Cout" />
            <blockpin signalname="XLXN_210" name="sum" />
            <blockpin signalname="XLXN_207" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_50">
            <blockpin signalname="XLXN_90" name="Ain" />
            <blockpin name="Bin" />
            <blockpin signalname="XLXN_68" name="Cin" />
            <blockpin name="Cout" />
            <blockpin signalname="XLXN_209" name="sum" />
            <blockpin signalname="XLXN_207" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_21">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_90" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <attr value="Inch" name="LengthUnitName" />
        <attr value="10" name="GridsPerUnit" />
        <branch name="X(3:0)">
            <wire x2="768" y1="512" y2="512" x1="240" />
        </branch>
        <branch name="Y(3:0)">
            <wire x2="768" y1="656" y2="656" x1="224" />
        </branch>
        <iomarker fontsize="28" x="240" y="512" name="X(3:0)" orien="R180" />
        <iomarker fontsize="28" x="224" y="656" name="Y(3:0)" orien="R180" />
        <branch name="O(7:0)">
            <wire x2="768" y1="864" y2="864" x1="192" />
        </branch>
        <iomarker fontsize="28" x="768" y="864" name="O(7:0)" orien="R0" />
        <text style="fontsize:150;fontname:Arial" x="64" y="248">SIGNED COMBINATIONAL 4 BIT MULTIPLIER </text>
        <branch name="Y(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5104" y="1376" type="branch" />
            <wire x2="4144" y1="1376" y2="1376" x1="3792" />
            <wire x2="4608" y1="1376" y2="1376" x1="4144" />
            <wire x2="5024" y1="1376" y2="1376" x1="4608" />
            <wire x2="5104" y1="1376" y2="1376" x1="5024" />
            <wire x2="5024" y1="1376" y2="1488" x1="5024" />
            <wire x2="4608" y1="1376" y2="1488" x1="4608" />
            <wire x2="4144" y1="1376" y2="1488" x1="4144" />
            <wire x2="3792" y1="1376" y2="1472" x1="3792" />
        </branch>
        <instance x="2544" y="2128" name="XLXI_254" orien="M90">
        </instance>
        <instance x="2912" y="2128" name="XLXI_253" orien="M90">
        </instance>
        <instance x="3264" y="2128" name="XLXI_252" orien="M90">
        </instance>
        <instance x="2656" y="3152" name="XLXI_251" orien="M90">
        </instance>
        <instance x="2960" y="3152" name="XLXI_250" orien="M90">
        </instance>
        <instance x="2576" y="4384" name="XLXI_249" orien="M90">
        </instance>
        <instance x="3696" y="4864" name="XLXI_246" orien="R90" />
        <instance x="2928" y="4384" name="XLXI_191" orien="M90">
        </instance>
        <instance x="3856" y="4400" name="XLXI_188" orien="M90">
        </instance>
        <instance x="3632" y="4384" name="XLXI_187" orien="M90">
        </instance>
        <instance x="2608" y="4064" name="XLXI_186" orien="R90" />
        <instance x="3280" y="4384" name="XLXI_185" orien="M90">
        </instance>
        <instance x="2960" y="4064" name="XLXI_184" orien="R90" />
        <instance x="3312" y="4064" name="XLXI_183" orien="R90" />
        <instance x="3632" y="4064" name="XLXI_182" orien="R90" />
        <instance x="4048" y="3616" name="XLXI_197" orien="R90" />
        <instance x="3280" y="3152" name="XLXI_95" orien="M90">
        </instance>
        <instance x="4208" y="3168" name="XLXI_101" orien="M90">
        </instance>
        <instance x="3984" y="3152" name="XLXI_100" orien="M90">
        </instance>
        <instance x="2960" y="2832" name="XLXI_104" orien="R90" />
        <instance x="3632" y="3152" name="XLXI_99" orien="M90">
        </instance>
        <instance x="3312" y="2832" name="XLXI_105" orien="R90" />
        <instance x="3664" y="2832" name="XLXI_106" orien="R90" />
        <instance x="3984" y="2832" name="XLXI_108" orien="R90" />
        <instance x="4928" y="2576" name="XLXI_93" orien="R90" />
        <instance x="4512" y="2576" name="XLXI_94" orien="R90" />
        <instance x="4960" y="1488" name="XLXI_92" orien="R90" />
        <instance x="4544" y="1488" name="XLXI_7" orien="R90" />
        <instance x="4080" y="1488" name="XLXI_6" orien="R90" />
        <instance x="3728" y="1472" name="XLXI_5" orien="R90" />
        <instance x="4448" y="1840" name="XLXI_65" orien="R90" />
        <instance x="4672" y="2128" name="XLXI_36" orien="M90">
        </instance>
        <instance x="4016" y="1840" name="XLXI_56" orien="R90" />
        <instance x="3664" y="1840" name="XLXI_55" orien="R90" />
        <instance x="4336" y="2128" name="XLXI_53" orien="M90">
        </instance>
        <instance x="3984" y="2128" name="XLXI_51" orien="M90">
        </instance>
        <instance x="3632" y="2128" name="XLXI_50" orien="M90">
        </instance>
        <instance x="3312" y="1840" name="XLXI_21" orien="R90" />
        <branch name="XLXN_649">
            <wire x2="2944" y1="4848" y2="4848" x1="2288" />
            <wire x2="3296" y1="4848" y2="4848" x1="2944" />
            <wire x2="3648" y1="4848" y2="4848" x1="3296" />
            <wire x2="3760" y1="4848" y2="4848" x1="3648" />
            <wire x2="3760" y1="4848" y2="4864" x1="3760" />
            <wire x2="2944" y1="4576" y2="4576" x1="2928" />
            <wire x2="2944" y1="4576" y2="4848" x1="2944" />
            <wire x2="3296" y1="4576" y2="4576" x1="3280" />
            <wire x2="3296" y1="4576" y2="4848" x1="3296" />
            <wire x2="3648" y1="4576" y2="4576" x1="3632" />
            <wire x2="3648" y1="4576" y2="4848" x1="3648" />
        </branch>
        <branch name="XLXN_514">
            <wire x2="3824" y1="4704" y2="4864" x1="3824" />
        </branch>
        <branch name="Y(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3968" y="3984" type="branch" />
            <wire x2="3024" y1="3984" y2="3984" x1="2672" />
            <wire x2="3376" y1="3984" y2="3984" x1="3024" />
            <wire x2="3696" y1="3984" y2="3984" x1="3376" />
            <wire x2="3696" y1="3984" y2="4064" x1="3696" />
            <wire x2="3968" y1="3984" y2="3984" x1="3696" />
            <wire x2="3376" y1="3984" y2="4064" x1="3376" />
            <wire x2="3024" y1="3984" y2="4064" x1="3024" />
            <wire x2="2672" y1="3984" y2="4064" x1="2672" />
        </branch>
        <branch name="XLXN_498">
            <wire x2="3120" y1="3472" y2="4384" x1="3120" />
        </branch>
        <branch name="XLXN_497">
            <wire x2="3472" y1="3472" y2="4384" x1="3472" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3088" y="4048" type="branch" />
            <wire x2="3088" y1="4048" y2="4064" x1="3088" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3440" y="4048" type="branch" />
            <wire x2="3440" y1="4048" y2="4064" x1="3440" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="2736" y="4048" type="branch" />
            <wire x2="2736" y1="4048" y2="4064" x1="2736" />
        </branch>
        <branch name="XLXN_400">
            <wire x2="2992" y1="4368" y2="4368" x1="2832" />
            <wire x2="2992" y1="4368" y2="4720" x1="2992" />
            <wire x2="3056" y1="4720" y2="4720" x1="2992" />
            <wire x2="2832" y1="4368" y2="4384" x1="2832" />
            <wire x2="3056" y1="4704" y2="4720" x1="3056" />
        </branch>
        <branch name="XLXN_397">
            <wire x2="3536" y1="4368" y2="4384" x1="3536" />
            <wire x2="3680" y1="4368" y2="4368" x1="3536" />
            <wire x2="3680" y1="4368" y2="4720" x1="3680" />
            <wire x2="3728" y1="4720" y2="4720" x1="3680" />
            <wire x2="3728" y1="4704" y2="4720" x1="3728" />
        </branch>
        <branch name="XLXN_396">
            <wire x2="2704" y1="4320" y2="4384" x1="2704" />
        </branch>
        <branch name="XLXN_395">
            <wire x2="3184" y1="4368" y2="4384" x1="3184" />
            <wire x2="3344" y1="4368" y2="4368" x1="3184" />
            <wire x2="3344" y1="4368" y2="4720" x1="3344" />
            <wire x2="3408" y1="4720" y2="4720" x1="3344" />
            <wire x2="3408" y1="4704" y2="4720" x1="3408" />
        </branch>
        <branch name="XLXN_394">
            <wire x2="3056" y1="4320" y2="4384" x1="3056" />
        </branch>
        <branch name="XLXN_393">
            <wire x2="3408" y1="4320" y2="4384" x1="3408" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3760" y="4048" type="branch" />
            <wire x2="3760" y1="4048" y2="4064" x1="3760" />
        </branch>
        <branch name="XLXN_390">
            <wire x2="3728" y1="4320" y2="4400" x1="3728" />
        </branch>
        <branch name="XLXN_423">
            <wire x2="3824" y1="3472" y2="4400" x1="3824" />
        </branch>
        <branch name="XLXN_421">
            <wire x2="2672" y1="3600" y2="3600" x1="2288" />
            <wire x2="2976" y1="3600" y2="3600" x1="2672" />
            <wire x2="3296" y1="3600" y2="3600" x1="2976" />
            <wire x2="3648" y1="3600" y2="3600" x1="3296" />
            <wire x2="4000" y1="3600" y2="3600" x1="3648" />
            <wire x2="4112" y1="3600" y2="3600" x1="4000" />
            <wire x2="4112" y1="3600" y2="3616" x1="4112" />
            <wire x2="2672" y1="3344" y2="3344" x1="2656" />
            <wire x2="2672" y1="3344" y2="3600" x1="2672" />
            <wire x2="2976" y1="3344" y2="3344" x1="2960" />
            <wire x2="2976" y1="3344" y2="3600" x1="2976" />
            <wire x2="3296" y1="3344" y2="3344" x1="3280" />
            <wire x2="3296" y1="3344" y2="3600" x1="3296" />
            <wire x2="3648" y1="3344" y2="3344" x1="3632" />
            <wire x2="3648" y1="3344" y2="3600" x1="3648" />
            <wire x2="4000" y1="3344" y2="3344" x1="3984" />
            <wire x2="4000" y1="3344" y2="3600" x1="4000" />
        </branch>
        <branch name="XLXN_416">
            <wire x2="4176" y1="3472" y2="3616" x1="4176" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3440" y="2816" type="branch" />
            <wire x2="3440" y1="2816" y2="2832" x1="3440" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3792" y="2816" type="branch" />
            <wire x2="3792" y1="2816" y2="2832" x1="3792" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3088" y="2816" type="branch" />
            <wire x2="3088" y1="2816" y2="2832" x1="3088" />
        </branch>
        <branch name="XLXN_180">
            <wire x2="3344" y1="3136" y2="3136" x1="3184" />
            <wire x2="3344" y1="3136" y2="3488" x1="3344" />
            <wire x2="3408" y1="3488" y2="3488" x1="3344" />
            <wire x2="3184" y1="3136" y2="3152" x1="3184" />
            <wire x2="3408" y1="3472" y2="3488" x1="3408" />
        </branch>
        <branch name="XLXN_189">
            <wire x2="3888" y1="3136" y2="3152" x1="3888" />
            <wire x2="4032" y1="3136" y2="3136" x1="3888" />
            <wire x2="4032" y1="3136" y2="3488" x1="4032" />
            <wire x2="4080" y1="3488" y2="3488" x1="4032" />
            <wire x2="4080" y1="3472" y2="3488" x1="4080" />
        </branch>
        <branch name="XLXN_184">
            <wire x2="3056" y1="3088" y2="3152" x1="3056" />
        </branch>
        <branch name="XLXN_187">
            <wire x2="3536" y1="3136" y2="3152" x1="3536" />
            <wire x2="3696" y1="3136" y2="3136" x1="3536" />
            <wire x2="3696" y1="3136" y2="3488" x1="3696" />
            <wire x2="3760" y1="3488" y2="3488" x1="3696" />
            <wire x2="3760" y1="3472" y2="3488" x1="3760" />
        </branch>
        <branch name="XLXN_186">
            <wire x2="3408" y1="3088" y2="3152" x1="3408" />
        </branch>
        <branch name="XLXN_209">
            <wire x2="3472" y1="2448" y2="3152" x1="3472" />
        </branch>
        <branch name="XLXN_188">
            <wire x2="3760" y1="3088" y2="3152" x1="3760" />
        </branch>
        <branch name="XLXN_210">
            <wire x2="3824" y1="2448" y2="3152" x1="3824" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4112" y="2816" type="branch" />
            <wire x2="4112" y1="2816" y2="2832" x1="4112" />
        </branch>
        <branch name="Y(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="2752" type="branch" />
            <wire x2="3376" y1="2752" y2="2752" x1="3024" />
            <wire x2="3728" y1="2752" y2="2752" x1="3376" />
            <wire x2="4048" y1="2752" y2="2752" x1="3728" />
            <wire x2="4048" y1="2752" y2="2832" x1="4048" />
            <wire x2="4320" y1="2752" y2="2752" x1="4048" />
            <wire x2="3728" y1="2752" y2="2832" x1="3728" />
            <wire x2="3376" y1="2752" y2="2832" x1="3376" />
            <wire x2="3024" y1="2752" y2="2832" x1="3024" />
        </branch>
        <branch name="XLXN_190">
            <wire x2="4080" y1="3088" y2="3168" x1="4080" />
        </branch>
        <branch name="Z(1)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4608" y="2928" type="branch" />
            <wire x2="4608" y1="2832" y2="2928" x1="4608" />
        </branch>
        <branch name="XLXN_212">
            <wire x2="4176" y1="2448" y2="3168" x1="4176" />
        </branch>
        <branch name="Z(0)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5024" y="2848" type="branch" />
            <wire x2="5024" y1="2832" y2="2848" x1="5024" />
        </branch>
        <branch name="XLXN_207">
            <wire x2="2560" y1="2560" y2="2560" x1="2304" />
            <wire x2="2928" y1="2560" y2="2560" x1="2560" />
            <wire x2="3296" y1="2560" y2="2560" x1="2928" />
            <wire x2="3648" y1="2560" y2="2560" x1="3296" />
            <wire x2="4000" y1="2560" y2="2560" x1="3648" />
            <wire x2="4400" y1="2560" y2="2560" x1="4000" />
            <wire x2="4576" y1="2560" y2="2560" x1="4400" />
            <wire x2="4576" y1="2560" y2="2576" x1="4576" />
            <wire x2="4992" y1="2560" y2="2560" x1="4576" />
            <wire x2="4992" y1="2560" y2="2576" x1="4992" />
            <wire x2="2560" y1="2320" y2="2320" x1="2544" />
            <wire x2="2560" y1="2320" y2="2560" x1="2560" />
            <wire x2="2928" y1="2320" y2="2320" x1="2912" />
            <wire x2="2928" y1="2320" y2="2560" x1="2928" />
            <wire x2="3296" y1="2320" y2="2320" x1="3264" />
            <wire x2="3296" y1="2320" y2="2560" x1="3296" />
            <wire x2="3648" y1="2320" y2="2320" x1="3632" />
            <wire x2="3648" y1="2320" y2="2560" x1="3648" />
            <wire x2="4000" y1="2320" y2="2320" x1="3984" />
            <wire x2="4000" y1="2320" y2="2560" x1="4000" />
            <wire x2="4400" y1="2320" y2="2320" x1="4336" />
            <wire x2="4400" y1="2320" y2="2560" x1="4400" />
        </branch>
        <branch name="XLXN_175">
            <wire x2="4640" y1="2432" y2="2576" x1="4640" />
        </branch>
        <branch name="XLXN_173">
            <wire x2="5056" y1="1744" y2="2576" x1="5056" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4144" y="1824" type="branch" />
            <wire x2="4144" y1="1824" y2="1840" x1="4144" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4576" y="1824" type="branch" />
            <wire x2="4576" y1="1824" y2="1840" x1="4576" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3792" y="1824" type="branch" />
            <wire x2="3792" y1="1824" y2="1840" x1="3792" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5088" y="1472" type="branch" />
            <wire x2="5088" y1="1472" y2="1488" x1="5088" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4672" y="1472" type="branch" />
            <wire x2="4672" y1="1472" y2="1488" x1="4672" />
        </branch>
        <branch name="XLXN_166">
            <wire x2="4640" y1="1744" y2="2128" x1="4640" />
        </branch>
        <branch name="Y(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4784" y="1760" type="branch" />
            <wire x2="3376" y1="1760" y2="1840" x1="3376" />
            <wire x2="3728" y1="1760" y2="1760" x1="3376" />
            <wire x2="3728" y1="1760" y2="1840" x1="3728" />
            <wire x2="4080" y1="1760" y2="1760" x1="3728" />
            <wire x2="4080" y1="1760" y2="1840" x1="4080" />
            <wire x2="4512" y1="1760" y2="1760" x1="4080" />
            <wire x2="4512" y1="1760" y2="1840" x1="4512" />
            <wire x2="4784" y1="1760" y2="1760" x1="4512" />
        </branch>
        <branch name="XLXN_161">
            <wire x2="4176" y1="1744" y2="2128" x1="4176" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4208" y="1472" type="branch" />
            <wire x2="4208" y1="1472" y2="1488" x1="4208" />
        </branch>
        <branch name="XLXN_155">
            <wire x2="3824" y1="1728" y2="2128" x1="3824" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3856" y="1456" type="branch" />
            <wire x2="3856" y1="1456" y2="1472" x1="3856" />
        </branch>
        <branch name="XLXN_118">
            <wire x2="4544" y1="2096" y2="2128" x1="4544" />
        </branch>
        <branch name="XLXN_113">
            <wire x2="4240" y1="2112" y2="2128" x1="4240" />
            <wire x2="4464" y1="2112" y2="2112" x1="4240" />
            <wire x2="4464" y1="2112" y2="2512" x1="4464" />
            <wire x2="4544" y1="2512" y2="2512" x1="4464" />
            <wire x2="4544" y1="2432" y2="2512" x1="4544" />
        </branch>
        <branch name="XLXN_91">
            <wire x2="4112" y1="2096" y2="2128" x1="4112" />
        </branch>
        <branch name="XLXN_69">
            <wire x2="3888" y1="2112" y2="2128" x1="3888" />
            <wire x2="4048" y1="2112" y2="2112" x1="3888" />
            <wire x2="4048" y1="2112" y2="2464" x1="4048" />
            <wire x2="4112" y1="2464" y2="2464" x1="4048" />
            <wire x2="4112" y1="2448" y2="2464" x1="4112" />
        </branch>
        <branch name="XLXN_92">
            <wire x2="3760" y1="2096" y2="2128" x1="3760" />
        </branch>
        <branch name="XLXN_90">
            <wire x2="3408" y1="2096" y2="2128" x1="3408" />
        </branch>
        <branch name="XLXN_68">
            <wire x2="3536" y1="2112" y2="2128" x1="3536" />
            <wire x2="3696" y1="2112" y2="2112" x1="3536" />
            <wire x2="3696" y1="2112" y2="2464" x1="3696" />
            <wire x2="3760" y1="2464" y2="2464" x1="3696" />
            <wire x2="3760" y1="2448" y2="2464" x1="3760" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3440" y="1824" type="branch" />
            <wire x2="3440" y1="1824" y2="1840" x1="3440" />
        </branch>
    </sheet>
</drawing>