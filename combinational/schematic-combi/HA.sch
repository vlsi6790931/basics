<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="S" />
        <signal name="C" />
        <signal name="B" />
        <signal name="A" />
        <port polarity="Output" name="S" />
        <port polarity="Output" name="C" />
        <port polarity="Input" name="B" />
        <port polarity="Input" name="A" />
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="xor2" name="XLXI_1">
            <blockpin signalname="B" name="I0" />
            <blockpin signalname="A" name="I1" />
            <blockpin signalname="S" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="B" name="I0" />
            <blockpin signalname="A" name="I1" />
            <blockpin signalname="C" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="800" y="624" name="XLXI_1" orien="R0" />
        <branch name="S">
            <wire x2="1072" y1="528" y2="528" x1="1056" />
            <wire x2="1264" y1="528" y2="528" x1="1072" />
        </branch>
        <branch name="C">
            <wire x2="1072" y1="688" y2="688" x1="1056" />
            <wire x2="1280" y1="688" y2="688" x1="1072" />
        </branch>
        <branch name="B">
            <wire x2="688" y1="560" y2="560" x1="384" />
            <wire x2="800" y1="560" y2="560" x1="688" />
            <wire x2="688" y1="560" y2="720" x1="688" />
            <wire x2="800" y1="720" y2="720" x1="688" />
        </branch>
        <branch name="A">
            <wire x2="656" y1="496" y2="496" x1="384" />
            <wire x2="800" y1="496" y2="496" x1="656" />
            <wire x2="656" y1="496" y2="656" x1="656" />
            <wire x2="800" y1="656" y2="656" x1="656" />
        </branch>
        <instance x="800" y="784" name="XLXI_2" orien="R0" />
        <text style="fontsize:70;fontname:Arial" x="640" y="316">HALF-ADDER</text>
        <rect width="536" x="596" y="372" height="444" />
        <iomarker fontsize="28" x="384" y="560" name="B" orien="R180" />
        <iomarker fontsize="28" x="384" y="496" name="A" orien="R180" />
        <iomarker fontsize="28" x="1280" y="688" name="C" orien="R0" />
        <iomarker fontsize="28" x="1264" y="528" name="S" orien="R0" />
    </sheet>
</drawing>