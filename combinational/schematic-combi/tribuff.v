`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:45:42 08/11/2023 
// Design Name: 
// Module Name:    tribuff 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module tribuff(
		output 	reg 	O,
		input 			I,
		input 			E
 
    );
	
always@(*)
begin
	if(E === 1)
		O = I;
	else
		O = 1'bX;
end
endmodule
