<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_57" />
        <signal name="XLXN_58" />
        <signal name="XLXN_59" />
        <signal name="XLXN_60" />
        <signal name="XLXN_61" />
        <signal name="XLXN_1578" />
        <signal name="XLXN_1579" />
        <signal name="XLXN_1580" />
        <signal name="XLXN_1581" />
        <signal name="XLXN_1582" />
        <signal name="XLXN_1583" />
        <signal name="XLXN_1584" />
        <signal name="XLXN_1585" />
        <signal name="XLXN_1586" />
        <signal name="XLXN_1587" />
        <signal name="XLXN_1588" />
        <signal name="Y(3)" />
        <signal name="X(2)" />
        <signal name="X(1)" />
        <signal name="XLXN_416" />
        <signal name="XLXN_1561" />
        <signal name="XLXN_1562" />
        <signal name="X(3)" />
        <signal name="XLXN_180" />
        <signal name="XLXN_189" />
        <signal name="XLXN_184" />
        <signal name="XLXN_187" />
        <signal name="XLXN_186" />
        <signal name="XLXN_209" />
        <signal name="XLXN_188" />
        <signal name="XLXN_210" />
        <signal name="X(0)" />
        <signal name="Y(2)" />
        <signal name="XLXN_190" />
        <signal name="Z(1)" />
        <signal name="XLXN_212" />
        <signal name="Z(0)" />
        <signal name="XLXN_1670" />
        <signal name="XLXN_1671" />
        <signal name="XLXN_1672" />
        <signal name="XLXN_1673" />
        <signal name="XLXN_1674" />
        <signal name="XLXN_1675" />
        <signal name="XLXN_1676" />
        <signal name="XLXN_1677" />
        <signal name="XLXN_1678" />
        <signal name="XLXN_1679" />
        <signal name="XLXN_1680" />
        <signal name="RESET" />
        <signal name="XLXN_175" />
        <signal name="XLXN_173" />
        <signal name="XLXN_166" />
        <signal name="XLXN_161" />
        <signal name="XLXN_118" />
        <signal name="XLXN_113" />
        <signal name="XLXN_91" />
        <signal name="XLXN_69" />
        <signal name="XLXN_92" />
        <signal name="XLXN_75" />
        <signal name="XLXN_653" />
        <signal name="XLXN_155" />
        <signal name="XLXN_1602" />
        <signal name="Y(1)" />
        <signal name="XLXN_1604" />
        <signal name="XLXN_1605" />
        <signal name="XLXN_1606" />
        <signal name="XLXN_1607" />
        <signal name="XLXN_1608" />
        <signal name="XLXN_1609" />
        <signal name="XLXN_1610" />
        <signal name="Y(0)" />
        <signal name="XLXN_680" />
        <signal name="XLXN_650" />
        <signal name="XLXN_682" />
        <signal name="XLXN_683" />
        <signal name="XLXN_651" />
        <signal name="XLXN_686" />
        <signal name="XLXN_652" />
        <signal name="XLXN_690" />
        <signal name="XLXN_1620" />
        <signal name="Z(2)" />
        <signal name="XLXN_423" />
        <signal name="XLXN_497" />
        <signal name="XLXN_498" />
        <signal name="XLXN_691" />
        <signal name="Z(3)" />
        <signal name="XLXN_400" />
        <signal name="XLXN_395" />
        <signal name="XLXN_397" />
        <signal name="XLXN_1525" />
        <signal name="XLXN_1526" />
        <signal name="XLXN_1527" />
        <signal name="XLXN_1633" />
        <signal name="XLXN_1497" />
        <signal name="XLXN_1530" />
        <signal name="XLXN_1531" />
        <signal name="XLXN_1535" />
        <signal name="XLXN_1536" />
        <signal name="XLXN_1537" />
        <signal name="XLXN_1538" />
        <signal name="Z(4)" />
        <signal name="Z(5)" />
        <signal name="Z(6)" />
        <signal name="Z(7)" />
        <signal name="XLXN_1547" />
        <signal name="X(3:0)" />
        <signal name="Y(3:0)" />
        <signal name="Z(7:0)" />
        <port polarity="Input" name="RESET" />
        <port polarity="Input" name="X(3:0)" />
        <port polarity="Input" name="Y(3:0)" />
        <port polarity="Output" name="Z(7:0)" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="FA">
            <timestamp>2023-8-11T19:21:31</timestamp>
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
            <line x2="192" y1="-64" y2="0" x1="192" />
        </blockdef>
        <blockdef name="HA">
            <timestamp>2023-8-10T21:23:14</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="176" x="64" y="-156" height="156" />
            <line x2="304" y1="-128" y2="-128" x1="240" />
            <line x2="304" y1="-32" y2="-32" x1="240" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="FA" name="XLXI_251">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin signalname="XLXN_686" name="Bin" />
            <blockpin signalname="XLXN_690" name="Cin" />
            <blockpin name="Cout" />
            <blockpin signalname="XLXN_1535" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_250">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin signalname="XLXN_682" name="Bin" />
            <blockpin signalname="XLXN_683" name="Cin" />
            <blockpin signalname="XLXN_690" name="Cout" />
            <blockpin signalname="XLXN_691" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_184">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_1527" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_183">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_1525" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_197">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_416" name="I1" />
            <blockpin signalname="Z(2)" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_95">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin signalname="XLXN_680" name="Bin" />
            <blockpin signalname="XLXN_180" name="Cin" />
            <blockpin signalname="XLXN_683" name="Cout" />
            <blockpin signalname="XLXN_498" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_101">
            <blockpin signalname="XLXN_212" name="B" />
            <blockpin signalname="XLXN_190" name="A" />
            <blockpin signalname="XLXN_189" name="C" />
            <blockpin signalname="XLXN_416" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_100">
            <blockpin signalname="XLXN_188" name="Ain" />
            <blockpin signalname="XLXN_210" name="Bin" />
            <blockpin signalname="XLXN_189" name="Cin" />
            <blockpin signalname="XLXN_187" name="Cout" />
            <blockpin signalname="XLXN_423" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_104">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_184" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_99">
            <blockpin signalname="XLXN_186" name="Ain" />
            <blockpin signalname="XLXN_209" name="Bin" />
            <blockpin signalname="XLXN_187" name="Cin" />
            <blockpin signalname="XLXN_180" name="Cout" />
            <blockpin signalname="XLXN_497" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_105">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="XLXN_1561" name="I1" />
            <blockpin signalname="XLXN_186" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_106">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="XLXN_1562" name="I1" />
            <blockpin signalname="XLXN_188" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_108">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_190" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_93">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_173" name="I1" />
            <blockpin signalname="Z(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_94">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_175" name="I1" />
            <blockpin signalname="Z(1)" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_32">
            <blockpin signalname="XLXN_166" name="B" />
            <blockpin signalname="XLXN_118" name="A" />
            <blockpin signalname="XLXN_113" name="C" />
            <blockpin signalname="XLXN_175" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_53">
            <blockpin signalname="XLXN_91" name="Ain" />
            <blockpin signalname="XLXN_161" name="Bin" />
            <blockpin signalname="XLXN_113" name="Cin" />
            <blockpin signalname="XLXN_69" name="Cout" />
            <blockpin signalname="XLXN_212" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_51">
            <blockpin signalname="XLXN_92" name="Ain" />
            <blockpin signalname="XLXN_155" name="Bin" />
            <blockpin signalname="XLXN_69" name="Cin" />
            <blockpin signalname="XLXN_75" name="Cout" />
            <blockpin signalname="XLXN_210" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_50">
            <blockpin signalname="XLXN_653" name="Ain" />
            <blockpin signalname="XLXN_155" name="Bin" />
            <blockpin signalname="XLXN_75" name="Cin" />
            <blockpin signalname="XLXN_650" name="Cout" />
            <blockpin signalname="XLXN_209" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_21">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="XLXN_1602" name="I1" />
            <blockpin signalname="XLXN_653" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_55">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="XLXN_1604" name="I1" />
            <blockpin signalname="XLXN_92" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_56">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="XLXN_1606" name="I1" />
            <blockpin signalname="XLXN_91" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_65">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="XLXN_1605" name="I1" />
            <blockpin signalname="XLXN_118" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_92">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="XLXN_1610" name="I1" />
            <blockpin signalname="XLXN_173" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="XLXN_1609" name="I1" />
            <blockpin signalname="XLXN_166" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="XLXN_1608" name="I1" />
            <blockpin signalname="XLXN_161" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="XLXN_1607" name="I1" />
            <blockpin signalname="XLXN_155" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_252">
            <blockpin signalname="XLXN_155" name="Ain" />
            <blockpin signalname="XLXN_653" name="Bin" />
            <blockpin signalname="XLXN_650" name="Cin" />
            <blockpin signalname="XLXN_651" name="Cout" />
            <blockpin signalname="XLXN_680" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_253">
            <blockpin signalname="XLXN_155" name="Ain" />
            <blockpin signalname="XLXN_653" name="Bin" />
            <blockpin signalname="XLXN_651" name="Cin" />
            <blockpin signalname="XLXN_652" name="Cout" />
            <blockpin signalname="XLXN_682" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_254">
            <blockpin signalname="XLXN_155" name="Ain" />
            <blockpin signalname="XLXN_653" name="Bin" />
            <blockpin signalname="XLXN_652" name="Cin" />
            <blockpin name="Cout" />
            <blockpin signalname="XLXN_686" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_186">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="XLXN_1620" name="I1" />
            <blockpin signalname="XLXN_1526" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_191">
            <blockpin signalname="XLXN_1537" name="Ain" />
            <blockpin signalname="XLXN_691" name="Bin" />
            <blockpin signalname="XLXN_400" name="Cin" />
            <blockpin signalname="XLXN_1536" name="Cout" />
            <blockpin signalname="Z(6)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_185">
            <blockpin signalname="XLXN_1538" name="Ain" />
            <blockpin signalname="XLXN_498" name="Bin" />
            <blockpin signalname="XLXN_395" name="Cin" />
            <blockpin signalname="XLXN_400" name="Cout" />
            <blockpin signalname="Z(5)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_187">
            <blockpin signalname="XLXN_1531" name="Ain" />
            <blockpin signalname="XLXN_497" name="Bin" />
            <blockpin signalname="XLXN_397" name="Cin" />
            <blockpin signalname="XLXN_395" name="Cout" />
            <blockpin signalname="Z(4)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="inv" name="XLXI_676">
            <blockpin signalname="XLXN_1525" name="I" />
            <blockpin signalname="XLXN_1531" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_677">
            <blockpin signalname="XLXN_1527" name="I" />
            <blockpin signalname="XLXN_1538" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_652">
            <blockpin signalname="XLXN_1530" name="Ain" />
            <blockpin signalname="XLXN_423" name="Bin" />
            <blockpin signalname="XLXN_1547" name="Cin" />
            <blockpin signalname="XLXN_397" name="Cout" />
            <blockpin signalname="Z(3)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_182">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="XLXN_1633" name="I1" />
            <blockpin signalname="XLXN_1497" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_656">
            <blockpin signalname="XLXN_1497" name="I" />
            <blockpin signalname="XLXN_1530" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_249">
            <blockpin signalname="XLXN_1535" name="Ain" />
            <blockpin signalname="XLXN_1537" name="Bin" />
            <blockpin signalname="XLXN_1536" name="Cin" />
            <blockpin name="Cout" />
            <blockpin signalname="Z(7)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="inv" name="XLXI_678">
            <blockpin signalname="XLXN_1526" name="I" />
            <blockpin signalname="XLXN_1537" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_680">
            <blockpin signalname="XLXN_1547" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="3424" y="2992" name="XLXI_251" orien="M90">
        </instance>
        <instance x="3728" y="2992" name="XLXI_250" orien="M90">
        </instance>
        <instance x="3728" y="3904" name="XLXI_184" orien="R90" />
        <instance x="4080" y="3904" name="XLXI_183" orien="R90" />
        <instance x="4816" y="3456" name="XLXI_197" orien="R90" />
        <instance x="4048" y="2992" name="XLXI_95" orien="M90">
        </instance>
        <instance x="4976" y="3008" name="XLXI_101" orien="M90">
        </instance>
        <instance x="4752" y="2992" name="XLXI_100" orien="M90">
        </instance>
        <instance x="3728" y="2672" name="XLXI_104" orien="R90" />
        <instance x="4400" y="2992" name="XLXI_99" orien="M90">
        </instance>
        <instance x="4080" y="2672" name="XLXI_105" orien="R90" />
        <instance x="4432" y="2672" name="XLXI_106" orien="R90" />
        <instance x="4752" y="2672" name="XLXI_108" orien="R90" />
        <instance x="5696" y="2416" name="XLXI_93" orien="R90" />
        <instance x="5280" y="2416" name="XLXI_94" orien="R90" />
        <instance x="5440" y="1968" name="XLXI_32" orien="M90">
        </instance>
        <instance x="5104" y="1968" name="XLXI_53" orien="M90">
        </instance>
        <instance x="4752" y="1968" name="XLXI_51" orien="M90">
        </instance>
        <instance x="4400" y="1968" name="XLXI_50" orien="M90">
        </instance>
        <branch name="Y(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4736" y="3824" type="branch" />
            <wire x2="3792" y1="3824" y2="3824" x1="3472" />
            <wire x2="4144" y1="3824" y2="3824" x1="3792" />
            <wire x2="4144" y1="3824" y2="3904" x1="4144" />
            <wire x2="4496" y1="3824" y2="3824" x1="4144" />
            <wire x2="4736" y1="3824" y2="3824" x1="4496" />
            <wire x2="4496" y1="3824" y2="3904" x1="4496" />
            <wire x2="3792" y1="3824" y2="3904" x1="3792" />
            <wire x2="3472" y1="3824" y2="3888" x1="3472" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3856" y="3888" type="branch" />
            <wire x2="3856" y1="3888" y2="3904" x1="3856" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4208" y="3888" type="branch" />
            <wire x2="4208" y1="3888" y2="3904" x1="4208" />
        </branch>
        <branch name="XLXN_416">
            <wire x2="4944" y1="3312" y2="3456" x1="4944" />
        </branch>
        <branch name="XLXN_1561">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4208" y="2656" type="branch" />
            <wire x2="4208" y1="2656" y2="2672" x1="4208" />
        </branch>
        <branch name="XLXN_1562">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4560" y="2656" type="branch" />
            <wire x2="4560" y1="2656" y2="2672" x1="4560" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3856" y="2656" type="branch" />
            <wire x2="3856" y1="2656" y2="2672" x1="3856" />
        </branch>
        <branch name="XLXN_180">
            <wire x2="4112" y1="2976" y2="2976" x1="3952" />
            <wire x2="4112" y1="2976" y2="3328" x1="4112" />
            <wire x2="4176" y1="3328" y2="3328" x1="4112" />
            <wire x2="3952" y1="2976" y2="2992" x1="3952" />
            <wire x2="4176" y1="3312" y2="3328" x1="4176" />
        </branch>
        <branch name="XLXN_189">
            <wire x2="4656" y1="2976" y2="2992" x1="4656" />
            <wire x2="4800" y1="2976" y2="2976" x1="4656" />
            <wire x2="4800" y1="2976" y2="3328" x1="4800" />
            <wire x2="4848" y1="3328" y2="3328" x1="4800" />
            <wire x2="4848" y1="3312" y2="3328" x1="4848" />
        </branch>
        <branch name="XLXN_184">
            <wire x2="3504" y1="2944" y2="2944" x1="3200" />
            <wire x2="3824" y1="2944" y2="2944" x1="3504" />
            <wire x2="3824" y1="2944" y2="2992" x1="3824" />
            <wire x2="3504" y1="2944" y2="2992" x1="3504" />
            <wire x2="3200" y1="2944" y2="2992" x1="3200" />
            <wire x2="3824" y1="2928" y2="2944" x1="3824" />
        </branch>
        <branch name="XLXN_187">
            <wire x2="4304" y1="2976" y2="2992" x1="4304" />
            <wire x2="4464" y1="2976" y2="2976" x1="4304" />
            <wire x2="4464" y1="2976" y2="3328" x1="4464" />
            <wire x2="4528" y1="3328" y2="3328" x1="4464" />
            <wire x2="4528" y1="3312" y2="3328" x1="4528" />
        </branch>
        <branch name="XLXN_186">
            <wire x2="4176" y1="2928" y2="2992" x1="4176" />
        </branch>
        <branch name="XLXN_209">
            <wire x2="4240" y1="2288" y2="2992" x1="4240" />
        </branch>
        <branch name="XLXN_188">
            <wire x2="4528" y1="2928" y2="2992" x1="4528" />
        </branch>
        <branch name="XLXN_210">
            <wire x2="4592" y1="2288" y2="2992" x1="4592" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4880" y="2656" type="branch" />
            <wire x2="4880" y1="2656" y2="2672" x1="4880" />
        </branch>
        <branch name="Y(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5088" y="2592" type="branch" />
            <wire x2="4144" y1="2592" y2="2592" x1="3792" />
            <wire x2="4496" y1="2592" y2="2592" x1="4144" />
            <wire x2="4816" y1="2592" y2="2592" x1="4496" />
            <wire x2="4816" y1="2592" y2="2672" x1="4816" />
            <wire x2="5088" y1="2592" y2="2592" x1="4816" />
            <wire x2="4496" y1="2592" y2="2672" x1="4496" />
            <wire x2="4144" y1="2592" y2="2672" x1="4144" />
            <wire x2="3792" y1="2592" y2="2672" x1="3792" />
        </branch>
        <branch name="XLXN_190">
            <wire x2="4848" y1="2928" y2="3008" x1="4848" />
        </branch>
        <branch name="Z(1)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5376" y="2768" type="branch" />
            <wire x2="5376" y1="2672" y2="2768" x1="5376" />
        </branch>
        <branch name="XLXN_212">
            <wire x2="4944" y1="2288" y2="3008" x1="4944" />
        </branch>
        <branch name="Z(0)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5792" y="2688" type="branch" />
            <wire x2="5792" y1="2672" y2="2688" x1="5792" />
        </branch>
        <branch name="RESET">
            <wire x2="1792" y1="2400" y2="2400" x1="1680" />
            <wire x2="1792" y1="2400" y2="3440" x1="1792" />
            <wire x2="1792" y1="3440" y2="4912" x1="1792" />
            <wire x2="3408" y1="4912" y2="4912" x1="1792" />
            <wire x2="3728" y1="4912" y2="4912" x1="3408" />
            <wire x2="4064" y1="4912" y2="4912" x1="3728" />
            <wire x2="4416" y1="4912" y2="4912" x1="4064" />
            <wire x2="4832" y1="4912" y2="4912" x1="4416" />
            <wire x2="3424" y1="3440" y2="3440" x1="1792" />
            <wire x2="3728" y1="3440" y2="3440" x1="3424" />
            <wire x2="4064" y1="3440" y2="3440" x1="3728" />
            <wire x2="4416" y1="3440" y2="3440" x1="4064" />
            <wire x2="4768" y1="3440" y2="3440" x1="4416" />
            <wire x2="4880" y1="3440" y2="3440" x1="4768" />
            <wire x2="4880" y1="3440" y2="3456" x1="4880" />
            <wire x2="3424" y1="2400" y2="2400" x1="1792" />
            <wire x2="3744" y1="2400" y2="2400" x1="3424" />
            <wire x2="4064" y1="2400" y2="2400" x1="3744" />
            <wire x2="4416" y1="2400" y2="2400" x1="4064" />
            <wire x2="4768" y1="2400" y2="2400" x1="4416" />
            <wire x2="5168" y1="2400" y2="2400" x1="4768" />
            <wire x2="5344" y1="2400" y2="2400" x1="5168" />
            <wire x2="5344" y1="2400" y2="2416" x1="5344" />
            <wire x2="5760" y1="2400" y2="2400" x1="5344" />
            <wire x2="5760" y1="2400" y2="2416" x1="5760" />
            <wire x2="3408" y1="4640" y2="4912" x1="3408" />
            <wire x2="3424" y1="2144" y2="2400" x1="3424" />
            <wire x2="3424" y1="3184" y2="3440" x1="3424" />
            <wire x2="3744" y1="2160" y2="2160" x1="3728" />
            <wire x2="3744" y1="2160" y2="2400" x1="3744" />
            <wire x2="3728" y1="3184" y2="3440" x1="3728" />
            <wire x2="3728" y1="4640" y2="4912" x1="3728" />
            <wire x2="4064" y1="2160" y2="2160" x1="4048" />
            <wire x2="4064" y1="2160" y2="2400" x1="4064" />
            <wire x2="4064" y1="3184" y2="3184" x1="4048" />
            <wire x2="4064" y1="3184" y2="3440" x1="4064" />
            <wire x2="4064" y1="4640" y2="4640" x1="4048" />
            <wire x2="4064" y1="4640" y2="4912" x1="4064" />
            <wire x2="4416" y1="2160" y2="2160" x1="4400" />
            <wire x2="4416" y1="2160" y2="2400" x1="4416" />
            <wire x2="4416" y1="3184" y2="3184" x1="4400" />
            <wire x2="4416" y1="3184" y2="3440" x1="4416" />
            <wire x2="4416" y1="4640" y2="4640" x1="4400" />
            <wire x2="4416" y1="4640" y2="4912" x1="4416" />
            <wire x2="4768" y1="2160" y2="2160" x1="4752" />
            <wire x2="4768" y1="2160" y2="2400" x1="4768" />
            <wire x2="4768" y1="3184" y2="3184" x1="4752" />
            <wire x2="4768" y1="3184" y2="3440" x1="4768" />
            <wire x2="4832" y1="4640" y2="4640" x1="4752" />
            <wire x2="4832" y1="4640" y2="4912" x1="4832" />
            <wire x2="5168" y1="2160" y2="2160" x1="5104" />
            <wire x2="5168" y1="2160" y2="2400" x1="5168" />
        </branch>
        <branch name="XLXN_175">
            <wire x2="5408" y1="2272" y2="2416" x1="5408" />
        </branch>
        <branch name="XLXN_173">
            <wire x2="5824" y1="1360" y2="2416" x1="5824" />
        </branch>
        <branch name="XLXN_166">
            <wire x2="5408" y1="1360" y2="1968" x1="5408" />
        </branch>
        <branch name="XLXN_161">
            <wire x2="4944" y1="1360" y2="1968" x1="4944" />
        </branch>
        <branch name="XLXN_118">
            <wire x2="5312" y1="1808" y2="1968" x1="5312" />
        </branch>
        <branch name="XLXN_113">
            <wire x2="5008" y1="1952" y2="1968" x1="5008" />
            <wire x2="5232" y1="1952" y2="1952" x1="5008" />
            <wire x2="5232" y1="1952" y2="2352" x1="5232" />
            <wire x2="5312" y1="2352" y2="2352" x1="5232" />
            <wire x2="5312" y1="2272" y2="2352" x1="5312" />
        </branch>
        <branch name="XLXN_91">
            <wire x2="4880" y1="1808" y2="1968" x1="4880" />
        </branch>
        <branch name="XLXN_69">
            <wire x2="4656" y1="1952" y2="1968" x1="4656" />
            <wire x2="4816" y1="1952" y2="1952" x1="4656" />
            <wire x2="4816" y1="1952" y2="2304" x1="4816" />
            <wire x2="4880" y1="2304" y2="2304" x1="4816" />
            <wire x2="4880" y1="2288" y2="2304" x1="4880" />
        </branch>
        <branch name="XLXN_92">
            <wire x2="4528" y1="1808" y2="1968" x1="4528" />
        </branch>
        <branch name="XLXN_75">
            <wire x2="4304" y1="1952" y2="1968" x1="4304" />
            <wire x2="4464" y1="1952" y2="1952" x1="4304" />
            <wire x2="4464" y1="1952" y2="2304" x1="4464" />
            <wire x2="4528" y1="2304" y2="2304" x1="4464" />
            <wire x2="4528" y1="2288" y2="2304" x1="4528" />
        </branch>
        <branch name="XLXN_653">
            <wire x2="3568" y1="1936" y2="1936" x1="3264" />
            <wire x2="3888" y1="1936" y2="1936" x1="3568" />
            <wire x2="4176" y1="1936" y2="1936" x1="3888" />
            <wire x2="4176" y1="1936" y2="1968" x1="4176" />
            <wire x2="3888" y1="1936" y2="1968" x1="3888" />
            <wire x2="3568" y1="1936" y2="1968" x1="3568" />
            <wire x2="3264" y1="1936" y2="1952" x1="3264" />
            <wire x2="4176" y1="1808" y2="1936" x1="4176" />
        </branch>
        <branch name="XLXN_155">
            <wire x2="3504" y1="1856" y2="1856" x1="3200" />
            <wire x2="3824" y1="1856" y2="1856" x1="3504" />
            <wire x2="4240" y1="1856" y2="1856" x1="3824" />
            <wire x2="4592" y1="1856" y2="1856" x1="4240" />
            <wire x2="4592" y1="1856" y2="1968" x1="4592" />
            <wire x2="4240" y1="1856" y2="1968" x1="4240" />
            <wire x2="3824" y1="1856" y2="1968" x1="3824" />
            <wire x2="3504" y1="1856" y2="1968" x1="3504" />
            <wire x2="3200" y1="1856" y2="1952" x1="3200" />
            <wire x2="4592" y1="1344" y2="1856" x1="4592" />
        </branch>
        <instance x="4080" y="1552" name="XLXI_21" orien="R90" />
        <instance x="4432" y="1552" name="XLXI_55" orien="R90" />
        <instance x="4784" y="1552" name="XLXI_56" orien="R90" />
        <instance x="5216" y="1552" name="XLXI_65" orien="R90" />
        <branch name="XLXN_1602">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4208" y="1536" type="branch" />
            <wire x2="4208" y1="1536" y2="1552" x1="4208" />
        </branch>
        <branch name="Y(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5552" y="1472" type="branch" />
            <wire x2="4144" y1="1472" y2="1552" x1="4144" />
            <wire x2="4496" y1="1472" y2="1472" x1="4144" />
            <wire x2="4496" y1="1472" y2="1552" x1="4496" />
            <wire x2="4848" y1="1472" y2="1472" x1="4496" />
            <wire x2="4848" y1="1472" y2="1552" x1="4848" />
            <wire x2="5280" y1="1472" y2="1472" x1="4848" />
            <wire x2="5280" y1="1472" y2="1552" x1="5280" />
            <wire x2="5552" y1="1472" y2="1472" x1="5280" />
        </branch>
        <branch name="XLXN_1604">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4560" y="1536" type="branch" />
            <wire x2="4560" y1="1536" y2="1552" x1="4560" />
        </branch>
        <branch name="XLXN_1605">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5344" y="1536" type="branch" />
            <wire x2="5344" y1="1536" y2="1552" x1="5344" />
        </branch>
        <branch name="XLXN_1606">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4912" y="1536" type="branch" />
            <wire x2="4912" y1="1536" y2="1552" x1="4912" />
        </branch>
        <instance x="5728" y="1104" name="XLXI_92" orien="R90" />
        <instance x="5312" y="1104" name="XLXI_7" orien="R90" />
        <instance x="4848" y="1104" name="XLXI_6" orien="R90" />
        <instance x="4496" y="1088" name="XLXI_5" orien="R90" />
        <branch name="XLXN_1607">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4624" y="1072" type="branch" />
            <wire x2="4624" y1="1072" y2="1088" x1="4624" />
        </branch>
        <branch name="XLXN_1608">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4976" y="1088" type="branch" />
            <wire x2="4976" y1="1088" y2="1104" x1="4976" />
        </branch>
        <branch name="XLXN_1609">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5440" y="1088" type="branch" />
            <wire x2="5440" y1="1088" y2="1104" x1="5440" />
        </branch>
        <branch name="XLXN_1610">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5856" y="1088" type="branch" />
            <wire x2="5856" y1="1088" y2="1104" x1="5856" />
        </branch>
        <branch name="Y(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5872" y="992" type="branch" />
            <wire x2="4912" y1="992" y2="992" x1="4560" />
            <wire x2="5376" y1="992" y2="992" x1="4912" />
            <wire x2="5792" y1="992" y2="992" x1="5376" />
            <wire x2="5872" y1="992" y2="992" x1="5792" />
            <wire x2="5792" y1="992" y2="1104" x1="5792" />
            <wire x2="5376" y1="992" y2="1104" x1="5376" />
            <wire x2="4912" y1="992" y2="1104" x1="4912" />
            <wire x2="4560" y1="992" y2="1088" x1="4560" />
        </branch>
        <branch name="XLXN_680">
            <wire x2="3888" y1="2288" y2="2992" x1="3888" />
        </branch>
        <instance x="4048" y="1968" name="XLXI_252" orien="M90">
        </instance>
        <branch name="XLXN_650">
            <wire x2="3952" y1="1952" y2="1968" x1="3952" />
            <wire x2="4112" y1="1952" y2="1952" x1="3952" />
            <wire x2="4112" y1="1952" y2="2304" x1="4112" />
            <wire x2="4176" y1="2304" y2="2304" x1="4112" />
            <wire x2="4176" y1="2288" y2="2304" x1="4176" />
        </branch>
        <branch name="XLXN_682">
            <wire x2="3568" y1="2288" y2="2992" x1="3568" />
        </branch>
        <branch name="XLXN_683">
            <wire x2="3632" y1="2976" y2="2992" x1="3632" />
            <wire x2="3760" y1="2976" y2="2976" x1="3632" />
            <wire x2="3760" y1="2976" y2="3328" x1="3760" />
            <wire x2="3824" y1="3328" y2="3328" x1="3760" />
            <wire x2="3824" y1="3312" y2="3328" x1="3824" />
        </branch>
        <branch name="XLXN_651">
            <wire x2="3776" y1="1952" y2="1952" x1="3632" />
            <wire x2="3776" y1="1952" y2="2304" x1="3776" />
            <wire x2="3824" y1="2304" y2="2304" x1="3776" />
            <wire x2="3632" y1="1952" y2="1968" x1="3632" />
            <wire x2="3824" y1="2288" y2="2304" x1="3824" />
        </branch>
        <instance x="3728" y="1968" name="XLXI_253" orien="M90">
        </instance>
        <branch name="XLXN_686">
            <wire x2="3264" y1="2272" y2="2992" x1="3264" />
        </branch>
        <instance x="3424" y="1952" name="XLXI_254" orien="M90">
        </instance>
        <branch name="XLXN_652">
            <wire x2="3456" y1="1952" y2="1952" x1="3328" />
            <wire x2="3456" y1="1952" y2="2304" x1="3456" />
            <wire x2="3504" y1="2304" y2="2304" x1="3456" />
            <wire x2="3504" y1="2288" y2="2304" x1="3504" />
        </branch>
        <branch name="XLXN_690">
            <wire x2="3456" y1="2992" y2="2992" x1="3328" />
            <wire x2="3456" y1="2992" y2="3328" x1="3456" />
            <wire x2="3504" y1="3328" y2="3328" x1="3456" />
            <wire x2="3504" y1="3312" y2="3328" x1="3504" />
        </branch>
        <branch name="XLXN_1620">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3536" y="3872" type="branch" />
            <wire x2="3536" y1="3872" y2="3888" x1="3536" />
        </branch>
        <instance x="3408" y="3888" name="XLXI_186" orien="R90" />
        <branch name="Z(2)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4912" y="3744" type="branch" />
            <wire x2="4912" y1="3712" y2="3744" x1="4912" />
        </branch>
        <branch name="XLXN_423">
            <wire x2="4592" y1="3312" y2="4448" x1="4592" />
        </branch>
        <branch name="XLXN_497">
            <wire x2="4240" y1="3312" y2="4448" x1="4240" />
        </branch>
        <branch name="XLXN_498">
            <wire x2="3888" y1="3312" y2="4448" x1="3888" />
        </branch>
        <branch name="XLXN_691">
            <wire x2="3568" y1="3312" y2="4448" x1="3568" />
        </branch>
        <instance x="3728" y="4448" name="XLXI_191" orien="M90">
        </instance>
        <instance x="4048" y="4448" name="XLXI_185" orien="M90">
        </instance>
        <instance x="4400" y="4448" name="XLXI_187" orien="M90">
        </instance>
        <branch name="Z(3)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4592" y="5200" type="branch" />
            <wire x2="4592" y1="4768" y2="5200" x1="4592" />
        </branch>
        <branch name="XLXN_400">
            <wire x2="3632" y1="4432" y2="4448" x1="3632" />
            <wire x2="3760" y1="4432" y2="4432" x1="3632" />
            <wire x2="3760" y1="4432" y2="4784" x1="3760" />
            <wire x2="3824" y1="4784" y2="4784" x1="3760" />
            <wire x2="3824" y1="4768" y2="4784" x1="3824" />
        </branch>
        <branch name="XLXN_395">
            <wire x2="3952" y1="4432" y2="4448" x1="3952" />
            <wire x2="4112" y1="4432" y2="4432" x1="3952" />
            <wire x2="4112" y1="4432" y2="4784" x1="4112" />
            <wire x2="4176" y1="4784" y2="4784" x1="4112" />
            <wire x2="4176" y1="4768" y2="4784" x1="4176" />
        </branch>
        <branch name="XLXN_397">
            <wire x2="4304" y1="4432" y2="4448" x1="4304" />
            <wire x2="4464" y1="4432" y2="4432" x1="4304" />
            <wire x2="4464" y1="4432" y2="4832" x1="4464" />
            <wire x2="4528" y1="4832" y2="4832" x1="4464" />
            <wire x2="4528" y1="4768" y2="4832" x1="4528" />
        </branch>
        <branch name="XLXN_1525">
            <wire x2="4176" y1="4160" y2="4192" x1="4176" />
        </branch>
        <instance x="4144" y="4192" name="XLXI_676" orien="R90" />
        <branch name="XLXN_1526">
            <wire x2="3504" y1="4144" y2="4160" x1="3504" />
        </branch>
        <branch name="XLXN_1527">
            <wire x2="3824" y1="4160" y2="4192" x1="3824" />
        </branch>
        <instance x="3792" y="4192" name="XLXI_677" orien="R90" />
        <instance x="4752" y="4448" name="XLXI_652" orien="M90">
        </instance>
        <instance x="4432" y="3904" name="XLXI_182" orien="R90" />
        <branch name="XLXN_1633">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4560" y="3888" type="branch" />
            <wire x2="4560" y1="3888" y2="3904" x1="4560" />
        </branch>
        <branch name="XLXN_1497">
            <wire x2="4528" y1="4160" y2="4192" x1="4528" />
        </branch>
        <branch name="XLXN_1530">
            <wire x2="4528" y1="4416" y2="4448" x1="4528" />
        </branch>
        <instance x="4496" y="4192" name="XLXI_656" orien="R90" />
        <branch name="XLXN_1531">
            <wire x2="4176" y1="4416" y2="4448" x1="4176" />
        </branch>
        <instance x="3408" y="4448" name="XLXI_249" orien="M90">
        </instance>
        <branch name="XLXN_1535">
            <wire x2="3184" y1="3872" y2="4448" x1="3184" />
            <wire x2="3264" y1="3872" y2="3872" x1="3184" />
            <wire x2="3264" y1="3312" y2="3872" x1="3264" />
        </branch>
        <branch name="XLXN_1536">
            <wire x2="3312" y1="4432" y2="4448" x1="3312" />
            <wire x2="3440" y1="4432" y2="4432" x1="3312" />
            <wire x2="3440" y1="4432" y2="4784" x1="3440" />
            <wire x2="3504" y1="4784" y2="4784" x1="3440" />
            <wire x2="3504" y1="4768" y2="4784" x1="3504" />
        </branch>
        <branch name="XLXN_1537">
            <wire x2="3504" y1="4400" y2="4400" x1="3248" />
            <wire x2="3504" y1="4400" y2="4448" x1="3504" />
            <wire x2="3248" y1="4400" y2="4448" x1="3248" />
            <wire x2="3504" y1="4384" y2="4400" x1="3504" />
        </branch>
        <instance x="3472" y="4160" name="XLXI_678" orien="R90" />
        <branch name="XLXN_1538">
            <wire x2="3824" y1="4416" y2="4448" x1="3824" />
        </branch>
        <branch name="Z(4)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4240" y="5200" type="branch" />
            <wire x2="4240" y1="4768" y2="5200" x1="4240" />
        </branch>
        <branch name="Z(5)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3888" y="5184" type="branch" />
            <wire x2="3888" y1="4768" y2="5184" x1="3888" />
        </branch>
        <branch name="Z(6)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3568" y="5184" type="branch" />
            <wire x2="3568" y1="4768" y2="5184" x1="3568" />
        </branch>
        <branch name="Z(7)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3248" y="5184" type="branch" />
            <wire x2="3248" y1="4768" y2="5184" x1="3248" />
        </branch>
        <branch name="XLXN_1547">
            <wire x2="4656" y1="4416" y2="4448" x1="4656" />
        </branch>
        <instance x="4592" y="4416" name="XLXI_680" orien="R0" />
        <branch name="X(3:0)">
            <wire x2="2416" y1="1424" y2="1424" x1="1888" />
        </branch>
        <iomarker fontsize="28" x="1680" y="2400" name="RESET" orien="R180" />
        <iomarker fontsize="28" x="1888" y="1424" name="X(3:0)" orien="R180" />
        <iomarker fontsize="28" x="1888" y="1568" name="Y(3:0)" orien="R180" />
        <branch name="Y(3:0)">
            <wire x2="2416" y1="1568" y2="1568" x1="1888" />
        </branch>
        <iomarker fontsize="28" x="2320" y="1776" name="Z(7:0)" orien="R0" />
        <branch name="Z(7:0)">
            <wire x2="2320" y1="1776" y2="1776" x1="1776" />
        </branch>
        <text style="fontsize:150;fontname:Arial" x="2560" y="408">SIGNED MULTIPLIER (4 BIT)</text>
    </sheet>
</drawing>