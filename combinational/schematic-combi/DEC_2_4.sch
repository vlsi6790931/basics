<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_5" />
        <signal name="XLXN_38" />
        <signal name="XLXN_41" />
        <signal name="I1" />
        <signal name="I0" />
        <signal name="XLXN_9" />
        <signal name="XLXN_8" />
        <signal name="XLXN_52" />
        <signal name="R" />
        <signal name="XLXN_54" />
        <signal name="XLXN_2" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_58" />
        <signal name="XLXN_61" />
        <signal name="XLXN_12" />
        <signal name="XLXN_40" />
        <signal name="XLXN_39" />
        <signal name="XLXN_65" />
        <signal name="XLXN_37" />
        <signal name="XLXN_36" />
        <signal name="XLXN_69" />
        <signal name="O0" />
        <signal name="O1" />
        <signal name="O2" />
        <signal name="O3" />
        <port polarity="Input" name="I1" />
        <port polarity="Input" name="I0" />
        <port polarity="Input" name="R" />
        <port polarity="Output" name="O0" />
        <port polarity="Output" name="O1" />
        <port polarity="Output" name="O2" />
        <port polarity="Output" name="O3" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="inv" name="XLXI_9">
            <blockpin signalname="XLXN_2" name="I" />
            <blockpin signalname="XLXN_39" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="XLXN_9" name="I" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="XLXN_8" name="I" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_19">
            <blockpin signalname="I0" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_20">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="I1" name="I1" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="XLXN_39" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="XLXN_36" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_11">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="XLXN_40" name="I1" />
            <blockpin signalname="XLXN_35" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="XLXN_34" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_7">
            <blockpin signalname="XLXN_12" name="I" />
            <blockpin signalname="XLXN_40" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_13">
            <blockpin signalname="XLXN_39" name="I0" />
            <blockpin signalname="XLXN_40" name="I1" />
            <blockpin signalname="XLXN_37" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_66">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_34" name="I1" />
            <blockpin signalname="O0" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_67">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_35" name="I1" />
            <blockpin signalname="O1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_68">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_36" name="I1" />
            <blockpin signalname="O2" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_69">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_37" name="I1" />
            <blockpin signalname="O3" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="1152" y="896" name="XLXI_9" orien="R0" />
        <instance x="864" y="896" name="XLXI_8" orien="R0" />
        <instance x="864" y="704" name="XLXI_6" orien="R0" />
        <instance x="576" y="768" name="XLXI_19" orien="R0" />
        <instance x="576" y="960" name="XLXI_20" orien="R0" />
        <branch name="I1">
            <wire x2="576" y1="832" y2="832" x1="464" />
        </branch>
        <branch name="I0">
            <wire x2="576" y1="704" y2="704" x1="464" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="864" y1="864" y2="864" x1="832" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="864" y1="672" y2="672" x1="832" />
        </branch>
        <branch name="R">
            <wire x2="496" y1="1088" y2="1088" x1="432" />
            <wire x2="496" y1="1088" y2="1120" x1="496" />
            <wire x2="2016" y1="1120" y2="1120" x1="496" />
            <wire x2="576" y1="640" y2="640" x1="496" />
            <wire x2="496" y1="640" y2="896" x1="496" />
            <wire x2="576" y1="896" y2="896" x1="496" />
            <wire x2="496" y1="896" y2="1088" x1="496" />
            <wire x2="2032" y1="624" y2="624" x1="2016" />
            <wire x2="2016" y1="624" y2="768" x1="2016" />
            <wire x2="2032" y1="768" y2="768" x1="2016" />
            <wire x2="2016" y1="768" y2="896" x1="2016" />
            <wire x2="2016" y1="896" y2="1056" x1="2016" />
            <wire x2="2032" y1="1056" y2="1056" x1="2016" />
            <wire x2="2016" y1="1056" y2="1120" x1="2016" />
            <wire x2="2032" y1="896" y2="896" x1="2016" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1152" y1="864" y2="864" x1="1088" />
            <wire x2="1536" y1="592" y2="592" x1="1152" />
            <wire x2="1152" y1="592" y2="736" x1="1152" />
            <wire x2="1536" y1="736" y2="736" x1="1152" />
            <wire x2="1152" y1="736" y2="864" x1="1152" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="2032" y1="560" y2="560" x1="1792" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="2032" y1="704" y2="704" x1="1792" />
        </branch>
        <instance x="1552" y="928" name="XLXI_12" orien="R0" />
        <instance x="1536" y="800" name="XLXI_11" orien="R0" />
        <instance x="1536" y="656" name="XLXI_10" orien="R0" />
        <branch name="XLXN_12">
            <wire x2="1104" y1="672" y2="672" x1="1088" />
            <wire x2="1104" y1="672" y2="800" x1="1104" />
            <wire x2="1552" y1="800" y2="800" x1="1104" />
            <wire x2="1168" y1="672" y2="672" x1="1104" />
            <wire x2="1536" y1="528" y2="528" x1="1104" />
            <wire x2="1104" y1="528" y2="672" x1="1104" />
        </branch>
        <instance x="1168" y="704" name="XLXI_7" orien="R0" />
        <instance x="1552" y="1088" name="XLXI_13" orien="R0" />
        <branch name="XLXN_40">
            <wire x2="1424" y1="672" y2="672" x1="1392" />
            <wire x2="1536" y1="672" y2="672" x1="1424" />
            <wire x2="1424" y1="672" y2="960" x1="1424" />
            <wire x2="1552" y1="960" y2="960" x1="1424" />
        </branch>
        <branch name="XLXN_39">
            <wire x2="1392" y1="864" y2="864" x1="1376" />
            <wire x2="1552" y1="864" y2="864" x1="1392" />
            <wire x2="1392" y1="864" y2="1024" x1="1392" />
            <wire x2="1552" y1="1024" y2="1024" x1="1392" />
        </branch>
        <iomarker fontsize="28" x="464" y="832" name="I1" orien="R180" />
        <iomarker fontsize="28" x="464" y="704" name="I0" orien="R180" />
        <iomarker fontsize="28" x="432" y="1088" name="R" orien="R180" />
        <branch name="XLXN_37">
            <wire x2="2032" y1="992" y2="992" x1="1808" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="2032" y1="832" y2="832" x1="1808" />
        </branch>
        <instance x="2032" y="688" name="XLXI_66" orien="R0" />
        <instance x="2032" y="832" name="XLXI_67" orien="R0" />
        <instance x="2032" y="960" name="XLXI_68" orien="R0" />
        <instance x="2032" y="1120" name="XLXI_69" orien="R0" />
        <branch name="O0">
            <wire x2="2320" y1="592" y2="592" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2320" y="592" name="O0" orien="R0" />
        <branch name="O1">
            <wire x2="2320" y1="736" y2="736" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2320" y="736" name="O1" orien="R0" />
        <branch name="O2">
            <wire x2="2320" y1="864" y2="864" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2320" y="864" name="O2" orien="R0" />
        <branch name="O3">
            <wire x2="2320" y1="1024" y2="1024" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2320" y="1024" name="O3" orien="R0" />
    </sheet>
</drawing>