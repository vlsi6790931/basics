<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_1" />
        <signal name="XLXN_9" />
        <signal name="XLXN_19" />
        <signal name="XLXN_21" />
        <signal name="XLXN_29" />
        <signal name="XLXN_31" />
        <signal name="XLXN_35" />
        <signal name="XLXN_40" />
        <signal name="XLXN_46" />
        <signal name="XLXN_50" />
        <signal name="XLXN_66" />
        <signal name="XLXN_59" />
        <signal name="XLXN_64" />
        <signal name="R" />
        <signal name="XLXN_75" />
        <signal name="XLXN_61" />
        <signal name="I" />
        <signal name="O0" />
        <signal name="O1" />
        <signal name="XLXN_81" />
        <signal name="XLXN_83" />
        <signal name="XLXN_85" />
        <signal name="XLXN_87" />
        <port polarity="Input" name="R" />
        <port polarity="Input" name="I" />
        <port polarity="Output" name="O0" />
        <port polarity="Output" name="O1" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="inv" name="XLXI_28">
            <blockpin signalname="XLXN_59" name="I" />
            <blockpin signalname="XLXN_61" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_30">
            <blockpin signalname="XLXN_61" name="I" />
            <blockpin signalname="XLXN_64" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_33">
            <blockpin signalname="I" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="XLXN_59" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_31">
            <blockpin signalname="XLXN_61" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="O0" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_32">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_64" name="I1" />
            <blockpin signalname="O1" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <branch name="XLXN_59">
            <wire x2="752" y1="624" y2="624" x1="720" />
        </branch>
        <instance x="752" y="656" name="XLXI_28" orien="R0" />
        <instance x="1008" y="656" name="XLXI_30" orien="R0" />
        <branch name="R">
            <wire x2="432" y1="592" y2="592" x1="368" />
            <wire x2="464" y1="592" y2="592" x1="432" />
            <wire x2="432" y1="592" y2="720" x1="432" />
            <wire x2="1264" y1="720" y2="720" x1="432" />
            <wire x2="1264" y1="480" y2="480" x1="432" />
            <wire x2="432" y1="480" y2="592" x1="432" />
        </branch>
        <branch name="I">
            <wire x2="448" y1="656" y2="656" x1="352" />
            <wire x2="464" y1="656" y2="656" x1="448" />
        </branch>
        <branch name="O0">
            <wire x2="1552" y1="512" y2="512" x1="1520" />
        </branch>
        <iomarker fontsize="28" x="1552" y="512" name="O0" orien="R0" />
        <branch name="O1">
            <wire x2="1536" y1="688" y2="688" x1="1520" />
            <wire x2="1552" y1="688" y2="688" x1="1536" />
        </branch>
        <iomarker fontsize="28" x="1552" y="688" name="O1" orien="R0" />
        <branch name="XLXN_64">
            <wire x2="1248" y1="624" y2="624" x1="1232" />
            <wire x2="1248" y1="624" y2="656" x1="1248" />
            <wire x2="1264" y1="656" y2="656" x1="1248" />
        </branch>
        <instance x="464" y="720" name="XLXI_33" orien="R0" />
        <iomarker fontsize="28" x="368" y="592" name="R" orien="R180" />
        <iomarker fontsize="28" x="352" y="656" name="I" orien="R180" />
        <instance x="1264" y="784" name="XLXI_32" orien="R0" />
        <instance x="1264" y="608" name="XLXI_31" orien="R0" />
        <branch name="XLXN_61">
            <wire x2="992" y1="624" y2="624" x1="976" />
            <wire x2="1008" y1="624" y2="624" x1="992" />
            <wire x2="1264" y1="544" y2="544" x1="992" />
            <wire x2="992" y1="544" y2="624" x1="992" />
        </branch>
    </sheet>
</drawing>