<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_15" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_29" />
        <signal name="XLXN_67" />
        <signal name="XLXN_68" />
        <signal name="XLXN_69" />
        <signal name="XLXN_71" />
        <signal name="XLXN_72" />
        <signal name="XLXN_2" />
        <signal name="qweqr" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_6" />
        <signal name="qeqwe" />
        <signal name="XLXN_3" />
        <signal name="L" />
        <signal name="G" />
        <signal name="XLXN_66" />
        <signal name="XLXN_89" />
        <signal name="XLXN_90" />
        <signal name="R" />
        <signal name="E" />
        <signal name="I1" />
        <signal name="I2" />
        <port polarity="Output" name="L" />
        <port polarity="Output" name="G" />
        <port polarity="Input" name="R" />
        <port polarity="Output" name="E" />
        <port polarity="Input" name="I1" />
        <port polarity="Input" name="I2" />
        <blockdef name="xnor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
            <circle r="8" cx="220" cy="-96" />
            <line x2="256" y1="-96" y2="-96" x1="228" />
            <line x2="60" y1="-28" y2="-28" x1="60" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <block symbolname="xnor2" name="XLXI_2">
            <blockpin signalname="qweqr" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="xnor2" name="XLXI_3">
            <blockpin signalname="qweqr" name="I0" />
            <blockpin signalname="qeqwe" name="I1" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_4">
            <blockpin signalname="XLXN_6" name="P" />
        </block>
        <block symbolname="inv" name="XLXI_10">
            <blockpin signalname="XLXN_25" name="I" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="xnor2" name="XLXI_1">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="qeqwe" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_14">
            <blockpin signalname="XLXN_24" name="I" />
            <blockpin signalname="XLXN_66" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_23">
            <blockpin signalname="XLXN_24" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="R" name="I2" />
            <blockpin signalname="G" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_24">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_2" name="I1" />
            <blockpin signalname="XLXN_24" name="I2" />
            <blockpin signalname="L" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_25">
            <blockpin signalname="I1" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="qeqwe" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_26">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="I2" name="I1" />
            <blockpin signalname="qweqr" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_47">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_66" name="I1" />
            <blockpin signalname="E" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="1280" y="1168" name="XLXI_2" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="1952" y1="1072" y2="1072" x1="1536" />
        </branch>
        <instance x="1392" y="960" name="XLXI_3" orien="R0" />
        <instance x="1104" y="1024" name="XLXI_4" orien="R0" />
        <branch name="qweqr">
            <wire x2="944" y1="976" y2="976" x1="832" />
            <wire x2="944" y1="896" y2="976" x1="944" />
            <wire x2="1056" y1="896" y2="896" x1="944" />
            <wire x2="1392" y1="896" y2="896" x1="1056" />
            <wire x2="1056" y1="896" y2="1104" x1="1056" />
            <wire x2="1280" y1="1104" y2="1104" x1="1056" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="1920" y1="864" y2="864" x1="1904" />
            <wire x2="1920" y1="864" y2="1008" x1="1920" />
            <wire x2="1952" y1="1008" y2="1008" x1="1920" />
            <wire x2="1984" y1="864" y2="864" x1="1920" />
            <wire x2="1920" y1="720" y2="864" x1="1920" />
            <wire x2="1952" y1="720" y2="720" x1="1920" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="1680" y1="864" y2="864" x1="1648" />
        </branch>
        <instance x="1680" y="896" name="XLXI_10" orien="R0" />
        <branch name="XLXN_6">
            <wire x2="1168" y1="1024" y2="1040" x1="1168" />
            <wire x2="1264" y1="1040" y2="1040" x1="1168" />
            <wire x2="1280" y1="1040" y2="1040" x1="1264" />
            <wire x2="1264" y1="688" y2="1040" x1="1264" />
            <wire x2="1280" y1="688" y2="688" x1="1264" />
        </branch>
        <branch name="qeqwe">
            <wire x2="944" y1="752" y2="752" x1="832" />
            <wire x2="944" y1="752" y2="832" x1="944" />
            <wire x2="1056" y1="832" y2="832" x1="944" />
            <wire x2="1392" y1="832" y2="832" x1="1056" />
            <wire x2="1056" y1="624" y2="832" x1="1056" />
            <wire x2="1280" y1="624" y2="624" x1="1056" />
        </branch>
        <instance x="1280" y="752" name="XLXI_1" orien="R0" />
        <branch name="XLXN_3">
            <wire x2="1952" y1="656" y2="656" x1="1536" />
        </branch>
        <instance x="1984" y="896" name="XLXI_14" orien="R0" />
        <instance x="1952" y="784" name="XLXI_23" orien="R0" />
        <instance x="1952" y="1200" name="XLXI_24" orien="R0" />
        <branch name="L">
            <wire x2="2240" y1="1072" y2="1072" x1="2208" />
        </branch>
        <branch name="G">
            <wire x2="2240" y1="656" y2="656" x1="2208" />
        </branch>
        <branch name="XLXN_66">
            <wire x2="2368" y1="864" y2="864" x1="2208" />
        </branch>
        <instance x="576" y="848" name="XLXI_25" orien="R0" />
        <instance x="576" y="1072" name="XLXI_26" orien="R0" />
        <branch name="R">
            <wire x2="528" y1="1200" y2="1200" x1="480" />
            <wire x2="1952" y1="1200" y2="1200" x1="528" />
            <wire x2="2352" y1="1200" y2="1200" x1="1952" />
            <wire x2="1952" y1="560" y2="560" x1="528" />
            <wire x2="1952" y1="560" y2="592" x1="1952" />
            <wire x2="528" y1="560" y2="720" x1="528" />
            <wire x2="576" y1="720" y2="720" x1="528" />
            <wire x2="528" y1="720" y2="1008" x1="528" />
            <wire x2="576" y1="1008" y2="1008" x1="528" />
            <wire x2="528" y1="1008" y2="1200" x1="528" />
            <wire x2="1952" y1="1136" y2="1200" x1="1952" />
            <wire x2="2368" y1="928" y2="928" x1="2352" />
            <wire x2="2352" y1="928" y2="1200" x1="2352" />
        </branch>
        <instance x="2368" y="992" name="XLXI_47" orien="R0" />
        <branch name="E">
            <wire x2="2656" y1="896" y2="896" x1="2624" />
        </branch>
        <branch name="I1">
            <wire x2="496" y1="784" y2="784" x1="464" />
            <wire x2="576" y1="784" y2="784" x1="496" />
        </branch>
        <branch name="I2">
            <wire x2="496" y1="944" y2="944" x1="464" />
            <wire x2="576" y1="944" y2="944" x1="496" />
        </branch>
        <iomarker fontsize="28" x="2240" y="1072" name="L" orien="R0" />
        <iomarker fontsize="28" x="2240" y="656" name="G" orien="R0" />
        <iomarker fontsize="28" x="2656" y="896" name="E" orien="R0" />
        <iomarker fontsize="28" x="464" y="784" name="I1" orien="R180" />
        <iomarker fontsize="28" x="464" y="944" name="I2" orien="R180" />
        <iomarker fontsize="28" x="480" y="1200" name="R" orien="R180" />
    </sheet>
</drawing>