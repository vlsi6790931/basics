<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_3" />
        <signal name="XLXN_7(7:0)" />
        <signal name="XLXN_8(7:0)" />
        <signal name="I(7:0)" />
        <signal name="XLXN_13" />
        <signal name="O(7:0)" />
        <signal name="reset" />
        <port polarity="Input" name="I(7:0)" />
        <port polarity="Output" name="O(7:0)" />
        <port polarity="Input" name="reset" />
        <blockdef name="inv8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <circle r="16" cx="144" cy="-32" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <rect width="64" x="160" y="-44" height="24" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
        </blockdef>
        <blockdef name="EBA">
            <timestamp>2023-7-28T18:43:12</timestamp>
            <rect width="128" x="64" y="-192" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="256" y1="-160" y2="-160" x1="192" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <rect width="64" x="192" y="-108" height="24" />
            <line x2="128" y1="-64" y2="0" x1="128" />
            <line x2="128" y1="-192" y2="-256" x1="128" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="inv8" name="not_ins(7:0)">
            <blockpin signalname="I(7:0)" name="I(7:0)" />
            <blockpin signalname="XLXN_8(7:0)" name="O(7:0)" />
        </block>
        <block symbolname="EBA" name="XLXI_2">
            <blockpin signalname="XLXN_8(7:0)" name="A_IN(7:0)" />
            <blockpin signalname="XLXN_7(7:0)" name="B_IN(7:0)" />
            <blockpin signalname="XLXN_13" name="C_OUT" />
            <blockpin signalname="O(7:0)" name="SUM(7:0)" />
            <blockpin signalname="XLXN_3" name="C_IN" />
            <blockpin signalname="reset" name="reset" />
        </block>
        <block symbolname="vcc" name="XLXI_3">
            <blockpin signalname="XLXN_3" name="P" />
        </block>
        <block symbolname="gnd" name="gnd_ins(7:0)">
            <blockpin signalname="XLXN_7(7:0)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_6">
            <blockpin signalname="XLXN_13" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="800" y="736" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_7(7:0)">
            <wire x2="800" y1="576" y2="576" x1="720" />
        </branch>
        <branch name="XLXN_8(7:0)">
            <wire x2="800" y1="640" y2="640" x1="656" />
        </branch>
        <branch name="I(7:0)">
            <wire x2="432" y1="640" y2="640" x1="240" />
        </branch>
        <iomarker fontsize="28" x="240" y="640" name="I(7:0)" orien="R180" />
        <instance x="784" y="448" name="gnd_ins(7:0)" orien="R180" />
        <instance x="432" y="672" name="not_ins(7:0)" orien="R0" />
        <rect width="1024" x="404" y="340" height="524" />
        <text style="fontsize:60;fontname:Arial" x="577" y="232">8-BIT TWOs COMPLIMENT</text>
        <instance x="832" y="736" name="XLXI_3" orien="R180" />
        <branch name="XLXN_3">
            <wire x2="928" y1="736" y2="736" x1="768" />
        </branch>
        <instance x="1152" y="448" name="XLXI_6" orien="R180" />
        <branch name="XLXN_13">
            <wire x2="1088" y1="576" y2="576" x1="1056" />
        </branch>
        <branch name="O(7:0)">
            <wire x2="1472" y1="640" y2="640" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="1472" y="640" name="O(7:0)" orien="R0" />
        <branch name="reset">
            <wire x2="928" y1="416" y2="416" x1="240" />
            <wire x2="928" y1="416" y2="464" x1="928" />
            <wire x2="928" y1="464" y2="480" x1="928" />
        </branch>
        <iomarker fontsize="28" x="240" y="416" name="reset" orien="R180" />
    </sheet>
</drawing>