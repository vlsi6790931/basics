<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="I(1)" />
        <signal name="XLXN_115" />
        <signal name="XLXN_120" />
        <signal name="I(3)" />
        <signal name="I(2)" />
        <signal name="I(0)" />
        <signal name="I(5)" />
        <signal name="XLXN_153" />
        <signal name="XLXN_155" />
        <signal name="I(7)" />
        <signal name="I(6)" />
        <signal name="I(4)" />
        <signal name="A0" />
        <signal name="A1" />
        <signal name="A2" />
        <signal name="XLXN_47" />
        <signal name="O(0)" />
        <signal name="XLXN_192" />
        <signal name="O(1)" />
        <signal name="XLXN_194" />
        <signal name="O(2)" />
        <signal name="XLXN_196" />
        <signal name="reset" />
        <signal name="O(2:0)" />
        <signal name="I(7:0)" />
        <signal name="XLXN_386" />
        <signal name="XLXN_392" />
        <signal name="XLXN_393" />
        <signal name="XLXN_394" />
        <signal name="XLXN_400" />
        <signal name="XLXN_402" />
        <signal name="XLXN_407" />
        <signal name="XLXN_410" />
        <signal name="XLXN_411" />
        <signal name="XLXN_412" />
        <signal name="XLXN_413" />
        <signal name="XLXN_414" />
        <signal name="XLXN_416" />
        <signal name="XLXN_418" />
        <signal name="XLXN_419" />
        <signal name="XLXN_420" />
        <signal name="XLXN_421" />
        <signal name="XLXN_422" />
        <signal name="XLXN_425" />
        <signal name="XLXN_428" />
        <signal name="XLXN_431" />
        <signal name="XLXN_434" />
        <signal name="XLXN_436" />
        <port polarity="Input" name="reset" />
        <port polarity="Output" name="O(2:0)" />
        <port polarity="Input" name="I(7:0)" />
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="tribuff">
            <timestamp>2023-8-11T11:20:44</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="xor4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="60" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="208" y1="-160" y2="-160" x1="256" />
            <arc ex="64" ey="-208" sx="64" sy="-112" r="56" cx="32" cy="-160" />
            <line x2="64" y1="-208" y2="-208" x1="128" />
            <line x2="64" y1="-112" y2="-112" x1="128" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <arc ex="128" ey="-208" sx="208" sy="-160" r="88" cx="132" cy="-120" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="208" ey="-160" sx="128" sy="-112" r="88" cx="132" cy="-200" />
        </blockdef>
        <blockdef name="and4b4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="40" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="52" cy="-64" />
            <line x2="40" y1="-128" y2="-128" x1="0" />
            <circle r="12" cx="52" cy="-128" />
            <line x2="40" y1="-192" y2="-192" x1="0" />
            <circle r="12" cx="52" cy="-192" />
            <line x2="40" y1="-256" y2="-256" x1="0" />
            <circle r="12" cx="52" cy="-256" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="64" y1="-112" y2="-112" x1="144" />
        </blockdef>
        <block symbolname="or2" name="XLXI_72">
            <blockpin signalname="I(1)" name="I0" />
            <blockpin signalname="I(0)" name="I1" />
            <blockpin signalname="XLXN_115" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_73">
            <blockpin signalname="I(3)" name="I0" />
            <blockpin signalname="I(2)" name="I1" />
            <blockpin signalname="XLXN_120" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_80">
            <blockpin signalname="XLXN_120" name="I0" />
            <blockpin signalname="I(1)" name="I1" />
            <blockpin signalname="XLXN_420" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_81">
            <blockpin signalname="XLXN_120" name="I0" />
            <blockpin signalname="I(0)" name="I1" />
            <blockpin signalname="XLXN_419" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_82">
            <blockpin signalname="I(3)" name="I0" />
            <blockpin signalname="XLXN_115" name="I1" />
            <blockpin signalname="XLXN_436" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_83">
            <blockpin signalname="I(2)" name="I0" />
            <blockpin signalname="XLXN_115" name="I1" />
            <blockpin signalname="XLXN_421" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_102">
            <blockpin signalname="I(5)" name="I0" />
            <blockpin signalname="I(4)" name="I1" />
            <blockpin signalname="XLXN_153" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_103">
            <blockpin signalname="I(7)" name="I0" />
            <blockpin signalname="I(6)" name="I1" />
            <blockpin signalname="XLXN_155" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_104">
            <blockpin signalname="XLXN_155" name="I0" />
            <blockpin signalname="I(5)" name="I1" />
            <blockpin signalname="XLXN_412" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_105">
            <blockpin signalname="XLXN_155" name="I0" />
            <blockpin signalname="I(4)" name="I1" />
            <blockpin signalname="XLXN_411" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_106">
            <blockpin signalname="I(7)" name="I0" />
            <blockpin signalname="XLXN_153" name="I1" />
            <blockpin signalname="XLXN_418" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_107">
            <blockpin signalname="I(6)" name="I0" />
            <blockpin signalname="XLXN_153" name="I1" />
            <blockpin signalname="XLXN_413" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_25">
            <blockpin signalname="reset" name="I0" />
            <blockpin signalname="XLXN_47" name="I1" />
            <blockpin signalname="O(0)" name="O" />
        </block>
        <block symbolname="tribuff" name="XLXI_38">
            <blockpin signalname="A0" name="I" />
            <blockpin signalname="XLXN_196" name="E" />
            <blockpin signalname="XLXN_47" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_117">
            <blockpin signalname="reset" name="I0" />
            <blockpin signalname="XLXN_192" name="I1" />
            <blockpin signalname="O(1)" name="O" />
        </block>
        <block symbolname="tribuff" name="XLXI_118">
            <blockpin signalname="A1" name="I" />
            <blockpin signalname="XLXN_196" name="E" />
            <blockpin signalname="XLXN_192" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_119">
            <blockpin signalname="reset" name="I0" />
            <blockpin signalname="XLXN_194" name="I1" />
            <blockpin signalname="O(2)" name="O" />
        </block>
        <block symbolname="tribuff" name="XLXI_120">
            <blockpin signalname="A2" name="I" />
            <blockpin signalname="XLXN_196" name="E" />
            <blockpin signalname="XLXN_194" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_125">
            <blockpin signalname="I(4)" name="I0" />
            <blockpin signalname="I(5)" name="I1" />
            <blockpin signalname="I(6)" name="I2" />
            <blockpin signalname="I(7)" name="I3" />
            <blockpin signalname="A2" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_124">
            <blockpin signalname="I(1)" name="I0" />
            <blockpin signalname="I(3)" name="I1" />
            <blockpin signalname="I(5)" name="I2" />
            <blockpin signalname="I(7)" name="I3" />
            <blockpin signalname="A0" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_126">
            <blockpin signalname="I(2)" name="I0" />
            <blockpin signalname="I(3)" name="I1" />
            <blockpin signalname="I(6)" name="I2" />
            <blockpin signalname="I(7)" name="I3" />
            <blockpin signalname="A1" name="O" />
        </block>
        <block symbolname="xor4" name="XLXI_163">
            <blockpin signalname="XLXN_436" name="I0" />
            <blockpin signalname="XLXN_421" name="I1" />
            <blockpin signalname="XLXN_420" name="I2" />
            <blockpin signalname="XLXN_419" name="I3" />
            <blockpin signalname="XLXN_386" name="O" />
        </block>
        <block symbolname="xor4" name="XLXI_164">
            <blockpin signalname="XLXN_418" name="I0" />
            <blockpin signalname="XLXN_413" name="I1" />
            <blockpin signalname="XLXN_412" name="I2" />
            <blockpin signalname="XLXN_411" name="I3" />
            <blockpin signalname="XLXN_392" name="O" />
        </block>
        <block symbolname="and4b4" name="XLXI_165">
            <blockpin signalname="I(0)" name="I0" />
            <blockpin signalname="I(1)" name="I1" />
            <blockpin signalname="I(2)" name="I2" />
            <blockpin signalname="I(3)" name="I3" />
            <blockpin signalname="XLXN_407" name="O" />
        </block>
        <block symbolname="and4b4" name="XLXI_166">
            <blockpin signalname="I(4)" name="I0" />
            <blockpin signalname="I(5)" name="I1" />
            <blockpin signalname="I(6)" name="I2" />
            <blockpin signalname="I(7)" name="I3" />
            <blockpin signalname="XLXN_410" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_167">
            <blockpin signalname="XLXN_394" name="I0" />
            <blockpin signalname="XLXN_393" name="I1" />
            <blockpin signalname="XLXN_196" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_169">
            <blockpin signalname="XLXN_410" name="I0" />
            <blockpin signalname="XLXN_386" name="I1" />
            <blockpin signalname="XLXN_393" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_172">
            <blockpin signalname="XLXN_392" name="I0" />
            <blockpin signalname="XLXN_407" name="I1" />
            <blockpin signalname="XLXN_394" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="384" y="496" name="XLXI_72" orien="R0" />
        <branch name="I(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="288" y="432" type="branch" />
            <wire x2="368" y1="432" y2="432" x1="288" />
            <wire x2="384" y1="432" y2="432" x1="368" />
            <wire x2="736" y1="288" y2="288" x1="368" />
            <wire x2="736" y1="288" y2="400" x1="736" />
            <wire x2="752" y1="400" y2="400" x1="736" />
            <wire x2="368" y1="288" y2="432" x1="368" />
        </branch>
        <instance x="384" y="720" name="XLXI_73" orien="R0" />
        <instance x="752" y="528" name="XLXI_80" orien="R0" />
        <instance x="752" y="304" name="XLXI_81" orien="R0" />
        <instance x="768" y="992" name="XLXI_82" orien="R0" />
        <instance x="768" y="768" name="XLXI_83" orien="R0" />
        <branch name="XLXN_115">
            <wire x2="720" y1="400" y2="400" x1="640" />
            <wire x2="720" y1="400" y2="640" x1="720" />
            <wire x2="768" y1="640" y2="640" x1="720" />
            <wire x2="720" y1="640" y2="864" x1="720" />
            <wire x2="768" y1="864" y2="864" x1="720" />
        </branch>
        <branch name="XLXN_120">
            <wire x2="656" y1="624" y2="624" x1="640" />
            <wire x2="656" y1="240" y2="464" x1="656" />
            <wire x2="656" y1="464" y2="624" x1="656" />
            <wire x2="752" y1="464" y2="464" x1="656" />
            <wire x2="752" y1="240" y2="240" x1="656" />
        </branch>
        <branch name="I(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="288" y="656" type="branch" />
            <wire x2="320" y1="656" y2="656" x1="288" />
            <wire x2="384" y1="656" y2="656" x1="320" />
            <wire x2="320" y1="656" y2="928" x1="320" />
            <wire x2="768" y1="928" y2="928" x1="320" />
        </branch>
        <branch name="I(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="288" y="592" type="branch" />
            <wire x2="352" y1="592" y2="592" x1="288" />
            <wire x2="384" y1="592" y2="592" x1="352" />
            <wire x2="352" y1="592" y2="704" x1="352" />
            <wire x2="768" y1="704" y2="704" x1="352" />
        </branch>
        <branch name="I(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="272" y="368" type="branch" />
            <wire x2="336" y1="368" y2="368" x1="272" />
            <wire x2="384" y1="368" y2="368" x1="336" />
            <wire x2="752" y1="176" y2="176" x1="336" />
            <wire x2="336" y1="176" y2="368" x1="336" />
        </branch>
        <instance x="416" y="1392" name="XLXI_102" orien="R0" />
        <branch name="I(5)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="320" y="1328" type="branch" />
            <wire x2="400" y1="1328" y2="1328" x1="320" />
            <wire x2="416" y1="1328" y2="1328" x1="400" />
            <wire x2="768" y1="1184" y2="1184" x1="400" />
            <wire x2="768" y1="1184" y2="1296" x1="768" />
            <wire x2="784" y1="1296" y2="1296" x1="768" />
            <wire x2="400" y1="1184" y2="1328" x1="400" />
        </branch>
        <instance x="416" y="1616" name="XLXI_103" orien="R0" />
        <instance x="784" y="1424" name="XLXI_104" orien="R0" />
        <instance x="784" y="1200" name="XLXI_105" orien="R0" />
        <instance x="800" y="1888" name="XLXI_106" orien="R0" />
        <instance x="800" y="1664" name="XLXI_107" orien="R0" />
        <branch name="XLXN_153">
            <wire x2="752" y1="1296" y2="1296" x1="672" />
            <wire x2="752" y1="1296" y2="1536" x1="752" />
            <wire x2="800" y1="1536" y2="1536" x1="752" />
            <wire x2="752" y1="1536" y2="1760" x1="752" />
            <wire x2="800" y1="1760" y2="1760" x1="752" />
        </branch>
        <branch name="XLXN_155">
            <wire x2="688" y1="1520" y2="1520" x1="672" />
            <wire x2="688" y1="1136" y2="1360" x1="688" />
            <wire x2="688" y1="1360" y2="1520" x1="688" />
            <wire x2="784" y1="1360" y2="1360" x1="688" />
            <wire x2="784" y1="1136" y2="1136" x1="688" />
        </branch>
        <branch name="I(7)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="320" y="1552" type="branch" />
            <wire x2="352" y1="1552" y2="1552" x1="320" />
            <wire x2="416" y1="1552" y2="1552" x1="352" />
            <wire x2="352" y1="1552" y2="1824" x1="352" />
            <wire x2="800" y1="1824" y2="1824" x1="352" />
        </branch>
        <branch name="I(6)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="320" y="1488" type="branch" />
            <wire x2="384" y1="1488" y2="1488" x1="320" />
            <wire x2="416" y1="1488" y2="1488" x1="384" />
            <wire x2="384" y1="1488" y2="1600" x1="384" />
            <wire x2="800" y1="1600" y2="1600" x1="384" />
        </branch>
        <branch name="I(4)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="304" y="1264" type="branch" />
            <wire x2="368" y1="1264" y2="1264" x1="304" />
            <wire x2="416" y1="1264" y2="1264" x1="368" />
            <wire x2="784" y1="1072" y2="1072" x1="368" />
            <wire x2="368" y1="1072" y2="1264" x1="368" />
        </branch>
        <branch name="XLXN_47">
            <wire x2="2928" y1="848" y2="848" x1="2896" />
        </branch>
        <instance x="2928" y="976" name="XLXI_25" orien="R0" />
        <branch name="O(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3232" y="880" type="branch" />
            <wire x2="3232" y1="880" y2="880" x1="3184" />
        </branch>
        <instance x="2512" y="944" name="XLXI_38" orien="R0">
        </instance>
        <branch name="XLXN_192">
            <wire x2="2928" y1="1264" y2="1264" x1="2896" />
        </branch>
        <instance x="2928" y="1392" name="XLXI_117" orien="R0" />
        <branch name="O(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3232" y="1296" type="branch" />
            <wire x2="3232" y1="1296" y2="1296" x1="3184" />
        </branch>
        <instance x="2512" y="1360" name="XLXI_118" orien="R0">
        </instance>
        <branch name="XLXN_194">
            <wire x2="2944" y1="1648" y2="1648" x1="2912" />
        </branch>
        <instance x="2944" y="1776" name="XLXI_119" orien="R0" />
        <branch name="O(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3248" y="1680" type="branch" />
            <wire x2="3248" y1="1680" y2="1680" x1="3200" />
        </branch>
        <instance x="2528" y="1744" name="XLXI_120" orien="R0">
        </instance>
        <branch name="reset">
            <wire x2="2912" y1="688" y2="912" x1="2912" />
            <wire x2="2928" y1="912" y2="912" x1="2912" />
            <wire x2="2912" y1="912" y2="1328" x1="2912" />
            <wire x2="2928" y1="1328" y2="1328" x1="2912" />
            <wire x2="2912" y1="1328" y2="1712" x1="2912" />
            <wire x2="2944" y1="1712" y2="1712" x1="2912" />
        </branch>
        <branch name="A2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2496" y="1648" type="branch" />
            <wire x2="2528" y1="1648" y2="1648" x1="2496" />
        </branch>
        <branch name="A1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="1264" type="branch" />
            <wire x2="2512" y1="1264" y2="1264" x1="2464" />
        </branch>
        <branch name="A0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="848" type="branch" />
            <wire x2="2512" y1="848" y2="848" x1="2464" />
        </branch>
        <branch name="O(2:0)">
            <wire x2="3200" y1="1120" y2="1120" x1="3120" />
        </branch>
        <iomarker fontsize="28" x="2912" y="688" name="reset" orien="R270" />
        <iomarker fontsize="28" x="3200" y="1120" name="O(2:0)" orien="R0" />
        <branch name="I(7)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3008" y="2480" type="branch" />
            <wire x2="3152" y1="2480" y2="2480" x1="3008" />
        </branch>
        <branch name="I(6)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3008" y="2544" type="branch" />
            <wire x2="3152" y1="2544" y2="2544" x1="3008" />
        </branch>
        <branch name="I(5)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3008" y="2608" type="branch" />
            <wire x2="3152" y1="2608" y2="2608" x1="3008" />
        </branch>
        <branch name="I(4)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3008" y="2672" type="branch" />
            <wire x2="3152" y1="2672" y2="2672" x1="3008" />
        </branch>
        <instance x="3152" y="2736" name="XLXI_125" orien="R0" />
        <branch name="A2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3504" y="2576" type="branch" />
            <wire x2="3504" y1="2576" y2="2576" x1="3408" />
        </branch>
        <branch name="I(7)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1808" y="2480" type="branch" />
            <wire x2="1952" y1="2480" y2="2480" x1="1808" />
        </branch>
        <branch name="I(5)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1808" y="2544" type="branch" />
            <wire x2="1952" y1="2544" y2="2544" x1="1808" />
        </branch>
        <branch name="I(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1808" y="2608" type="branch" />
            <wire x2="1952" y1="2608" y2="2608" x1="1808" />
        </branch>
        <branch name="I(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1808" y="2672" type="branch" />
            <wire x2="1952" y1="2672" y2="2672" x1="1808" />
        </branch>
        <branch name="I(7)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2432" y="2480" type="branch" />
            <wire x2="2576" y1="2480" y2="2480" x1="2432" />
        </branch>
        <branch name="I(6)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2432" y="2544" type="branch" />
            <wire x2="2576" y1="2544" y2="2544" x1="2432" />
        </branch>
        <branch name="I(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2432" y="2608" type="branch" />
            <wire x2="2576" y1="2608" y2="2608" x1="2432" />
        </branch>
        <branch name="I(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2432" y="2672" type="branch" />
            <wire x2="2576" y1="2672" y2="2672" x1="2432" />
        </branch>
        <instance x="1952" y="2736" name="XLXI_124" orien="R0" />
        <instance x="2576" y="2736" name="XLXI_126" orien="R0" />
        <branch name="A0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="2576" type="branch" />
            <wire x2="2272" y1="2576" y2="2576" x1="2208" />
        </branch>
        <branch name="A1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2912" y="2576" type="branch" />
            <wire x2="2912" y1="2576" y2="2576" x1="2832" />
        </branch>
        <branch name="I(7:0)">
            <wire x2="3408" y1="2352" y2="2352" x1="3312" />
        </branch>
        <iomarker fontsize="28" x="3312" y="2352" name="I(7:0)" orien="R180" />
        <branch name="XLXN_407">
            <wire x2="1552" y1="1248" y2="1248" x1="1520" />
            <wire x2="1552" y1="1248" y2="1472" x1="1552" />
            <wire x2="1568" y1="1472" y2="1472" x1="1552" />
        </branch>
        <branch name="XLXN_392">
            <wire x2="1568" y1="1536" y2="1536" x1="1456" />
        </branch>
        <branch name="XLXN_411">
            <wire x2="1104" y1="1104" y2="1104" x1="1040" />
            <wire x2="1104" y1="1104" y2="1440" x1="1104" />
            <wire x2="1200" y1="1440" y2="1440" x1="1104" />
        </branch>
        <branch name="XLXN_413">
            <wire x2="1200" y1="1568" y2="1568" x1="1056" />
        </branch>
        <instance x="1200" y="1696" name="XLXI_164" orien="R0" />
        <branch name="XLXN_412">
            <wire x2="1056" y1="1328" y2="1328" x1="1040" />
            <wire x2="1056" y1="1328" y2="1504" x1="1056" />
            <wire x2="1200" y1="1504" y2="1504" x1="1056" />
        </branch>
        <branch name="XLXN_418">
            <wire x2="1200" y1="1792" y2="1792" x1="1056" />
            <wire x2="1200" y1="1632" y2="1792" x1="1200" />
        </branch>
        <branch name="XLXN_419">
            <wire x2="1184" y1="208" y2="208" x1="1008" />
            <wire x2="1184" y1="208" y2="368" x1="1184" />
        </branch>
        <branch name="XLXN_420">
            <wire x2="1184" y1="432" y2="432" x1="1008" />
        </branch>
        <instance x="1184" y="624" name="XLXI_163" orien="R0" />
        <branch name="XLXN_421">
            <wire x2="1040" y1="672" y2="672" x1="1024" />
            <wire x2="1184" y1="496" y2="496" x1="1040" />
            <wire x2="1040" y1="496" y2="672" x1="1040" />
        </branch>
        <branch name="XLXN_436">
            <wire x2="1104" y1="896" y2="896" x1="1024" />
            <wire x2="1104" y1="560" y2="896" x1="1104" />
            <wire x2="1184" y1="560" y2="560" x1="1104" />
        </branch>
        <instance x="1264" y="1408" name="XLXI_165" orien="R0" />
        <branch name="XLXN_386">
            <wire x2="1536" y1="464" y2="464" x1="1440" />
        </branch>
        <branch name="XLXN_393">
            <wire x2="1856" y1="496" y2="496" x1="1792" />
            <wire x2="1856" y1="496" y2="928" x1="1856" />
            <wire x2="1872" y1="928" y2="928" x1="1856" />
        </branch>
        <branch name="XLXN_394">
            <wire x2="1840" y1="1504" y2="1504" x1="1824" />
            <wire x2="1872" y1="992" y2="992" x1="1840" />
            <wire x2="1840" y1="992" y2="1504" x1="1840" />
        </branch>
        <branch name="XLXN_196">
            <wire x2="2224" y1="960" y2="960" x1="2128" />
            <wire x2="2224" y1="960" y2="1232" x1="2224" />
            <wire x2="2320" y1="1232" y2="1232" x1="2224" />
            <wire x2="2320" y1="1232" y2="1328" x1="2320" />
            <wire x2="2512" y1="1328" y2="1328" x1="2320" />
            <wire x2="2320" y1="1328" y2="1712" x1="2320" />
            <wire x2="2528" y1="1712" y2="1712" x1="2320" />
            <wire x2="2320" y1="912" y2="1232" x1="2320" />
            <wire x2="2512" y1="912" y2="912" x1="2320" />
        </branch>
        <instance x="1264" y="1072" name="XLXI_166" orien="R0" />
        <branch name="XLXN_410">
            <wire x2="1536" y1="528" y2="528" x1="1520" />
            <wire x2="1520" y1="528" y2="912" x1="1520" />
        </branch>
        <branch name="I(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1344" type="branch" />
            <wire x2="1264" y1="1344" y2="1344" x1="1248" />
        </branch>
        <branch name="I(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1280" type="branch" />
            <wire x2="1264" y1="1280" y2="1280" x1="1248" />
        </branch>
        <branch name="I(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1216" type="branch" />
            <wire x2="1264" y1="1216" y2="1216" x1="1248" />
        </branch>
        <branch name="I(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1152" type="branch" />
            <wire x2="1264" y1="1152" y2="1152" x1="1248" />
        </branch>
        <branch name="I(4)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1008" type="branch" />
            <wire x2="1264" y1="1008" y2="1008" x1="1248" />
        </branch>
        <branch name="I(5)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="944" type="branch" />
            <wire x2="1264" y1="944" y2="944" x1="1248" />
        </branch>
        <branch name="I(6)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="880" type="branch" />
            <wire x2="1264" y1="880" y2="880" x1="1248" />
        </branch>
        <branch name="I(7)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="816" type="branch" />
            <wire x2="1264" y1="816" y2="816" x1="1248" />
        </branch>
        <instance x="1536" y="592" name="XLXI_169" orien="R0" />
        <instance x="1568" y="1600" name="XLXI_172" orien="R0" />
        <instance x="1872" y="1056" name="XLXI_167" orien="R0" />
    </sheet>
</drawing>