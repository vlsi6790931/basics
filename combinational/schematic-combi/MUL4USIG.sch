<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_57" />
        <signal name="XLXN_58" />
        <signal name="XLXN_59" />
        <signal name="XLXN_60" />
        <signal name="XLXN_61" />
        <signal name="XLXN_1610" />
        <signal name="XLXN_1611" />
        <signal name="XLXN_1612" />
        <signal name="XLXN_1613" />
        <signal name="XLXN_1614" />
        <signal name="XLXN_1683" />
        <signal name="XLXN_1684" />
        <signal name="XLXN_1685" />
        <signal name="XLXN_1686" />
        <signal name="XLXN_1687" />
        <signal name="Y(3)" />
        <signal name="X(2)" />
        <signal name="X(1)" />
        <signal name="XLXN_416" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="X(3)" />
        <signal name="XLXN_180" />
        <signal name="XLXN_184" />
        <signal name="XLXN_187" />
        <signal name="XLXN_186" />
        <signal name="XLXN_209" />
        <signal name="X(0)" />
        <signal name="Y(2)" />
        <signal name="XLXN_190" />
        <signal name="Z(1)" />
        <signal name="XLXN_212" />
        <signal name="Z(0)" />
        <signal name="XLXN_175" />
        <signal name="XLXN_173" />
        <signal name="XLXN_166" />
        <signal name="XLXN_161" />
        <signal name="XLXN_118" />
        <signal name="XLXN_113" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="Y(1)" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="Y(0)" />
        <signal name="XLXN_680" />
        <signal name="XLXN_36" />
        <signal name="Z(2)" />
        <signal name="XLXN_497" />
        <signal name="XLXN_498" />
        <signal name="XLXN_691" />
        <signal name="XLXN_400" />
        <signal name="XLXN_395" />
        <signal name="XLXN_397" />
        <signal name="XLXN_1525" />
        <signal name="XLXN_1526" />
        <signal name="XLXN_1527" />
        <signal name="XLXN_1497" />
        <signal name="Z(7)" />
        <signal name="Z(4)" />
        <signal name="Z(5)" />
        <signal name="Z(6)" />
        <signal name="Z(3)" />
        <signal name="XLXN_1553" />
        <signal name="XLXN_423" />
        <signal name="XLXN_55" />
        <signal name="XLXN_1550" />
        <signal name="XLXN_1756" />
        <signal name="XLXN_1757" />
        <signal name="XLXN_1758" />
        <signal name="XLXN_1759" />
        <signal name="XLXN_1760" />
        <signal name="RESET" />
        <signal name="XLXN_188" />
        <signal name="XLXN_210" />
        <signal name="XLXN_189" />
        <signal name="XLXN_45" />
        <signal name="XLXN_1552" />
        <signal name="XLXN_44" />
        <signal name="XLXN_69" />
        <signal name="XLXN_70" />
        <signal name="Y(3:0)" />
        <signal name="X(3:0)" />
        <signal name="Z(7:0)" />
        <port polarity="Input" name="RESET" />
        <port polarity="Input" name="Y(3:0)" />
        <port polarity="Input" name="X(3:0)" />
        <port polarity="Output" name="Z(7:0)" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="FA">
            <timestamp>2023-8-11T19:21:31</timestamp>
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
            <line x2="192" y1="-64" y2="0" x1="192" />
        </blockdef>
        <blockdef name="HA">
            <timestamp>2023-8-10T21:23:14</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="176" x="64" y="-156" height="156" />
            <line x2="304" y1="-128" y2="-128" x1="240" />
            <line x2="304" y1="-32" y2="-32" x1="240" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
        </blockdef>
        <block symbolname="and2" name="XLXI_184">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_1527" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_183">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_1525" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_197">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_416" name="I1" />
            <blockpin signalname="Z(2)" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_95">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin signalname="XLXN_680" name="Bin" />
            <blockpin signalname="XLXN_180" name="Cin" />
            <blockpin signalname="XLXN_691" name="Cout" />
            <blockpin signalname="XLXN_498" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_101">
            <blockpin signalname="XLXN_212" name="B" />
            <blockpin signalname="XLXN_190" name="A" />
            <blockpin signalname="XLXN_189" name="C" />
            <blockpin signalname="XLXN_416" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_104">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_184" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_99">
            <blockpin signalname="XLXN_186" name="Ain" />
            <blockpin signalname="XLXN_209" name="Bin" />
            <blockpin signalname="XLXN_187" name="Cin" />
            <blockpin signalname="XLXN_180" name="Cout" />
            <blockpin signalname="XLXN_497" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_105">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="XLXN_186" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_108">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_190" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_93">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_173" name="I1" />
            <blockpin signalname="Z(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_94">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_175" name="I1" />
            <blockpin signalname="Z(1)" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_12">
            <blockpin signalname="XLXN_166" name="B" />
            <blockpin signalname="XLXN_118" name="A" />
            <blockpin signalname="XLXN_113" name="C" />
            <blockpin signalname="XLXN_175" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_53">
            <blockpin signalname="XLXN_25" name="Ain" />
            <blockpin signalname="XLXN_161" name="Bin" />
            <blockpin signalname="XLXN_113" name="Cin" />
            <blockpin signalname="XLXN_44" name="Cout" />
            <blockpin signalname="XLXN_212" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_56">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_65">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="XLXN_29" name="I1" />
            <blockpin signalname="XLXN_118" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_92">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_173" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="XLXN_32" name="I1" />
            <blockpin signalname="XLXN_166" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="XLXN_31" name="I1" />
            <blockpin signalname="XLXN_161" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_186">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="XLXN_36" name="I1" />
            <blockpin signalname="XLXN_1526" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_191">
            <blockpin signalname="XLXN_1526" name="Ain" />
            <blockpin signalname="XLXN_691" name="Bin" />
            <blockpin signalname="XLXN_400" name="Cin" />
            <blockpin signalname="Z(7)" name="Cout" />
            <blockpin signalname="Z(6)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_185">
            <blockpin signalname="XLXN_1527" name="Ain" />
            <blockpin signalname="XLXN_498" name="Bin" />
            <blockpin signalname="XLXN_395" name="Cin" />
            <blockpin signalname="XLXN_400" name="Cout" />
            <blockpin signalname="Z(5)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_187">
            <blockpin signalname="XLXN_1525" name="Ain" />
            <blockpin signalname="XLXN_497" name="Bin" />
            <blockpin signalname="XLXN_397" name="Cin" />
            <blockpin signalname="XLXN_395" name="Cout" />
            <blockpin signalname="Z(4)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_681">
            <blockpin signalname="XLXN_26" name="B" />
            <blockpin signalname="XLXN_1553" name="A" />
            <blockpin signalname="XLXN_680" name="C" />
            <blockpin signalname="XLXN_209" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_20">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="XLXN_1553" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_682">
            <blockpin signalname="XLXN_423" name="B" />
            <blockpin signalname="XLXN_1497" name="A" />
            <blockpin signalname="XLXN_397" name="C" />
            <blockpin signalname="XLXN_1550" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_182">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="XLXN_55" name="I1" />
            <blockpin signalname="XLXN_1497" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_683">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_1550" name="I1" />
            <blockpin signalname="Z(3)" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_100">
            <blockpin signalname="XLXN_188" name="Ain" />
            <blockpin signalname="XLXN_210" name="Bin" />
            <blockpin signalname="XLXN_189" name="Cin" />
            <blockpin signalname="XLXN_187" name="Cout" />
            <blockpin signalname="XLXN_423" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_106">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="XLXN_188" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_51">
            <blockpin signalname="XLXN_45" name="Ain" />
            <blockpin signalname="XLXN_1552" name="Bin" />
            <blockpin signalname="XLXN_44" name="Cin" />
            <blockpin signalname="XLXN_26" name="Cout" />
            <blockpin signalname="XLXN_210" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="XLXN_70" name="I1" />
            <blockpin signalname="XLXN_1552" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_55">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="XLXN_69" name="I1" />
            <blockpin signalname="XLXN_45" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="3664" y="3840" name="XLXI_184" orien="R90" />
        <instance x="4016" y="3840" name="XLXI_183" orien="R90" />
        <instance x="4752" y="3392" name="XLXI_197" orien="R90" />
        <instance x="3984" y="2928" name="XLXI_95" orien="M90">
        </instance>
        <instance x="4912" y="2944" name="XLXI_101" orien="M90">
        </instance>
        <instance x="3664" y="2608" name="XLXI_104" orien="R90" />
        <instance x="4336" y="2928" name="XLXI_99" orien="M90">
        </instance>
        <instance x="4016" y="2608" name="XLXI_105" orien="R90" />
        <instance x="4688" y="2608" name="XLXI_108" orien="R90" />
        <instance x="5632" y="2352" name="XLXI_93" orien="R90" />
        <instance x="5216" y="2352" name="XLXI_94" orien="R90" />
        <instance x="5376" y="1904" name="XLXI_12" orien="M90">
        </instance>
        <instance x="5040" y="1904" name="XLXI_53" orien="M90">
        </instance>
        <branch name="Y(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4672" y="3760" type="branch" />
            <wire x2="3728" y1="3760" y2="3760" x1="3408" />
            <wire x2="4080" y1="3760" y2="3760" x1="3728" />
            <wire x2="4080" y1="3760" y2="3840" x1="4080" />
            <wire x2="4416" y1="3760" y2="3760" x1="4080" />
            <wire x2="4416" y1="3760" y2="3840" x1="4416" />
            <wire x2="4672" y1="3760" y2="3760" x1="4416" />
            <wire x2="3728" y1="3760" y2="3840" x1="3728" />
            <wire x2="3408" y1="3760" y2="3824" x1="3408" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3792" y="3824" type="branch" />
            <wire x2="3792" y1="3824" y2="3840" x1="3792" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4144" y="3824" type="branch" />
            <wire x2="4144" y1="3824" y2="3840" x1="4144" />
        </branch>
        <branch name="XLXN_416">
            <wire x2="4880" y1="3248" y2="3392" x1="4880" />
        </branch>
        <branch name="XLXN_5">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4144" y="2592" type="branch" />
            <wire x2="4144" y1="2592" y2="2608" x1="4144" />
        </branch>
        <branch name="XLXN_6">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4512" y="2592" type="branch" />
            <wire x2="4512" y1="2592" y2="2608" x1="4512" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3792" y="2592" type="branch" />
            <wire x2="3792" y1="2592" y2="2608" x1="3792" />
        </branch>
        <branch name="XLXN_180">
            <wire x2="4048" y1="2912" y2="2912" x1="3888" />
            <wire x2="4048" y1="2912" y2="3264" x1="4048" />
            <wire x2="4112" y1="3264" y2="3264" x1="4048" />
            <wire x2="3888" y1="2912" y2="2928" x1="3888" />
            <wire x2="4112" y1="3248" y2="3264" x1="4112" />
        </branch>
        <branch name="XLXN_184">
            <wire x2="3760" y1="2864" y2="2928" x1="3760" />
        </branch>
        <branch name="XLXN_187">
            <wire x2="4240" y1="2912" y2="2928" x1="4240" />
            <wire x2="4400" y1="2912" y2="2912" x1="4240" />
            <wire x2="4400" y1="2912" y2="3344" x1="4400" />
            <wire x2="4480" y1="3344" y2="3344" x1="4400" />
            <wire x2="4480" y1="3264" y2="3344" x1="4480" />
        </branch>
        <branch name="XLXN_186">
            <wire x2="4112" y1="2864" y2="2928" x1="4112" />
        </branch>
        <branch name="XLXN_209">
            <wire x2="4176" y1="2224" y2="2928" x1="4176" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4816" y="2592" type="branch" />
            <wire x2="4816" y1="2592" y2="2608" x1="4816" />
        </branch>
        <branch name="Y(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5024" y="2528" type="branch" />
            <wire x2="4080" y1="2528" y2="2528" x1="3728" />
            <wire x2="4080" y1="2528" y2="2608" x1="4080" />
            <wire x2="4448" y1="2528" y2="2528" x1="4080" />
            <wire x2="4752" y1="2528" y2="2528" x1="4448" />
            <wire x2="4752" y1="2528" y2="2608" x1="4752" />
            <wire x2="5024" y1="2528" y2="2528" x1="4752" />
            <wire x2="4448" y1="2528" y2="2608" x1="4448" />
            <wire x2="3728" y1="2528" y2="2608" x1="3728" />
        </branch>
        <branch name="XLXN_190">
            <wire x2="4784" y1="2864" y2="2944" x1="4784" />
        </branch>
        <branch name="Z(1)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5312" y="2704" type="branch" />
            <wire x2="5312" y1="2608" y2="2704" x1="5312" />
        </branch>
        <branch name="XLXN_212">
            <wire x2="4880" y1="2224" y2="2944" x1="4880" />
        </branch>
        <branch name="Z(0)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5728" y="2624" type="branch" />
            <wire x2="5728" y1="2608" y2="2624" x1="5728" />
        </branch>
        <branch name="XLXN_175">
            <wire x2="5344" y1="2208" y2="2352" x1="5344" />
        </branch>
        <branch name="XLXN_173">
            <wire x2="5760" y1="1296" y2="2352" x1="5760" />
        </branch>
        <branch name="XLXN_166">
            <wire x2="5344" y1="1296" y2="1904" x1="5344" />
        </branch>
        <branch name="XLXN_161">
            <wire x2="4880" y1="1296" y2="1904" x1="4880" />
        </branch>
        <branch name="XLXN_118">
            <wire x2="5248" y1="1744" y2="1904" x1="5248" />
        </branch>
        <branch name="XLXN_113">
            <wire x2="4944" y1="1888" y2="1904" x1="4944" />
            <wire x2="5168" y1="1888" y2="1888" x1="4944" />
            <wire x2="5168" y1="1888" y2="2240" x1="5168" />
            <wire x2="5248" y1="2240" y2="2240" x1="5168" />
            <wire x2="5248" y1="2208" y2="2240" x1="5248" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="4816" y1="1744" y2="1904" x1="4816" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="4176" y1="1904" y2="1920" x1="4176" />
            <wire x2="4272" y1="1904" y2="1904" x1="4176" />
            <wire x2="4272" y1="1904" y2="2304" x1="4272" />
            <wire x2="4480" y1="2304" y2="2304" x1="4272" />
            <wire x2="4480" y1="2224" y2="2304" x1="4480" />
        </branch>
        <instance x="4720" y="1488" name="XLXI_56" orien="R90" />
        <instance x="5152" y="1488" name="XLXI_65" orien="R90" />
        <branch name="XLXN_27">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4112" y="1472" type="branch" />
            <wire x2="4112" y1="1472" y2="1488" x1="4112" />
        </branch>
        <branch name="Y(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5488" y="1408" type="branch" />
            <wire x2="4048" y1="1408" y2="1488" x1="4048" />
            <wire x2="4448" y1="1408" y2="1408" x1="4048" />
            <wire x2="4784" y1="1408" y2="1408" x1="4448" />
            <wire x2="4784" y1="1408" y2="1488" x1="4784" />
            <wire x2="5216" y1="1408" y2="1408" x1="4784" />
            <wire x2="5216" y1="1408" y2="1488" x1="5216" />
            <wire x2="5488" y1="1408" y2="1408" x1="5216" />
            <wire x2="4448" y1="1408" y2="1488" x1="4448" />
        </branch>
        <branch name="XLXN_29">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5280" y="1472" type="branch" />
            <wire x2="5280" y1="1472" y2="1488" x1="5280" />
        </branch>
        <branch name="XLXN_30">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4848" y="1472" type="branch" />
            <wire x2="4848" y1="1472" y2="1488" x1="4848" />
        </branch>
        <instance x="5664" y="1040" name="XLXI_92" orien="R90" />
        <instance x="5248" y="1040" name="XLXI_7" orien="R90" />
        <instance x="4784" y="1040" name="XLXI_6" orien="R90" />
        <branch name="XLXN_31">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4912" y="1024" type="branch" />
            <wire x2="4912" y1="1024" y2="1040" x1="4912" />
        </branch>
        <branch name="XLXN_32">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5376" y="1024" type="branch" />
            <wire x2="5376" y1="1024" y2="1040" x1="5376" />
        </branch>
        <branch name="XLXN_33">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5792" y="1024" type="branch" />
            <wire x2="5792" y1="1024" y2="1040" x1="5792" />
        </branch>
        <branch name="Y(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5808" y="928" type="branch" />
            <wire x2="4848" y1="928" y2="928" x1="4512" />
            <wire x2="5312" y1="928" y2="928" x1="4848" />
            <wire x2="5728" y1="928" y2="928" x1="5312" />
            <wire x2="5808" y1="928" y2="928" x1="5728" />
            <wire x2="5728" y1="928" y2="1040" x1="5728" />
            <wire x2="5312" y1="928" y2="1040" x1="5312" />
            <wire x2="4848" y1="928" y2="1040" x1="4848" />
            <wire x2="4512" y1="928" y2="1024" x1="4512" />
        </branch>
        <branch name="XLXN_680">
            <wire x2="3824" y1="2304" y2="2928" x1="3824" />
            <wire x2="4080" y1="2304" y2="2304" x1="3824" />
            <wire x2="4080" y1="2224" y2="2304" x1="4080" />
        </branch>
        <branch name="XLXN_36">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="3472" y="3808" type="branch" />
            <wire x2="3472" y1="3808" y2="3824" x1="3472" />
        </branch>
        <instance x="3344" y="3824" name="XLXI_186" orien="R90" />
        <branch name="Z(2)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4848" y="3680" type="branch" />
            <wire x2="4848" y1="3648" y2="3680" x1="4848" />
        </branch>
        <branch name="XLXN_497">
            <wire x2="4176" y1="3248" y2="4384" x1="4176" />
        </branch>
        <branch name="XLXN_498">
            <wire x2="3824" y1="3248" y2="4384" x1="3824" />
        </branch>
        <branch name="XLXN_691">
            <wire x2="3504" y1="3296" y2="4384" x1="3504" />
            <wire x2="3760" y1="3296" y2="3296" x1="3504" />
            <wire x2="3760" y1="3248" y2="3296" x1="3760" />
        </branch>
        <instance x="3664" y="4384" name="XLXI_191" orien="M90">
        </instance>
        <instance x="3984" y="4384" name="XLXI_185" orien="M90">
        </instance>
        <instance x="4336" y="4384" name="XLXI_187" orien="M90">
        </instance>
        <branch name="XLXN_400">
            <wire x2="3568" y1="4368" y2="4384" x1="3568" />
            <wire x2="3696" y1="4368" y2="4368" x1="3568" />
            <wire x2="3696" y1="4368" y2="4720" x1="3696" />
            <wire x2="3760" y1="4720" y2="4720" x1="3696" />
            <wire x2="3760" y1="4704" y2="4720" x1="3760" />
        </branch>
        <branch name="XLXN_395">
            <wire x2="3888" y1="4368" y2="4384" x1="3888" />
            <wire x2="4048" y1="4368" y2="4368" x1="3888" />
            <wire x2="4048" y1="4368" y2="4720" x1="4048" />
            <wire x2="4112" y1="4720" y2="4720" x1="4048" />
            <wire x2="4112" y1="4704" y2="4720" x1="4112" />
        </branch>
        <branch name="XLXN_397">
            <wire x2="4240" y1="4368" y2="4384" x1="4240" />
            <wire x2="4400" y1="4368" y2="4368" x1="4240" />
            <wire x2="4400" y1="4368" y2="4784" x1="4400" />
            <wire x2="4448" y1="4784" y2="4784" x1="4400" />
            <wire x2="4448" y1="4688" y2="4784" x1="4448" />
        </branch>
        <branch name="XLXN_1525">
            <wire x2="4112" y1="4096" y2="4384" x1="4112" />
        </branch>
        <branch name="XLXN_1526">
            <wire x2="3440" y1="4080" y2="4384" x1="3440" />
        </branch>
        <branch name="XLXN_1527">
            <wire x2="3760" y1="4096" y2="4384" x1="3760" />
        </branch>
        <branch name="XLXN_1497">
            <wire x2="4448" y1="4096" y2="4384" x1="4448" />
        </branch>
        <branch name="Z(7)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3184" y="5120" type="branch" />
            <wire x2="3376" y1="4368" y2="4368" x1="3184" />
            <wire x2="3376" y1="4368" y2="4720" x1="3376" />
            <wire x2="3440" y1="4720" y2="4720" x1="3376" />
            <wire x2="3184" y1="4368" y2="5120" x1="3184" />
            <wire x2="3440" y1="4704" y2="4720" x1="3440" />
        </branch>
        <branch name="Z(4)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4176" y="5136" type="branch" />
            <wire x2="4176" y1="4704" y2="5136" x1="4176" />
        </branch>
        <branch name="Z(5)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3824" y="5120" type="branch" />
            <wire x2="3824" y1="4704" y2="5120" x1="3824" />
        </branch>
        <branch name="Z(6)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3504" y="5120" type="branch" />
            <wire x2="3504" y1="4704" y2="5120" x1="3504" />
        </branch>
        <instance x="4208" y="1920" name="XLXI_681" orien="M90">
        </instance>
        <branch name="Z(3)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4512" y="5152" type="branch" />
            <wire x2="4512" y1="5136" y2="5152" x1="4512" />
        </branch>
        <branch name="XLXN_1553">
            <wire x2="4080" y1="1744" y2="1920" x1="4080" />
        </branch>
        <instance x="3984" y="1488" name="XLXI_20" orien="R90" />
        <instance x="4576" y="4384" name="XLXI_682" orien="M90">
        </instance>
        <branch name="XLXN_423">
            <wire x2="4544" y1="3264" y2="4384" x1="4544" />
        </branch>
        <instance x="4352" y="3840" name="XLXI_182" orien="R90" />
        <branch name="XLXN_55">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4480" y="3824" type="branch" />
            <wire x2="4480" y1="3824" y2="3840" x1="4480" />
        </branch>
        <branch name="XLXN_1550">
            <wire x2="4544" y1="4688" y2="4880" x1="4544" />
        </branch>
        <branch name="RESET">
            <wire x2="1728" y1="2336" y2="2336" x1="1616" />
            <wire x2="1728" y1="2336" y2="3376" x1="1728" />
            <wire x2="1728" y1="3376" y2="4848" x1="1728" />
            <wire x2="3664" y1="4848" y2="4848" x1="1728" />
            <wire x2="4000" y1="4848" y2="4848" x1="3664" />
            <wire x2="4352" y1="4848" y2="4848" x1="4000" />
            <wire x2="4480" y1="4848" y2="4848" x1="4352" />
            <wire x2="4480" y1="4848" y2="4880" x1="4480" />
            <wire x2="4000" y1="3376" y2="3376" x1="1728" />
            <wire x2="4352" y1="3376" y2="3376" x1="4000" />
            <wire x2="4704" y1="3376" y2="3376" x1="4352" />
            <wire x2="4816" y1="3376" y2="3376" x1="4704" />
            <wire x2="4816" y1="3376" y2="3392" x1="4816" />
            <wire x2="4704" y1="2336" y2="2336" x1="1728" />
            <wire x2="5104" y1="2336" y2="2336" x1="4704" />
            <wire x2="5280" y1="2336" y2="2336" x1="5104" />
            <wire x2="5280" y1="2336" y2="2352" x1="5280" />
            <wire x2="5696" y1="2336" y2="2336" x1="5280" />
            <wire x2="5696" y1="2336" y2="2352" x1="5696" />
            <wire x2="3664" y1="4576" y2="4848" x1="3664" />
            <wire x2="4000" y1="3120" y2="3120" x1="3984" />
            <wire x2="4000" y1="3120" y2="3376" x1="4000" />
            <wire x2="4000" y1="4576" y2="4576" x1="3984" />
            <wire x2="4000" y1="4576" y2="4848" x1="4000" />
            <wire x2="4352" y1="3120" y2="3120" x1="4336" />
            <wire x2="4352" y1="3120" y2="3376" x1="4352" />
            <wire x2="4352" y1="4576" y2="4576" x1="4336" />
            <wire x2="4352" y1="4576" y2="4848" x1="4352" />
            <wire x2="4704" y1="2096" y2="2336" x1="4704" />
            <wire x2="4704" y1="3136" y2="3376" x1="4704" />
            <wire x2="5104" y1="2096" y2="2096" x1="5040" />
            <wire x2="5104" y1="2096" y2="2336" x1="5104" />
        </branch>
        <instance x="4416" y="4880" name="XLXI_683" orien="R90" />
        <instance x="4704" y="2944" name="XLXI_100" orien="M90">
        </instance>
        <branch name="XLXN_188">
            <wire x2="4480" y1="2864" y2="2944" x1="4480" />
        </branch>
        <branch name="XLXN_210">
            <wire x2="4544" y1="2224" y2="2944" x1="4544" />
        </branch>
        <branch name="XLXN_189">
            <wire x2="4608" y1="2880" y2="2944" x1="4608" />
            <wire x2="4736" y1="2880" y2="2880" x1="4608" />
            <wire x2="4736" y1="2880" y2="3264" x1="4736" />
            <wire x2="4784" y1="3264" y2="3264" x1="4736" />
            <wire x2="4784" y1="3248" y2="3264" x1="4784" />
        </branch>
        <instance x="4384" y="2608" name="XLXI_106" orien="R90" />
        <instance x="4704" y="1904" name="XLXI_51" orien="M90">
        </instance>
        <branch name="XLXN_45">
            <wire x2="4480" y1="1744" y2="1904" x1="4480" />
        </branch>
        <branch name="XLXN_1552">
            <wire x2="4544" y1="1280" y2="1904" x1="4544" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="4752" y1="1904" y2="1904" x1="4608" />
            <wire x2="4752" y1="1904" y2="2304" x1="4752" />
            <wire x2="4816" y1="2304" y2="2304" x1="4752" />
            <wire x2="4816" y1="2224" y2="2304" x1="4816" />
        </branch>
        <instance x="4448" y="1024" name="XLXI_5" orien="R90" />
        <instance x="4384" y="1488" name="XLXI_55" orien="R90" />
        <branch name="XLXN_69">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4512" y="1472" type="branch" />
            <wire x2="4512" y1="1472" y2="1488" x1="4512" />
        </branch>
        <branch name="XLXN_70">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4576" y="1008" type="branch" />
            <wire x2="4576" y1="1008" y2="1024" x1="4576" />
        </branch>
        <branch name="Y(3:0)">
            <wire x2="2400" y1="1200" y2="1200" x1="1872" />
        </branch>
        <branch name="X(3:0)">
            <wire x2="2400" y1="1056" y2="1056" x1="1872" />
        </branch>
        <branch name="Z(7:0)">
            <wire x2="2288" y1="1408" y2="1408" x1="1760" />
        </branch>
        <iomarker fontsize="28" x="1616" y="2336" name="RESET" orien="R180" />
        <iomarker fontsize="28" x="1872" y="1200" name="Y(3:0)" orien="R180" />
        <iomarker fontsize="28" x="1872" y="1056" name="X(3:0)" orien="R180" />
        <iomarker fontsize="28" x="2288" y="1408" name="Z(7:0)" orien="R0" />
        <text style="fontsize:150;fontname:Arial" x="2428" y="360">UNSIGNED MULTIPLIER (4 BIT)</text>
    </sheet>
</drawing>