<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_28" />
        <signal name="XLXN_39" />
        <signal name="XLXN_41" />
        <signal name="reset" />
        <signal name="A(3:0)" />
        <signal name="B(3:0)" />
        <signal name="A(7:4)" />
        <signal name="B(7:4)" />
        <signal name="XLXN_21" />
        <signal name="O(3:0)" />
        <signal name="O(7:4)" />
        <signal name="XLXN_54" />
        <signal name="Bin" />
        <signal name="XLXN_56" />
        <signal name="Bout" />
        <signal name="O(7:0)" />
        <signal name="A(7:0)" />
        <signal name="B(7:0)" />
        <port polarity="Input" name="reset" />
        <port polarity="Input" name="Bin" />
        <port polarity="Output" name="Bout" />
        <port polarity="Output" name="O(7:0)" />
        <port polarity="Input" name="A(7:0)" />
        <port polarity="Input" name="B(7:0)" />
        <blockdef name="FBFS">
            <timestamp>2023-8-11T1:2:22</timestamp>
            <rect width="192" x="64" y="-288" height="192" />
            <line x2="0" y1="-256" y2="-256" x1="64" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="0" y1="-192" y2="-192" x1="64" />
            <rect width="64" x="0" y="-204" height="24" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="320" y1="-144" y2="-144" x1="256" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <rect width="64" x="256" y="-236" height="24" />
            <line x2="160" y1="-32" y2="-96" x1="160" />
        </blockdef>
        <block symbolname="FBFS" name="XLXI_1">
            <blockpin signalname="A(7:4)" name="A(3:0)" />
            <blockpin signalname="B(7:4)" name="B(3:0)" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="Bout" name="Bout" />
            <blockpin signalname="O(7:4)" name="O(3:0)" />
            <blockpin signalname="XLXN_21" name="Bin" />
        </block>
        <block symbolname="FBFS" name="XLXI_2">
            <blockpin signalname="A(3:0)" name="A(3:0)" />
            <blockpin signalname="B(3:0)" name="B(3:0)" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="XLXN_21" name="Bout" />
            <blockpin signalname="O(3:0)" name="O(3:0)" />
            <blockpin signalname="Bin" name="Bin" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="848" y="848" name="XLXI_1" orien="R0">
        </instance>
        <instance x="864" y="1168" name="XLXI_2" orien="R0">
        </instance>
        <branch name="reset">
            <wire x2="800" y1="816" y2="816" x1="720" />
            <wire x2="800" y1="816" y2="1040" x1="800" />
            <wire x2="864" y1="1040" y2="1040" x1="800" />
            <wire x2="800" y1="720" y2="816" x1="800" />
            <wire x2="848" y1="720" y2="720" x1="800" />
        </branch>
        <branch name="A(3:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="752" y="912" type="branch" />
            <wire x2="864" y1="912" y2="912" x1="752" />
        </branch>
        <branch name="B(3:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="752" y="976" type="branch" />
            <wire x2="864" y1="976" y2="976" x1="752" />
        </branch>
        <branch name="A(7:4)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="816" y="592" type="branch" />
            <wire x2="848" y1="592" y2="592" x1="816" />
        </branch>
        <branch name="B(7:4)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="816" y="656" type="branch" />
            <wire x2="848" y1="656" y2="656" x1="816" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="1008" y1="816" y2="832" x1="1008" />
            <wire x2="1216" y1="832" y2="832" x1="1008" />
            <wire x2="1216" y1="832" y2="1024" x1="1216" />
            <wire x2="1216" y1="1024" y2="1024" x1="1184" />
        </branch>
        <branch name="O(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1296" y="944" type="branch" />
            <wire x2="1296" y1="944" y2="944" x1="1184" />
        </branch>
        <branch name="O(7:4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1296" y="624" type="branch" />
            <wire x2="1296" y1="624" y2="624" x1="1168" />
        </branch>
        <branch name="Bin">
            <wire x2="1024" y1="1184" y2="1184" x1="688" />
            <wire x2="1024" y1="1136" y2="1152" x1="1024" />
            <wire x2="1024" y1="1152" y2="1184" x1="1024" />
        </branch>
        <branch name="Bout">
            <wire x2="1184" y1="704" y2="704" x1="1168" />
            <wire x2="1296" y1="448" y2="448" x1="1184" />
            <wire x2="1184" y1="448" y2="704" x1="1184" />
        </branch>
        <branch name="O(7:0)">
            <wire x2="1232" y1="368" y2="368" x1="1120" />
        </branch>
        <branch name="A(7:0)">
            <wire x2="576" y1="368" y2="368" x1="400" />
        </branch>
        <branch name="B(7:0)">
            <wire x2="576" y1="448" y2="448" x1="400" />
        </branch>
        <iomarker fontsize="28" x="720" y="816" name="reset" orien="R180" />
        <iomarker fontsize="28" x="688" y="1184" name="Bin" orien="R180" />
        <iomarker fontsize="28" x="1232" y="368" name="O(7:0)" orien="R0" />
        <iomarker fontsize="28" x="1296" y="448" name="Bout" orien="R0" />
        <iomarker fontsize="28" x="400" y="368" name="A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="400" y="448" name="B(7:0)" orien="R180" />
        <text style="fontsize:100;fontname:Arial" x="144" y="132">EIGHT BIT FULL SUBTRACTOR</text>
    </sheet>
</drawing>