<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_12" />
        <signal name="XLXN_14" />
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_24" />
        <signal name="XLXN_13" />
        <signal name="S" />
        <signal name="R" />
        <signal name="O1" />
        <signal name="O0" />
        <signal name="I" />
        <port polarity="Input" name="S" />
        <port polarity="Input" name="R" />
        <port polarity="Output" name="O1" />
        <port polarity="Output" name="O0" />
        <port polarity="Input" name="I" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="S" name="I0" />
            <blockpin signalname="XLXN_13" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="XLXN_3" name="I0" />
            <blockpin signalname="XLXN_13" name="I1" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="S" name="I" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_1" name="I1" />
            <blockpin signalname="O1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_19">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="O0" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="I" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <branch name="XLXN_1">
            <wire x2="1264" y1="560" y2="560" x1="1232" />
        </branch>
        <instance x="976" y="656" name="XLXI_4" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="1264" y1="800" y2="800" x1="1232" />
        </branch>
        <instance x="976" y="896" name="XLXI_5" orien="R0" />
        <branch name="XLXN_3">
            <wire x2="976" y1="832" y2="832" x1="944" />
        </branch>
        <instance x="720" y="864" name="XLXI_6" orien="R0" />
        <instance x="1264" y="688" name="XLXI_2" orien="R0" />
        <instance x="1264" y="864" name="XLXI_19" orien="R0" />
        <branch name="XLXN_13">
            <wire x2="896" y1="688" y2="688" x1="512" />
            <wire x2="896" y1="688" y2="768" x1="896" />
            <wire x2="976" y1="768" y2="768" x1="896" />
            <wire x2="976" y1="528" y2="528" x1="896" />
            <wire x2="896" y1="528" y2="688" x1="896" />
        </branch>
        <branch name="S">
            <wire x2="656" y1="592" y2="832" x1="656" />
            <wire x2="720" y1="832" y2="832" x1="656" />
            <wire x2="656" y1="832" y2="1072" x1="656" />
            <wire x2="976" y1="592" y2="592" x1="656" />
        </branch>
        <instance x="256" y="784" name="XLXI_1" orien="R0" />
        <branch name="R">
            <wire x2="240" y1="448" y2="448" x1="192" />
            <wire x2="1248" y1="448" y2="448" x1="240" />
            <wire x2="1248" y1="448" y2="624" x1="1248" />
            <wire x2="1264" y1="624" y2="624" x1="1248" />
            <wire x2="1248" y1="624" y2="736" x1="1248" />
            <wire x2="1264" y1="736" y2="736" x1="1248" />
            <wire x2="240" y1="448" y2="656" x1="240" />
            <wire x2="256" y1="656" y2="656" x1="240" />
        </branch>
        <branch name="O1">
            <wire x2="1552" y1="592" y2="592" x1="1520" />
        </branch>
        <branch name="O0">
            <wire x2="1552" y1="768" y2="768" x1="1520" />
        </branch>
        <branch name="I">
            <wire x2="256" y1="720" y2="720" x1="224" />
        </branch>
        <iomarker fontsize="28" x="1552" y="592" name="O1" orien="R0" />
        <iomarker fontsize="28" x="1552" y="768" name="O0" orien="R0" />
        <iomarker fontsize="28" x="656" y="1072" name="S" orien="R90" />
        <iomarker fontsize="28" x="224" y="720" name="I" orien="R180" />
        <iomarker fontsize="28" x="192" y="448" name="R" orien="R180" />
    </sheet>
</drawing>