<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_4" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="reset" />
        <signal name="S(0)" />
        <signal name="O(0)" />
        <signal name="O(1)" />
        <signal name="O(2)" />
        <signal name="O(3)" />
        <signal name="I" />
        <signal name="S(1)" />
        <signal name="O(3:0)" />
        <signal name="S(1:0)" />
        <port polarity="Input" name="reset" />
        <port polarity="Input" name="I" />
        <port polarity="Output" name="O(3:0)" />
        <port polarity="Input" name="S(1:0)" />
        <blockdef name="DEMUX_1_2">
            <timestamp>2023-8-11T6:50:44</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="0" y1="-64" y2="-64" x1="64" />
        </blockdef>
        <block symbolname="DEMUX_1_2" name="XLXI_2">
            <blockpin signalname="O(2)" name="O0" />
            <blockpin signalname="O(3)" name="O1" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="XLXN_2" name="I" />
            <blockpin signalname="S(0)" name="S" />
        </block>
        <block symbolname="DEMUX_1_2" name="XLXI_1">
            <blockpin signalname="O(0)" name="O0" />
            <blockpin signalname="O(1)" name="O1" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="XLXN_1" name="I" />
            <blockpin signalname="S(0)" name="S" />
        </block>
        <block symbolname="DEMUX_1_2" name="XLXI_5">
            <blockpin signalname="XLXN_1" name="O0" />
            <blockpin signalname="XLXN_2" name="O1" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="I" name="I" />
            <blockpin signalname="S(1)" name="S" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="864" y="960" name="XLXI_2" orien="R0">
        </instance>
        <instance x="864" y="640" name="XLXI_1" orien="R0">
        </instance>
        <instance x="352" y="832" name="XLXI_5" orien="R0">
        </instance>
        <branch name="XLXN_1">
            <wire x2="800" y1="672" y2="672" x1="736" />
            <wire x2="800" y1="512" y2="672" x1="800" />
            <wire x2="864" y1="512" y2="512" x1="800" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="800" y1="736" y2="736" x1="736" />
            <wire x2="800" y1="736" y2="832" x1="800" />
            <wire x2="864" y1="832" y2="832" x1="800" />
        </branch>
        <branch name="reset">
            <wire x2="752" y1="1040" y2="1040" x1="304" />
            <wire x2="1264" y1="1040" y2="1040" x1="752" />
            <wire x2="752" y1="800" y2="800" x1="736" />
            <wire x2="752" y1="800" y2="1040" x1="752" />
            <wire x2="1264" y1="608" y2="608" x1="1248" />
            <wire x2="1264" y1="608" y2="928" x1="1264" />
            <wire x2="1264" y1="928" y2="1040" x1="1264" />
            <wire x2="1264" y1="928" y2="928" x1="1248" />
        </branch>
        <branch name="S(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="848" y="400" type="branch" />
            <wire x2="848" y1="400" y2="576" x1="848" />
            <wire x2="864" y1="576" y2="576" x1="848" />
            <wire x2="848" y1="576" y2="896" x1="848" />
            <wire x2="864" y1="896" y2="896" x1="848" />
        </branch>
        <branch name="O(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="480" type="branch" />
            <wire x2="1376" y1="480" y2="480" x1="1248" />
        </branch>
        <branch name="O(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="544" type="branch" />
            <wire x2="1376" y1="544" y2="544" x1="1248" />
        </branch>
        <branch name="O(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="800" type="branch" />
            <wire x2="1376" y1="800" y2="800" x1="1248" />
        </branch>
        <branch name="O(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="864" type="branch" />
            <wire x2="1376" y1="864" y2="864" x1="1248" />
        </branch>
        <branch name="I">
            <wire x2="352" y1="704" y2="704" x1="272" />
        </branch>
        <branch name="S(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="272" y="768" type="branch" />
            <wire x2="352" y1="768" y2="768" x1="272" />
        </branch>
        <branch name="O(3:0)">
            <wire x2="1520" y1="704" y2="704" x1="1424" />
        </branch>
        <branch name="S(1:0)">
            <wire x2="416" y1="432" y2="432" x1="256" />
        </branch>
        <iomarker fontsize="28" x="1520" y="704" name="O(3:0)" orien="R0" />
        <iomarker fontsize="28" x="272" y="704" name="I" orien="R180" />
        <iomarker fontsize="28" x="256" y="432" name="S(1:0)" orien="R180" />
        <iomarker fontsize="28" x="304" y="1040" name="reset" orien="R180" />
    </sheet>
</drawing>