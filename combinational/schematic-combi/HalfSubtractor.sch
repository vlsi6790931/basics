<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="A" />
        <signal name="B" />
        <signal name="R" />
        <signal name="Borrow" />
        <signal name="Diff" />
        <port polarity="Input" name="A" />
        <port polarity="Input" name="B" />
        <port polarity="Input" name="R" />
        <port polarity="Output" name="Borrow" />
        <port polarity="Output" name="Diff" />
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="A" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="XLXN_5" name="I" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_1">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="B" name="I1" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="Borrow" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="XLXN_8" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="Diff" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="384" y="720" name="XLXI_2" orien="R0" />
        <instance x="1008" y="960" name="XLXI_4" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="1008" y1="896" y2="896" x1="976" />
        </branch>
        <instance x="752" y="928" name="XLXI_5" orien="R0" />
        <instance x="832" y="752" name="XLXI_1" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="704" y1="624" y2="624" x1="640" />
            <wire x2="832" y1="624" y2="624" x1="704" />
            <wire x2="704" y1="624" y2="896" x1="704" />
            <wire x2="752" y1="896" y2="896" x1="704" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="816" y1="832" y2="832" x1="624" />
            <wire x2="1008" y1="832" y2="832" x1="816" />
            <wire x2="832" y1="688" y2="688" x1="816" />
            <wire x2="816" y1="688" y2="832" x1="816" />
        </branch>
        <instance x="368" y="928" name="XLXI_3" orien="R0" />
        <branch name="XLXN_7">
            <wire x2="1296" y1="864" y2="864" x1="1264" />
        </branch>
        <instance x="1296" y="992" name="XLXI_7" orien="R0" />
        <branch name="XLXN_8">
            <wire x2="1296" y1="656" y2="656" x1="1088" />
        </branch>
        <branch name="A">
            <wire x2="384" y1="656" y2="656" x1="208" />
        </branch>
        <branch name="B">
            <wire x2="368" y1="800" y2="800" x1="208" />
        </branch>
        <branch name="R">
            <wire x2="352" y1="1008" y2="1008" x1="208" />
            <wire x2="1264" y1="1008" y2="1008" x1="352" />
            <wire x2="1248" y1="496" y2="496" x1="352" />
            <wire x2="1248" y1="496" y2="592" x1="1248" />
            <wire x2="1296" y1="592" y2="592" x1="1248" />
            <wire x2="352" y1="496" y2="592" x1="352" />
            <wire x2="352" y1="592" y2="864" x1="352" />
            <wire x2="368" y1="864" y2="864" x1="352" />
            <wire x2="352" y1="864" y2="1008" x1="352" />
            <wire x2="384" y1="592" y2="592" x1="352" />
            <wire x2="1296" y1="928" y2="928" x1="1264" />
            <wire x2="1264" y1="928" y2="1008" x1="1264" />
        </branch>
        <instance x="1296" y="720" name="XLXI_6" orien="R0" />
        <branch name="Borrow">
            <wire x2="1584" y1="896" y2="896" x1="1552" />
        </branch>
        <branch name="Diff">
            <wire x2="1584" y1="624" y2="624" x1="1552" />
        </branch>
        <iomarker fontsize="28" x="1584" y="896" name="Borrow" orien="R0" />
        <iomarker fontsize="28" x="1584" y="624" name="Diff" orien="R0" />
        <iomarker fontsize="28" x="208" y="656" name="A" orien="R180" />
        <iomarker fontsize="28" x="208" y="800" name="B" orien="R180" />
        <iomarker fontsize="28" x="208" y="1008" name="R" orien="R180" />
        <text style="fontsize:150;fontname:Arial" x="148" y="236">HALF SUBTRACTOR</text>
    </sheet>
</drawing>