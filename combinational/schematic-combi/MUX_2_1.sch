<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_25" />
        <signal name="XLXN_11" />
        <signal name="XLXN_32" />
        <signal name="XLXN_40" />
        <signal name="XLXN_55" />
        <signal name="XLXN_58" />
        <signal name="XLXN_60" />
        <signal name="XLXN_1" />
        <signal name="XLXN_9" />
        <signal name="XLXN_68" />
        <signal name="XLXN_4" />
        <signal name="XLXN_23" />
        <signal name="XLXN_71" />
        <signal name="R" />
        <signal name="XLXN_73" />
        <signal name="S" />
        <signal name="I0" />
        <signal name="I1" />
        <signal name="XLXN_3" />
        <signal name="XLXN_78" />
        <signal name="XLXN_79" />
        <signal name="O" />
        <port polarity="Input" name="R" />
        <port polarity="Input" name="S" />
        <port polarity="Input" name="I0" />
        <port polarity="Input" name="I1" />
        <port polarity="Output" name="O" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="and2" name="XLXI_21">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_78" name="I1" />
            <blockpin signalname="O" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="XLXN_1" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_3">
            <blockpin signalname="S" name="I" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="S" name="I0" />
            <blockpin signalname="XLXN_23" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="I0" name="I1" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_11">
            <blockpin signalname="I1" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_20">
            <blockpin signalname="XLXN_4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_78" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="1232" y="1008" name="XLXI_2" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="1232" y1="944" y2="944" x1="1200" />
        </branch>
        <instance x="976" y="976" name="XLXI_3" orien="R0" />
        <instance x="1232" y="832" name="XLXI_1" orien="R0" />
        <instance x="624" y="976" name="XLXI_12" orien="R0" />
        <branch name="XLXN_9">
            <wire x2="1232" y1="880" y2="880" x1="880" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1504" y1="912" y2="912" x1="1488" />
            <wire x2="1584" y1="848" y2="848" x1="1504" />
            <wire x2="1504" y1="848" y2="912" x1="1504" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="1232" y1="704" y2="704" x1="880" />
        </branch>
        <instance x="624" y="800" name="XLXI_11" orien="R0" />
        <branch name="R">
            <wire x2="592" y1="1136" y2="1136" x1="480" />
            <wire x2="624" y1="672" y2="672" x1="592" />
            <wire x2="592" y1="672" y2="912" x1="592" />
            <wire x2="592" y1="912" y2="1024" x1="592" />
            <wire x2="592" y1="1024" y2="1136" x1="592" />
            <wire x2="1824" y1="1024" y2="1024" x1="592" />
            <wire x2="624" y1="912" y2="912" x1="592" />
            <wire x2="1856" y1="880" y2="880" x1="1824" />
            <wire x2="1824" y1="880" y2="1024" x1="1824" />
        </branch>
        <branch name="S">
            <wire x2="928" y1="992" y2="992" x1="480" />
            <wire x2="928" y1="768" y2="944" x1="928" />
            <wire x2="976" y1="944" y2="944" x1="928" />
            <wire x2="928" y1="944" y2="992" x1="928" />
            <wire x2="1232" y1="768" y2="768" x1="928" />
        </branch>
        <branch name="I0">
            <wire x2="624" y1="848" y2="848" x1="480" />
        </branch>
        <branch name="I1">
            <wire x2="624" y1="736" y2="736" x1="480" />
        </branch>
        <instance x="1584" y="912" name="XLXI_20" orien="R0" />
        <branch name="XLXN_3">
            <wire x2="1504" y1="736" y2="736" x1="1488" />
            <wire x2="1504" y1="736" y2="784" x1="1504" />
            <wire x2="1584" y1="784" y2="784" x1="1504" />
        </branch>
        <iomarker fontsize="28" x="480" y="736" name="I1" orien="R180" />
        <iomarker fontsize="28" x="480" y="1136" name="R" orien="R180" />
        <iomarker fontsize="28" x="480" y="992" name="S" orien="R180" />
        <iomarker fontsize="28" x="480" y="848" name="I0" orien="R180" />
        <instance x="1856" y="944" name="XLXI_21" orien="R0" />
        <branch name="XLXN_78">
            <wire x2="1856" y1="816" y2="816" x1="1840" />
        </branch>
        <branch name="O">
            <wire x2="2144" y1="848" y2="848" x1="2112" />
        </branch>
        <iomarker fontsize="28" x="2144" y="848" name="O" orien="R0" />
    </sheet>
</drawing>