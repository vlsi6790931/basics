<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="reset" />
        <signal name="XLXN_12" />
        <signal name="XLXN_6" />
        <signal name="XLXN_14" />
        <signal name="XLXN_7" />
        <signal name="XLXN_16" />
        <signal name="XLXN_8" />
        <signal name="A(3:0)" />
        <signal name="B(3:0)" />
        <signal name="O(3:0)" />
        <signal name="Bin" />
        <signal name="A(0)" />
        <signal name="B(0)" />
        <signal name="A(1)" />
        <signal name="B(1)" />
        <signal name="A(2)" />
        <signal name="B(2)" />
        <signal name="A(3)" />
        <signal name="B(3)" />
        <signal name="O(0)" />
        <signal name="O(1)" />
        <signal name="O(2)" />
        <signal name="O(3)" />
        <signal name="Bout" />
        <port polarity="Input" name="reset" />
        <port polarity="Input" name="A(3:0)" />
        <port polarity="Input" name="B(3:0)" />
        <port polarity="Output" name="O(3:0)" />
        <port polarity="Input" name="Bin" />
        <port polarity="Output" name="Bout" />
        <blockdef name="FullSubtractor">
            <timestamp>2023-8-11T0:25:34</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-80" y2="-80" x1="320" />
            <line x2="384" y1="-192" y2="-192" x1="320" />
        </blockdef>
        <block symbolname="FullSubtractor" name="XLXI_1">
            <blockpin signalname="A(0)" name="A" />
            <blockpin signalname="B(0)" name="B" />
            <blockpin signalname="Bin" name="Bin" />
            <blockpin signalname="reset" name="RESET" />
            <blockpin signalname="XLXN_6" name="Bout" />
            <blockpin signalname="O(0)" name="DIFF" />
        </block>
        <block symbolname="FullSubtractor" name="XLXI_2">
            <blockpin signalname="A(1)" name="A" />
            <blockpin signalname="B(1)" name="B" />
            <blockpin signalname="XLXN_6" name="Bin" />
            <blockpin signalname="reset" name="RESET" />
            <blockpin signalname="XLXN_7" name="Bout" />
            <blockpin signalname="O(1)" name="DIFF" />
        </block>
        <block symbolname="FullSubtractor" name="XLXI_3">
            <blockpin signalname="A(2)" name="A" />
            <blockpin signalname="B(2)" name="B" />
            <blockpin signalname="XLXN_7" name="Bin" />
            <blockpin signalname="reset" name="RESET" />
            <blockpin signalname="XLXN_8" name="Bout" />
            <blockpin signalname="O(2)" name="DIFF" />
        </block>
        <block symbolname="FullSubtractor" name="XLXI_4">
            <blockpin signalname="A(3)" name="A" />
            <blockpin signalname="B(3)" name="B" />
            <blockpin signalname="XLXN_8" name="Bin" />
            <blockpin signalname="reset" name="RESET" />
            <blockpin signalname="Bout" name="Bout" />
            <blockpin signalname="O(3)" name="DIFF" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1472" y="1168" name="XLXI_1" orien="R0">
        </instance>
        <branch name="reset">
            <wire x2="1408" y1="1136" y2="1136" x1="1360" />
            <wire x2="1472" y1="1136" y2="1136" x1="1408" />
            <wire x2="1408" y1="1136" y2="1536" x1="1408" />
            <wire x2="1408" y1="1536" y2="1952" x1="1408" />
            <wire x2="1472" y1="1952" y2="1952" x1="1408" />
            <wire x2="1408" y1="1952" y2="2400" x1="1408" />
            <wire x2="1472" y1="2400" y2="2400" x1="1408" />
            <wire x2="1472" y1="1536" y2="1536" x1="1408" />
        </branch>
        <instance x="1472" y="1568" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1472" y="1984" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_6">
            <wire x2="1872" y1="1216" y2="1216" x1="1456" />
            <wire x2="1456" y1="1216" y2="1344" x1="1456" />
            <wire x2="1472" y1="1344" y2="1344" x1="1456" />
            <wire x2="1872" y1="1088" y2="1088" x1="1856" />
            <wire x2="1872" y1="1088" y2="1216" x1="1872" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1456" y1="1616" y2="1760" x1="1456" />
            <wire x2="1472" y1="1760" y2="1760" x1="1456" />
            <wire x2="1872" y1="1616" y2="1616" x1="1456" />
            <wire x2="1872" y1="1488" y2="1488" x1="1856" />
            <wire x2="1872" y1="1488" y2="1616" x1="1872" />
        </branch>
        <instance x="1472" y="2432" name="XLXI_4" orien="R0">
        </instance>
        <branch name="XLXN_8">
            <wire x2="1872" y1="2064" y2="2064" x1="1456" />
            <wire x2="1456" y1="2064" y2="2208" x1="1456" />
            <wire x2="1472" y1="2208" y2="2208" x1="1456" />
            <wire x2="1872" y1="1904" y2="1904" x1="1856" />
            <wire x2="1872" y1="1904" y2="2064" x1="1872" />
        </branch>
        <branch name="A(3:0)">
            <wire x2="1280" y1="576" y2="576" x1="1008" />
        </branch>
        <branch name="B(3:0)">
            <wire x2="1280" y1="704" y2="704" x1="1024" />
        </branch>
        <branch name="O(3:0)">
            <wire x2="2384" y1="576" y2="576" x1="2128" />
        </branch>
        <branch name="Bin">
            <wire x2="1472" y1="944" y2="944" x1="1424" />
        </branch>
        <branch name="A(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="1008" type="branch" />
            <wire x2="1472" y1="1008" y2="1008" x1="1392" />
        </branch>
        <branch name="B(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="1072" type="branch" />
            <wire x2="1472" y1="1072" y2="1072" x1="1392" />
        </branch>
        <branch name="A(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="1408" type="branch" />
            <wire x2="1472" y1="1408" y2="1408" x1="1392" />
        </branch>
        <branch name="B(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="1472" type="branch" />
            <wire x2="1472" y1="1472" y2="1472" x1="1392" />
        </branch>
        <branch name="A(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="1824" type="branch" />
            <wire x2="1472" y1="1824" y2="1824" x1="1392" />
        </branch>
        <branch name="B(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="1888" type="branch" />
            <wire x2="1472" y1="1888" y2="1888" x1="1392" />
        </branch>
        <branch name="A(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="2272" type="branch" />
            <wire x2="1472" y1="2272" y2="2272" x1="1392" />
        </branch>
        <branch name="B(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="2336" type="branch" />
            <wire x2="1472" y1="2336" y2="2336" x1="1392" />
        </branch>
        <branch name="O(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="976" type="branch" />
            <wire x2="1920" y1="976" y2="976" x1="1856" />
        </branch>
        <branch name="O(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="1376" type="branch" />
            <wire x2="1920" y1="1376" y2="1376" x1="1856" />
        </branch>
        <branch name="O(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="1792" type="branch" />
            <wire x2="1920" y1="1792" y2="1792" x1="1856" />
        </branch>
        <branch name="O(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="2240" type="branch" />
            <wire x2="1920" y1="2240" y2="2240" x1="1856" />
        </branch>
        <branch name="Bout">
            <wire x2="1872" y1="2352" y2="2352" x1="1856" />
            <wire x2="1936" y1="2352" y2="2352" x1="1872" />
        </branch>
        <iomarker fontsize="28" x="1008" y="576" name="A(3:0)" orien="R180" />
        <iomarker fontsize="28" x="1024" y="704" name="B(3:0)" orien="R180" />
        <iomarker fontsize="28" x="2384" y="576" name="O(3:0)" orien="R0" />
        <iomarker fontsize="28" x="1424" y="944" name="Bin" orien="R180" />
        <iomarker fontsize="28" x="1360" y="1136" name="reset" orien="R180" />
        <iomarker fontsize="28" x="1936" y="2352" name="Bout" orien="R0" />
        <text style="fontsize:150;fontname:Arial" x="768" y="224">FOUR BIT FULL SUBTRACTOR</text>
    </sheet>
</drawing>