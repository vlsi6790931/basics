<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_3" />
        <signal name="B_IN(7:4)" />
        <signal name="B_IN(3:0)" />
        <signal name="C_OUT" />
        <signal name="SUM(7:4)" />
        <signal name="SUM(3:0)" />
        <signal name="SUM(7:0)" />
        <signal name="C_IN" />
        <signal name="XLXN_21" />
        <signal name="A_IN(7:4)" />
        <signal name="A_IN(3:0)" />
        <signal name="A_IN(7:0)" />
        <signal name="B_IN(7:0)" />
        <signal name="reset" />
        <port polarity="Output" name="C_OUT" />
        <port polarity="Output" name="SUM(7:0)" />
        <port polarity="Input" name="C_IN" />
        <port polarity="Input" name="A_IN(7:0)" />
        <port polarity="Input" name="B_IN(7:0)" />
        <port polarity="Input" name="reset" />
        <blockdef name="FBA">
            <timestamp>2023-7-28T18:32:55</timestamp>
            <rect width="128" x="64" y="-192" height="128" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="256" y1="-160" y2="-160" x1="192" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <rect width="64" x="192" y="-108" height="24" />
            <line x2="128" y1="-64" y2="0" x1="128" />
            <line x2="128" y1="-192" y2="-256" x1="128" />
        </blockdef>
        <block symbolname="FBA" name="XLXI_2">
            <blockpin signalname="A_IN(3:0)" name="A(3:0)" />
            <blockpin signalname="B_IN(3:0)" name="B(3:0)" />
            <blockpin signalname="XLXN_3" name="C" />
            <blockpin signalname="SUM(3:0)" name="S(3:0)" />
            <blockpin signalname="C_IN" name="Cin" />
            <blockpin signalname="reset" name="reset" />
        </block>
        <block symbolname="FBA" name="XLXI_1">
            <blockpin signalname="A_IN(7:4)" name="A(3:0)" />
            <blockpin signalname="B_IN(7:4)" name="B(3:0)" />
            <blockpin signalname="C_OUT" name="C" />
            <blockpin signalname="SUM(7:4)" name="S(3:0)" />
            <blockpin signalname="XLXN_3" name="Cin" />
            <blockpin signalname="reset" name="reset" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <branch name="XLXN_3">
            <wire x2="848" y1="560" y2="624" x1="848" />
            <wire x2="992" y1="624" y2="624" x1="848" />
            <wire x2="992" y1="624" y2="752" x1="992" />
            <wire x2="992" y1="752" y2="752" x1="928" />
        </branch>
        <branch name="B_IN(3:0)">
            <wire x2="672" y1="816" y2="816" x1="624" />
        </branch>
        <branch name="C_OUT">
            <wire x2="1328" y1="400" y2="400" x1="976" />
        </branch>
        <branch name="SUM(7:4)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1056" y="464" type="branch" />
            <wire x2="1056" y1="464" y2="464" x1="976" />
            <wire x2="1072" y1="464" y2="464" x1="1056" />
        </branch>
        <branch name="SUM(3:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1056" y="816" type="branch" />
            <wire x2="1056" y1="816" y2="816" x1="928" />
            <wire x2="1072" y1="816" y2="816" x1="1056" />
        </branch>
        <branch name="SUM(7:0)">
            <wire x2="1168" y1="432" y2="464" x1="1168" />
            <wire x2="1168" y1="464" y2="624" x1="1168" />
            <wire x2="1168" y1="624" y2="816" x1="1168" />
            <wire x2="1168" y1="816" y2="848" x1="1168" />
            <wire x2="1328" y1="624" y2="624" x1="1168" />
        </branch>
        <bustap x2="1072" y1="816" y2="816" x1="1168" />
        <bustap x2="1072" y1="464" y2="464" x1="1168" />
        <text style="fontsize:60;fontname:Arial" x="692" y="140">8-BIT ADDER</text>
        <rect width="756" x="488" y="204" height="860" />
        <iomarker fontsize="28" x="1328" y="624" name="SUM(7:0)" orien="R0" />
        <iomarker fontsize="28" x="1328" y="400" name="C_OUT" orien="R0" />
        <instance x="672" y="912" name="XLXI_2" orien="R0">
        </instance>
        <instance x="720" y="560" name="XLXI_1" orien="R0">
        </instance>
        <branch name="B_IN(7:4)">
            <wire x2="720" y1="464" y2="464" x1="624" />
        </branch>
        <text style="fontsize:40;fontname:Arial" x="636" y="332">high order</text>
        <branch name="C_IN">
            <wire x2="800" y1="928" y2="928" x1="320" />
            <wire x2="800" y1="912" y2="928" x1="800" />
        </branch>
        <iomarker fontsize="28" x="320" y="928" name="C_IN" orien="R180" />
        <text style="fontsize:40;fontname:Arial" x="592" y="672">low order</text>
        <branch name="A_IN(7:4)">
            <wire x2="720" y1="400" y2="400" x1="624" />
        </branch>
        <branch name="A_IN(3:0)">
            <wire x2="672" y1="752" y2="752" x1="624" />
        </branch>
        <branch name="A_IN(7:0)">
            <wire x2="496" y1="320" y2="320" x1="368" />
        </branch>
        <iomarker fontsize="28" x="368" y="320" name="A_IN(7:0)" orien="R180" />
        <branch name="reset">
            <wire x2="560" y1="576" y2="576" x1="304" />
            <wire x2="800" y1="576" y2="576" x1="560" />
            <wire x2="800" y1="576" y2="656" x1="800" />
            <wire x2="560" y1="272" y2="576" x1="560" />
            <wire x2="848" y1="272" y2="272" x1="560" />
            <wire x2="848" y1="272" y2="304" x1="848" />
        </branch>
        <iomarker fontsize="28" x="304" y="576" name="reset" orien="R180" />
        <iomarker fontsize="28" x="368" y="752" name="B_IN(7:0)" orien="R180" />
        <branch name="B_IN(7:0)">
            <wire x2="480" y1="752" y2="752" x1="368" />
        </branch>
    </sheet>
</drawing>