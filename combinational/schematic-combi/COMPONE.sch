<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_1" />
        <signal name="qweqr" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_5" />
        <signal name="qeqwe" />
        <signal name="XLXN_3" />
        <signal name="L" />
        <signal name="W" />
        <signal name="XLXN_66" />
        <signal name="XLXN_77" />
        <signal name="XLXN_78" />
        <signal name="R" />
        <signal name="E" />
        <signal name="I1" />
        <signal name="I2" />
        <port polarity="Output" name="L" />
        <port polarity="Output" name="W" />
        <port polarity="Input" name="R" />
        <port polarity="Output" name="E" />
        <port polarity="Input" name="I1" />
        <port polarity="Input" name="I2" />
        <blockdef name="xnor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
            <circle r="8" cx="220" cy="-96" />
            <line x2="256" y1="-96" y2="-96" x1="228" />
            <line x2="60" y1="-28" y2="-28" x1="60" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="xnor2" name="XLXI_1">
            <blockpin signalname="qweqr" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="xnor2" name="XLXI_2">
            <blockpin signalname="qweqr" name="I0" />
            <blockpin signalname="qeqwe" name="I1" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_3">
            <blockpin signalname="XLXN_5" name="P" />
        </block>
        <block symbolname="inv" name="XLXI_4">
            <blockpin signalname="XLXN_25" name="I" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="xnor2" name="XLXI_5">
            <blockpin signalname="XLXN_5" name="I0" />
            <blockpin signalname="qeqwe" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_14">
            <blockpin signalname="XLXN_24" name="I" />
            <blockpin signalname="XLXN_66" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_23">
            <blockpin signalname="XLXN_24" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="R" name="I2" />
            <blockpin signalname="W" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_24">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_1" name="I1" />
            <blockpin signalname="XLXN_24" name="I2" />
            <blockpin signalname="L" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_25">
            <blockpin signalname="I1" name="I0" />
            <blockpin signalname="R" name="I1" />
            <blockpin signalname="qeqwe" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_26">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="I2" name="I1" />
            <blockpin signalname="qweqr" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_47">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="XLXN_66" name="I1" />
            <blockpin signalname="E" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="1207" y="1028" name="XLXI_1" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="1879" y1="932" y2="932" x1="1463" />
        </branch>
        <instance x="1319" y="820" name="XLXI_2" orien="R0" />
        <instance x="1031" y="884" name="XLXI_3" orien="R0" />
        <branch name="qweqr">
            <wire x2="871" y1="836" y2="836" x1="759" />
            <wire x2="871" y1="756" y2="836" x1="871" />
            <wire x2="983" y1="756" y2="756" x1="871" />
            <wire x2="1319" y1="756" y2="756" x1="983" />
            <wire x2="983" y1="756" y2="964" x1="983" />
            <wire x2="1207" y1="964" y2="964" x1="983" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="1847" y1="724" y2="724" x1="1831" />
            <wire x2="1847" y1="724" y2="868" x1="1847" />
            <wire x2="1879" y1="868" y2="868" x1="1847" />
            <wire x2="1911" y1="724" y2="724" x1="1847" />
            <wire x2="1847" y1="580" y2="724" x1="1847" />
            <wire x2="1879" y1="580" y2="580" x1="1847" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="1607" y1="724" y2="724" x1="1575" />
        </branch>
        <instance x="1607" y="756" name="XLXI_4" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="1095" y1="884" y2="900" x1="1095" />
            <wire x2="1191" y1="900" y2="900" x1="1095" />
            <wire x2="1207" y1="900" y2="900" x1="1191" />
            <wire x2="1191" y1="548" y2="900" x1="1191" />
            <wire x2="1207" y1="548" y2="548" x1="1191" />
        </branch>
        <branch name="qeqwe">
            <wire x2="871" y1="612" y2="612" x1="759" />
            <wire x2="871" y1="612" y2="692" x1="871" />
            <wire x2="983" y1="692" y2="692" x1="871" />
            <wire x2="1319" y1="692" y2="692" x1="983" />
            <wire x2="983" y1="484" y2="692" x1="983" />
            <wire x2="1207" y1="484" y2="484" x1="983" />
        </branch>
        <instance x="1207" y="612" name="XLXI_5" orien="R0" />
        <branch name="XLXN_3">
            <wire x2="1879" y1="516" y2="516" x1="1463" />
        </branch>
        <instance x="1911" y="756" name="XLXI_14" orien="R0" />
        <instance x="1879" y="644" name="XLXI_23" orien="R0" />
        <instance x="1879" y="1060" name="XLXI_24" orien="R0" />
        <branch name="L">
            <wire x2="2167" y1="932" y2="932" x1="2135" />
        </branch>
        <branch name="W">
            <wire x2="2167" y1="516" y2="516" x1="2135" />
        </branch>
        <branch name="XLXN_66">
            <wire x2="2295" y1="724" y2="724" x1="2135" />
        </branch>
        <instance x="503" y="708" name="XLXI_25" orien="R0" />
        <instance x="503" y="932" name="XLXI_26" orien="R0" />
        <branch name="R">
            <wire x2="455" y1="1060" y2="1060" x1="407" />
            <wire x2="1879" y1="1060" y2="1060" x1="455" />
            <wire x2="2279" y1="1060" y2="1060" x1="1879" />
            <wire x2="1879" y1="420" y2="420" x1="455" />
            <wire x2="1879" y1="420" y2="452" x1="1879" />
            <wire x2="455" y1="420" y2="580" x1="455" />
            <wire x2="503" y1="580" y2="580" x1="455" />
            <wire x2="455" y1="580" y2="868" x1="455" />
            <wire x2="503" y1="868" y2="868" x1="455" />
            <wire x2="455" y1="868" y2="1060" x1="455" />
            <wire x2="1879" y1="996" y2="1060" x1="1879" />
            <wire x2="2295" y1="788" y2="788" x1="2279" />
            <wire x2="2279" y1="788" y2="1060" x1="2279" />
        </branch>
        <instance x="2295" y="852" name="XLXI_47" orien="R0" />
        <branch name="E">
            <wire x2="2583" y1="756" y2="756" x1="2551" />
        </branch>
        <branch name="I1">
            <wire x2="423" y1="644" y2="644" x1="391" />
            <wire x2="503" y1="644" y2="644" x1="423" />
        </branch>
        <branch name="I2">
            <wire x2="423" y1="804" y2="804" x1="391" />
            <wire x2="503" y1="804" y2="804" x1="423" />
        </branch>
        <iomarker fontsize="28" x="2167" y="932" name="L" orien="R0" />
        <iomarker fontsize="28" x="2167" y="516" name="W" orien="R0" />
        <iomarker fontsize="28" x="2583" y="756" name="E" orien="R0" />
        <iomarker fontsize="28" x="391" y="644" name="I1" orien="R180" />
        <iomarker fontsize="28" x="391" y="804" name="I2" orien="R180" />
        <iomarker fontsize="28" x="407" y="1060" name="R" orien="R180" />
    </sheet>
</drawing>