<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="S(0)" />
        <signal name="I(3:0)" />
        <signal name="S(1)" />
        <signal name="XLXN_40" />
        <signal name="XLXN_41" />
        <signal name="reset" />
        <signal name="I(7:4)" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="S(2)" />
        <signal name="O" />
        <signal name="I(7:0)" />
        <signal name="S(2:0)" />
        <port polarity="Input" name="reset" />
        <port polarity="Output" name="O" />
        <port polarity="Input" name="I(7:0)" />
        <port polarity="Input" name="S(2:0)" />
        <blockdef name="MUX_4_1">
            <timestamp>2023-8-11T1:58:3</timestamp>
            <rect width="128" x="64" y="-192" height="128" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="160" y1="-64" y2="0" x1="160" />
            <line x2="96" y1="-64" y2="0" x1="96" />
            <line x2="256" y1="-128" y2="-128" x1="192" />
        </blockdef>
        <blockdef name="MUX_2_1">
            <timestamp>2023-8-11T1:39:32</timestamp>
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="160" y1="-64" y2="0" x1="160" />
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
        </blockdef>
        <block symbolname="MUX_4_1" name="XLXI_5">
            <blockpin signalname="I(3:0)" name="I(3:0)" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="S(0)" name="S0" />
            <blockpin signalname="S(1)" name="S1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="MUX_4_1" name="XLXI_4">
            <blockpin signalname="I(7:4)" name="I(3:0)" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="S(0)" name="S0" />
            <blockpin signalname="S(1)" name="S1" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="MUX_2_1" name="XLXI_6">
            <blockpin signalname="XLXN_16" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="S(2)" name="S" />
            <blockpin signalname="O" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="1296" y="544" name="XLXI_5" orien="R180">
        </instance>
        <branch name="S(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="528" type="branch" />
            <wire x2="688" y1="528" y2="528" x1="384" />
            <wire x2="1136" y1="528" y2="528" x1="688" />
            <wire x2="1136" y1="528" y2="544" x1="1136" />
            <wire x2="688" y1="528" y2="544" x1="688" />
        </branch>
        <branch name="I(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1312" y="704" type="branch" />
            <wire x2="1312" y1="704" y2="704" x1="1296" />
        </branch>
        <instance x="848" y="544" name="XLXI_4" orien="R180">
        </instance>
        <branch name="S(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="464" type="branch" />
            <wire x2="752" y1="464" y2="464" x1="384" />
            <wire x2="1200" y1="464" y2="464" x1="752" />
            <wire x2="1200" y1="464" y2="544" x1="1200" />
            <wire x2="752" y1="464" y2="544" x1="752" />
        </branch>
        <branch name="reset">
            <wire x2="960" y1="640" y2="640" x1="848" />
            <wire x2="960" y1="640" y2="800" x1="960" />
            <wire x2="1408" y1="800" y2="800" x1="960" />
            <wire x2="880" y1="800" y2="912" x1="880" />
            <wire x2="960" y1="800" y2="800" x1="880" />
            <wire x2="1408" y1="640" y2="640" x1="1296" />
            <wire x2="1408" y1="640" y2="800" x1="1408" />
            <wire x2="1504" y1="640" y2="640" x1="1408" />
        </branch>
        <branch name="I(7:4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="864" y="704" type="branch" />
            <wire x2="864" y1="704" y2="704" x1="848" />
        </branch>
        <instance x="784" y="912" name="XLXI_6" orien="R90">
        </instance>
        <branch name="XLXN_16">
            <wire x2="1040" y1="672" y2="672" x1="1008" />
            <wire x2="1008" y1="672" y2="912" x1="1008" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="592" y1="672" y2="672" x1="576" />
            <wire x2="576" y1="672" y2="864" x1="576" />
            <wire x2="944" y1="864" y2="864" x1="576" />
            <wire x2="944" y1="864" y2="912" x1="944" />
        </branch>
        <branch name="S(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="400" type="branch" />
            <wire x2="480" y1="400" y2="400" x1="384" />
            <wire x2="480" y1="400" y2="1072" x1="480" />
            <wire x2="784" y1="1072" y2="1072" x1="480" />
        </branch>
        <branch name="O">
            <wire x2="944" y1="1232" y2="1264" x1="944" />
        </branch>
        <iomarker fontsize="28" x="1504" y="640" name="reset" orien="R0" />
        <iomarker fontsize="28" x="944" y="1264" name="O" orien="R90" />
        <branch name="I(7:0)">
            <wire x2="352" y1="272" y2="272" x1="128" />
        </branch>
        <iomarker fontsize="28" x="128" y="272" name="I(7:0)" orien="R180" />
        <branch name="S(2:0)">
            <wire x2="208" y1="480" y2="480" x1="160" />
        </branch>
        <iomarker fontsize="28" x="160" y="480" name="S(2:0)" orien="R180" />
        <text style="fontsize:150;fontname:Arial" x="580" y="136">MUX 8X1</text>
    </sheet>
</drawing>