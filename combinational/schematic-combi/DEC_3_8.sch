<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_25" />
        <signal name="XLXN_27" />
        <signal name="XLXN_29" />
        <signal name="XLXN_31" />
        <signal name="XLXN_33" />
        <signal name="XLXN_35" />
        <signal name="XLXN_55" />
        <signal name="XLXN_63" />
        <signal name="XLXN_75" />
        <signal name="XLXN_76" />
        <signal name="XLXN_77" />
        <signal name="XLXN_80" />
        <signal name="XLXN_81" />
        <signal name="XLXN_84" />
        <signal name="XLXN_85" />
        <signal name="XLXN_86" />
        <signal name="XLXN_88" />
        <signal name="XLXN_89" />
        <signal name="XLXN_92" />
        <signal name="XLXN_94" />
        <signal name="XLXN_95" />
        <signal name="XLXN_96" />
        <signal name="XLXN_97" />
        <signal name="XLXN_99" />
        <signal name="XLXN_185" />
        <signal name="XLXN_324" />
        <signal name="XLXN_325" />
        <signal name="XLXN_326" />
        <signal name="XLXN_382" />
        <signal name="XLXN_439" />
        <signal name="I(0)">
        </signal>
        <signal name="I(1)">
        </signal>
        <signal name="I(2)">
        </signal>
        <signal name="XLXN_5" />
        <signal name="XLXN_73" />
        <signal name="XLXN_74" />
        <signal name="I0" />
        <signal name="I1" />
        <signal name="I2" />
        <signal name="XLXN_78" />
        <signal name="XLXN_79" />
        <signal name="XLXN_496" />
        <signal name="RESET" />
        <signal name="XLXN_498" />
        <signal name="XLXN_499" />
        <signal name="XLXN_501" />
        <signal name="XLXN_502" />
        <signal name="XLXN_503" />
        <signal name="XLXN_504" />
        <signal name="XLXN_505" />
        <signal name="XLXN_506" />
        <signal name="XLXN_507" />
        <signal name="XLXN_508" />
        <signal name="XLXN_509" />
        <signal name="XLXN_510" />
        <signal name="XLXN_511" />
        <signal name="XLXN_512" />
        <signal name="XLXN_513" />
        <signal name="XLXN_514" />
        <signal name="O(7)">
        </signal>
        <signal name="O(5)">
        </signal>
        <signal name="O(4)">
        </signal>
        <signal name="O(6)">
        </signal>
        <signal name="I(2:0)" />
        <signal name="O(7:0)" />
        <signal name="O(3)" />
        <signal name="O(2)" />
        <signal name="O(1)" />
        <signal name="O(0)" />
        <port polarity="Input" name="RESET" />
        <port polarity="Input" name="I(2:0)" />
        <port polarity="Output" name="O(7:0)" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="I(2)" name="I1" />
            <blockpin signalname="XLXN_74" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="I(1)" name="I1" />
            <blockpin signalname="XLXN_73" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="I(0)" name="I1" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_4">
            <blockpin signalname="XLXN_73" name="I" />
            <blockpin signalname="XLXN_505" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="XLXN_5" name="I" />
            <blockpin signalname="XLXN_508" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_10">
            <blockpin signalname="XLXN_74" name="I" />
            <blockpin signalname="XLXN_513" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="XLXN_505" name="I" />
            <blockpin signalname="I1" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_9">
            <blockpin signalname="XLXN_508" name="I" />
            <blockpin signalname="I0" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_11">
            <blockpin signalname="XLXN_513" name="I" />
            <blockpin signalname="I2" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_129">
            <blockpin signalname="I2" name="I0" />
            <blockpin signalname="I1" name="I1" />
            <blockpin signalname="XLXN_508" name="I2" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_128">
            <blockpin signalname="I2" name="I0" />
            <blockpin signalname="I1" name="I1" />
            <blockpin signalname="I0" name="I2" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_131">
            <blockpin signalname="I2" name="I0" />
            <blockpin signalname="XLXN_505" name="I1" />
            <blockpin signalname="XLXN_508" name="I2" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_117">
            <blockpin signalname="XLXN_513" name="I0" />
            <blockpin signalname="I1" name="I1" />
            <blockpin signalname="XLXN_508" name="I2" />
            <blockpin signalname="XLXN_78" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_118">
            <blockpin signalname="XLXN_513" name="I0" />
            <blockpin signalname="XLXN_505" name="I1" />
            <blockpin signalname="I0" name="I2" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_119">
            <blockpin signalname="XLXN_513" name="I0" />
            <blockpin signalname="XLXN_505" name="I1" />
            <blockpin signalname="XLXN_508" name="I2" />
            <blockpin signalname="XLXN_79" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_130">
            <blockpin signalname="I2" name="I0" />
            <blockpin signalname="XLXN_505" name="I1" />
            <blockpin signalname="I0" name="I2" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_116">
            <blockpin signalname="XLXN_513" name="I0" />
            <blockpin signalname="I1" name="I1" />
            <blockpin signalname="I0" name="I2" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_28">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="O(7)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_31">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="O(4)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_34">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_13" name="I1" />
            <blockpin signalname="O(1)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_30">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="O(5)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_32">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_11" name="I1" />
            <blockpin signalname="O(3)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_33">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_78" name="I1" />
            <blockpin signalname="O(2)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_35">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_79" name="I1" />
            <blockpin signalname="O(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_29">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="O(6)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="560" y="1168" name="XLXI_3" orien="R0" />
        <instance x="560" y="912" name="XLXI_2" orien="R0" />
        <instance x="560" y="656" name="XLXI_1" orien="R0" />
        <branch name="I(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="528" y="528" type="branch" />
            <wire x2="560" y1="528" y2="528" x1="528" />
        </branch>
        <branch name="I(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="528" y="784" type="branch" />
            <wire x2="560" y1="784" y2="784" x1="528" />
        </branch>
        <branch name="I(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="528" y="1040" type="branch" />
            <wire x2="560" y1="1040" y2="1040" x1="528" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="832" y1="560" y2="560" x1="816" />
        </branch>
        <branch name="XLXN_73">
            <wire x2="832" y1="816" y2="816" x1="816" />
        </branch>
        <branch name="XLXN_74">
            <wire x2="832" y1="1072" y2="1072" x1="816" />
        </branch>
        <instance x="832" y="848" name="XLXI_4" orien="R0" />
        <instance x="832" y="592" name="XLXI_8" orien="R0" />
        <instance x="832" y="1104" name="XLXI_10" orien="R0" />
        <instance x="1168" y="848" name="XLXI_5" orien="R0" />
        <instance x="1168" y="592" name="XLXI_9" orien="R0" />
        <instance x="1168" y="1104" name="XLXI_11" orien="R0" />
        <branch name="I0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="560" type="branch" />
            <wire x2="1408" y1="560" y2="560" x1="1392" />
        </branch>
        <branch name="I1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="816" type="branch" />
            <wire x2="1408" y1="816" y2="816" x1="1392" />
        </branch>
        <branch name="I2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="1072" type="branch" />
            <wire x2="1408" y1="1072" y2="1072" x1="1392" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="2080" y1="160" y2="160" x1="1952" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="2064" y1="384" y2="384" x1="1952" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="2096" y1="816" y2="816" x1="1952" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="2096" y1="1408" y2="1408" x1="1952" />
        </branch>
        <instance x="1696" y="512" name="XLXI_129" orien="R0" />
        <instance x="1696" y="288" name="XLXI_128" orien="R0" />
        <instance x="1696" y="944" name="XLXI_131" orien="R0" />
        <instance x="1696" y="1328" name="XLXI_117" orien="R0" />
        <instance x="1696" y="1536" name="XLXI_118" orien="R0" />
        <instance x="1696" y="1760" name="XLXI_119" orien="R0" />
        <branch name="XLXN_78">
            <wire x2="2096" y1="1200" y2="1200" x1="1952" />
        </branch>
        <branch name="XLXN_79">
            <wire x2="2096" y1="1632" y2="1632" x1="1952" />
        </branch>
        <branch name="I0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="96" type="branch" />
            <wire x2="1696" y1="96" y2="96" x1="1664" />
        </branch>
        <branch name="I1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="160" type="branch" />
            <wire x2="1696" y1="160" y2="160" x1="1664" />
        </branch>
        <branch name="I2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="224" type="branch" />
            <wire x2="1696" y1="224" y2="224" x1="1664" />
        </branch>
        <branch name="I1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="384" type="branch" />
            <wire x2="1696" y1="384" y2="384" x1="1664" />
        </branch>
        <branch name="I2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="448" type="branch" />
            <wire x2="1696" y1="448" y2="448" x1="1664" />
        </branch>
        <branch name="I2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="880" type="branch" />
            <wire x2="1696" y1="880" y2="880" x1="1664" />
        </branch>
        <branch name="I1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="1200" type="branch" />
            <wire x2="1696" y1="1200" y2="1200" x1="1664" />
        </branch>
        <branch name="I0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="1344" type="branch" />
            <wire x2="1696" y1="1344" y2="1344" x1="1664" />
        </branch>
        <instance x="1696" y="720" name="XLXI_130" orien="R0" />
        <branch name="I2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="656" type="branch" />
            <wire x2="1696" y1="656" y2="656" x1="1664" />
        </branch>
        <branch name="I0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="528" type="branch" />
            <wire x2="1696" y1="528" y2="528" x1="1664" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="2096" y1="592" y2="592" x1="1952" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="2096" y1="1008" y2="1008" x1="1952" />
        </branch>
        <instance x="1696" y="1136" name="XLXI_116" orien="R0" />
        <branch name="I0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="944" type="branch" />
            <wire x2="1696" y1="944" y2="944" x1="1664" />
        </branch>
        <branch name="I1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="1008" type="branch" />
            <wire x2="1696" y1="1008" y2="1008" x1="1664" />
        </branch>
        <instance x="2080" y="288" name="XLXI_28" orien="R0" />
        <instance x="2096" y="944" name="XLXI_31" orien="R0" />
        <instance x="2096" y="1536" name="XLXI_34" orien="R0" />
        <branch name="O(7)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2368" y="192" type="branch" />
            <wire x2="2368" y1="192" y2="192" x1="2336" />
        </branch>
        <instance x="2096" y="720" name="XLXI_30" orien="R0" />
        <instance x="2096" y="1136" name="XLXI_32" orien="R0" />
        <instance x="2096" y="1328" name="XLXI_33" orien="R0" />
        <instance x="2096" y="1760" name="XLXI_35" orien="R0" />
        <instance x="2064" y="512" name="XLXI_29" orien="R0" />
        <branch name="O(6)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="416" type="branch" />
            <wire x2="2352" y1="416" y2="416" x1="2320" />
        </branch>
        <branch name="RESET">
            <wire x2="368" y1="384" y2="384" x1="336" />
            <wire x2="368" y1="384" y2="592" x1="368" />
            <wire x2="560" y1="592" y2="592" x1="368" />
            <wire x2="368" y1="592" y2="848" x1="368" />
            <wire x2="368" y1="848" y2="1104" x1="368" />
            <wire x2="560" y1="1104" y2="1104" x1="368" />
            <wire x2="368" y1="1104" y2="1728" x1="368" />
            <wire x2="2048" y1="1728" y2="1728" x1="368" />
            <wire x2="560" y1="848" y2="848" x1="368" />
            <wire x2="2080" y1="224" y2="224" x1="2048" />
            <wire x2="2048" y1="224" y2="448" x1="2048" />
            <wire x2="2064" y1="448" y2="448" x1="2048" />
            <wire x2="2048" y1="448" y2="656" x1="2048" />
            <wire x2="2096" y1="656" y2="656" x1="2048" />
            <wire x2="2048" y1="656" y2="880" x1="2048" />
            <wire x2="2096" y1="880" y2="880" x1="2048" />
            <wire x2="2048" y1="880" y2="1072" x1="2048" />
            <wire x2="2096" y1="1072" y2="1072" x1="2048" />
            <wire x2="2048" y1="1072" y2="1264" x1="2048" />
            <wire x2="2048" y1="1264" y2="1472" x1="2048" />
            <wire x2="2096" y1="1472" y2="1472" x1="2048" />
            <wire x2="2048" y1="1472" y2="1696" x1="2048" />
            <wire x2="2096" y1="1696" y2="1696" x1="2048" />
            <wire x2="2048" y1="1696" y2="1728" x1="2048" />
            <wire x2="2096" y1="1264" y2="1264" x1="2048" />
        </branch>
        <iomarker fontsize="28" x="336" y="384" name="RESET" orien="R180" />
        <branch name="XLXN_505">
            <wire x2="1136" y1="816" y2="816" x1="1056" />
            <wire x2="1168" y1="816" y2="816" x1="1136" />
            <wire x2="1136" y1="720" y2="816" x1="1136" />
            <wire x2="1472" y1="720" y2="720" x1="1136" />
            <wire x2="1472" y1="720" y2="816" x1="1472" />
            <wire x2="1696" y1="816" y2="816" x1="1472" />
            <wire x2="1472" y1="816" y2="1408" x1="1472" />
            <wire x2="1696" y1="1408" y2="1408" x1="1472" />
            <wire x2="1472" y1="1408" y2="1632" x1="1472" />
            <wire x2="1696" y1="1632" y2="1632" x1="1472" />
            <wire x2="1696" y1="592" y2="592" x1="1472" />
            <wire x2="1472" y1="592" y2="720" x1="1472" />
        </branch>
        <branch name="XLXN_508">
            <wire x2="1072" y1="560" y2="560" x1="1056" />
            <wire x2="1168" y1="560" y2="560" x1="1072" />
            <wire x2="1072" y1="320" y2="560" x1="1072" />
            <wire x2="1536" y1="320" y2="320" x1="1072" />
            <wire x2="1696" y1="320" y2="320" x1="1536" />
            <wire x2="1536" y1="320" y2="752" x1="1536" />
            <wire x2="1696" y1="752" y2="752" x1="1536" />
            <wire x2="1536" y1="752" y2="1136" x1="1536" />
            <wire x2="1696" y1="1136" y2="1136" x1="1536" />
            <wire x2="1536" y1="1136" y2="1568" x1="1536" />
            <wire x2="1696" y1="1568" y2="1568" x1="1536" />
        </branch>
        <branch name="XLXN_513">
            <wire x2="1120" y1="1072" y2="1072" x1="1056" />
            <wire x2="1168" y1="1072" y2="1072" x1="1120" />
            <wire x2="1120" y1="1072" y2="1264" x1="1120" />
            <wire x2="1120" y1="1264" y2="1472" x1="1120" />
            <wire x2="1696" y1="1472" y2="1472" x1="1120" />
            <wire x2="1120" y1="1472" y2="1696" x1="1120" />
            <wire x2="1696" y1="1696" y2="1696" x1="1120" />
            <wire x2="1696" y1="1264" y2="1264" x1="1120" />
            <wire x2="1120" y1="992" y2="1072" x1="1120" />
            <wire x2="1600" y1="992" y2="992" x1="1120" />
            <wire x2="1600" y1="992" y2="1072" x1="1600" />
            <wire x2="1696" y1="1072" y2="1072" x1="1600" />
        </branch>
        <branch name="O(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="1664" type="branch" />
            <wire x2="2384" y1="1664" y2="1664" x1="2352" />
        </branch>
        <branch name="O(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="1232" type="branch" />
            <wire x2="2384" y1="1232" y2="1232" x1="2352" />
        </branch>
        <branch name="O(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="1040" type="branch" />
            <wire x2="2384" y1="1040" y2="1040" x1="2352" />
        </branch>
        <branch name="O(5)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2368" y="624" type="branch" />
            <wire x2="2368" y1="624" y2="624" x1="2352" />
        </branch>
        <branch name="O(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="1440" type="branch" />
            <wire x2="2384" y1="1440" y2="1440" x1="2352" />
        </branch>
        <branch name="O(4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="848" type="branch" />
            <wire x2="2384" y1="848" y2="848" x1="2352" />
        </branch>
        <branch name="I(2:0)">
            <wire x2="736" y1="288" y2="288" x1="512" />
        </branch>
        <branch name="O(7:0)">
            <wire x2="2560" y1="960" y2="960" x1="2400" />
        </branch>
        <iomarker fontsize="28" x="512" y="288" name="I(2:0)" orien="R180" />
        <iomarker fontsize="28" x="2560" y="960" name="O(7:0)" orien="R0" />
    </sheet>
</drawing>