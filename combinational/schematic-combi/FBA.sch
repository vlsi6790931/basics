<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="B(1)" />
        <signal name="A(1)" />
        <signal name="B(2)" />
        <signal name="A(2)" />
        <signal name="B(3)" />
        <signal name="A(3)" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="B(0)" />
        <signal name="A(0)" />
        <signal name="S(3)" />
        <signal name="S(2)" />
        <signal name="S(0)" />
        <signal name="S(3:0)" />
        <signal name="Cin" />
        <signal name="B(3:0)" />
        <signal name="A(3:0)" />
        <signal name="S(1)" />
        <signal name="C" />
        <signal name="reset" />
        <port polarity="Output" name="S(3:0)" />
        <port polarity="Input" name="Cin" />
        <port polarity="Input" name="B(3:0)" />
        <port polarity="Input" name="A(3:0)" />
        <port polarity="Output" name="C" />
        <port polarity="Input" name="reset" />
        <blockdef name="FA">
            <timestamp>2023-8-11T19:21:31</timestamp>
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
            <line x2="192" y1="-64" y2="0" x1="192" />
        </blockdef>
        <block symbolname="FA" name="XLXI_15">
            <blockpin signalname="A(3)" name="Ain" />
            <blockpin signalname="B(3)" name="Bin" />
            <blockpin signalname="XLXN_20" name="Cin" />
            <blockpin signalname="C" name="Cout" />
            <blockpin signalname="S(3)" name="sum" />
            <blockpin signalname="reset" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_16">
            <blockpin signalname="A(2)" name="Ain" />
            <blockpin signalname="B(2)" name="Bin" />
            <blockpin signalname="XLXN_19" name="Cin" />
            <blockpin signalname="XLXN_20" name="Cout" />
            <blockpin signalname="S(2)" name="sum" />
            <blockpin signalname="reset" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_17">
            <blockpin signalname="A(1)" name="Ain" />
            <blockpin signalname="B(1)" name="Bin" />
            <blockpin signalname="XLXN_18" name="Cin" />
            <blockpin signalname="XLXN_19" name="Cout" />
            <blockpin signalname="S(1)" name="sum" />
            <blockpin signalname="reset" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_18">
            <blockpin signalname="A(0)" name="Ain" />
            <blockpin signalname="B(0)" name="Bin" />
            <blockpin signalname="Cin" name="Cin" />
            <blockpin signalname="XLXN_18" name="Cout" />
            <blockpin signalname="S(0)" name="sum" />
            <blockpin signalname="reset" name="reset" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <text style="fontsize:50;fontname:Arial" x="936" y="104">4-BIT ADDER</text>
        <branch name="B(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1120" type="branch" />
            <wire x2="1008" y1="1120" y2="1120" x1="976" />
        </branch>
        <branch name="A(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1056" type="branch" />
            <wire x2="1008" y1="1056" y2="1056" x1="976" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="1008" y1="1184" y2="1328" x1="1008" />
            <wire x2="1328" y1="1328" y2="1328" x1="1008" />
            <wire x2="1328" y1="1328" y2="1392" x1="1328" />
        </branch>
        <branch name="B(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1456" type="branch" />
            <wire x2="1008" y1="1456" y2="1456" x1="976" />
        </branch>
        <branch name="A(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1392" type="branch" />
            <wire x2="1008" y1="1392" y2="1392" x1="976" />
        </branch>
        <branch name="S(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="416" type="branch" />
            <wire x2="1408" y1="416" y2="416" x1="1328" />
            <wire x2="1424" y1="416" y2="416" x1="1408" />
            <wire x2="1424" y1="416" y2="464" x1="1424" />
        </branch>
        <branch name="S(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="768" type="branch" />
            <wire x2="1408" y1="768" y2="768" x1="1328" />
            <wire x2="1424" y1="768" y2="768" x1="1408" />
            <wire x2="1424" y1="768" y2="816" x1="1424" />
        </branch>
        <branch name="S(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="1456" type="branch" />
            <wire x2="1408" y1="1456" y2="1456" x1="1328" />
            <wire x2="1424" y1="1456" y2="1456" x1="1408" />
            <wire x2="1424" y1="1456" y2="1504" x1="1424" />
        </branch>
        <branch name="S(3:0)">
            <wire x2="1664" y1="432" y2="432" x1="1520" />
            <wire x2="1520" y1="432" y2="464" x1="1520" />
            <wire x2="1520" y1="464" y2="816" x1="1520" />
            <wire x2="1520" y1="816" y2="1168" x1="1520" />
            <wire x2="1520" y1="1168" y2="1504" x1="1520" />
            <wire x2="1520" y1="1504" y2="1584" x1="1520" />
        </branch>
        <bustap x2="1424" y1="1168" y2="1168" x1="1520" />
        <bustap x2="1424" y1="1504" y2="1504" x1="1520" />
        <bustap x2="1424" y1="816" y2="816" x1="1520" />
        <bustap x2="1424" y1="464" y2="464" x1="1520" />
        <branch name="Cin">
            <wire x2="1008" y1="1520" y2="1520" x1="416" />
        </branch>
        <rect width="880" x="712" y="168" height="1544" />
        <branch name="B(3:0)">
            <wire x2="560" y1="1056" y2="1056" x1="464" />
        </branch>
        <branch name="A(3:0)">
            <wire x2="560" y1="720" y2="720" x1="464" />
        </branch>
        <branch name="S(1)">
            <wire x2="1408" y1="1120" y2="1120" x1="1328" />
            <wire x2="1408" y1="1120" y2="1168" x1="1408" />
            <wire x2="1424" y1="1168" y2="1168" x1="1408" />
        </branch>
        <branch name="C">
            <wire x2="1728" y1="352" y2="352" x1="1328" />
        </branch>
        <iomarker fontsize="28" x="1664" y="432" name="S(3:0)" orien="R0" />
        <iomarker fontsize="28" x="464" y="1056" name="B(3:0)" orien="R180" />
        <iomarker fontsize="28" x="464" y="720" name="A(3:0)" orien="R180" />
        <iomarker fontsize="28" x="1728" y="352" name="C" orien="R0" />
        <iomarker fontsize="28" x="416" y="1520" name="Cin" orien="R180" />
        <branch name="XLXN_20">
            <wire x2="1008" y1="480" y2="624" x1="1008" />
            <wire x2="1328" y1="624" y2="624" x1="1008" />
            <wire x2="1328" y1="624" y2="704" x1="1328" />
        </branch>
        <branch name="B(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="416" type="branch" />
            <wire x2="1008" y1="416" y2="416" x1="976" />
        </branch>
        <branch name="A(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="352" type="branch" />
            <wire x2="1008" y1="352" y2="352" x1="976" />
        </branch>
        <iomarker fontsize="28" x="448" y="896" name="reset" orien="R180" />
        <branch name="XLXN_19">
            <wire x2="1008" y1="832" y2="976" x1="1008" />
            <wire x2="1328" y1="976" y2="976" x1="1008" />
            <wire x2="1328" y1="976" y2="1056" x1="1328" />
        </branch>
        <branch name="B(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="768" type="branch" />
            <wire x2="1008" y1="768" y2="768" x1="976" />
        </branch>
        <branch name="A(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="704" type="branch" />
            <wire x2="1008" y1="704" y2="704" x1="976" />
        </branch>
        <branch name="reset">
            <wire x2="784" y1="896" y2="896" x1="448" />
            <wire x2="784" y1="896" y2="944" x1="784" />
            <wire x2="784" y1="944" y2="1312" x1="784" />
            <wire x2="784" y1="1312" y2="1648" x1="784" />
            <wire x2="1200" y1="1648" y2="1648" x1="784" />
            <wire x2="1200" y1="1312" y2="1312" x1="784" />
            <wire x2="1200" y1="944" y2="944" x1="784" />
            <wire x2="784" y1="640" y2="896" x1="784" />
            <wire x2="1200" y1="640" y2="640" x1="784" />
            <wire x2="1200" y1="576" y2="640" x1="1200" />
            <wire x2="1200" y1="928" y2="944" x1="1200" />
            <wire x2="1200" y1="1280" y2="1312" x1="1200" />
            <wire x2="1200" y1="1616" y2="1648" x1="1200" />
        </branch>
        <instance x="1008" y="576" name="XLXI_15" orien="R0">
        </instance>
        <instance x="1008" y="928" name="XLXI_16" orien="R0">
        </instance>
        <instance x="1008" y="1280" name="XLXI_17" orien="R0">
        </instance>
        <instance x="1008" y="1616" name="XLXI_18" orien="R0">
        </instance>
        <text style="fontsize:28;fontname:Arial;textcolor:rgb(0,0,128)" x="1396" y="1148">S(1)</text>
    </sheet>
</drawing>