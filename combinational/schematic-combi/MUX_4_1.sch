<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_9" />
        <signal name="XLXN_18" />
        <signal name="XLXN_21" />
        <signal name="XLXN_24" />
        <signal name="XLXN_6" />
        <signal name="XLXN_37" />
        <signal name="reset" />
        <signal name="I(0)" />
        <signal name="I(1)" />
        <signal name="I(2)" />
        <signal name="I(3)" />
        <signal name="S1" />
        <signal name="O" />
        <signal name="S0" />
        <signal name="XLXN_46" />
        <signal name="XLXN_33" />
        <signal name="I(3:0)" />
        <port polarity="Input" name="reset" />
        <port polarity="Input" name="S1" />
        <port polarity="Output" name="O" />
        <port polarity="Input" name="S0" />
        <port polarity="Input" name="I(3:0)" />
        <blockdef name="MUX_2_1">
            <timestamp>2023-8-11T1:39:32</timestamp>
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="160" y1="-64" y2="0" x1="160" />
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
        </blockdef>
        <block symbolname="MUX_2_1" name="XLXI_2">
            <blockpin signalname="I(2)" name="I0" />
            <blockpin signalname="I(3)" name="I1" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="S0" name="S" />
            <blockpin signalname="XLXN_33" name="O" />
        </block>
        <block symbolname="MUX_2_1" name="XLXI_3">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="S1" name="S" />
            <blockpin signalname="O" name="O" />
        </block>
        <block symbolname="MUX_2_1" name="XLXI_1">
            <blockpin signalname="I(0)" name="I0" />
            <blockpin signalname="I(1)" name="I1" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="S0" name="S" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="624" y="1056" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1120" y="816" name="XLXI_3" orien="R0">
        </instance>
        <instance x="624" y="672" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_6">
            <wire x2="1024" y1="512" y2="512" x1="944" />
            <wire x2="1024" y1="512" y2="592" x1="1024" />
            <wire x2="1120" y1="592" y2="592" x1="1024" />
        </branch>
        <branch name="reset">
            <wire x2="592" y1="720" y2="720" x1="480" />
            <wire x2="1120" y1="720" y2="720" x1="592" />
            <wire x2="592" y1="720" y2="960" x1="592" />
            <wire x2="624" y1="960" y2="960" x1="592" />
            <wire x2="624" y1="576" y2="576" x1="592" />
            <wire x2="592" y1="576" y2="720" x1="592" />
        </branch>
        <branch name="I(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="480" y="448" type="branch" />
            <wire x2="624" y1="448" y2="448" x1="480" />
        </branch>
        <branch name="I(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="480" y="512" type="branch" />
            <wire x2="624" y1="512" y2="512" x1="480" />
        </branch>
        <branch name="I(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="480" y="832" type="branch" />
            <wire x2="624" y1="832" y2="832" x1="480" />
        </branch>
        <branch name="I(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="480" y="896" type="branch" />
            <wire x2="624" y1="896" y2="896" x1="480" />
        </branch>
        <branch name="S1">
            <wire x2="1056" y1="1056" y2="1184" x1="1056" />
            <wire x2="1280" y1="1056" y2="1056" x1="1056" />
            <wire x2="1280" y1="816" y2="1056" x1="1280" />
        </branch>
        <branch name="O">
            <wire x2="1472" y1="656" y2="656" x1="1440" />
        </branch>
        <branch name="S0">
            <wire x2="784" y1="672" y2="688" x1="784" />
            <wire x2="992" y1="688" y2="688" x1="784" />
            <wire x2="992" y1="688" y2="1056" x1="992" />
            <wire x2="992" y1="1056" y2="1184" x1="992" />
            <wire x2="992" y1="1056" y2="1056" x1="784" />
        </branch>
        <branch name="XLXN_33">
            <wire x2="1072" y1="896" y2="896" x1="944" />
            <wire x2="1120" y1="656" y2="656" x1="1072" />
            <wire x2="1072" y1="656" y2="896" x1="1072" />
        </branch>
        <branch name="I(3:0)">
            <wire x2="624" y1="304" y2="304" x1="320" />
        </branch>
        <iomarker fontsize="28" x="480" y="720" name="reset" orien="R180" />
        <iomarker fontsize="28" x="1472" y="656" name="O" orien="R0" />
        <iomarker fontsize="28" x="992" y="1184" name="S0" orien="R90" />
        <iomarker fontsize="28" x="1056" y="1184" name="S1" orien="R90" />
        <iomarker fontsize="28" x="320" y="304" name="I(3:0)" orien="R180" />
    </sheet>
</drawing>