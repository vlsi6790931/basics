<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="reset" />
        <signal name="XLXN_43" />
        <signal name="XLXN_48" />
        <signal name="XLXN_69" />
        <signal name="XLXN_71" />
        <signal name="XLXN_72" />
        <signal name="XLXN_73" />
        <signal name="XLXN_70" />
        <signal name="XLXN_85" />
        <signal name="XLXN_87" />
        <signal name="XLXN_88" />
        <signal name="XLXN_89" />
        <signal name="XLXN_90" />
        <signal name="XLXN_91" />
        <signal name="XLXN_100" />
        <signal name="XLXN_102" />
        <signal name="XLXN_103" />
        <signal name="XLXN_116" />
        <signal name="XLXN_117" />
        <signal name="XLXN_118" />
        <signal name="XLXN_119" />
        <signal name="XLXN_122" />
        <signal name="XLXN_133" />
        <signal name="XLXN_135" />
        <signal name="XLXN_138" />
        <signal name="XLXN_142" />
        <signal name="XLXN_177" />
        <signal name="XLXN_180" />
        <signal name="XLXN_184" />
        <signal name="XLXN_190" />
        <signal name="XLXN_197" />
        <signal name="XLXN_47" />
        <signal name="O0" />
        <signal name="XLXN_49" />
        <signal name="O1" />
        <signal name="XLXN_205" />
        <signal name="XLXN_207" />
        <signal name="I(1)" />
        <signal name="XLXN_210" />
        <signal name="I(3)" />
        <signal name="I(2)" />
        <signal name="XLXN_214" />
        <signal name="XLXN_220" />
        <signal name="XLXN_167" />
        <signal name="XLXN_227" />
        <signal name="XLXN_168" />
        <signal name="I(3:0)" />
        <signal name="XLXN_230" />
        <signal name="XLXN_115" />
        <signal name="XLXN_233" />
        <signal name="XLXN_120" />
        <signal name="XLXN_237" />
        <signal name="I(0)" />
        <signal name="XLXN_127" />
        <signal name="XLXN_128" />
        <signal name="XLXN_129" />
        <signal name="XLXN_130" />
        <port polarity="Input" name="reset" />
        <port polarity="Output" name="O0" />
        <port polarity="Output" name="O1" />
        <port polarity="Input" name="I(3:0)" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="tribuff">
            <timestamp>2023-8-11T11:20:44</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="xor4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="60" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="208" y1="-160" y2="-160" x1="256" />
            <arc ex="64" ey="-208" sx="64" sy="-112" r="56" cx="32" cy="-160" />
            <line x2="64" y1="-208" y2="-208" x1="128" />
            <line x2="64" y1="-112" y2="-112" x1="128" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <arc ex="128" ey="-208" sx="208" sy="-160" r="88" cx="132" cy="-120" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="208" ey="-160" sx="128" sy="-112" r="88" cx="132" cy="-200" />
        </blockdef>
        <block symbolname="and2" name="XLXI_25">
            <blockpin signalname="reset" name="I0" />
            <blockpin signalname="XLXN_47" name="I1" />
            <blockpin signalname="O0" name="O" />
        </block>
        <block symbolname="tribuff" name="XLXI_38">
            <blockpin signalname="XLXN_167" name="I" />
            <blockpin signalname="XLXN_43" name="E" />
            <blockpin signalname="XLXN_47" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_39">
            <blockpin signalname="XLXN_49" name="I0" />
            <blockpin signalname="reset" name="I1" />
            <blockpin signalname="O1" name="O" />
        </block>
        <block symbolname="tribuff" name="XLXI_40">
            <blockpin signalname="XLXN_168" name="I" />
            <blockpin signalname="XLXN_43" name="E" />
            <blockpin signalname="XLXN_49" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_102">
            <blockpin signalname="I(2)" name="I0" />
            <blockpin signalname="I(3)" name="I1" />
            <blockpin signalname="XLXN_168" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_103">
            <blockpin signalname="I(1)" name="I0" />
            <blockpin signalname="I(3)" name="I1" />
            <blockpin signalname="XLXN_167" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_72">
            <blockpin signalname="I(1)" name="I0" />
            <blockpin signalname="I(0)" name="I1" />
            <blockpin signalname="XLXN_115" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_73">
            <blockpin signalname="I(3)" name="I0" />
            <blockpin signalname="I(2)" name="I1" />
            <blockpin signalname="XLXN_120" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_80">
            <blockpin signalname="XLXN_120" name="I0" />
            <blockpin signalname="I(1)" name="I1" />
            <blockpin signalname="XLXN_128" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_81">
            <blockpin signalname="XLXN_120" name="I0" />
            <blockpin signalname="I(0)" name="I1" />
            <blockpin signalname="XLXN_127" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_82">
            <blockpin signalname="I(3)" name="I0" />
            <blockpin signalname="XLXN_115" name="I1" />
            <blockpin signalname="XLXN_130" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_83">
            <blockpin signalname="I(2)" name="I0" />
            <blockpin signalname="XLXN_115" name="I1" />
            <blockpin signalname="XLXN_129" name="O" />
        </block>
        <block symbolname="xor4" name="XLXI_92">
            <blockpin signalname="XLXN_130" name="I0" />
            <blockpin signalname="XLXN_129" name="I1" />
            <blockpin signalname="XLXN_128" name="I2" />
            <blockpin signalname="XLXN_127" name="I3" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <branch name="XLXN_47">
            <wire x2="2144" y1="672" y2="672" x1="2112" />
        </branch>
        <instance x="2144" y="800" name="XLXI_25" orien="R0" />
        <branch name="O0">
            <wire x2="2448" y1="704" y2="704" x1="2400" />
        </branch>
        <instance x="1728" y="768" name="XLXI_38" orien="R0">
        </instance>
        <instance x="2176" y="1328" name="XLXI_39" orien="R0" />
        <instance x="1760" y="1360" name="XLXI_40" orien="R0">
        </instance>
        <branch name="XLXN_49">
            <wire x2="2176" y1="1264" y2="1264" x1="2144" />
        </branch>
        <branch name="O1">
            <wire x2="2512" y1="1232" y2="1232" x1="2432" />
        </branch>
        <branch name="reset">
            <wire x2="2128" y1="976" y2="976" x1="1968" />
            <wire x2="2128" y1="976" y2="1200" x1="2128" />
            <wire x2="2176" y1="1200" y2="1200" x1="2128" />
            <wire x2="2144" y1="736" y2="736" x1="2128" />
            <wire x2="2128" y1="736" y2="976" x1="2128" />
        </branch>
        <instance x="992" y="1712" name="XLXI_102" orien="R0" />
        <branch name="I(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1584" type="branch" />
            <wire x2="992" y1="1584" y2="1584" x1="912" />
        </branch>
        <branch name="I(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1648" type="branch" />
            <wire x2="992" y1="1648" y2="1648" x1="912" />
        </branch>
        <branch name="I(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1424" type="branch" />
            <wire x2="992" y1="1424" y2="1424" x1="912" />
        </branch>
        <branch name="I(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1488" type="branch" />
            <wire x2="992" y1="1488" y2="1488" x1="912" />
        </branch>
        <instance x="992" y="1552" name="XLXI_103" orien="R0" />
        <branch name="XLXN_167">
            <wire x2="1488" y1="1456" y2="1456" x1="1248" />
            <wire x2="1488" y1="672" y2="1456" x1="1488" />
            <wire x2="1728" y1="672" y2="672" x1="1488" />
        </branch>
        <branch name="XLXN_168">
            <wire x2="1632" y1="1616" y2="1616" x1="1248" />
            <wire x2="1760" y1="1264" y2="1264" x1="1632" />
            <wire x2="1632" y1="1264" y2="1616" x1="1632" />
        </branch>
        <branch name="I(3:0)">
            <wire x2="544" y1="1296" y2="1296" x1="336" />
        </branch>
        <iomarker fontsize="28" x="2448" y="704" name="O0" orien="R0" />
        <iomarker fontsize="28" x="2512" y="1232" name="O1" orien="R0" />
        <iomarker fontsize="28" x="336" y="1296" name="I(3:0)" orien="R180" />
        <iomarker fontsize="28" x="1968" y="976" name="reset" orien="R180" />
        <instance x="464" y="768" name="XLXI_72" orien="R0" />
        <branch name="I(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="368" y="704" type="branch" />
            <wire x2="448" y1="704" y2="704" x1="368" />
            <wire x2="464" y1="704" y2="704" x1="448" />
            <wire x2="816" y1="560" y2="560" x1="448" />
            <wire x2="816" y1="560" y2="672" x1="816" />
            <wire x2="832" y1="672" y2="672" x1="816" />
            <wire x2="448" y1="560" y2="704" x1="448" />
        </branch>
        <instance x="464" y="992" name="XLXI_73" orien="R0" />
        <instance x="832" y="800" name="XLXI_80" orien="R0" />
        <instance x="832" y="576" name="XLXI_81" orien="R0" />
        <instance x="848" y="1264" name="XLXI_82" orien="R0" />
        <instance x="848" y="1040" name="XLXI_83" orien="R0" />
        <branch name="XLXN_115">
            <wire x2="800" y1="672" y2="672" x1="720" />
            <wire x2="800" y1="672" y2="912" x1="800" />
            <wire x2="848" y1="912" y2="912" x1="800" />
            <wire x2="800" y1="912" y2="1136" x1="800" />
            <wire x2="848" y1="1136" y2="1136" x1="800" />
        </branch>
        <branch name="XLXN_120">
            <wire x2="736" y1="896" y2="896" x1="720" />
            <wire x2="736" y1="512" y2="736" x1="736" />
            <wire x2="736" y1="736" y2="896" x1="736" />
            <wire x2="832" y1="736" y2="736" x1="736" />
            <wire x2="832" y1="512" y2="512" x1="736" />
        </branch>
        <branch name="I(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="368" y="928" type="branch" />
            <wire x2="400" y1="928" y2="928" x1="368" />
            <wire x2="464" y1="928" y2="928" x1="400" />
            <wire x2="400" y1="928" y2="1200" x1="400" />
            <wire x2="848" y1="1200" y2="1200" x1="400" />
        </branch>
        <branch name="I(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="368" y="864" type="branch" />
            <wire x2="432" y1="864" y2="864" x1="368" />
            <wire x2="464" y1="864" y2="864" x1="432" />
            <wire x2="432" y1="864" y2="976" x1="432" />
            <wire x2="848" y1="976" y2="976" x1="432" />
        </branch>
        <branch name="I(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="352" y="640" type="branch" />
            <wire x2="416" y1="640" y2="640" x1="352" />
            <wire x2="464" y1="640" y2="640" x1="416" />
            <wire x2="832" y1="448" y2="448" x1="416" />
            <wire x2="416" y1="448" y2="640" x1="416" />
        </branch>
        <branch name="XLXN_127">
            <wire x2="1216" y1="480" y2="480" x1="1088" />
            <wire x2="1216" y1="480" y2="720" x1="1216" />
        </branch>
        <branch name="XLXN_128">
            <wire x2="1152" y1="704" y2="704" x1="1088" />
            <wire x2="1152" y1="704" y2="784" x1="1152" />
            <wire x2="1216" y1="784" y2="784" x1="1152" />
        </branch>
        <branch name="XLXN_129">
            <wire x2="1152" y1="944" y2="944" x1="1104" />
            <wire x2="1152" y1="848" y2="944" x1="1152" />
            <wire x2="1216" y1="848" y2="848" x1="1152" />
        </branch>
        <branch name="XLXN_130">
            <wire x2="1216" y1="1168" y2="1168" x1="1104" />
            <wire x2="1216" y1="912" y2="1168" x1="1216" />
        </branch>
        <instance x="1216" y="976" name="XLXI_92" orien="R0" />
        <branch name="XLXN_43">
            <wire x2="1584" y1="816" y2="816" x1="1472" />
            <wire x2="1584" y1="816" y2="864" x1="1584" />
            <wire x2="1696" y1="864" y2="864" x1="1584" />
            <wire x2="1696" y1="864" y2="1328" x1="1696" />
            <wire x2="1760" y1="1328" y2="1328" x1="1696" />
            <wire x2="1728" y1="736" y2="736" x1="1696" />
            <wire x2="1696" y1="736" y2="864" x1="1696" />
        </branch>
        <text style="fontsize:150;fontname:Arial" x="896" y="196">ENCODER 4:2</text>
    </sheet>
</drawing>