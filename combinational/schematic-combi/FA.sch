<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_20" />
        <signal name="XLXN_18" />
        <signal name="XLXN_3" />
        <signal name="Cin" />
        <signal name="XLXN_22" />
        <signal name="XLXN_21" />
        <signal name="reset" />
        <signal name="Ain" />
        <signal name="Bin" />
        <signal name="wire1" />
        <signal name="XLXN_55" />
        <signal name="Cout" />
        <signal name="sum" />
        <port polarity="Input" name="Cin" />
        <port polarity="Input" name="reset" />
        <port polarity="Input" name="Ain" />
        <port polarity="Input" name="Bin" />
        <port polarity="Output" name="Cout" />
        <port polarity="Output" name="sum" />
        <blockdef name="HA">
            <timestamp>2023-6-29T23:12:55</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="176" x="64" y="-156" height="156" />
            <line x2="304" y1="-128" y2="-128" x1="240" />
            <line x2="304" y1="-32" y2="-32" x1="240" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="HA" name="XLXI_1">
            <blockpin signalname="XLXN_22" name="B" />
            <blockpin signalname="XLXN_21" name="A" />
            <blockpin signalname="XLXN_3" name="C" />
            <blockpin signalname="XLXN_18" name="S" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="Bin" name="I0" />
            <blockpin signalname="reset" name="I1" />
            <blockpin signalname="XLXN_22" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="reset" name="I0" />
            <blockpin signalname="Ain" name="I1" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_2">
            <blockpin signalname="Cin" name="B" />
            <blockpin signalname="XLXN_18" name="A" />
            <blockpin signalname="XLXN_20" name="C" />
            <blockpin signalname="wire1" name="S" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="XLXN_20" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_55" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_29">
            <blockpin signalname="reset" name="I0" />
            <blockpin signalname="wire1" name="I1" />
            <blockpin signalname="sum" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_28">
            <blockpin signalname="XLXN_55" name="I0" />
            <blockpin signalname="reset" name="I1" />
            <blockpin signalname="Cout" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <text style="fontsize:64;fontname:Arial" x="820" y="248">1-BIT FULL ADDER</text>
        <instance x="944" y="896" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_20">
            <wire x2="1584" y1="864" y2="864" x1="1568" />
            <wire x2="1584" y1="832" y2="848" x1="1584" />
            <wire x2="1584" y1="848" y2="864" x1="1584" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="1264" y1="864" y2="864" x1="1248" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1264" y1="768" y2="768" x1="1248" />
            <wire x2="1584" y1="768" y2="768" x1="1264" />
        </branch>
        <branch name="Cin">
            <wire x2="1248" y1="960" y2="960" x1="448" />
            <wire x2="1264" y1="960" y2="960" x1="1248" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="928" y1="864" y2="864" x1="912" />
            <wire x2="944" y1="864" y2="864" x1="928" />
        </branch>
        <instance x="656" y="960" name="XLXI_7" orien="R0" />
        <branch name="XLXN_21">
            <wire x2="928" y1="720" y2="720" x1="912" />
            <wire x2="928" y1="720" y2="768" x1="928" />
            <wire x2="944" y1="768" y2="768" x1="928" />
        </branch>
        <instance x="656" y="816" name="XLXI_6" orien="R0" />
        <branch name="Ain">
            <wire x2="640" y1="688" y2="688" x1="448" />
            <wire x2="656" y1="688" y2="688" x1="640" />
        </branch>
        <branch name="Bin">
            <wire x2="640" y1="896" y2="896" x1="448" />
            <wire x2="656" y1="896" y2="896" x1="640" />
        </branch>
        <instance x="1264" y="992" name="XLXI_2" orien="R0">
        </instance>
        <branch name="wire1">
            <wire x2="1840" y1="960" y2="960" x1="1568" />
            <wire x2="1856" y1="960" y2="960" x1="1840" />
        </branch>
        <instance x="1584" y="896" name="XLXI_5" orien="R0" />
        <instance x="1856" y="1088" name="XLXI_29" orien="R0" />
        <instance x="1872" y="752" name="XLXI_28" orien="R0" />
        <branch name="XLXN_55">
            <wire x2="1856" y1="800" y2="800" x1="1840" />
            <wire x2="1872" y1="688" y2="688" x1="1856" />
            <wire x2="1856" y1="688" y2="800" x1="1856" />
        </branch>
        <branch name="Cout">
            <wire x2="2160" y1="656" y2="656" x1="2128" />
        </branch>
        <branch name="sum">
            <wire x2="2144" y1="992" y2="992" x1="2112" />
        </branch>
        <iomarker fontsize="28" x="2160" y="656" name="Cout" orien="R0" />
        <iomarker fontsize="28" x="2144" y="992" name="sum" orien="R0" />
        <branch name="reset">
            <wire x2="640" y1="800" y2="800" x1="464" />
            <wire x2="640" y1="800" y2="832" x1="640" />
            <wire x2="656" y1="832" y2="832" x1="640" />
            <wire x2="608" y1="624" y2="752" x1="608" />
            <wire x2="640" y1="752" y2="752" x1="608" />
            <wire x2="656" y1="752" y2="752" x1="640" />
            <wire x2="640" y1="752" y2="800" x1="640" />
            <wire x2="1872" y1="624" y2="624" x1="608" />
            <wire x2="640" y1="832" y2="832" x1="608" />
            <wire x2="608" y1="832" y2="1024" x1="608" />
            <wire x2="1856" y1="1024" y2="1024" x1="608" />
        </branch>
        <iomarker fontsize="28" x="448" y="688" name="Ain" orien="R180" />
        <iomarker fontsize="28" x="464" y="800" name="reset" orien="R180" />
        <iomarker fontsize="28" x="448" y="896" name="Bin" orien="R180" />
        <iomarker fontsize="28" x="448" y="960" name="Cin" orien="R180" />
        <rect width="1572" x="524" y="520" height="660" />
    </sheet>
</drawing>