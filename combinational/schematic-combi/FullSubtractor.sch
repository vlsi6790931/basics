<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="RESET" />
        <signal name="XLXN_4" />
        <signal name="Bin" />
        <signal name="A" />
        <signal name="B" />
        <signal name="XLXN_10" />
        <signal name="XLXN_9" />
        <signal name="DIFF" />
        <signal name="Bout" />
        <port polarity="Input" name="RESET" />
        <port polarity="Input" name="Bin" />
        <port polarity="Input" name="A" />
        <port polarity="Input" name="B" />
        <port polarity="Output" name="DIFF" />
        <port polarity="Output" name="Bout" />
        <blockdef name="HalfSubtractor">
            <timestamp>2023-8-11T0:47:34</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-64" y2="-64" x1="320" />
            <line x2="384" y1="-128" y2="-128" x1="320" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="HalfSubtractor" name="XLXI_3">
            <blockpin signalname="A" name="A" />
            <blockpin signalname="B" name="B" />
            <blockpin signalname="RESET" name="R" />
            <blockpin signalname="XLXN_9" name="Borrow" />
            <blockpin signalname="XLXN_4" name="Diff" />
        </block>
        <block symbolname="HalfSubtractor" name="XLXI_4">
            <blockpin signalname="XLXN_4" name="A" />
            <blockpin signalname="Bin" name="B" />
            <blockpin signalname="RESET" name="R" />
            <blockpin signalname="XLXN_10" name="Borrow" />
            <blockpin signalname="DIFF" name="Diff" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="XLXN_9" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="Bout" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="320" y="880" name="XLXI_3" orien="R0">
        </instance>
        <branch name="RESET">
            <wire x2="304" y1="848" y2="848" x1="240" />
            <wire x2="320" y1="848" y2="848" x1="304" />
            <wire x2="304" y1="848" y2="944" x1="304" />
            <wire x2="688" y1="944" y2="944" x1="304" />
            <wire x2="816" y1="880" y2="880" x1="688" />
            <wire x2="688" y1="880" y2="944" x1="688" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="816" y1="752" y2="752" x1="704" />
        </branch>
        <instance x="816" y="912" name="XLXI_4" orien="R0">
        </instance>
        <branch name="Bin">
            <wire x2="800" y1="624" y2="624" x1="240" />
            <wire x2="800" y1="624" y2="816" x1="800" />
            <wire x2="816" y1="816" y2="816" x1="800" />
        </branch>
        <branch name="A">
            <wire x2="320" y1="720" y2="720" x1="240" />
        </branch>
        <branch name="B">
            <wire x2="320" y1="784" y2="784" x1="240" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="1216" y1="848" y2="848" x1="1200" />
            <wire x2="1216" y1="848" y2="944" x1="1216" />
            <wire x2="1296" y1="944" y2="944" x1="1216" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="720" y1="816" y2="816" x1="704" />
            <wire x2="720" y1="816" y2="1008" x1="720" />
            <wire x2="1296" y1="1008" y2="1008" x1="720" />
        </branch>
        <branch name="DIFF">
            <wire x2="1216" y1="784" y2="784" x1="1200" />
            <wire x2="1568" y1="784" y2="784" x1="1216" />
        </branch>
        <iomarker fontsize="28" x="240" y="848" name="RESET" orien="R180" />
        <iomarker fontsize="28" x="240" y="784" name="B" orien="R180" />
        <iomarker fontsize="28" x="240" y="720" name="A" orien="R180" />
        <iomarker fontsize="28" x="240" y="624" name="Bin" orien="R180" />
        <iomarker fontsize="28" x="1568" y="784" name="DIFF" orien="R0" />
        <text style="fontsize:160;fontname:Arial" x="92" y="220">FULL SUBTRACTOR</text>
        <branch name="Bout">
            <wire x2="1568" y1="976" y2="976" x1="1552" />
            <wire x2="1584" y1="976" y2="976" x1="1568" />
        </branch>
        <instance x="1296" y="1072" name="XLXI_5" orien="R0" />
        <text style="fontsize:25;fontname:Arial" x="1220" y="924">Bin(A^B)'</text>
        <text style="fontsize:25;fontname:Arial" x="1256" y="992">B(A)'</text>
        <iomarker fontsize="28" x="1584" y="976" name="Bout" orien="R0" />
    </sheet>
</drawing>