<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_36" />
        <signal name="XLXN_35" />
        <signal name="E" />
        <signal name="W" />
        <signal name="IA(0)" />
        <signal name="IB(0)" />
        <signal name="IA(1)" />
        <signal name="IB(1)" />
        <signal name="IA(1:0)" />
        <signal name="IB(1:0)" />
        <signal name="XLXN_1" />
        <signal name="XLXN_124" />
        <signal name="reset" />
        <signal name="XLXN_126" />
        <signal name="XLXN_44" />
        <signal name="XLXN_102" />
        <signal name="XLXN_103" />
        <signal name="XLXN_130" />
        <signal name="XLXN_100" />
        <signal name="XLXN_132" />
        <signal name="XLXN_101" />
        <signal name="L" />
        <signal name="XLXN_109" />
        <signal name="XLXN_136" />
        <signal name="XLXN_137" />
        <signal name="XLXN_34" />
        <port polarity="Output" name="E" />
        <port polarity="Output" name="W" />
        <port polarity="Input" name="IA(1:0)" />
        <port polarity="Input" name="IB(1:0)" />
        <port polarity="Input" name="reset" />
        <port polarity="Output" name="L" />
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="COMPONE">
            <timestamp>2023-8-12T10:49:4</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <block symbolname="COMPONE" name="XLXI_36">
            <blockpin signalname="XLXN_1" name="E" />
            <blockpin signalname="IA(0)" name="I1" />
            <blockpin signalname="IB(0)" name="I2" />
            <blockpin signalname="XLXN_36" name="L" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="XLXN_35" name="W" />
        </block>
        <block symbolname="COMPONE" name="XLXI_37">
            <blockpin signalname="XLXN_34" name="E" />
            <blockpin signalname="IA(1)" name="I1" />
            <blockpin signalname="IB(1)" name="I2" />
            <blockpin signalname="XLXN_101" name="L" />
            <blockpin signalname="reset" name="R" />
            <blockpin signalname="XLXN_100" name="W" />
        </block>
        <block symbolname="and2" name="XLXI_34">
            <blockpin signalname="XLXN_44" name="I0" />
            <blockpin signalname="reset" name="I1" />
            <blockpin signalname="W" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_57">
            <blockpin signalname="XLXN_34" name="I0" />
            <blockpin signalname="XLXN_35" name="I1" />
            <blockpin signalname="XLXN_103" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_58">
            <blockpin signalname="XLXN_34" name="I0" />
            <blockpin signalname="XLXN_36" name="I1" />
            <blockpin signalname="XLXN_102" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_59">
            <blockpin signalname="XLXN_100" name="I0" />
            <blockpin signalname="XLXN_103" name="I1" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_60">
            <blockpin signalname="XLXN_101" name="I0" />
            <blockpin signalname="XLXN_102" name="I1" />
            <blockpin signalname="XLXN_109" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_35">
            <blockpin signalname="reset" name="I0" />
            <blockpin signalname="XLXN_109" name="I1" />
            <blockpin signalname="L" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_8">
            <blockpin signalname="reset" name="I0" />
            <blockpin signalname="XLXN_34" name="I1" />
            <blockpin signalname="XLXN_1" name="I2" />
            <blockpin signalname="E" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <branch name="XLXN_36">
            <wire x2="1392" y1="816" y2="816" x1="880" />
            <wire x2="1392" y1="816" y2="1136" x1="1392" />
            <wire x2="1408" y1="1136" y2="1136" x1="1392" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="1376" y1="688" y2="688" x1="880" />
        </branch>
        <branch name="E">
            <wire x2="2448" y1="944" y2="944" x1="1264" />
        </branch>
        <branch name="W">
            <wire x2="2448" y1="688" y2="688" x1="2416" />
        </branch>
        <branch name="IA(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="688" type="branch" />
            <wire x2="496" y1="688" y2="688" x1="240" />
        </branch>
        <branch name="IB(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="752" type="branch" />
            <wire x2="496" y1="752" y2="752" x1="240" />
        </branch>
        <branch name="IA(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="272" y="1136" type="branch" />
            <wire x2="496" y1="1136" y2="1136" x1="272" />
        </branch>
        <branch name="IB(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="272" y="1200" type="branch" />
            <wire x2="496" y1="1200" y2="1200" x1="272" />
        </branch>
        <instance x="496" y="848" name="XLXI_36" orien="R0">
        </instance>
        <instance x="496" y="1296" name="XLXI_37" orien="R0">
        </instance>
        <branch name="IA(1:0)">
            <wire x2="464" y1="288" y2="288" x1="288" />
        </branch>
        <branch name="IB(1:0)">
            <wire x2="464" y1="400" y2="400" x1="288" />
        </branch>
        <branch name="XLXN_1">
            <wire x2="1008" y1="752" y2="752" x1="880" />
            <wire x2="1008" y1="752" y2="880" x1="1008" />
        </branch>
        <branch name="reset">
            <wire x2="480" y1="1328" y2="1328" x1="352" />
            <wire x2="2208" y1="1328" y2="1328" x1="480" />
            <wire x2="480" y1="576" y2="816" x1="480" />
            <wire x2="496" y1="816" y2="816" x1="480" />
            <wire x2="480" y1="816" y2="1008" x1="480" />
            <wire x2="480" y1="1008" y2="1264" x1="480" />
            <wire x2="496" y1="1264" y2="1264" x1="480" />
            <wire x2="480" y1="1264" y2="1328" x1="480" />
            <wire x2="1008" y1="1008" y2="1008" x1="480" />
            <wire x2="2160" y1="576" y2="576" x1="480" />
            <wire x2="2160" y1="576" y2="656" x1="2160" />
            <wire x2="2208" y1="1264" y2="1328" x1="2208" />
        </branch>
        <instance x="2160" y="784" name="XLXI_34" orien="R0" />
        <branch name="XLXN_44">
            <wire x2="2144" y1="752" y2="752" x1="2112" />
            <wire x2="2160" y1="720" y2="720" x1="2144" />
            <wire x2="2144" y1="720" y2="752" x1="2144" />
        </branch>
        <instance x="1376" y="816" name="XLXI_57" orien="R0" />
        <instance x="1408" y="1264" name="XLXI_58" orien="R0" />
        <branch name="XLXN_102">
            <wire x2="1888" y1="1168" y2="1168" x1="1664" />
        </branch>
        <branch name="XLXN_103">
            <wire x2="1856" y1="720" y2="720" x1="1632" />
        </branch>
        <instance x="1856" y="848" name="XLXI_59" orien="R0" />
        <branch name="XLXN_100">
            <wire x2="1696" y1="1040" y2="1040" x1="880" />
            <wire x2="880" y1="1040" y2="1136" x1="880" />
            <wire x2="1856" y1="784" y2="784" x1="1696" />
            <wire x2="1696" y1="784" y2="1040" x1="1696" />
        </branch>
        <instance x="1888" y="1296" name="XLXI_60" orien="R0" />
        <branch name="XLXN_101">
            <wire x2="896" y1="1264" y2="1264" x1="880" />
            <wire x2="1888" y1="1232" y2="1232" x1="896" />
            <wire x2="896" y1="1232" y2="1264" x1="896" />
        </branch>
        <branch name="L">
            <wire x2="2480" y1="1232" y2="1232" x1="2464" />
        </branch>
        <branch name="XLXN_109">
            <wire x2="2208" y1="1200" y2="1200" x1="2144" />
        </branch>
        <instance x="2208" y="1328" name="XLXI_35" orien="R0" />
        <instance x="1008" y="1072" name="XLXI_8" orien="R0" />
        <branch name="XLXN_34">
            <wire x2="944" y1="1200" y2="1200" x1="880" />
            <wire x2="1344" y1="1200" y2="1200" x1="944" />
            <wire x2="1408" y1="1200" y2="1200" x1="1344" />
            <wire x2="1008" y1="944" y2="944" x1="944" />
            <wire x2="944" y1="944" y2="1200" x1="944" />
            <wire x2="1376" y1="752" y2="752" x1="1344" />
            <wire x2="1344" y1="752" y2="1200" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="2448" y="944" name="E" orien="R0" />
        <iomarker fontsize="28" x="288" y="288" name="IA(1:0)" orien="R180" />
        <iomarker fontsize="28" x="288" y="400" name="IB(1:0)" orien="R180" />
        <iomarker fontsize="28" x="2448" y="688" name="W" orien="R0" />
        <iomarker fontsize="28" x="2480" y="1232" name="L" orien="R0" />
        <iomarker fontsize="28" x="352" y="1328" name="reset" orien="R180" />
    </sheet>
</drawing>