<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="X(3:0)" />
        <signal name="Y(3:0)" />
        <signal name="Z(7:0)" />
        <signal name="Y(3)" />
        <signal name="XLXN_498" />
        <signal name="XLXN_497" />
        <signal name="X(2)" />
        <signal name="X(1)" />
        <signal name="X(0)" />
        <signal name="XLXN_423" />
        <signal name="XLXN_416" />
        <signal name="X(3)" />
        <signal name="XLXN_180" />
        <signal name="XLXN_189" />
        <signal name="XLXN_184" />
        <signal name="XLXN_187" />
        <signal name="XLXN_186" />
        <signal name="XLXN_209" />
        <signal name="XLXN_188" />
        <signal name="XLXN_210" />
        <signal name="Y(2)" />
        <signal name="XLXN_190" />
        <signal name="Z(1)" />
        <signal name="XLXN_212" />
        <signal name="Z(0)" />
        <signal name="RESET" />
        <signal name="XLXN_175" />
        <signal name="XLXN_173" />
        <signal name="XLXN_166" />
        <signal name="XLXN_161" />
        <signal name="XLXN_118" />
        <signal name="XLXN_113" />
        <signal name="XLXN_91" />
        <signal name="XLXN_69" />
        <signal name="XLXN_92" />
        <signal name="XLXN_75" />
        <signal name="XLXN_653" />
        <signal name="XLXN_155" />
        <signal name="Y(1)" />
        <signal name="Y(0)" />
        <signal name="XLXN_680" />
        <signal name="XLXN_650" />
        <signal name="XLXN_682" />
        <signal name="XLXN_683" />
        <signal name="XLXN_651" />
        <signal name="XLXN_686" />
        <signal name="XLXN_652" />
        <signal name="XLXN_690" />
        <signal name="XLXN_691" />
        <signal name="Z(2)" />
        <signal name="XLXN_1497" />
        <signal name="Z(3)" />
        <signal name="XLXN_400" />
        <signal name="XLXN_395" />
        <signal name="XLXN_397" />
        <signal name="XLXN_1525" />
        <signal name="XLXN_1526" />
        <signal name="XLXN_1527" />
        <signal name="XLXN_1530" />
        <signal name="XLXN_1531" />
        <signal name="XLXN_1535" />
        <signal name="XLXN_1536" />
        <signal name="XLXN_1537" />
        <signal name="XLXN_1538" />
        <signal name="Z(4)" />
        <signal name="Z(5)" />
        <signal name="Z(6)" />
        <signal name="Z(7)" />
        <signal name="XLXN_1547" />
        <port polarity="Input" name="X(3:0)" />
        <port polarity="Input" name="Y(3:0)" />
        <port polarity="Output" name="Z(7:0)" />
        <port polarity="Input" name="RESET" />
        <blockdef name="FA">
            <timestamp>2023-8-11T19:21:31</timestamp>
            <rect width="192" x="64" y="-256" height="192" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="320" y1="-224" y2="-224" x1="256" />
            <line x2="320" y1="-160" y2="-160" x1="256" />
            <line x2="192" y1="-64" y2="0" x1="192" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="HA">
            <timestamp>2023-8-10T21:23:14</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="176" x="64" y="-156" height="156" />
            <line x2="304" y1="-128" y2="-128" x1="240" />
            <line x2="304" y1="-32" y2="-32" x1="240" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="FA" name="XLXI_251">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin signalname="XLXN_686" name="Bin" />
            <blockpin signalname="XLXN_690" name="Cin" />
            <blockpin name="Cout" />
            <blockpin signalname="XLXN_1535" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_250">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin signalname="XLXN_682" name="Bin" />
            <blockpin signalname="XLXN_683" name="Cin" />
            <blockpin signalname="XLXN_690" name="Cout" />
            <blockpin signalname="XLXN_691" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_184">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_1527" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_183">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_1525" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_197">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_416" name="I1" />
            <blockpin signalname="Z(2)" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_95">
            <blockpin signalname="XLXN_184" name="Ain" />
            <blockpin signalname="XLXN_680" name="Bin" />
            <blockpin signalname="XLXN_180" name="Cin" />
            <blockpin signalname="XLXN_683" name="Cout" />
            <blockpin signalname="XLXN_498" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="HA" name="XLXI_101">
            <blockpin signalname="XLXN_212" name="B" />
            <blockpin signalname="XLXN_190" name="A" />
            <blockpin signalname="XLXN_189" name="C" />
            <blockpin signalname="XLXN_416" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_100">
            <blockpin signalname="XLXN_188" name="Ain" />
            <blockpin signalname="XLXN_210" name="Bin" />
            <blockpin signalname="XLXN_189" name="Cin" />
            <blockpin signalname="XLXN_187" name="Cout" />
            <blockpin signalname="XLXN_423" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_104">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_184" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_99">
            <blockpin signalname="XLXN_186" name="Ain" />
            <blockpin signalname="XLXN_209" name="Bin" />
            <blockpin signalname="XLXN_187" name="Cin" />
            <blockpin signalname="XLXN_180" name="Cout" />
            <blockpin signalname="XLXN_497" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_105">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_186" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_106">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_188" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_108">
            <blockpin signalname="Y(2)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_190" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_93">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_173" name="I1" />
            <blockpin signalname="Z(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_94">
            <blockpin signalname="RESET" name="I0" />
            <blockpin signalname="XLXN_175" name="I1" />
            <blockpin signalname="Z(1)" name="O" />
        </block>
        <block symbolname="HA" name="XLXI_32">
            <blockpin signalname="XLXN_166" name="B" />
            <blockpin signalname="XLXN_118" name="A" />
            <blockpin signalname="XLXN_113" name="C" />
            <blockpin signalname="XLXN_175" name="S" />
        </block>
        <block symbolname="FA" name="XLXI_53">
            <blockpin signalname="XLXN_91" name="Ain" />
            <blockpin signalname="XLXN_161" name="Bin" />
            <blockpin signalname="XLXN_113" name="Cin" />
            <blockpin signalname="XLXN_69" name="Cout" />
            <blockpin signalname="XLXN_212" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_51">
            <blockpin signalname="XLXN_92" name="Ain" />
            <blockpin signalname="XLXN_155" name="Bin" />
            <blockpin signalname="XLXN_69" name="Cin" />
            <blockpin signalname="XLXN_75" name="Cout" />
            <blockpin signalname="XLXN_210" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_50">
            <blockpin signalname="XLXN_653" name="Ain" />
            <blockpin signalname="XLXN_155" name="Bin" />
            <blockpin signalname="XLXN_75" name="Cin" />
            <blockpin signalname="XLXN_650" name="Cout" />
            <blockpin signalname="XLXN_209" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_21">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_653" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_55">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_92" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_56">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_91" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_65">
            <blockpin signalname="Y(1)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_118" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_92">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_173" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(1)" name="I1" />
            <blockpin signalname="XLXN_166" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(2)" name="I1" />
            <blockpin signalname="XLXN_161" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="Y(0)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_155" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_252">
            <blockpin signalname="XLXN_155" name="Ain" />
            <blockpin signalname="XLXN_653" name="Bin" />
            <blockpin signalname="XLXN_650" name="Cin" />
            <blockpin signalname="XLXN_651" name="Cout" />
            <blockpin signalname="XLXN_680" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_253">
            <blockpin signalname="XLXN_155" name="Ain" />
            <blockpin signalname="XLXN_653" name="Bin" />
            <blockpin signalname="XLXN_651" name="Cin" />
            <blockpin signalname="XLXN_652" name="Cout" />
            <blockpin signalname="XLXN_682" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_254">
            <blockpin signalname="XLXN_155" name="Ain" />
            <blockpin signalname="XLXN_653" name="Bin" />
            <blockpin signalname="XLXN_652" name="Cin" />
            <blockpin name="Cout" />
            <blockpin signalname="XLXN_686" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="and2" name="XLXI_186">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(3)" name="I1" />
            <blockpin signalname="XLXN_1526" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_656">
            <blockpin signalname="XLXN_1497" name="I" />
            <blockpin signalname="XLXN_1530" name="O" />
        </block>
        <block symbolname="FA" name="XLXI_652">
            <blockpin signalname="XLXN_1530" name="Ain" />
            <blockpin signalname="XLXN_423" name="Bin" />
            <blockpin signalname="XLXN_1547" name="Cin" />
            <blockpin signalname="XLXN_397" name="Cout" />
            <blockpin signalname="Z(3)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_249">
            <blockpin signalname="XLXN_1535" name="Ain" />
            <blockpin signalname="XLXN_1537" name="Bin" />
            <blockpin signalname="XLXN_1536" name="Cin" />
            <blockpin name="Cout" />
            <blockpin signalname="Z(7)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_191">
            <blockpin signalname="XLXN_1537" name="Ain" />
            <blockpin signalname="XLXN_691" name="Bin" />
            <blockpin signalname="XLXN_400" name="Cin" />
            <blockpin signalname="XLXN_1536" name="Cout" />
            <blockpin signalname="Z(6)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_185">
            <blockpin signalname="XLXN_1538" name="Ain" />
            <blockpin signalname="XLXN_498" name="Bin" />
            <blockpin signalname="XLXN_395" name="Cin" />
            <blockpin signalname="XLXN_400" name="Cout" />
            <blockpin signalname="Z(5)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="FA" name="XLXI_187">
            <blockpin signalname="XLXN_1531" name="Ain" />
            <blockpin signalname="XLXN_497" name="Bin" />
            <blockpin signalname="XLXN_397" name="Cin" />
            <blockpin signalname="XLXN_395" name="Cout" />
            <blockpin signalname="Z(4)" name="sum" />
            <blockpin signalname="RESET" name="reset" />
        </block>
        <block symbolname="inv" name="XLXI_676">
            <blockpin signalname="XLXN_1525" name="I" />
            <blockpin signalname="XLXN_1531" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_677">
            <blockpin signalname="XLXN_1527" name="I" />
            <blockpin signalname="XLXN_1538" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_678">
            <blockpin signalname="XLXN_1526" name="I" />
            <blockpin signalname="XLXN_1537" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_182">
            <blockpin signalname="Y(3)" name="I0" />
            <blockpin signalname="X(0)" name="I1" />
            <blockpin signalname="XLXN_1497" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_680">
            <blockpin signalname="XLXN_1547" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7609" height="5382">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <branch name="X(3:0)">
            <wire x2="928" y1="224" y2="224" x1="400" />
        </branch>
        <branch name="Y(3:0)">
            <wire x2="928" y1="368" y2="368" x1="384" />
        </branch>
        <branch name="Z(7:0)">
            <wire x2="928" y1="576" y2="576" x1="352" />
        </branch>
        <iomarker fontsize="28" x="400" y="224" name="X(3:0)" orien="R180" />
        <iomarker fontsize="28" x="384" y="368" name="Y(3:0)" orien="R180" />
        <iomarker fontsize="28" x="928" y="576" name="Z(7:0)" orien="R0" />
        <instance x="4592" y="2224" name="XLXI_251" orien="M90">
        </instance>
        <instance x="4896" y="2224" name="XLXI_250" orien="M90">
        </instance>
        <instance x="4896" y="3136" name="XLXI_184" orien="R90" />
        <instance x="5248" y="3136" name="XLXI_183" orien="R90" />
        <instance x="5984" y="2688" name="XLXI_197" orien="R90" />
        <instance x="5216" y="2224" name="XLXI_95" orien="M90">
        </instance>
        <instance x="6144" y="2240" name="XLXI_101" orien="M90">
        </instance>
        <instance x="5920" y="2224" name="XLXI_100" orien="M90">
        </instance>
        <instance x="4896" y="1904" name="XLXI_104" orien="R90" />
        <instance x="5568" y="2224" name="XLXI_99" orien="M90">
        </instance>
        <instance x="5248" y="1904" name="XLXI_105" orien="R90" />
        <instance x="5600" y="1904" name="XLXI_106" orien="R90" />
        <instance x="5920" y="1904" name="XLXI_108" orien="R90" />
        <instance x="6864" y="1648" name="XLXI_93" orien="R90" />
        <instance x="6448" y="1648" name="XLXI_94" orien="R90" />
        <instance x="6608" y="1200" name="XLXI_32" orien="M90">
        </instance>
        <instance x="6272" y="1200" name="XLXI_53" orien="M90">
        </instance>
        <instance x="5920" y="1200" name="XLXI_51" orien="M90">
        </instance>
        <instance x="5568" y="1200" name="XLXI_50" orien="M90">
        </instance>
        <branch name="Y(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="5904" y="3056" type="branch" />
            <wire x2="4960" y1="3056" y2="3056" x1="4640" />
            <wire x2="5312" y1="3056" y2="3056" x1="4960" />
            <wire x2="5312" y1="3056" y2="3136" x1="5312" />
            <wire x2="5664" y1="3056" y2="3056" x1="5312" />
            <wire x2="5904" y1="3056" y2="3056" x1="5664" />
            <wire x2="5664" y1="3056" y2="3136" x1="5664" />
            <wire x2="4960" y1="3056" y2="3136" x1="4960" />
            <wire x2="4640" y1="3056" y2="3120" x1="4640" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5024" y="3120" type="branch" />
            <wire x2="5024" y1="3120" y2="3136" x1="5024" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5376" y="3120" type="branch" />
            <wire x2="5376" y1="3120" y2="3136" x1="5376" />
        </branch>
        <branch name="XLXN_416">
            <wire x2="6112" y1="2544" y2="2688" x1="6112" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5376" y="1888" type="branch" />
            <wire x2="5376" y1="1888" y2="1904" x1="5376" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5728" y="1888" type="branch" />
            <wire x2="5728" y1="1888" y2="1904" x1="5728" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5024" y="1888" type="branch" />
            <wire x2="5024" y1="1888" y2="1904" x1="5024" />
        </branch>
        <branch name="XLXN_180">
            <wire x2="5280" y1="2208" y2="2208" x1="5120" />
            <wire x2="5280" y1="2208" y2="2560" x1="5280" />
            <wire x2="5344" y1="2560" y2="2560" x1="5280" />
            <wire x2="5120" y1="2208" y2="2224" x1="5120" />
            <wire x2="5344" y1="2544" y2="2560" x1="5344" />
        </branch>
        <branch name="XLXN_189">
            <wire x2="5824" y1="2208" y2="2224" x1="5824" />
            <wire x2="5968" y1="2208" y2="2208" x1="5824" />
            <wire x2="5968" y1="2208" y2="2560" x1="5968" />
            <wire x2="6016" y1="2560" y2="2560" x1="5968" />
            <wire x2="6016" y1="2544" y2="2560" x1="6016" />
        </branch>
        <branch name="XLXN_184">
            <wire x2="4672" y1="2176" y2="2176" x1="4368" />
            <wire x2="4992" y1="2176" y2="2176" x1="4672" />
            <wire x2="4992" y1="2176" y2="2224" x1="4992" />
            <wire x2="4672" y1="2176" y2="2224" x1="4672" />
            <wire x2="4368" y1="2176" y2="2224" x1="4368" />
            <wire x2="4992" y1="2160" y2="2176" x1="4992" />
        </branch>
        <branch name="XLXN_187">
            <wire x2="5472" y1="2208" y2="2224" x1="5472" />
            <wire x2="5632" y1="2208" y2="2208" x1="5472" />
            <wire x2="5632" y1="2208" y2="2560" x1="5632" />
            <wire x2="5696" y1="2560" y2="2560" x1="5632" />
            <wire x2="5696" y1="2544" y2="2560" x1="5696" />
        </branch>
        <branch name="XLXN_186">
            <wire x2="5344" y1="2160" y2="2224" x1="5344" />
        </branch>
        <branch name="XLXN_209">
            <wire x2="5408" y1="1520" y2="2224" x1="5408" />
        </branch>
        <branch name="XLXN_188">
            <wire x2="5696" y1="2160" y2="2224" x1="5696" />
        </branch>
        <branch name="XLXN_210">
            <wire x2="5760" y1="1520" y2="2224" x1="5760" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6048" y="1888" type="branch" />
            <wire x2="6048" y1="1888" y2="1904" x1="6048" />
        </branch>
        <branch name="Y(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="6256" y="1824" type="branch" />
            <wire x2="5312" y1="1824" y2="1824" x1="4960" />
            <wire x2="5664" y1="1824" y2="1824" x1="5312" />
            <wire x2="5984" y1="1824" y2="1824" x1="5664" />
            <wire x2="5984" y1="1824" y2="1904" x1="5984" />
            <wire x2="6256" y1="1824" y2="1824" x1="5984" />
            <wire x2="5664" y1="1824" y2="1904" x1="5664" />
            <wire x2="5312" y1="1824" y2="1904" x1="5312" />
            <wire x2="4960" y1="1824" y2="1904" x1="4960" />
        </branch>
        <branch name="XLXN_190">
            <wire x2="6016" y1="2160" y2="2240" x1="6016" />
        </branch>
        <branch name="Z(1)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="6544" y="2000" type="branch" />
            <wire x2="6544" y1="1904" y2="2000" x1="6544" />
        </branch>
        <branch name="XLXN_212">
            <wire x2="6112" y1="1520" y2="2240" x1="6112" />
        </branch>
        <branch name="Z(0)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="6960" y="1920" type="branch" />
            <wire x2="6960" y1="1904" y2="1920" x1="6960" />
        </branch>
        <branch name="RESET">
            <wire x2="2960" y1="1632" y2="1632" x1="2848" />
            <wire x2="2960" y1="1632" y2="2672" x1="2960" />
            <wire x2="2960" y1="2672" y2="4144" x1="2960" />
            <wire x2="4576" y1="4144" y2="4144" x1="2960" />
            <wire x2="4896" y1="4144" y2="4144" x1="4576" />
            <wire x2="5232" y1="4144" y2="4144" x1="4896" />
            <wire x2="5584" y1="4144" y2="4144" x1="5232" />
            <wire x2="6000" y1="4144" y2="4144" x1="5584" />
            <wire x2="4592" y1="2672" y2="2672" x1="2960" />
            <wire x2="4896" y1="2672" y2="2672" x1="4592" />
            <wire x2="5232" y1="2672" y2="2672" x1="4896" />
            <wire x2="5584" y1="2672" y2="2672" x1="5232" />
            <wire x2="5936" y1="2672" y2="2672" x1="5584" />
            <wire x2="6048" y1="2672" y2="2672" x1="5936" />
            <wire x2="6048" y1="2672" y2="2688" x1="6048" />
            <wire x2="4592" y1="1632" y2="1632" x1="2960" />
            <wire x2="4912" y1="1632" y2="1632" x1="4592" />
            <wire x2="5232" y1="1632" y2="1632" x1="4912" />
            <wire x2="5584" y1="1632" y2="1632" x1="5232" />
            <wire x2="5936" y1="1632" y2="1632" x1="5584" />
            <wire x2="6336" y1="1632" y2="1632" x1="5936" />
            <wire x2="6512" y1="1632" y2="1632" x1="6336" />
            <wire x2="6512" y1="1632" y2="1648" x1="6512" />
            <wire x2="6928" y1="1632" y2="1632" x1="6512" />
            <wire x2="6928" y1="1632" y2="1648" x1="6928" />
            <wire x2="4576" y1="3872" y2="4144" x1="4576" />
            <wire x2="4592" y1="1376" y2="1632" x1="4592" />
            <wire x2="4592" y1="2416" y2="2672" x1="4592" />
            <wire x2="4912" y1="1392" y2="1392" x1="4896" />
            <wire x2="4912" y1="1392" y2="1632" x1="4912" />
            <wire x2="4896" y1="2416" y2="2672" x1="4896" />
            <wire x2="4896" y1="3872" y2="4144" x1="4896" />
            <wire x2="5232" y1="1392" y2="1392" x1="5216" />
            <wire x2="5232" y1="1392" y2="1632" x1="5232" />
            <wire x2="5232" y1="2416" y2="2416" x1="5216" />
            <wire x2="5232" y1="2416" y2="2672" x1="5232" />
            <wire x2="5232" y1="3872" y2="3872" x1="5216" />
            <wire x2="5232" y1="3872" y2="4144" x1="5232" />
            <wire x2="5584" y1="1392" y2="1392" x1="5568" />
            <wire x2="5584" y1="1392" y2="1632" x1="5584" />
            <wire x2="5584" y1="2416" y2="2416" x1="5568" />
            <wire x2="5584" y1="2416" y2="2672" x1="5584" />
            <wire x2="5584" y1="3872" y2="3872" x1="5568" />
            <wire x2="5584" y1="3872" y2="4144" x1="5584" />
            <wire x2="5936" y1="1392" y2="1392" x1="5920" />
            <wire x2="5936" y1="1392" y2="1632" x1="5936" />
            <wire x2="5936" y1="2416" y2="2416" x1="5920" />
            <wire x2="5936" y1="2416" y2="2672" x1="5936" />
            <wire x2="6000" y1="3872" y2="3872" x1="5920" />
            <wire x2="6000" y1="3872" y2="4144" x1="6000" />
            <wire x2="6336" y1="1392" y2="1392" x1="6272" />
            <wire x2="6336" y1="1392" y2="1632" x1="6336" />
        </branch>
        <branch name="XLXN_175">
            <wire x2="6576" y1="1504" y2="1648" x1="6576" />
        </branch>
        <branch name="XLXN_173">
            <wire x2="6992" y1="592" y2="1648" x1="6992" />
        </branch>
        <branch name="XLXN_166">
            <wire x2="6576" y1="592" y2="1200" x1="6576" />
        </branch>
        <branch name="XLXN_161">
            <wire x2="6112" y1="592" y2="1200" x1="6112" />
        </branch>
        <branch name="XLXN_118">
            <wire x2="6480" y1="1040" y2="1200" x1="6480" />
        </branch>
        <branch name="XLXN_113">
            <wire x2="6176" y1="1184" y2="1200" x1="6176" />
            <wire x2="6400" y1="1184" y2="1184" x1="6176" />
            <wire x2="6400" y1="1184" y2="1584" x1="6400" />
            <wire x2="6480" y1="1584" y2="1584" x1="6400" />
            <wire x2="6480" y1="1504" y2="1584" x1="6480" />
        </branch>
        <branch name="XLXN_91">
            <wire x2="6048" y1="1040" y2="1200" x1="6048" />
        </branch>
        <branch name="XLXN_69">
            <wire x2="5824" y1="1184" y2="1200" x1="5824" />
            <wire x2="5984" y1="1184" y2="1184" x1="5824" />
            <wire x2="5984" y1="1184" y2="1536" x1="5984" />
            <wire x2="6048" y1="1536" y2="1536" x1="5984" />
            <wire x2="6048" y1="1520" y2="1536" x1="6048" />
        </branch>
        <branch name="XLXN_92">
            <wire x2="5696" y1="1040" y2="1200" x1="5696" />
        </branch>
        <branch name="XLXN_75">
            <wire x2="5472" y1="1184" y2="1200" x1="5472" />
            <wire x2="5632" y1="1184" y2="1184" x1="5472" />
            <wire x2="5632" y1="1184" y2="1536" x1="5632" />
            <wire x2="5696" y1="1536" y2="1536" x1="5632" />
            <wire x2="5696" y1="1520" y2="1536" x1="5696" />
        </branch>
        <branch name="XLXN_653">
            <wire x2="4736" y1="1168" y2="1168" x1="4432" />
            <wire x2="5056" y1="1168" y2="1168" x1="4736" />
            <wire x2="5344" y1="1168" y2="1168" x1="5056" />
            <wire x2="5344" y1="1168" y2="1200" x1="5344" />
            <wire x2="5056" y1="1168" y2="1200" x1="5056" />
            <wire x2="4736" y1="1168" y2="1200" x1="4736" />
            <wire x2="4432" y1="1168" y2="1184" x1="4432" />
            <wire x2="5344" y1="1040" y2="1168" x1="5344" />
        </branch>
        <branch name="XLXN_155">
            <wire x2="4672" y1="1088" y2="1088" x1="4368" />
            <wire x2="4992" y1="1088" y2="1088" x1="4672" />
            <wire x2="5408" y1="1088" y2="1088" x1="4992" />
            <wire x2="5760" y1="1088" y2="1088" x1="5408" />
            <wire x2="5760" y1="1088" y2="1200" x1="5760" />
            <wire x2="5408" y1="1088" y2="1200" x1="5408" />
            <wire x2="4992" y1="1088" y2="1200" x1="4992" />
            <wire x2="4672" y1="1088" y2="1200" x1="4672" />
            <wire x2="4368" y1="1088" y2="1184" x1="4368" />
            <wire x2="5760" y1="576" y2="1088" x1="5760" />
        </branch>
        <instance x="5248" y="784" name="XLXI_21" orien="R90" />
        <instance x="5600" y="784" name="XLXI_55" orien="R90" />
        <instance x="5952" y="784" name="XLXI_56" orien="R90" />
        <instance x="6384" y="784" name="XLXI_65" orien="R90" />
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5376" y="768" type="branch" />
            <wire x2="5376" y1="768" y2="784" x1="5376" />
        </branch>
        <branch name="Y(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="6720" y="704" type="branch" />
            <wire x2="5312" y1="704" y2="784" x1="5312" />
            <wire x2="5664" y1="704" y2="704" x1="5312" />
            <wire x2="5664" y1="704" y2="784" x1="5664" />
            <wire x2="6016" y1="704" y2="704" x1="5664" />
            <wire x2="6016" y1="704" y2="784" x1="6016" />
            <wire x2="6448" y1="704" y2="704" x1="6016" />
            <wire x2="6448" y1="704" y2="784" x1="6448" />
            <wire x2="6720" y1="704" y2="704" x1="6448" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5728" y="768" type="branch" />
            <wire x2="5728" y1="768" y2="784" x1="5728" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6512" y="768" type="branch" />
            <wire x2="6512" y1="768" y2="784" x1="6512" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6080" y="768" type="branch" />
            <wire x2="6080" y1="768" y2="784" x1="6080" />
        </branch>
        <instance x="6896" y="336" name="XLXI_92" orien="R90" />
        <instance x="6480" y="336" name="XLXI_7" orien="R90" />
        <instance x="6016" y="336" name="XLXI_6" orien="R90" />
        <instance x="5664" y="320" name="XLXI_5" orien="R90" />
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5792" y="304" type="branch" />
            <wire x2="5792" y1="304" y2="320" x1="5792" />
        </branch>
        <branch name="X(2)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6144" y="320" type="branch" />
            <wire x2="6144" y1="320" y2="336" x1="6144" />
        </branch>
        <branch name="X(1)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="6608" y="320" type="branch" />
            <wire x2="6608" y1="320" y2="336" x1="6608" />
        </branch>
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="7024" y="320" type="branch" />
            <wire x2="7024" y1="320" y2="336" x1="7024" />
        </branch>
        <branch name="Y(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="7040" y="224" type="branch" />
            <wire x2="6080" y1="224" y2="224" x1="5728" />
            <wire x2="6544" y1="224" y2="224" x1="6080" />
            <wire x2="6960" y1="224" y2="224" x1="6544" />
            <wire x2="7040" y1="224" y2="224" x1="6960" />
            <wire x2="6960" y1="224" y2="336" x1="6960" />
            <wire x2="6544" y1="224" y2="336" x1="6544" />
            <wire x2="6080" y1="224" y2="336" x1="6080" />
            <wire x2="5728" y1="224" y2="320" x1="5728" />
        </branch>
        <branch name="XLXN_680">
            <wire x2="5056" y1="1520" y2="2224" x1="5056" />
        </branch>
        <instance x="5216" y="1200" name="XLXI_252" orien="M90">
        </instance>
        <branch name="XLXN_650">
            <wire x2="5120" y1="1184" y2="1200" x1="5120" />
            <wire x2="5280" y1="1184" y2="1184" x1="5120" />
            <wire x2="5280" y1="1184" y2="1536" x1="5280" />
            <wire x2="5344" y1="1536" y2="1536" x1="5280" />
            <wire x2="5344" y1="1520" y2="1536" x1="5344" />
        </branch>
        <branch name="XLXN_682">
            <wire x2="4736" y1="1520" y2="2224" x1="4736" />
        </branch>
        <branch name="XLXN_683">
            <wire x2="4800" y1="2208" y2="2224" x1="4800" />
            <wire x2="4928" y1="2208" y2="2208" x1="4800" />
            <wire x2="4928" y1="2208" y2="2560" x1="4928" />
            <wire x2="4992" y1="2560" y2="2560" x1="4928" />
            <wire x2="4992" y1="2544" y2="2560" x1="4992" />
        </branch>
        <branch name="XLXN_651">
            <wire x2="4944" y1="1184" y2="1184" x1="4800" />
            <wire x2="4944" y1="1184" y2="1536" x1="4944" />
            <wire x2="4992" y1="1536" y2="1536" x1="4944" />
            <wire x2="4800" y1="1184" y2="1200" x1="4800" />
            <wire x2="4992" y1="1520" y2="1536" x1="4992" />
        </branch>
        <instance x="4896" y="1200" name="XLXI_253" orien="M90">
        </instance>
        <branch name="XLXN_686">
            <wire x2="4432" y1="1504" y2="2224" x1="4432" />
        </branch>
        <instance x="4592" y="1184" name="XLXI_254" orien="M90">
        </instance>
        <branch name="XLXN_652">
            <wire x2="4624" y1="1184" y2="1184" x1="4496" />
            <wire x2="4624" y1="1184" y2="1536" x1="4624" />
            <wire x2="4672" y1="1536" y2="1536" x1="4624" />
            <wire x2="4672" y1="1520" y2="1536" x1="4672" />
        </branch>
        <branch name="XLXN_690">
            <wire x2="4624" y1="2224" y2="2224" x1="4496" />
            <wire x2="4624" y1="2224" y2="2560" x1="4624" />
            <wire x2="4672" y1="2560" y2="2560" x1="4624" />
            <wire x2="4672" y1="2544" y2="2560" x1="4672" />
        </branch>
        <branch name="X(3)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="4704" y="3104" type="branch" />
            <wire x2="4704" y1="3104" y2="3120" x1="4704" />
        </branch>
        <instance x="4576" y="3120" name="XLXI_186" orien="R90" />
        <branch name="Z(2)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="6080" y="2976" type="branch" />
            <wire x2="6080" y1="2944" y2="2976" x1="6080" />
        </branch>
        <branch name="XLXN_423">
            <wire x2="5760" y1="2544" y2="3680" x1="5760" />
        </branch>
        <branch name="XLXN_497">
            <wire x2="5408" y1="2544" y2="3680" x1="5408" />
        </branch>
        <branch name="XLXN_498">
            <wire x2="5056" y1="2544" y2="3680" x1="5056" />
        </branch>
        <branch name="XLXN_691">
            <wire x2="4736" y1="2544" y2="3680" x1="4736" />
        </branch>
        <instance x="4896" y="3680" name="XLXI_191" orien="M90">
        </instance>
        <instance x="5216" y="3680" name="XLXI_185" orien="M90">
        </instance>
        <instance x="5568" y="3680" name="XLXI_187" orien="M90">
        </instance>
        <branch name="Z(3)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5760" y="4432" type="branch" />
            <wire x2="5760" y1="4000" y2="4432" x1="5760" />
        </branch>
        <branch name="XLXN_400">
            <wire x2="4800" y1="3664" y2="3680" x1="4800" />
            <wire x2="4928" y1="3664" y2="3664" x1="4800" />
            <wire x2="4928" y1="3664" y2="4016" x1="4928" />
            <wire x2="4992" y1="4016" y2="4016" x1="4928" />
            <wire x2="4992" y1="4000" y2="4016" x1="4992" />
        </branch>
        <branch name="XLXN_395">
            <wire x2="5120" y1="3664" y2="3680" x1="5120" />
            <wire x2="5280" y1="3664" y2="3664" x1="5120" />
            <wire x2="5280" y1="3664" y2="4016" x1="5280" />
            <wire x2="5344" y1="4016" y2="4016" x1="5280" />
            <wire x2="5344" y1="4000" y2="4016" x1="5344" />
        </branch>
        <branch name="XLXN_397">
            <wire x2="5472" y1="3664" y2="3680" x1="5472" />
            <wire x2="5632" y1="3664" y2="3664" x1="5472" />
            <wire x2="5632" y1="3664" y2="4064" x1="5632" />
            <wire x2="5696" y1="4064" y2="4064" x1="5632" />
            <wire x2="5696" y1="4000" y2="4064" x1="5696" />
        </branch>
        <branch name="XLXN_1525">
            <wire x2="5344" y1="3392" y2="3424" x1="5344" />
        </branch>
        <instance x="5312" y="3424" name="XLXI_676" orien="R90" />
        <branch name="XLXN_1526">
            <wire x2="4672" y1="3376" y2="3392" x1="4672" />
        </branch>
        <branch name="XLXN_1527">
            <wire x2="4992" y1="3392" y2="3424" x1="4992" />
        </branch>
        <instance x="4960" y="3424" name="XLXI_677" orien="R90" />
        <instance x="5920" y="3680" name="XLXI_652" orien="M90">
        </instance>
        <instance x="5600" y="3136" name="XLXI_182" orien="R90" />
        <branch name="X(0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5728" y="3120" type="branch" />
            <wire x2="5728" y1="3120" y2="3136" x1="5728" />
        </branch>
        <branch name="XLXN_1497">
            <wire x2="5696" y1="3392" y2="3424" x1="5696" />
        </branch>
        <branch name="XLXN_1530">
            <wire x2="5696" y1="3648" y2="3680" x1="5696" />
        </branch>
        <instance x="5664" y="3424" name="XLXI_656" orien="R90" />
        <branch name="XLXN_1531">
            <wire x2="5344" y1="3648" y2="3680" x1="5344" />
        </branch>
        <instance x="4576" y="3680" name="XLXI_249" orien="M90">
        </instance>
        <branch name="XLXN_1535">
            <wire x2="4352" y1="3104" y2="3680" x1="4352" />
            <wire x2="4432" y1="3104" y2="3104" x1="4352" />
            <wire x2="4432" y1="2544" y2="3104" x1="4432" />
        </branch>
        <branch name="XLXN_1536">
            <wire x2="4480" y1="3664" y2="3680" x1="4480" />
            <wire x2="4608" y1="3664" y2="3664" x1="4480" />
            <wire x2="4608" y1="3664" y2="4016" x1="4608" />
            <wire x2="4672" y1="4016" y2="4016" x1="4608" />
            <wire x2="4672" y1="4000" y2="4016" x1="4672" />
        </branch>
        <branch name="XLXN_1537">
            <wire x2="4672" y1="3632" y2="3632" x1="4416" />
            <wire x2="4672" y1="3632" y2="3680" x1="4672" />
            <wire x2="4416" y1="3632" y2="3680" x1="4416" />
            <wire x2="4672" y1="3616" y2="3632" x1="4672" />
        </branch>
        <instance x="4640" y="3392" name="XLXI_678" orien="R90" />
        <branch name="XLXN_1538">
            <wire x2="4992" y1="3648" y2="3680" x1="4992" />
        </branch>
        <branch name="Z(4)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5408" y="4432" type="branch" />
            <wire x2="5408" y1="4000" y2="4432" x1="5408" />
        </branch>
        <branch name="Z(5)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5056" y="4416" type="branch" />
            <wire x2="5056" y1="4000" y2="4416" x1="5056" />
        </branch>
        <branch name="Z(6)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4736" y="4416" type="branch" />
            <wire x2="4736" y1="4000" y2="4416" x1="4736" />
        </branch>
        <branch name="Z(7)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4416" y="4416" type="branch" />
            <wire x2="4416" y1="4000" y2="4416" x1="4416" />
        </branch>
        <iomarker fontsize="28" x="2848" y="1632" name="RESET" orien="R180" />
        <branch name="XLXN_1547">
            <wire x2="5824" y1="3648" y2="3680" x1="5824" />
        </branch>
        <instance x="5760" y="3648" name="XLXI_680" orien="R0" />
    </sheet>
</drawing>