`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:26:53 08/02/2023 
// Design Name: 
// Module Name:    OneHertzUpDownCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
   module OneHertzUpDownCounter(

	output	reg	[3:0]	led,
	input clock,clearin,clearop,sel
	
);

reg [26:0] count;
reg onehertz;

always@(posedge clock)
begin
	if (clearin)
	begin
		count = count + 1'b1;
		if(count == 27'd99999999)
		begin
			onehertz = ~onehertz;
			count = 27'd0;
		end
	end
	
	else if(!clearin)
	begin
		onehertz = 1'b0;
		count = 27'd0;
	end
end
endmodule

