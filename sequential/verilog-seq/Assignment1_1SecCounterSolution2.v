`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:35:56 07/31/2023 
// Design Name: 
// Module Name:    Assignment1_1SecCounterSolution2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Assignment1_1SecCounterSolution2(
output	reg	O,
input		clk,clear
);

reg [26:0] count;
 
always@(posedge clk)
begin
	if (clear)
	begin
		count = count + 1;
		if(count == 27'd99999999)
		begin
			O <= ~O;
			count <= 8'b0;
		end
	end
	
	else if(!clear)
	begin
		O = 1'b0;
		count = 27'd0;
	end

end
endmodule