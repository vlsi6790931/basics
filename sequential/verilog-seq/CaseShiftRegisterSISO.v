`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:18:22 08/01/2023 
// Design Name: 
// Module Name:    CaseShiftRegisterSISO 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CaseShiftRegisterSISO(
output reg q,
input clock, clear, data
    );
	 
reg [2:0] shift;

always@(posedge clock)
begin
	case ({clear})
		1'b0	:	begin
						q <= 4'b0;
						shift <= 4'b0;
					end
		1'b1	:	begin
						q <= shift[2];
						shift[2] <= shift[1];
						shift[1] <= shift[0];
						shift[0] <= data;
					end
		default	:	q <= 4'b0000;
		
	endcase	
end
endmodule
