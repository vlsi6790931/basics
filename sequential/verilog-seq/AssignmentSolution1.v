`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:44:42 07/31/2023 
// Design Name: 
// Module Name:    AssignmentSolution1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AssignmentSolution1(
	output	o,
	input		T,clk,clear
);

wire w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11,w12,w13,w14,w15,w16,w17,w18,w19,w20,w21,w22,w23,w24,w25,w26,w27;


T_FF 	t1 	(w1,T,clk,clear);
T_FF	t2		(w2,T,w1,clear);
T_FF	t3		(w3,T,w2,clear);
T_FF	t4		(w4,T,w3,clear);
T_FF	t5		(w5,T,w4,clear);
T_FF	t6 	(w6,T,w5,clear);
T_FF	t7		(w7,T,w6,clear);
T_FF	t8		(w8,T,w7,clear);
T_FF	t9		(w9,T,w8,clear);
T_FF	t10	(w10,T,w9,clear);
T_FF	t11	(w11,T,w10,clear);
T_FF	t12	(w12,T,w11,clear);
T_FF	t13	(w13,T,w12,clear);
T_FF	t14	(w14,T,w13,clear);
T_FF	t15	(w15,T,w14,clear);
T_FF	t16	(w16,T,w15,clear);
T_FF	t17	(w17,T,w16,clear);
T_FF	t18	(w18,T,w17,clear);
T_FF	t19	(w19,T,w18,clear);
T_FF	t20	(w20,T,w19,clear);
T_FF	t21	(w21,T,w20,clear);
T_FF	t22	(w22,T,w21,clear);
T_FF	t23	(w23,T,w22,clear);
T_FF	t24	(w24,T,w23,clear);
T_FF	t25	(w25,T,w24,clear);
T_FF	t26	(w26,T,w25,clear);
T_FF	t27	(w27,T,w26,clear);

assign o = w27;

endmodule
