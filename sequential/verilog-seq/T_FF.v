`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:43:32 07/31/2023 
// Design Name: 
// Module Name:    T_FF 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module T_FF(
	output 	reg 	q,
	input 			T,clk,clear
    );
	 
	 
always@(*)					// should also be working outside clock
begin
		if(!clear)
			q <= 1'b0;
end
	
always@(posedge clk or posedge clear)
begin
	if(!clear)
			q <= 1'b0;
	else if (T)
		q <= ~q;	
	else if(!T)
		q <= q;
end

endmodule
