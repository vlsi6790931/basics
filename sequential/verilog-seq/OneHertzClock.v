`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:47:54 08/03/2023 
// Design Name: 
// Module Name:    OneHertzTimer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module OneHertzTimer(
	output	reg	O,
	input		clk,clear
);

reg [26:0] count;
 
always@(posedge clk)
begin
	if (clear)
	begin
		count <= count + 1;
		if(count == 27'd99999999) // size as per binary bits, data as required
		begin
			O <= ~O;
			count <= 27'd0;
		end
	end
	
	else if(!clear)
	begin
		O <= 1'b0;
		count <= 27'd0;
	end

end
endmodule