
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name verilog-seq -dir "C:/Users/qwerty/Documents/basics/sequential/verilog-seq/planAhead_run_1" -part xc7a100tcsg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "DIGITAL_CLOCK.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {DIGITAL_CLOCK.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
add_files [list {DualSSD.ngc}]
add_files [list {SSD.ngc}]
set_property top DIGITAL_CLOCK $srcset
add_files [list {DIGITAL_CLOCK.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc7a100tcsg324-3
