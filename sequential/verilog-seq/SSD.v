`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:46:51 08/03/2023 
// Design Name: 
// Module Name:    SSD 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module SSD(
output	reg	[7:0]		anode,
output 	reg	[6:0]		cathode,
input 	clock,clear
	 );
 

//	registers
reg	[36:0]	count;
reg	onehertz;
reg	[3:0]	display;

//	initialisation
initial 
begin
	count = 0;
	onehertz = 0;
//	display = 0; // do not initialise, it will be handled by cases
	
end

// 1 second clock (2hz clock)
always@(posedge clock)
begin
	if (clear)
	begin
		count = count + 1'b1;
		if(count == 27'd99999999)
		begin
			onehertz = ~onehertz;
			count = 27'd0;
		end
	end
	
	else if(!clear)
	begin
		onehertz = 1'b0;
		count = 27'd0;
	end
end

// seven segment display

		// increment - on onehertz
always@(posedge onehertz)
begin
	if (!clear || display == 4'd9)
		display = 4'd0;
	else
		display = display + 1;
end

		// cases - on clock
always@(posedge clock)
begin
	anode = 8'b11111110;
	case(display)
		4'b0000 	:	cathode = 7'b0000001;
		4'b0001	:	cathode = 7'b1001111;
		4'b0010	:	cathode = 7'b0010010;
		4'b0011	:	cathode = 7'b0000110;
		4'b0100	:	cathode = 7'b1001100;
		4'b0101	:	cathode = 7'b0100100;
		4'b0110	:	cathode = 7'b0100000;
		4'b0111	:	cathode = 7'b0001101; 
		4'b1000	:	cathode = 7'b0000000;
		4'b1001	:	cathode = 7'b0000100;
		default 	:	cathode = 7'd1;
	endcase 
end

endmodule
