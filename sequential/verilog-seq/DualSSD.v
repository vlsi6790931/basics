`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    03:20:11 08/04/2023 
// Design Name: 
// Module Name:    DualSSD 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DualSSD(
output	reg	[7:0]		anode,
output 	reg	[6:0]		cathode,
input 	clock,clear
	 );
 

//	registers
reg	[26:0]	count1;
reg	[17:0]	count2;
reg	onesec;
reg 	twoH;
reg	[3:0]	displayLSB;
reg	[3:0]	displayMSB;

//	initialisation
initial 
begin
	count1 = 0;
	count2 = 0;
	onesec = 0;
	twoH = 0;
//	display = 0; // do not initialise, it will be handled by cases
	
end



//////////////////////////////		CLOCK		/////////////////////////////////////


// 1 hz clock
always@(posedge clock)
begin
	if (clear)
	begin
		count1 = count1 + 1'b1;
		if(count1 == 27'd50000000)
		begin
			onesec = ~onesec;
			count1 = 27'd0;
		end
	end
	
	else if(!clear)
	begin
		onesec = 1'b0;
		count1 = 27'd0;
	end
end


// 40 hz  	- refresh rate
always@(posedge clock)
begin
	if (clear)
	begin
		count2 = count2 + 1'b1;
		if(count2 == 18'd250000)
		begin
			twoH = ~twoH;
			count2 = 18'd0;
		end
	end
	
	else if(!clear)
	begin
		twoH = 1'b0;
		count2 = 27'd0;
	end
end


//////////////////////////////		COUNTING		///////////////////////////////////

//	increment - on onesec
always@(posedge onesec)
begin
	if (!clear || (displayLSB == 4'd9 && displayMSB == 4'd9))
	begin	
		displayLSB = 4'd0;
		displayMSB = 4'b0;
	end
	else if (displayLSB == 9 && displayMSB != 9)
	begin
		displayMSB = displayMSB + 1;
		displayLSB = 0;
	end
	else
		displayLSB = displayLSB + 1;
end


///////////////////////////////	OUTPUT		/////////////////////////////////////

reg [7:0]	anode1;
reg [7:0]	anode2;
reg [6:0] 	cat1;
reg [6:0] 	cat2;

/*

always@(anodesel)	// mostly not used in sequential
begin 
	case(twoH)
	1'b0	:	begin
					display = displayLSB;
				end
				
	1'b1	:	begin
					display = displayMSB;
				end
	endcase
end



always@(posedge onesec)
begin
	case(display)
		4'b0000 	:	cat1	= 7'b0000001;
		4'b0001	:	cat1	= 7'b1001111;
		4'b0010	:	cat1 = 7'b0010010;
		4'b0011	:	cat1 = 7'b0000110;
		4'b0100	:	cat1 = 7'b1001100;			WILL GIVE SAME VALUE ON DISPLAY AS TWO DISPLAYS ARE TRYING TO GET VALUES FROM A SINGLE COUNTER
		4'b0101	:	cat1 = 7'b0100100;
		4'b0101	:	cat1 = 7'b0100000;
		4'b0111	:	cat1 = 7'b0001101;
		4'b1000	:	cat1 = 7'b0000000;
		4'b1001	:	cat1 = 7'b0000100;
		default 	:	cat1 = 7'd1;
	endcase 
end
																
*/


// SELECT VALUES FROM COUNTERS
always@(posedge onesec)
begin
	anode1 = 8'b11111110;
	case(displayLSB)
		4'b0000 	:	cat1 = 7'b0000001;
		4'b0001	:	cat1 = 7'b1001111;
		4'b0010	:	cat1 = 7'b0010010;
		4'b0011	:	cat1 = 7'b0000110;
		4'b0100	:	cat1 = 7'b1001100;
		4'b0101	:	cat1 = 7'b0100100;
		4'b0110	:	cat1 = 7'b0100000;
		4'b0111	:	cat1 = 7'b0001101;
		4'b1000	:	cat1 = 7'b0000000;
		4'b1001	:	cat1 = 7'b0000100;
		default 	:	cat1 = 7'd1;
	endcase 
end

always@(posedge onesec)
begin
	anode2 = 8'b11111101;
	case(displayMSB)
		4'b0000 	:	cat2 = 7'b0000001;
		4'b0001	:	cat2 = 7'b1001111;
		4'b0010	:	cat2 = 7'b0010010;
		4'b0011	:	cat2 = 7'b0000110;
		4'b0100	:	cat2 = 7'b1001100;
		4'b0101	:	cat2 = 7'b0100100;
		4'b0110	:	cat2 = 7'b0100000;
		4'b0111	:	cat2 = 7'b0001101;
		4'b1000	:	cat2 = 7'b0000000;
		4'b1001	:	cat2 = 7'b0000100;
		default 	:	cat2 = 7'd1;
	endcase 
end


// SELECT SSD BASED ON REFRESH RATE, MUX approach, combinational circuit
always@(anode1, anode2, cat1, cat2)
begin
	case(twoH)
	1'b0	:	begin
					anode		=	anode1;
					cathode	=	cat1;
				end
	1'b1	:	begin
					anode		=	anode2;
					cathode	=	cat2;
				end
	endcase
end


endmodule
