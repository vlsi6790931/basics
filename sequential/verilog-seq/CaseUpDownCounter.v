`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:23:20 08/01/2023 
// Design Name: 
// Module Name:    CaseUpDownCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CaseUpDownCounter(
output reg	[3:0] O,
input clock, clear, sel
    );

always@(posedge clock)
begin
	
	case ({clear,sel})

		2'b10 : O = O + 1;
		2'b11 : O = O - 1;
		default : O = 4'b0000;
	endcase
end


endmodule
