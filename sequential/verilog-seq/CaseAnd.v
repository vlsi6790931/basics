`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:48:06 08/01/2023 
// Design Name: 
// Module Name:    CaseAnd 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CaseAnd( 
output reg out,
input a,b
);

always@(a,b)
begin
	case ({a,b})
		2'b00 :	out = 1'b0;
		2'b01	:	out = 1'b0;
		2'b10	:	out = 1'b0;
		2'b11	:	out = 1'b1;
		2'b0x	:	out = 1'b0;
		2'bx0	:	out = 1'b0;
		2'b0z	:	out = 1'b0;
		2'bz0 : 	out = 1'b0;
		default	:	out = 1'bx;
	endcase
end

endmodule
