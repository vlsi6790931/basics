`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:52:37 08/01/2023 
// Design Name: 
// Module Name:    CaseShiftRegisterPISO 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CaseShiftRegisterPISO(
output reg O,
input clock, clear, load,
input reg q0, q1, q2 , q3
    );


always@(posedge clock)
begin
	
	case({clear,load})
	
	2'b10 :	begin
					o <= q3;
					q3 <= q2;
					q2 <= q1;
					q1 <= q0;
				end
	2'b11 : q = q;
	default : O = 0'b0;
	
	endcase
end

endmodule
