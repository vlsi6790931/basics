`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:10:51 08/01/2023 
// Design Name: 
// Module Name:    CaseMSJKFF 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CaseMSJKFF(

output reg q,qb,
input clear, clock, j, k
    );

always@(posedge clock)
begin
	case ({clear,j,k})
		3'b000 : begin q <= 0; qb <= 1; end
		3'b100 : begin q <= q; qb <= qb; end
		3'b101 : begin q <= 1'b0; qb <= 1'b1; end
		3'b110 : begin q <= 1'b1; qb <= 1'b0; end
		3'b111 : begin q <= qb; qb <= q; end
		default : begin q <= 0; qb <= 1; end
	endcase
end
endmodule
