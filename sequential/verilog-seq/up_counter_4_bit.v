`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:39:42 07/31/2023 
// Design Name: 
// Module Name:    up_counter_4_bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module up_counter_4_bit(
output	reg	[3:0]		count,
intput	clock,clear,T
);

always@(posedge clk or negedge clear)
begin

if (( clear == 1'b0))
	count <= 4'b0000;
else if ((clear === 1'b1)&&(t === 1'b1))
	count <= count+1;

end
endmodule
