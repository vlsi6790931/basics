`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:05:53 08/01/2023 
// Design Name: 
// Module Name:    CaseMux 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CaseMux(
    output reg out,
	 input a,
    input b,
    input sel
    );

always@(*)
begin
	case ({sel})
			
			1'b0 : out = a;
			1'b1 : out = b;
			default : out = 1'bx;
			
	endcase
end
endmodule
