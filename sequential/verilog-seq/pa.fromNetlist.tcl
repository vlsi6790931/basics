
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name verilog-seq -dir "C:/Users/qwerty/Documents/basics/sequential/verilog-seq/planAhead_run_2" -part xc7a100tcsg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/qwerty/Documents/basics/sequential/verilog-seq/DIGITAL_CLOCK.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/qwerty/Documents/basics/sequential/verilog-seq} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "DIGITAL_CLOCK.ucf" [current_fileset -constrset]
add_files [list {DIGITAL_CLOCK.ucf}] -fileset [get_property constrset [current_run]]
link_design
