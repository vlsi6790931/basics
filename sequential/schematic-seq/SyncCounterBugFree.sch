<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="clear" />
        <signal name="clock" />
        <signal name="Q2" />
        <signal name="T" />
        <signal name="Q0" />
        <signal name="Q1" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="clock" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="T" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q1" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-25T19:29:19</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin signalname="T" name="T" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin signalname="Q0" name="T" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_4">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin signalname="Q1" name="T" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1104" y="1248" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1568" y="1248" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2032" y="1248" name="XLXI_4" orien="R0">
        </instance>
        <branch name="clear">
            <wire x2="1024" y1="1216" y2="1216" x1="944" />
            <wire x2="1104" y1="1216" y2="1216" x1="1024" />
            <wire x2="1024" y1="1216" y2="1280" x1="1024" />
            <wire x2="1552" y1="1280" y2="1280" x1="1024" />
            <wire x2="2032" y1="1280" y2="1280" x1="1552" />
            <wire x2="1568" y1="1216" y2="1216" x1="1552" />
            <wire x2="1552" y1="1216" y2="1280" x1="1552" />
            <wire x2="2032" y1="1216" y2="1280" x1="2032" />
        </branch>
        <branch name="clock">
            <wire x2="1088" y1="1152" y2="1152" x1="944" />
            <wire x2="1104" y1="1152" y2="1152" x1="1088" />
            <wire x2="1088" y1="1152" y2="1312" x1="1088" />
            <wire x2="1536" y1="1312" y2="1312" x1="1088" />
            <wire x2="2016" y1="1312" y2="1312" x1="1536" />
            <wire x2="1536" y1="1152" y2="1312" x1="1536" />
            <wire x2="1568" y1="1152" y2="1152" x1="1536" />
            <wire x2="2016" y1="1152" y2="1312" x1="2016" />
            <wire x2="2032" y1="1152" y2="1152" x1="2016" />
        </branch>
        <branch name="Q2">
            <wire x2="2432" y1="1088" y2="1088" x1="2416" />
            <wire x2="2432" y1="1088" y2="1440" x1="2432" />
        </branch>
        <branch name="T">
            <wire x2="1088" y1="1088" y2="1088" x1="896" />
            <wire x2="1104" y1="1088" y2="1088" x1="1088" />
        </branch>
        <branch name="Q0">
            <wire x2="1504" y1="1088" y2="1088" x1="1488" />
            <wire x2="1504" y1="1088" y2="1456" x1="1504" />
            <wire x2="1568" y1="1088" y2="1088" x1="1504" />
            <wire x2="1504" y1="1456" y2="1456" x1="1488" />
        </branch>
        <branch name="Q1">
            <wire x2="1968" y1="1088" y2="1088" x1="1952" />
            <wire x2="1968" y1="1088" y2="1456" x1="1968" />
            <wire x2="2032" y1="1088" y2="1088" x1="1968" />
            <wire x2="1968" y1="1456" y2="1456" x1="1952" />
        </branch>
        <iomarker fontsize="28" x="944" y="1152" name="clock" orien="R180" />
        <iomarker fontsize="28" x="2432" y="1440" name="Q2" orien="R90" />
        <iomarker fontsize="28" x="1488" y="1456" name="Q0" orien="R90" />
        <iomarker fontsize="28" x="1952" y="1456" name="Q1" orien="R90" />
        <iomarker fontsize="28" x="896" y="1088" name="T" orien="R180" />
        <iomarker fontsize="28" x="944" y="1216" name="clear" orien="R180" />
    </sheet>
</drawing>