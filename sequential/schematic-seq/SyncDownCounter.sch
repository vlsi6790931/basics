<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_120" />
        <signal name="XLXN_123" />
        <signal name="XLXN_124" />
        <signal name="XLXN_128" />
        <signal name="XLXN_134" />
        <signal name="XLXN_137" />
        <signal name="XLXN_138" />
        <signal name="XLXN_142" />
        <signal name="XLXN_57" />
        <signal name="clear" />
        <signal name="XLXN_148" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="XLXN_151" />
        <signal name="XLXN_152" />
        <signal name="clock199" />
        <signal name="XLXN_70" />
        <signal name="XLXN_64" />
        <signal name="XLXN_156" />
        <signal name="XLXN_77" />
        <signal name="Q0" />
        <signal name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q0" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_17">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_64" name="T" />
            <blockpin signalname="clock199" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin signalname="XLXN_77" name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_18">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_77" name="T" />
            <blockpin signalname="clock199" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin signalname="XLXN_57" name="Qb" />
        </block>
        <block symbolname="and2" name="XLXI_20">
            <blockpin signalname="XLXN_77" name="I0" />
            <blockpin signalname="XLXN_57" name="I1" />
            <blockpin signalname="XLXN_70" name="O" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_19">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_70" name="T" />
            <blockpin signalname="clock199" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="vcc" name="XLXI_21">
            <blockpin signalname="XLXN_64" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_27">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="clock199" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <text style="fontsize:100;fontname:Arial" x="688" y="324">SYNC_DOWN_COUNTER</text>
        <instance x="912" y="944" name="XLXI_17" orien="R0">
        </instance>
        <instance x="1280" y="944" name="XLXI_18" orien="R0">
        </instance>
        <instance x="1568" y="1120" name="XLXI_20" orien="R0" />
        <branch name="XLXN_57">
            <wire x2="1552" y1="912" y2="912" x1="1536" />
            <wire x2="1552" y1="912" y2="992" x1="1552" />
            <wire x2="1568" y1="992" y2="992" x1="1552" />
        </branch>
        <branch name="clear">
            <wire x2="672" y1="736" y2="736" x1="464" />
            <wire x2="1008" y1="736" y2="736" x1="672" />
            <wire x2="1008" y1="736" y2="752" x1="1008" />
            <wire x2="1376" y1="736" y2="736" x1="1008" />
            <wire x2="1376" y1="736" y2="752" x1="1376" />
            <wire x2="2016" y1="736" y2="736" x1="1376" />
            <wire x2="2016" y1="736" y2="752" x1="2016" />
            <wire x2="672" y1="736" y2="816" x1="672" />
        </branch>
        <branch name="Q1">
            <wire x2="1552" y1="848" y2="848" x1="1536" />
            <wire x2="1568" y1="848" y2="848" x1="1552" />
            <wire x2="1568" y1="592" y2="592" x1="1552" />
            <wire x2="1568" y1="592" y2="848" x1="1568" />
        </branch>
        <branch name="Q2">
            <wire x2="2192" y1="592" y2="592" x1="2176" />
            <wire x2="2192" y1="592" y2="848" x1="2192" />
            <wire x2="2192" y1="848" y2="848" x1="2176" />
        </branch>
        <branch name="clock199">
            <wire x2="912" y1="912" y2="912" x1="784" />
            <wire x2="912" y1="912" y2="1120" x1="912" />
            <wire x2="1248" y1="1120" y2="1120" x1="912" />
            <wire x2="1888" y1="1120" y2="1120" x1="1248" />
            <wire x2="1280" y1="912" y2="912" x1="1248" />
            <wire x2="1248" y1="912" y2="1120" x1="1248" />
            <wire x2="1920" y1="912" y2="912" x1="1888" />
            <wire x2="1888" y1="912" y2="1120" x1="1888" />
        </branch>
        <instance x="1920" y="944" name="XLXI_19" orien="R0">
        </instance>
        <branch name="XLXN_70">
            <wire x2="1840" y1="1024" y2="1024" x1="1824" />
            <wire x2="1840" y1="848" y2="1024" x1="1840" />
            <wire x2="1920" y1="848" y2="848" x1="1840" />
        </branch>
        <branch name="XLXN_64">
            <wire x2="912" y1="848" y2="848" x1="896" />
        </branch>
        <branch name="XLXN_77">
            <wire x2="1184" y1="912" y2="912" x1="1168" />
            <wire x2="1184" y1="912" y2="1056" x1="1184" />
            <wire x2="1568" y1="1056" y2="1056" x1="1184" />
            <wire x2="1216" y1="912" y2="912" x1="1184" />
            <wire x2="1280" y1="848" y2="848" x1="1216" />
            <wire x2="1216" y1="848" y2="912" x1="1216" />
        </branch>
        <branch name="Q0">
            <wire x2="1184" y1="592" y2="592" x1="1168" />
            <wire x2="1184" y1="592" y2="848" x1="1184" />
            <wire x2="1184" y1="848" y2="848" x1="1168" />
        </branch>
        <rect width="1660" x="592" y="664" height="532" />
        <instance x="896" y="912" name="XLXI_21" orien="R270" />
        <instance x="560" y="992" name="XLXI_27" orien="R0">
        </instance>
        <branch name="clock">
            <wire x2="560" y1="912" y2="912" x1="528" />
        </branch>
        <iomarker fontsize="28" x="1552" y="592" name="Q1" orien="R180" />
        <iomarker fontsize="28" x="1168" y="592" name="Q0" orien="R180" />
        <iomarker fontsize="28" x="2176" y="592" name="Q2" orien="R180" />
        <iomarker fontsize="28" x="464" y="736" name="clear" orien="R180" />
        <iomarker fontsize="28" x="528" y="912" name="clock" orien="R180" />
    </sheet>
</drawing>