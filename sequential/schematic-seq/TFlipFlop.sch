<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clock" />
        <signal name="clear" />
        <signal name="Q" />
        <signal name="T" />
        <signal name="Qb" />
        <port polarity="Input" name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q" />
        <port polarity="Input" name="T" />
        <port polarity="Output" name="Qb" />
        <blockdef name="JKMasterSlave">
            <timestamp>2023-7-25T18:46:50</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="112" y1="-192" y2="-256" x1="112" />
            <line x2="320" y1="-48" y2="-48" x1="256" />
            <rect width="192" x="64" y="-192" height="192" />
            <line x2="320" y1="-128" y2="-128" x1="256" />
        </blockdef>
        <block symbolname="JKMasterSlave" name="XLXI_1">
            <blockpin signalname="clock" name="enable" />
            <blockpin signalname="T" name="J" />
            <blockpin signalname="T" name="K" />
            <blockpin signalname="clear" name="c" />
            <blockpin signalname="Qb" name="Qb" />
            <blockpin signalname="Q" name="Q" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="704" y="912" name="XLXI_1" orien="R0">
        </instance>
        <branch name="clock">
            <wire x2="688" y1="880" y2="880" x1="656" />
            <wire x2="704" y1="880" y2="880" x1="688" />
        </branch>
        <branch name="clear">
            <wire x2="816" y1="624" y2="656" x1="816" />
        </branch>
        <branch name="Q">
            <wire x2="1056" y1="784" y2="784" x1="1024" />
        </branch>
        <branch name="T">
            <wire x2="688" y1="752" y2="752" x1="608" />
            <wire x2="704" y1="752" y2="752" x1="688" />
            <wire x2="688" y1="752" y2="816" x1="688" />
            <wire x2="704" y1="816" y2="816" x1="688" />
        </branch>
        <iomarker fontsize="28" x="816" y="624" name="clear" orien="R270" />
        <iomarker fontsize="28" x="1056" y="784" name="Q" orien="R0" />
        <iomarker fontsize="28" x="608" y="752" name="T" orien="R180" />
        <text style="fontsize:100;fontname:Arial" x="516" y="364">T_FLIP_FLOP</text>
        <iomarker fontsize="28" x="656" y="880" name="clock" orien="R180" />
        <branch name="Qb">
            <wire x2="1056" y1="864" y2="864" x1="1024" />
        </branch>
        <iomarker fontsize="28" x="1056" y="864" name="Qb" orien="R0" />
    </sheet>
</drawing>