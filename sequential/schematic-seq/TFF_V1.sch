<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="XLXN_7" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="Clear" />
        <signal name="XLXN_15" />
        <signal name="XLXN_27" />
        <signal name="XLXN_31" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_37" />
        <signal name="Q1" />
        <signal name="Qb1" />
        <signal name="Qb0">
        </signal>
        <signal name="Q0">
        </signal>
        <signal name="Tin" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="Clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Qb1" />
        <port polarity="Input" name="Tin" />
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <blockdef name="nand3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="216" y1="-128" y2="-128" x1="256" />
            <circle r="12" cx="204" cy="-128" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="nand2" name="XLXI_1">
            <blockpin signalname="Qb0" name="I0" />
            <blockpin signalname="XLXN_15" name="I1" />
            <blockpin signalname="Q0" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_2">
            <blockpin signalname="Clear" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="Q0" name="I2" />
            <blockpin signalname="Qb0" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_3">
            <blockpin signalname="XLXN_12" name="I0" />
            <blockpin signalname="XLXN_11" name="I1" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="Clear" name="I" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_9">
            <blockpin signalname="Qb1" name="I0" />
            <blockpin signalname="CLK" name="I1" />
            <blockpin signalname="Tin" name="I2" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_10">
            <blockpin signalname="Q1" name="I0" />
            <blockpin signalname="Tin" name="I1" />
            <blockpin signalname="CLK" name="I2" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_17">
            <blockpin signalname="Qb1" name="I0" />
            <blockpin signalname="XLXN_37" name="I1" />
            <blockpin signalname="Q1" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_18">
            <blockpin signalname="Clear" name="I0" />
            <blockpin signalname="XLXN_31" name="I1" />
            <blockpin signalname="Q1" name="I2" />
            <blockpin signalname="Qb1" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_19">
            <blockpin signalname="XLXN_34" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_37" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_20">
            <blockpin signalname="Clear" name="I" />
            <blockpin signalname="XLXN_34" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_23">
            <blockpin signalname="XLXN_27" name="I0" />
            <blockpin signalname="Q0" name="I1" />
            <blockpin signalname="XLXN_33" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_24">
            <blockpin signalname="Qb0" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="XLXN_31" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_25">
            <blockpin signalname="CLK" name="I" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7609" height="5382">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <instance x="1600" y="1248" name="XLXI_1" orien="R0" />
        <branch name="CLK">
            <wire x2="656" y1="1328" y2="1328" x1="496" />
            <wire x2="944" y1="1328" y2="1328" x1="656" />
            <wire x2="944" y1="1328" y2="1440" x1="944" />
            <wire x2="992" y1="1440" y2="1440" x1="944" />
            <wire x2="656" y1="1328" y2="1776" x1="656" />
            <wire x2="1776" y1="1776" y2="1776" x1="656" />
            <wire x2="976" y1="1152" y2="1152" x1="944" />
            <wire x2="944" y1="1152" y2="1328" x1="944" />
        </branch>
        <instance x="1616" y="1616" name="XLXI_2" orien="R0" />
        <branch name="XLXN_7">
            <wire x2="1264" y1="1504" y2="1504" x1="1248" />
            <wire x2="1616" y1="1488" y2="1488" x1="1264" />
            <wire x2="1264" y1="1488" y2="1504" x1="1264" />
        </branch>
        <instance x="1312" y="928" name="XLXI_3" orien="R0" />
        <branch name="XLXN_11">
            <wire x2="1296" y1="1152" y2="1152" x1="1232" />
            <wire x2="1312" y1="800" y2="800" x1="1296" />
            <wire x2="1296" y1="800" y2="1152" x1="1296" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="1312" y1="864" y2="864" x1="1200" />
        </branch>
        <instance x="976" y="896" name="XLXI_8" orien="R0" />
        <branch name="XLXN_15">
            <wire x2="1632" y1="944" y2="944" x1="1520" />
            <wire x2="1520" y1="944" y2="1120" x1="1520" />
            <wire x2="1600" y1="1120" y2="1120" x1="1520" />
            <wire x2="1632" y1="832" y2="832" x1="1568" />
            <wire x2="1632" y1="832" y2="944" x1="1632" />
        </branch>
        <instance x="992" y="1632" name="XLXI_10" orien="R0" />
        <text style="fontsize:44;fontname:Arial" x="44" y="1296">SR_Gated_Latch</text>
        <iomarker fontsize="28" x="688" y="864" name="Clear" orien="R180" />
        <instance x="3088" y="1216" name="XLXI_17" orien="R0" />
        <instance x="3104" y="1584" name="XLXI_18" orien="R0" />
        <instance x="2800" y="896" name="XLXI_19" orien="R0" />
        <branch name="XLXN_33">
            <wire x2="2768" y1="1120" y2="1120" x1="2736" />
            <wire x2="2800" y1="768" y2="768" x1="2768" />
            <wire x2="2768" y1="768" y2="1120" x1="2768" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="2800" y1="832" y2="832" x1="2688" />
        </branch>
        <instance x="2464" y="864" name="XLXI_20" orien="R0" />
        <branch name="XLXN_37">
            <wire x2="3120" y1="912" y2="912" x1="3008" />
            <wire x2="3008" y1="912" y2="1088" x1="3008" />
            <wire x2="3088" y1="1088" y2="1088" x1="3008" />
            <wire x2="3120" y1="800" y2="800" x1="3056" />
            <wire x2="3120" y1="800" y2="912" x1="3120" />
        </branch>
        <instance x="2480" y="1216" name="XLXI_23" orien="R0" />
        <branch name="XLXN_27">
            <wire x2="2384" y1="1776" y2="1776" x1="2000" />
            <wire x2="2432" y1="1296" y2="1296" x1="2384" />
            <wire x2="2432" y1="1296" y2="1408" x1="2432" />
            <wire x2="2480" y1="1408" y2="1408" x1="2432" />
            <wire x2="2384" y1="1296" y2="1776" x1="2384" />
            <wire x2="2480" y1="1152" y2="1152" x1="2432" />
            <wire x2="2432" y1="1152" y2="1296" x1="2432" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="3088" y1="1440" y2="1440" x1="2736" />
            <wire x2="3088" y1="1440" y2="1456" x1="3088" />
            <wire x2="3104" y1="1456" y2="1456" x1="3088" />
        </branch>
        <instance x="2480" y="1536" name="XLXI_24" orien="R0" />
        <instance x="976" y="1280" name="XLXI_9" orien="R0" />
        <instance x="1776" y="1808" name="XLXI_25" orien="R0" />
        <branch name="Q1">
            <wire x2="992" y1="1568" y2="1648" x1="992" />
            <wire x2="3440" y1="1648" y2="1648" x1="992" />
            <wire x2="3104" y1="1312" y2="1392" x1="3104" />
            <wire x2="3472" y1="1312" y2="1312" x1="3104" />
            <wire x2="3248" y1="864" y2="896" x1="3248" />
            <wire x2="3440" y1="896" y2="896" x1="3248" />
            <wire x2="3440" y1="896" y2="1120" x1="3440" />
            <wire x2="3440" y1="1120" y2="1648" x1="3440" />
            <wire x2="3472" y1="1120" y2="1120" x1="3440" />
            <wire x2="3472" y1="1120" y2="1312" x1="3472" />
            <wire x2="3440" y1="1120" y2="1120" x1="3344" />
        </branch>
        <branch name="Qb1">
            <wire x2="976" y1="1216" y2="1264" x1="976" />
            <wire x2="3424" y1="1264" y2="1264" x1="976" />
            <wire x2="3424" y1="1264" y2="1456" x1="3424" />
            <wire x2="3424" y1="1456" y2="1600" x1="3424" />
            <wire x2="3088" y1="1152" y2="1152" x1="3040" />
            <wire x2="3040" y1="1152" y2="1600" x1="3040" />
            <wire x2="3408" y1="1600" y2="1600" x1="3040" />
            <wire x2="3424" y1="1600" y2="1600" x1="3408" />
            <wire x2="3408" y1="1760" y2="1760" x1="3264" />
            <wire x2="3264" y1="1760" y2="1776" x1="3264" />
            <wire x2="3408" y1="1456" y2="1456" x1="3360" />
            <wire x2="3424" y1="1456" y2="1456" x1="3408" />
            <wire x2="3408" y1="1456" y2="1760" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3248" y="864" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="3264" y="1776" name="Qb1" orien="R90" />
        <branch name="Qb0">
            <wire x2="1600" y1="1184" y2="1184" x1="1520" />
            <wire x2="1520" y1="1184" y2="1280" x1="1520" />
            <wire x2="1984" y1="1280" y2="1280" x1="1520" />
            <wire x2="1984" y1="1280" y2="1488" x1="1984" />
            <wire x2="2416" y1="1488" y2="1488" x1="1984" />
            <wire x2="1984" y1="1488" y2="1488" x1="1872" />
            <wire x2="2480" y1="1472" y2="1472" x1="2416" />
            <wire x2="2416" y1="1472" y2="1488" x1="2416" />
        </branch>
        <branch name="Q0">
            <wire x2="1616" y1="1248" y2="1424" x1="1616" />
            <wire x2="2000" y1="1248" y2="1248" x1="1616" />
            <wire x2="2000" y1="1152" y2="1152" x1="1856" />
            <wire x2="2128" y1="1152" y2="1152" x1="2000" />
            <wire x2="2000" y1="1152" y2="1248" x1="2000" />
            <wire x2="2128" y1="1056" y2="1152" x1="2128" />
            <wire x2="2416" y1="1056" y2="1056" x1="2128" />
            <wire x2="2416" y1="1056" y2="1088" x1="2416" />
            <wire x2="2480" y1="1088" y2="1088" x1="2416" />
        </branch>
        <iomarker fontsize="28" x="496" y="1328" name="CLK" orien="R180" />
        <branch name="Clear">
            <wire x2="752" y1="864" y2="864" x1="688" />
            <wire x2="768" y1="864" y2="864" x1="752" />
            <wire x2="976" y1="864" y2="864" x1="768" />
            <wire x2="752" y1="864" y2="1232" x1="752" />
            <wire x2="752" y1="1232" y2="1888" x1="752" />
            <wire x2="1616" y1="1888" y2="1888" x1="752" />
            <wire x2="2176" y1="720" y2="720" x1="768" />
            <wire x2="2176" y1="720" y2="832" x1="2176" />
            <wire x2="2240" y1="832" y2="832" x1="2176" />
            <wire x2="2464" y1="832" y2="832" x1="2240" />
            <wire x2="2240" y1="832" y2="1856" x1="2240" />
            <wire x2="3104" y1="1856" y2="1856" x1="2240" />
            <wire x2="768" y1="720" y2="864" x1="768" />
            <wire x2="1616" y1="1552" y2="1888" x1="1616" />
            <wire x2="3104" y1="1520" y2="1856" x1="3104" />
        </branch>
        <branch name="Tin">
            <wire x2="960" y1="1136" y2="1136" x1="624" />
            <wire x2="960" y1="1136" y2="1328" x1="960" />
            <wire x2="960" y1="1328" y2="1504" x1="960" />
            <wire x2="992" y1="1504" y2="1504" x1="960" />
            <wire x2="976" y1="1088" y2="1088" x1="960" />
            <wire x2="960" y1="1088" y2="1136" x1="960" />
        </branch>
        <iomarker fontsize="28" x="624" y="1136" name="Tin" orien="R180" />
    </sheet>
</drawing>