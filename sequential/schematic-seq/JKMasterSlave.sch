<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_5" />
        <signal name="K" />
        <signal name="enable" />
        <signal name="J" />
        <signal name="XLXN_46" />
        <signal name="XLXN_75" />
        <signal name="XLXN_76" />
        <signal name="c" />
        <signal name="XLXN_58" />
        <signal name="XLXN_55" />
        <signal name="XLXN_54" />
        <signal name="XLXN_88" />
        <signal name="Qb" />
        <signal name="Q" />
        <port polarity="Input" name="K" />
        <port polarity="Input" name="enable" />
        <port polarity="Input" name="J" />
        <port polarity="Input" name="c" />
        <port polarity="Output" name="Qb" />
        <port polarity="Output" name="Q" />
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <blockdef name="nand3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="216" y1="-128" y2="-128" x1="256" />
            <circle r="12" cx="204" cy="-128" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="or2b1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="32" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="44" cy="-64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="nand2" name="XLXI_1">
            <blockpin signalname="XLXN_76" name="I0" />
            <blockpin signalname="XLXN_1" name="I1" />
            <blockpin signalname="XLXN_75" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_2">
            <blockpin signalname="c" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="XLXN_75" name="I2" />
            <blockpin signalname="XLXN_76" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_7">
            <blockpin signalname="Q" name="I0" />
            <blockpin signalname="K" name="I1" />
            <blockpin signalname="enable" name="I2" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_6">
            <blockpin signalname="enable" name="I0" />
            <blockpin signalname="J" name="I1" />
            <blockpin signalname="Qb" name="I2" />
            <blockpin signalname="XLXN_46" name="O" />
        </block>
        <block symbolname="or2b1" name="XLXI_8">
            <blockpin signalname="c" name="I0" />
            <blockpin signalname="XLXN_46" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_29">
            <blockpin signalname="enable" name="I" />
            <blockpin signalname="XLXN_58" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_15">
            <blockpin signalname="c" name="I0" />
            <blockpin signalname="XLXN_55" name="I1" />
            <blockpin signalname="Q" name="I2" />
            <blockpin signalname="Qb" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_14">
            <blockpin signalname="Qb" name="I0" />
            <blockpin signalname="XLXN_54" name="I1" />
            <blockpin signalname="Q" name="O" />
        </block>
        <block symbolname="or2b1" name="XLXI_18">
            <blockpin signalname="c" name="I0" />
            <blockpin signalname="XLXN_88" name="I1" />
            <blockpin signalname="XLXN_54" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_38">
            <blockpin signalname="XLXN_76" name="I0" />
            <blockpin signalname="XLXN_58" name="I1" />
            <blockpin signalname="XLXN_55" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_39">
            <blockpin signalname="XLXN_58" name="I0" />
            <blockpin signalname="XLXN_75" name="I1" />
            <blockpin signalname="XLXN_88" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1248" y="1312" name="XLXI_1" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="1248" y1="1184" y2="1184" x1="1232" />
        </branch>
        <instance x="1264" y="1664" name="XLXI_2" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="1264" y1="1536" y2="1536" x1="928" />
        </branch>
        <instance x="672" y="1664" name="XLXI_7" orien="R0" />
        <branch name="K">
            <wire x2="672" y1="1536" y2="1536" x1="128" />
        </branch>
        <instance x="656" y="1280" name="XLXI_6" orien="R0" />
        <branch name="J">
            <wire x2="656" y1="1152" y2="1152" x1="128" />
        </branch>
        <instance x="976" y="1280" name="XLXI_8" orien="R0" />
        <branch name="XLXN_46">
            <wire x2="976" y1="1152" y2="1152" x1="912" />
        </branch>
        <rect width="232" x="976" y="1100" height="188" />
        <rect style="linecolor:rgb(255,0,0);fillcolor:rgb(0,255,255)" width="1025" x="555" y="1036" height="660" />
        <branch name="XLXN_76">
            <wire x2="1248" y1="1248" y2="1248" x1="1232" />
            <wire x2="1232" y1="1248" y2="1344" x1="1232" />
            <wire x2="1552" y1="1344" y2="1344" x1="1232" />
            <wire x2="1552" y1="1344" y2="1536" x1="1552" />
            <wire x2="2144" y1="1536" y2="1536" x1="1552" />
            <wire x2="1552" y1="1536" y2="1536" x1="1520" />
        </branch>
        <branch name="enable">
            <wire x2="480" y1="1344" y2="1344" x1="272" />
            <wire x2="656" y1="1344" y2="1344" x1="480" />
            <wire x2="656" y1="1344" y2="1472" x1="656" />
            <wire x2="672" y1="1472" y2="1472" x1="656" />
            <wire x2="480" y1="1344" y2="1808" x1="480" />
            <wire x2="1680" y1="1808" y2="1808" x1="480" />
            <wire x2="656" y1="1216" y2="1344" x1="656" />
            <wire x2="1792" y1="1360" y2="1360" x1="1680" />
            <wire x2="1680" y1="1360" y2="1808" x1="1680" />
        </branch>
        <instance x="1792" y="1392" name="XLXI_29" orien="R0" />
        <instance x="2720" y="1328" name="XLXI_14" orien="R0" />
        <branch name="XLXN_58">
            <wire x2="2128" y1="1360" y2="1360" x1="2016" />
            <wire x2="2128" y1="1360" y2="1472" x1="2128" />
            <wire x2="2144" y1="1472" y2="1472" x1="2128" />
            <wire x2="2128" y1="1248" y2="1360" x1="2128" />
        </branch>
        <branch name="XLXN_54">
            <wire x2="2720" y1="1200" y2="1200" x1="2704" />
        </branch>
        <branch name="c">
            <wire x2="944" y1="512" y2="992" x1="944" />
            <wire x2="944" y1="992" y2="1216" x1="944" />
            <wire x2="944" y1="1216" y2="1600" x1="944" />
            <wire x2="1264" y1="1600" y2="1600" x1="944" />
            <wire x2="976" y1="1216" y2="1216" x1="944" />
            <wire x2="2432" y1="992" y2="992" x1="944" />
            <wire x2="2432" y1="992" y2="1232" x1="2432" />
            <wire x2="2448" y1="1232" y2="1232" x1="2432" />
            <wire x2="2432" y1="1232" y2="1568" x1="2432" />
            <wire x2="2720" y1="1568" y2="1568" x1="2432" />
        </branch>
        <rect width="232" x="2448" y="1132" height="188" />
        <branch name="XLXN_75">
            <wire x2="1264" y1="1408" y2="1472" x1="1264" />
            <wire x2="1520" y1="1408" y2="1408" x1="1264" />
            <wire x2="1520" y1="1216" y2="1216" x1="1504" />
            <wire x2="1520" y1="1216" y2="1408" x1="1520" />
            <wire x2="1520" y1="1184" y2="1216" x1="1520" />
            <wire x2="2128" y1="1184" y2="1184" x1="1520" />
        </branch>
        <instance x="2128" y="1312" name="XLXI_39" orien="R0" />
        <branch name="XLXN_55">
            <wire x2="2720" y1="1504" y2="1504" x1="2400" />
        </branch>
        <instance x="2448" y="1296" name="XLXI_18" orien="R0" />
        <branch name="XLXN_88">
            <wire x2="2400" y1="1216" y2="1216" x1="2384" />
            <wire x2="2400" y1="1168" y2="1216" x1="2400" />
            <wire x2="2448" y1="1168" y2="1168" x1="2400" />
        </branch>
        <instance x="2144" y="1600" name="XLXI_38" orien="R0" />
        <instance x="2720" y="1632" name="XLXI_15" orien="R0" />
        <text style="fontsize:100;fontname:Arial" x="2484" y="968">SLAVE</text>
        <text style="fontsize:100;fontname:Arial" x="991" y="936">MASTER</text>
        <text style="fontsize:30;fontname:Arial" x="64" y="1344">clock/</text>
        <iomarker fontsize="28" x="272" y="1344" name="enable" orien="R180" />
        <iomarker fontsize="28" x="128" y="1536" name="K" orien="R180" />
        <iomarker fontsize="28" x="128" y="1152" name="J" orien="R180" />
        <iomarker fontsize="28" x="944" y="512" name="c" orien="R270" />
        <rect width="2984" x="320" y="628" height="1516" />
        <branch name="Qb">
            <wire x2="656" y1="816" y2="1088" x1="656" />
            <wire x2="3024" y1="816" y2="816" x1="656" />
            <wire x2="3024" y1="816" y2="1344" x1="3024" />
            <wire x2="3024" y1="1344" y2="1504" x1="3024" />
            <wire x2="3376" y1="1504" y2="1504" x1="3024" />
            <wire x2="2720" y1="1264" y2="1344" x1="2720" />
            <wire x2="3024" y1="1344" y2="1344" x1="2720" />
            <wire x2="3024" y1="1504" y2="1504" x1="2976" />
        </branch>
        <branch name="Q">
            <wire x2="672" y1="1600" y2="1936" x1="672" />
            <wire x2="2992" y1="1936" y2="1936" x1="672" />
            <wire x2="2720" y1="1392" y2="1440" x1="2720" />
            <wire x2="2992" y1="1392" y2="1392" x1="2720" />
            <wire x2="2992" y1="1392" y2="1936" x1="2992" />
            <wire x2="2992" y1="1232" y2="1232" x1="2976" />
            <wire x2="3360" y1="1232" y2="1232" x1="2992" />
            <wire x2="2992" y1="1232" y2="1392" x1="2992" />
        </branch>
        <iomarker fontsize="28" x="3360" y="1232" name="Q" orien="R0" />
        <iomarker fontsize="28" x="3376" y="1504" name="Qb" orien="R0" />
        <rect style="linecolor:rgb(0,255,255)" width="1077" x="2023" y="1056" height="628" />
        <text style="fontsize:150;fontname:Arial" x="1124" y="344">JK_MASTER_SLAVE</text>
    </sheet>
</drawing>