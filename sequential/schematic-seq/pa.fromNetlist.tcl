
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name schematic-seq -dir "C:/Users/salty/Desktop/vlsi/sequential/schematic-seq/planAhead_run_3" -part xc7a100tcsg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/salty/Desktop/vlsi/sequential/schematic-seq/JohnsonCounter.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/salty/Desktop/vlsi/sequential/schematic-seq} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "JohnsonCounter.ucf" [current_fileset -constrset]
add_files [list {JohnsonCounter.ucf}] -fileset [get_property constrset [current_run]]
link_design
