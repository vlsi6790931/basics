<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="Q0" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="XLXN_63" />
        <signal name="XLXN_64" />
        <signal name="XLXN_6" />
        <signal name="Q3" />
        <signal name="clear" />
        <signal name="XLXN_41" />
        <signal name="XLXN_70" />
        <signal name="clock" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q3" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="nor2b1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="32" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="44" cy="-64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:37:0</timestamp>
            <rect width="96" x="80" y="-176" height="96" />
            <line x2="16" y1="-128" y2="-128" x1="80" />
            <line x2="240" y1="-128" y2="-128" x1="176" />
            <line x2="128" y1="-224" y2="-172" x1="128" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_4">
            <blockpin signalname="XLXN_1" name="clear" />
            <blockpin signalname="XLXN_6" name="T" />
            <blockpin signalname="Q2" name="clock" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="XLXN_1" name="clear" />
            <blockpin signalname="XLXN_6" name="T" />
            <blockpin signalname="Q1" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="XLXN_1" name="clear" />
            <blockpin signalname="XLXN_6" name="T" />
            <blockpin signalname="Q0" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="XLXN_1" name="clear" />
            <blockpin signalname="XLXN_6" name="T" />
            <blockpin signalname="XLXN_70" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="vcc" name="XLXI_5">
            <blockpin signalname="XLXN_6" name="P" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="Q3" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="XLXN_41" name="O" />
        </block>
        <block symbolname="nor2b1" name="XLXI_27">
            <blockpin signalname="clear" name="I0" />
            <blockpin signalname="XLXN_41" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_35">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="XLXN_70" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="2000" y="1120" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1680" y="1120" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1360" y="1120" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1040" y="1120" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_1">
            <wire x2="1456" y1="880" y2="880" x1="1136" />
            <wire x2="1776" y1="880" y2="880" x1="1456" />
            <wire x2="2096" y1="880" y2="880" x1="1776" />
            <wire x2="2096" y1="880" y2="928" x1="2096" />
            <wire x2="1776" y1="880" y2="928" x1="1776" />
            <wire x2="1456" y1="880" y2="928" x1="1456" />
            <wire x2="1136" y1="880" y2="928" x1="1136" />
        </branch>
        <branch name="Q0">
            <wire x2="1312" y1="1024" y2="1024" x1="1296" />
            <wire x2="1312" y1="1024" y2="1088" x1="1312" />
            <wire x2="1360" y1="1088" y2="1088" x1="1312" />
            <wire x2="1312" y1="496" y2="1024" x1="1312" />
        </branch>
        <branch name="Q1">
            <wire x2="1632" y1="752" y2="752" x1="1200" />
            <wire x2="1632" y1="752" y2="1024" x1="1632" />
            <wire x2="1632" y1="1024" y2="1088" x1="1632" />
            <wire x2="1680" y1="1088" y2="1088" x1="1632" />
            <wire x2="1632" y1="1024" y2="1024" x1="1616" />
            <wire x2="1632" y1="496" y2="752" x1="1632" />
        </branch>
        <branch name="Q2">
            <wire x2="1968" y1="1024" y2="1024" x1="1936" />
            <wire x2="1968" y1="1024" y2="1088" x1="1968" />
            <wire x2="2000" y1="1088" y2="1088" x1="1968" />
            <wire x2="1968" y1="496" y2="1024" x1="1968" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="976" y1="1024" y2="1024" x1="944" />
            <wire x2="1040" y1="1024" y2="1024" x1="976" />
            <wire x2="976" y1="1024" y2="1152" x1="976" />
            <wire x2="1344" y1="1152" y2="1152" x1="976" />
            <wire x2="1664" y1="1152" y2="1152" x1="1344" />
            <wire x2="2000" y1="1152" y2="1152" x1="1664" />
            <wire x2="1360" y1="1024" y2="1024" x1="1344" />
            <wire x2="1344" y1="1024" y2="1152" x1="1344" />
            <wire x2="1680" y1="1024" y2="1024" x1="1664" />
            <wire x2="1664" y1="1024" y2="1152" x1="1664" />
            <wire x2="2000" y1="1024" y2="1152" x1="2000" />
        </branch>
        <instance x="944" y="1088" name="XLXI_5" orien="R270" />
        <branch name="Q3">
            <wire x2="2256" y1="688" y2="688" x1="1200" />
            <wire x2="2256" y1="688" y2="1024" x1="2256" />
            <wire x2="2256" y1="496" y2="688" x1="2256" />
        </branch>
        <instance x="1200" y="624" name="XLXI_12" orien="R180" />
        <branch name="clear">
            <wire x2="688" y1="912" y2="912" x1="448" />
            <wire x2="864" y1="912" y2="912" x1="688" />
            <wire x2="880" y1="912" y2="912" x1="864" />
            <wire x2="688" y1="912" y2="992" x1="688" />
        </branch>
        <branch name="XLXN_41">
            <wire x2="944" y1="720" y2="720" x1="880" />
            <wire x2="880" y1="720" y2="848" x1="880" />
        </branch>
        <instance x="880" y="976" name="XLXI_27" orien="R0" />
        <rect width="1772" x="560" y="584" height="644" />
        <text style="fontsize:100;fontname:Arial" x="828" y="256">ASYNC_DECADE_COUNTER</text>
        <iomarker fontsize="28" x="1312" y="496" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="1632" y="496" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="1968" y="496" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="2256" y="496" name="Q3" orien="R270" />
        <branch name="XLXN_70">
            <wire x2="1040" y1="1088" y2="1088" x1="800" />
        </branch>
        <instance x="560" y="1216" name="XLXI_35" orien="R0">
        </instance>
        <branch name="clock">
            <wire x2="560" y1="1088" y2="1088" x1="448" />
            <wire x2="576" y1="1088" y2="1088" x1="560" />
        </branch>
        <iomarker fontsize="28" x="448" y="1088" name="clock" orien="R180" />
        <iomarker fontsize="28" x="448" y="912" name="clear" orien="R180" />
    </sheet>
</drawing>