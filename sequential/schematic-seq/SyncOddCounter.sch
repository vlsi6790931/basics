<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clear" />
        <signal name="clock399">
        </signal>
        <signal name="XLXN_34" />
        <signal name="Q2" />
        <signal name="XLXN_48" />
        <signal name="Q1" />
        <signal name="Q3" />
        <signal name="XLXN_43" />
        <signal name="Q0" />
        <signal name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q3" />
        <port polarity="Output" name="Q0" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_43" name="T" />
            <blockpin signalname="clock399" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q1" name="T" />
            <blockpin signalname="clock399" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="Q2" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="XLXN_34" name="O" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_4">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_34" name="T" />
            <blockpin signalname="clock399" name="clock" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="vcc" name="XLXI_5">
            <blockpin signalname="XLXN_43" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_8">
            <blockpin signalname="Q0" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_15">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="clock399" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="896" y="816" name="XLXI_1" orien="R0">
        </instance>
        <branch name="clear">
            <wire x2="576" y1="608" y2="608" x1="304" />
            <wire x2="992" y1="608" y2="608" x1="576" />
            <wire x2="992" y1="608" y2="624" x1="992" />
            <wire x2="1328" y1="608" y2="608" x1="992" />
            <wire x2="1328" y1="608" y2="624" x1="1328" />
            <wire x2="1856" y1="608" y2="608" x1="1328" />
            <wire x2="1856" y1="608" y2="624" x1="1856" />
            <wire x2="576" y1="608" y2="688" x1="576" />
        </branch>
        <instance x="1232" y="816" name="XLXI_2" orien="R0">
        </instance>
        <branch name="clock399">
            <wire x2="880" y1="784" y2="784" x1="688" />
            <wire x2="880" y1="784" y2="864" x1="880" />
            <wire x2="1232" y1="864" y2="864" x1="880" />
            <wire x2="1760" y1="864" y2="864" x1="1232" />
            <wire x2="896" y1="784" y2="784" x1="880" />
            <wire x2="1232" y1="784" y2="864" x1="1232" />
            <wire x2="1760" y1="784" y2="864" x1="1760" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="1760" y1="496" y2="720" x1="1760" />
        </branch>
        <branch name="Q2">
            <wire x2="1504" y1="720" y2="720" x1="1488" />
            <wire x2="1568" y1="720" y2="720" x1="1504" />
            <wire x2="1568" y1="720" y2="1072" x1="1568" />
            <wire x2="1504" y1="528" y2="720" x1="1504" />
        </branch>
        <instance x="1504" y="592" name="XLXI_3" orien="R0" />
        <branch name="Q1">
            <wire x2="1168" y1="720" y2="720" x1="1152" />
            <wire x2="1200" y1="720" y2="720" x1="1168" />
            <wire x2="1232" y1="720" y2="720" x1="1200" />
            <wire x2="1200" y1="720" y2="1072" x1="1200" />
            <wire x2="1504" y1="464" y2="464" x1="1168" />
            <wire x2="1168" y1="464" y2="720" x1="1168" />
        </branch>
        <branch name="Q3">
            <wire x2="2032" y1="720" y2="720" x1="2016" />
            <wire x2="2112" y1="720" y2="720" x1="2032" />
            <wire x2="2112" y1="720" y2="1072" x1="2112" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="896" y1="720" y2="720" x1="832" />
        </branch>
        <instance x="1760" y="816" name="XLXI_4" orien="R0">
        </instance>
        <text style="fontsize:100;fontname:Arial" x="776" y="252">SYNC_ODD_COUNTER</text>
        <branch name="Q0">
            <wire x2="800" y1="960" y2="1072" x1="800" />
        </branch>
        <rect width="1720" x="456" y="400" height="584" />
        <instance x="832" y="784" name="XLXI_5" orien="R270" />
        <instance x="864" y="960" name="XLXI_8" orien="M0" />
        <iomarker fontsize="28" x="1200" y="1072" name="Q1" orien="R90" />
        <iomarker fontsize="28" x="2112" y="1072" name="Q3" orien="R90" />
        <iomarker fontsize="28" x="1568" y="1072" name="Q2" orien="R90" />
        <iomarker fontsize="28" x="800" y="1072" name="Q0" orien="R90" />
        <instance x="464" y="864" name="XLXI_15" orien="R0">
        </instance>
        <iomarker fontsize="28" x="304" y="608" name="clear" orien="R180" />
        <branch name="clock">
            <wire x2="448" y1="784" y2="784" x1="304" />
            <wire x2="464" y1="784" y2="784" x1="448" />
        </branch>
        <iomarker fontsize="28" x="304" y="784" name="clock" orien="R180" />
    </sheet>
</drawing>