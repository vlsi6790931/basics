<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clock" />
        <signal name="clear" />
        <signal name="Q1" />
        <signal name="Q0" />
        <signal name="Q2" />
        <signal name="XLXN_11" />
        <signal name="Q3" />
        <signal name="clk" />
        <signal name="XLXN_12" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q3" />
        <port polarity="Input" name="clk" />
        <blockdef name="DFlipFLop">
            <timestamp>2023-7-25T19:26:20</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="DFlipFLop" name="XLXI_1">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_11" name="DATA" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_2">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q0" name="DATA" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_3">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q1" name="DATA" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_4">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q2" name="DATA" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="inv" name="XLXI_14">
            <blockpin signalname="Q3" name="I" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_15">
            <blockpin signalname="clk" name="clock" />
            <blockpin signalname="clock" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="960" y="1728" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1520" y="1728" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2080" y="1728" name="XLXI_3" orien="R0">
        </instance>
        <instance x="2640" y="1712" name="XLXI_4" orien="R0">
        </instance>
        <branch name="clock">
            <wire x2="896" y1="1440" y2="1440" x1="480" />
            <wire x2="1504" y1="1440" y2="1440" x1="896" />
            <wire x2="1504" y1="1440" y2="1568" x1="1504" />
            <wire x2="1520" y1="1568" y2="1568" x1="1504" />
            <wire x2="2064" y1="1440" y2="1440" x1="1504" />
            <wire x2="2064" y1="1440" y2="1568" x1="2064" />
            <wire x2="2080" y1="1568" y2="1568" x1="2064" />
            <wire x2="2544" y1="1440" y2="1440" x1="2064" />
            <wire x2="2544" y1="1440" y2="1552" x1="2544" />
            <wire x2="2640" y1="1552" y2="1552" x1="2544" />
            <wire x2="896" y1="1440" y2="1568" x1="896" />
            <wire x2="960" y1="1568" y2="1568" x1="896" />
        </branch>
        <branch name="clear">
            <wire x2="368" y1="1840" y2="1840" x1="160" />
            <wire x2="880" y1="1840" y2="1840" x1="368" />
            <wire x2="1504" y1="1840" y2="1840" x1="880" />
            <wire x2="2048" y1="1840" y2="1840" x1="1504" />
            <wire x2="2576" y1="1840" y2="1840" x1="2048" />
            <wire x2="368" y1="1296" y2="1344" x1="368" />
            <wire x2="560" y1="1296" y2="1296" x1="368" />
            <wire x2="560" y1="1296" y2="1552" x1="560" />
            <wire x2="368" y1="1552" y2="1840" x1="368" />
            <wire x2="560" y1="1552" y2="1552" x1="368" />
            <wire x2="960" y1="1632" y2="1632" x1="880" />
            <wire x2="880" y1="1632" y2="1840" x1="880" />
            <wire x2="1520" y1="1632" y2="1632" x1="1504" />
            <wire x2="1504" y1="1632" y2="1840" x1="1504" />
            <wire x2="2080" y1="1632" y2="1632" x1="2048" />
            <wire x2="2048" y1="1632" y2="1840" x1="2048" />
            <wire x2="2640" y1="1616" y2="1616" x1="2576" />
            <wire x2="2576" y1="1616" y2="1840" x1="2576" />
        </branch>
        <branch name="Q1">
            <wire x2="1984" y1="1568" y2="1568" x1="1904" />
            <wire x2="1984" y1="1568" y2="1696" x1="1984" />
            <wire x2="2080" y1="1696" y2="1696" x1="1984" />
            <wire x2="1984" y1="1696" y2="2064" x1="1984" />
        </branch>
        <branch name="Q0">
            <wire x2="1424" y1="1568" y2="1568" x1="1344" />
            <wire x2="1424" y1="1568" y2="1696" x1="1424" />
            <wire x2="1520" y1="1696" y2="1696" x1="1424" />
            <wire x2="1424" y1="1696" y2="2064" x1="1424" />
        </branch>
        <branch name="Q2">
            <wire x2="2512" y1="1568" y2="1568" x1="2464" />
            <wire x2="2512" y1="1568" y2="1680" x1="2512" />
            <wire x2="2640" y1="1680" y2="1680" x1="2512" />
            <wire x2="2512" y1="1680" y2="2064" x1="2512" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="960" y1="1696" y2="1696" x1="912" />
        </branch>
        <branch name="Q3">
            <wire x2="3088" y1="1344" y2="1344" x1="624" />
            <wire x2="3088" y1="1344" y2="1552" x1="3088" />
            <wire x2="3088" y1="1552" y2="2064" x1="3088" />
            <wire x2="624" y1="1344" y2="1696" x1="624" />
            <wire x2="688" y1="1696" y2="1696" x1="624" />
            <wire x2="3088" y1="1552" y2="1552" x1="3024" />
        </branch>
        <rect width="2988" x="264" y="1216" height="708" />
        <iomarker fontsize="28" x="1424" y="2064" name="Q0" orien="R90" />
        <iomarker fontsize="28" x="1984" y="2064" name="Q1" orien="R90" />
        <iomarker fontsize="28" x="2512" y="2064" name="Q2" orien="R90" />
        <iomarker fontsize="28" x="3088" y="2064" name="Q3" orien="R90" />
        <instance x="688" y="1664" name="XLXI_14" orien="M180" />
        <text style="fontsize:150;fontname:Arial" x="1056" y="776">JOHNSON_COUNTER</text>
        <branch name="clk">
            <wire x2="240" y1="1440" y2="1440" x1="144" />
            <wire x2="256" y1="1440" y2="1440" x1="240" />
        </branch>
        <iomarker fontsize="28" x="160" y="1840" name="clear" orien="R180" />
        <instance x="256" y="1520" name="XLXI_15" orien="R0">
        </instance>
        <iomarker fontsize="28" x="144" y="1440" name="clk" orien="R180" />
    </sheet>
</drawing>