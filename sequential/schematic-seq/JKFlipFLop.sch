<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="Q" />
        <signal name="XLXN_10" />
        <signal name="Qb" />
        <signal name="enable" />
        <signal name="K" />
        <signal name="J" />
        <signal name="clear" />
        <signal name="XLXN_46" />
        <port polarity="Output" name="Q" />
        <port polarity="Output" name="Qb" />
        <port polarity="Input" name="enable" />
        <port polarity="Input" name="K" />
        <port polarity="Input" name="J" />
        <port polarity="Input" name="clear" />
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <blockdef name="nand3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="216" y1="-128" y2="-128" x1="256" />
            <circle r="12" cx="204" cy="-128" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="or2b1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="32" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="44" cy="-64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
        </blockdef>
        <block symbolname="nand2" name="XLXI_3">
            <blockpin signalname="Qb" name="I0" />
            <blockpin signalname="XLXN_1" name="I1" />
            <blockpin signalname="Q" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_4">
            <blockpin signalname="clear" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="Q" name="I2" />
            <blockpin signalname="Qb" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_7">
            <blockpin signalname="Q" name="I0" />
            <blockpin signalname="K" name="I1" />
            <blockpin signalname="enable" name="I2" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_6">
            <blockpin signalname="enable" name="I0" />
            <blockpin signalname="J" name="I1" />
            <blockpin signalname="Qb" name="I2" />
            <blockpin signalname="XLXN_46" name="O" />
        </block>
        <block symbolname="or2b1" name="XLXI_8">
            <blockpin signalname="clear" name="I0" />
            <blockpin signalname="XLXN_46" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1520" y="928" name="XLXI_3" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="1520" y1="800" y2="800" x1="1504" />
        </branch>
        <instance x="1536" y="1280" name="XLXI_4" orien="R0" />
        <branch name="Q">
            <wire x2="944" y1="1216" y2="1264" x1="944" />
            <wire x2="1856" y1="1264" y2="1264" x1="944" />
            <wire x2="1536" y1="1024" y2="1088" x1="1536" />
            <wire x2="1856" y1="1024" y2="1024" x1="1536" />
            <wire x2="1856" y1="1024" y2="1264" x1="1856" />
            <wire x2="1856" y1="832" y2="832" x1="1776" />
            <wire x2="1856" y1="832" y2="1024" x1="1856" />
            <wire x2="2336" y1="832" y2="832" x1="1856" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="1536" y1="1152" y2="1152" x1="1200" />
        </branch>
        <instance x="944" y="1280" name="XLXI_7" orien="R0" />
        <branch name="K">
            <wire x2="944" y1="1152" y2="1152" x1="592" />
        </branch>
        <branch name="clear">
            <wire x2="1216" y1="576" y2="576" x1="592" />
            <wire x2="1216" y1="576" y2="832" x1="1216" />
            <wire x2="1216" y1="832" y2="1216" x1="1216" />
            <wire x2="1536" y1="1216" y2="1216" x1="1216" />
            <wire x2="1248" y1="832" y2="832" x1="1216" />
        </branch>
        <rect width="1284" x="827" y="464" height="960" />
        <iomarker fontsize="28" x="2336" y="832" name="Q" orien="R0" />
        <iomarker fontsize="28" x="2336" y="1152" name="Qb" orien="R0" />
        <iomarker fontsize="28" x="592" y="960" name="enable" orien="R180" />
        <iomarker fontsize="28" x="592" y="1152" name="K" orien="R180" />
        <iomarker fontsize="28" x="592" y="576" name="clear" orien="R180" />
        <text style="fontsize:100;fontname:Arial" x="1095" y="352">JK_FLIP_FLOP</text>
        <text style="fontsize:30;fontname:Arial" x="384" y="960">clock/</text>
        <iomarker fontsize="28" x="592" y="768" name="J" orien="R180" />
        <branch name="enable">
            <wire x2="928" y1="960" y2="960" x1="592" />
            <wire x2="928" y1="960" y2="1088" x1="928" />
            <wire x2="944" y1="1088" y2="1088" x1="928" />
            <wire x2="928" y1="832" y2="960" x1="928" />
        </branch>
        <branch name="Qb">
            <wire x2="2032" y1="640" y2="640" x1="928" />
            <wire x2="2032" y1="640" y2="944" x1="2032" />
            <wire x2="2032" y1="944" y2="1152" x1="2032" />
            <wire x2="2336" y1="1152" y2="1152" x1="2032" />
            <wire x2="928" y1="640" y2="704" x1="928" />
            <wire x2="1520" y1="864" y2="944" x1="1520" />
            <wire x2="2032" y1="944" y2="944" x1="1520" />
            <wire x2="2032" y1="1152" y2="1152" x1="1792" />
        </branch>
        <instance x="928" y="896" name="XLXI_6" orien="R0" />
        <branch name="J">
            <wire x2="608" y1="768" y2="768" x1="592" />
            <wire x2="912" y1="768" y2="768" x1="608" />
            <wire x2="928" y1="768" y2="768" x1="912" />
        </branch>
        <instance x="1248" y="896" name="XLXI_8" orien="R0" />
        <branch name="XLXN_46">
            <wire x2="1248" y1="768" y2="768" x1="1184" />
        </branch>
        <rect width="232" x="1248" y="716" height="188" />
    </sheet>
</drawing>