<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="wire">
        </signal>
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="clear" />
        <signal name="I" />
        <signal name="Q0" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="Q3" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="I" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q3" />
        <port polarity="Input" name="clock" />
        <blockdef name="DFlipFLop">
            <timestamp>2023-7-25T19:26:20</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="DFlipFLop" name="XLXI_1">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="I" name="DATA" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_2">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q0" name="DATA" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_3">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q2" name="DATA" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_4">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q1" name="DATA" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_9">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="wire" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <text style="fontsize:150;fontname:Arial" x="1096" y="220">SIPO</text>
        <instance x="736" y="960" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1216" y="960" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2208" y="960" name="XLXI_3" orien="R0">
        </instance>
        <branch name="wire">
            <wire x2="672" y1="688" y2="688" x1="496" />
            <wire x2="672" y1="688" y2="800" x1="672" />
            <wire x2="736" y1="800" y2="800" x1="672" />
            <wire x2="1200" y1="688" y2="688" x1="672" />
            <wire x2="1200" y1="688" y2="800" x1="1200" />
            <wire x2="1216" y1="800" y2="800" x1="1200" />
            <wire x2="1696" y1="688" y2="688" x1="1200" />
            <wire x2="2192" y1="688" y2="688" x1="1696" />
            <wire x2="2192" y1="688" y2="800" x1="2192" />
            <wire x2="2208" y1="800" y2="800" x1="2192" />
            <wire x2="1696" y1="688" y2="800" x1="1696" />
        </branch>
        <branch name="clear">
            <wire x2="400" y1="1040" y2="1040" x1="288" />
            <wire x2="672" y1="1040" y2="1040" x1="400" />
            <wire x2="1184" y1="1040" y2="1040" x1="672" />
            <wire x2="1680" y1="1040" y2="1040" x1="1184" />
            <wire x2="2176" y1="1040" y2="1040" x1="1680" />
            <wire x2="400" y1="800" y2="1040" x1="400" />
            <wire x2="672" y1="864" y2="1040" x1="672" />
            <wire x2="736" y1="864" y2="864" x1="672" />
            <wire x2="1184" y1="864" y2="1040" x1="1184" />
            <wire x2="1216" y1="864" y2="864" x1="1184" />
            <wire x2="1696" y1="864" y2="864" x1="1680" />
            <wire x2="1680" y1="864" y2="1040" x1="1680" />
            <wire x2="2208" y1="864" y2="864" x1="2176" />
            <wire x2="2176" y1="864" y2="1040" x1="2176" />
        </branch>
        <instance x="1696" y="960" name="XLXI_4" orien="R0">
        </instance>
        <branch name="I">
            <wire x2="736" y1="928" y2="928" x1="240" />
        </branch>
        <branch name="Q0">
            <wire x2="1152" y1="800" y2="800" x1="1120" />
            <wire x2="1152" y1="800" y2="928" x1="1152" />
            <wire x2="1216" y1="928" y2="928" x1="1152" />
            <wire x2="1152" y1="928" y2="1136" x1="1152" />
        </branch>
        <branch name="Q1">
            <wire x2="1632" y1="800" y2="800" x1="1600" />
            <wire x2="1632" y1="800" y2="928" x1="1632" />
            <wire x2="1696" y1="928" y2="928" x1="1632" />
            <wire x2="1632" y1="928" y2="1152" x1="1632" />
        </branch>
        <branch name="Q2">
            <wire x2="2128" y1="800" y2="800" x1="2080" />
            <wire x2="2128" y1="800" y2="928" x1="2128" />
            <wire x2="2208" y1="928" y2="928" x1="2128" />
            <wire x2="2128" y1="928" y2="1136" x1="2128" />
        </branch>
        <branch name="Q3">
            <wire x2="2640" y1="800" y2="800" x1="2592" />
            <wire x2="2640" y1="800" y2="1136" x1="2640" />
        </branch>
        <iomarker fontsize="28" x="2640" y="1136" name="Q3" orien="R90" />
        <iomarker fontsize="28" x="1152" y="1136" name="Q0" orien="R90" />
        <iomarker fontsize="28" x="1632" y="1152" name="Q1" orien="R90" />
        <iomarker fontsize="28" x="2128" y="1136" name="Q2" orien="R90" />
        <iomarker fontsize="28" x="240" y="928" name="I" orien="R180" />
        <instance x="576" y="912" name="XLXI_9" orien="R270">
        </instance>
        <iomarker fontsize="28" x="352" y="816" name="clock" orien="R180" />
        <branch name="clock">
            <wire x2="384" y1="816" y2="816" x1="352" />
            <wire x2="384" y1="816" y2="912" x1="384" />
            <wire x2="496" y1="912" y2="912" x1="384" />
        </branch>
        <iomarker fontsize="28" x="288" y="1040" name="clear" orien="R180" />
    </sheet>
</drawing>