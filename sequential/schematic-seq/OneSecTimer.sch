<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clear" />
        <signal name="XLXN_132" />
        <signal name="XLXN_133" />
        <signal name="XLXN_264" />
        <signal name="XLXN_265" />
        <signal name="XLXN_266" />
        <signal name="XLXN_267" />
        <signal name="XLXN_268" />
        <signal name="XLXN_269" />
        <signal name="XLXN_270" />
        <signal name="XLXN_273" />
        <signal name="XLXN_274" />
        <signal name="XLXN_322" />
        <signal name="XLXN_323" />
        <signal name="XLXN_324" />
        <signal name="XLXN_325" />
        <signal name="XLXN_326" />
        <signal name="XLXN_327" />
        <signal name="XLXN_328" />
        <signal name="XLXN_329" />
        <signal name="XLXN_330" />
        <signal name="XLXN_331" />
        <signal name="XLXN_332" />
        <signal name="XLXN_333" />
        <signal name="XLXN_334" />
        <signal name="XLXN_335" />
        <signal name="XLXN_336" />
        <signal name="clock" />
        <signal name="XLXN_387" />
        <signal name="O" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="clock" />
        <port polarity="Output" name="O" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_8">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_324" name="clock" />
            <blockpin signalname="XLXN_329" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_9">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_329" name="clock" />
            <blockpin signalname="XLXN_323" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_10">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_323" name="clock" />
            <blockpin signalname="XLXN_322" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_11">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_322" name="clock" />
            <blockpin signalname="XLXN_132" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_4">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_327" name="clock" />
            <blockpin signalname="XLXN_328" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_5">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_328" name="clock" />
            <blockpin signalname="XLXN_326" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_6">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_326" name="clock" />
            <blockpin signalname="XLXN_325" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_7">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_325" name="clock" />
            <blockpin signalname="XLXN_324" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_38">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_132" name="clock" />
            <blockpin signalname="XLXN_133" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="XLXN_327" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_75">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_133" name="clock" />
            <blockpin signalname="XLXN_336" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_81">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_331" name="clock" />
            <blockpin signalname="XLXN_330" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_80">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_332" name="clock" />
            <blockpin signalname="XLXN_331" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_79">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_333" name="clock" />
            <blockpin signalname="XLXN_332" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_78">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_334" name="clock" />
            <blockpin signalname="XLXN_333" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_77">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_335" name="clock" />
            <blockpin signalname="XLXN_334" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_76">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_336" name="clock" />
            <blockpin signalname="XLXN_335" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_110">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_269" name="clock" />
            <blockpin signalname="XLXN_273" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_103">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_270" name="clock" />
            <blockpin signalname="XLXN_269" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_102">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_268" name="clock" />
            <blockpin signalname="XLXN_270" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_101">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_267" name="clock" />
            <blockpin signalname="XLXN_268" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_100">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_266" name="clock" />
            <blockpin signalname="XLXN_267" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_99">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_265" name="clock" />
            <blockpin signalname="XLXN_266" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_94">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_264" name="clock" />
            <blockpin signalname="XLXN_265" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_82">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_330" name="clock" />
            <blockpin signalname="XLXN_264" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_112">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_274" name="clock" />
            <blockpin signalname="O" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_111">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_387" name="T" />
            <blockpin signalname="XLXN_273" name="clock" />
            <blockpin signalname="XLXN_274" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="vcc" name="XLXI_276">
            <blockpin signalname="XLXN_387" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="7040">
        <instance x="2800" y="2112" name="XLXI_8" orien="R0">
        </instance>
        <instance x="768" y="2128" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1248" y="2128" name="XLXI_5" orien="R0">
        </instance>
        <instance x="1760" y="2128" name="XLXI_6" orien="R0">
        </instance>
        <instance x="2272" y="2112" name="XLXI_7" orien="R0">
        </instance>
        <instance x="320" y="2128" name="XLXI_1" orien="R0">
        </instance>
        <instance x="5024" y="4864" name="XLXI_81" orien="R90">
        </instance>
        <instance x="5024" y="4448" name="XLXI_80" orien="R90">
        </instance>
        <instance x="5024" y="4016" name="XLXI_79" orien="R90">
        </instance>
        <instance x="5024" y="3584" name="XLXI_78" orien="R90">
        </instance>
        <instance x="1664" y="5248" name="XLXI_110" orien="R180">
        </instance>
        <instance x="2144" y="5248" name="XLXI_103" orien="R180">
        </instance>
        <instance x="2704" y="5248" name="XLXI_102" orien="R180">
        </instance>
        <instance x="3216" y="5248" name="XLXI_101" orien="R180">
        </instance>
        <instance x="3680" y="5264" name="XLXI_100" orien="R180">
        </instance>
        <instance x="4240" y="5264" name="XLXI_99" orien="R180">
        </instance>
        <instance x="4720" y="5264" name="XLXI_94" orien="R180">
        </instance>
        <instance x="5184" y="5264" name="XLXI_82" orien="R180">
        </instance>
        <branch name="XLXN_264">
            <wire x2="4816" y1="5296" y2="5296" x1="4720" />
            <wire x2="4816" y1="5296" y2="5360" x1="4816" />
            <wire x2="4928" y1="5360" y2="5360" x1="4816" />
        </branch>
        <branch name="XLXN_265">
            <wire x2="4352" y1="5296" y2="5296" x1="4240" />
            <wire x2="4352" y1="5296" y2="5360" x1="4352" />
            <wire x2="4464" y1="5360" y2="5360" x1="4352" />
        </branch>
        <branch name="XLXN_266">
            <wire x2="3824" y1="5296" y2="5296" x1="3680" />
            <wire x2="3824" y1="5296" y2="5360" x1="3824" />
            <wire x2="3984" y1="5360" y2="5360" x1="3824" />
        </branch>
        <branch name="XLXN_267">
            <wire x2="3312" y1="5280" y2="5280" x1="3216" />
            <wire x2="3312" y1="5280" y2="5360" x1="3312" />
            <wire x2="3424" y1="5360" y2="5360" x1="3312" />
        </branch>
        <branch name="XLXN_268">
            <wire x2="2832" y1="5280" y2="5280" x1="2704" />
            <wire x2="2832" y1="5280" y2="5344" x1="2832" />
            <wire x2="2960" y1="5344" y2="5344" x1="2832" />
        </branch>
        <branch name="XLXN_269">
            <wire x2="1776" y1="5280" y2="5280" x1="1664" />
            <wire x2="1776" y1="5280" y2="5344" x1="1776" />
            <wire x2="1888" y1="5344" y2="5344" x1="1776" />
        </branch>
        <branch name="XLXN_270">
            <wire x2="2288" y1="5280" y2="5280" x1="2144" />
            <wire x2="2288" y1="5280" y2="5344" x1="2288" />
            <wire x2="2448" y1="5344" y2="5344" x1="2288" />
        </branch>
        <instance x="784" y="5248" name="XLXI_112" orien="R180">
        </instance>
        <instance x="1232" y="5248" name="XLXI_111" orien="R180">
        </instance>
        <branch name="XLXN_273">
            <wire x2="1312" y1="5280" y2="5280" x1="1232" />
            <wire x2="1312" y1="5280" y2="5344" x1="1312" />
            <wire x2="1408" y1="5344" y2="5344" x1="1312" />
        </branch>
        <branch name="XLXN_274">
            <wire x2="880" y1="5280" y2="5280" x1="784" />
            <wire x2="880" y1="5280" y2="5344" x1="880" />
            <wire x2="976" y1="5344" y2="5344" x1="880" />
        </branch>
        <instance x="3360" y="2112" name="XLXI_9" orien="R0">
        </instance>
        <instance x="3888" y="2112" name="XLXI_10" orien="R0">
        </instance>
        <instance x="4368" y="2112" name="XLXI_11" orien="R0">
        </instance>
        <branch name="XLXN_133">
            <wire x2="5056" y1="2016" y2="2016" x1="4992" />
            <wire x2="5056" y1="2016" y2="2464" x1="5056" />
        </branch>
        <instance x="4736" y="2112" name="XLXI_38" orien="R0">
        </instance>
        <branch name="XLXN_132">
            <wire x2="4688" y1="2016" y2="2016" x1="4624" />
            <wire x2="4688" y1="2016" y2="2080" x1="4688" />
            <wire x2="4736" y1="2080" y2="2080" x1="4688" />
        </branch>
        <branch name="XLXN_322">
            <wire x2="4256" y1="2016" y2="2016" x1="4144" />
            <wire x2="4256" y1="2016" y2="2080" x1="4256" />
            <wire x2="4368" y1="2080" y2="2080" x1="4256" />
        </branch>
        <branch name="XLXN_323">
            <wire x2="3744" y1="2016" y2="2016" x1="3616" />
            <wire x2="3744" y1="2016" y2="2080" x1="3744" />
            <wire x2="3888" y1="2080" y2="2080" x1="3744" />
        </branch>
        <branch name="XLXN_324">
            <wire x2="2656" y1="2016" y2="2016" x1="2528" />
            <wire x2="2656" y1="2016" y2="2080" x1="2656" />
            <wire x2="2800" y1="2080" y2="2080" x1="2656" />
        </branch>
        <branch name="XLXN_325">
            <wire x2="2144" y1="2032" y2="2032" x1="2016" />
            <wire x2="2144" y1="2032" y2="2080" x1="2144" />
            <wire x2="2272" y1="2080" y2="2080" x1="2144" />
        </branch>
        <branch name="XLXN_326">
            <wire x2="1632" y1="2032" y2="2032" x1="1504" />
            <wire x2="1632" y1="2032" y2="2096" x1="1632" />
            <wire x2="1760" y1="2096" y2="2096" x1="1632" />
        </branch>
        <branch name="XLXN_327">
            <wire x2="672" y1="2032" y2="2032" x1="576" />
            <wire x2="672" y1="2032" y2="2096" x1="672" />
            <wire x2="768" y1="2096" y2="2096" x1="672" />
        </branch>
        <branch name="XLXN_328">
            <wire x2="1136" y1="2032" y2="2032" x1="1024" />
            <wire x2="1136" y1="2032" y2="2096" x1="1136" />
            <wire x2="1248" y1="2096" y2="2096" x1="1136" />
        </branch>
        <branch name="XLXN_329">
            <wire x2="3200" y1="2016" y2="2016" x1="3056" />
            <wire x2="3200" y1="2016" y2="2080" x1="3200" />
            <wire x2="3360" y1="2080" y2="2080" x1="3200" />
        </branch>
        <branch name="XLXN_330">
            <wire x2="5120" y1="5120" y2="5200" x1="5120" />
            <wire x2="5248" y1="5200" y2="5200" x1="5120" />
            <wire x2="5248" y1="5200" y2="5296" x1="5248" />
            <wire x2="5248" y1="5296" y2="5296" x1="5184" />
        </branch>
        <branch name="XLXN_331">
            <wire x2="5120" y1="4768" y2="4768" x1="5056" />
            <wire x2="5056" y1="4768" y2="4864" x1="5056" />
            <wire x2="5120" y1="4704" y2="4768" x1="5120" />
        </branch>
        <branch name="XLXN_332">
            <wire x2="5056" y1="4352" y2="4448" x1="5056" />
            <wire x2="5120" y1="4352" y2="4352" x1="5056" />
            <wire x2="5120" y1="4272" y2="4352" x1="5120" />
        </branch>
        <branch name="XLXN_333">
            <wire x2="5056" y1="3920" y2="4016" x1="5056" />
            <wire x2="5120" y1="3920" y2="3920" x1="5056" />
            <wire x2="5120" y1="3840" y2="3920" x1="5120" />
        </branch>
        <branch name="XLXN_334">
            <wire x2="5120" y1="3504" y2="3504" x1="5056" />
            <wire x2="5056" y1="3504" y2="3584" x1="5056" />
            <wire x2="5120" y1="3440" y2="3504" x1="5120" />
        </branch>
        <iomarker fontsize="28" x="432" y="2912" name="clock" orien="R0" />
        <branch name="clock">
            <wire x2="320" y1="2096" y2="2096" x1="304" />
            <wire x2="304" y1="2096" y2="2912" x1="304" />
            <wire x2="432" y1="2912" y2="2912" x1="304" />
        </branch>
        <iomarker fontsize="28" x="400" y="3088" name="clear" orien="R0" />
        <branch name="clear">
            <wire x2="112" y1="1904" y2="3088" x1="112" />
            <wire x2="400" y1="3088" y2="3088" x1="112" />
            <wire x2="416" y1="1904" y2="1904" x1="112" />
            <wire x2="416" y1="1904" y2="1936" x1="416" />
            <wire x2="864" y1="1904" y2="1904" x1="416" />
            <wire x2="1344" y1="1904" y2="1904" x1="864" />
            <wire x2="1344" y1="1904" y2="1936" x1="1344" />
            <wire x2="1856" y1="1904" y2="1904" x1="1344" />
            <wire x2="2368" y1="1904" y2="1904" x1="1856" />
            <wire x2="2896" y1="1904" y2="1904" x1="2368" />
            <wire x2="3456" y1="1904" y2="1904" x1="2896" />
            <wire x2="3984" y1="1904" y2="1904" x1="3456" />
            <wire x2="3984" y1="1904" y2="1920" x1="3984" />
            <wire x2="4464" y1="1904" y2="1904" x1="3984" />
            <wire x2="4832" y1="1904" y2="1904" x1="4464" />
            <wire x2="5344" y1="1904" y2="1904" x1="4832" />
            <wire x2="5344" y1="1904" y2="2560" x1="5344" />
            <wire x2="5344" y1="2560" y2="2912" x1="5344" />
            <wire x2="5344" y1="2912" y2="3280" x1="5344" />
            <wire x2="5344" y1="3280" y2="3680" x1="5344" />
            <wire x2="5344" y1="3680" y2="4112" x1="5344" />
            <wire x2="5344" y1="4112" y2="4544" x1="5344" />
            <wire x2="5344" y1="4544" y2="4960" x1="5344" />
            <wire x2="5344" y1="4960" y2="5536" x1="5344" />
            <wire x2="4832" y1="1904" y2="1920" x1="4832" />
            <wire x2="4464" y1="1904" y2="1920" x1="4464" />
            <wire x2="3456" y1="1904" y2="1920" x1="3456" />
            <wire x2="2896" y1="1904" y2="1920" x1="2896" />
            <wire x2="2368" y1="1904" y2="1920" x1="2368" />
            <wire x2="1856" y1="1904" y2="1936" x1="1856" />
            <wire x2="864" y1="1904" y2="1936" x1="864" />
            <wire x2="688" y1="5440" y2="5536" x1="688" />
            <wire x2="1136" y1="5536" y2="5536" x1="688" />
            <wire x2="1568" y1="5536" y2="5536" x1="1136" />
            <wire x2="2048" y1="5536" y2="5536" x1="1568" />
            <wire x2="2608" y1="5536" y2="5536" x1="2048" />
            <wire x2="3120" y1="5536" y2="5536" x1="2608" />
            <wire x2="3584" y1="5536" y2="5536" x1="3120" />
            <wire x2="4144" y1="5536" y2="5536" x1="3584" />
            <wire x2="4624" y1="5536" y2="5536" x1="4144" />
            <wire x2="5088" y1="5536" y2="5536" x1="4624" />
            <wire x2="5344" y1="5536" y2="5536" x1="5088" />
            <wire x2="1136" y1="5440" y2="5536" x1="1136" />
            <wire x2="1568" y1="5440" y2="5536" x1="1568" />
            <wire x2="2048" y1="5440" y2="5536" x1="2048" />
            <wire x2="2608" y1="5440" y2="5536" x1="2608" />
            <wire x2="3120" y1="5440" y2="5536" x1="3120" />
            <wire x2="3584" y1="5456" y2="5536" x1="3584" />
            <wire x2="4144" y1="5456" y2="5536" x1="4144" />
            <wire x2="4624" y1="5456" y2="5536" x1="4624" />
            <wire x2="5088" y1="5456" y2="5536" x1="5088" />
            <wire x2="5344" y1="2560" y2="2560" x1="5216" />
            <wire x2="5344" y1="2912" y2="2912" x1="5216" />
            <wire x2="5344" y1="3280" y2="3280" x1="5216" />
            <wire x2="5344" y1="3680" y2="3680" x1="5216" />
            <wire x2="5344" y1="4112" y2="4112" x1="5216" />
            <wire x2="5344" y1="4544" y2="4544" x1="5216" />
            <wire x2="5344" y1="4960" y2="4960" x1="5216" />
        </branch>
        <instance x="400" y="3072" name="XLXI_276" orien="M270" />
        <instance x="5024" y="2464" name="XLXI_75" orien="R90">
        </instance>
        <branch name="XLXN_336">
            <wire x2="5056" y1="2752" y2="2816" x1="5056" />
            <wire x2="5120" y1="2752" y2="2752" x1="5056" />
            <wire x2="5120" y1="2720" y2="2752" x1="5120" />
        </branch>
        <instance x="5024" y="2816" name="XLXI_76" orien="R90">
        </instance>
        <branch name="XLXN_335">
            <wire x2="5120" y1="3088" y2="3088" x1="5056" />
            <wire x2="5056" y1="3088" y2="3184" x1="5056" />
            <wire x2="5120" y1="3072" y2="3088" x1="5120" />
        </branch>
        <instance x="5024" y="3184" name="XLXI_77" orien="R90">
        </instance>
        <branch name="XLXN_387">
            <wire x2="320" y1="2032" y2="2032" x1="208" />
            <wire x2="208" y1="2032" y2="2448" x1="208" />
            <wire x2="752" y1="2448" y2="2448" x1="208" />
            <wire x2="1216" y1="2448" y2="2448" x1="752" />
            <wire x2="1728" y1="2448" y2="2448" x1="1216" />
            <wire x2="2256" y1="2448" y2="2448" x1="1728" />
            <wire x2="2768" y1="2448" y2="2448" x1="2256" />
            <wire x2="3328" y1="2448" y2="2448" x1="2768" />
            <wire x2="3872" y1="2448" y2="2448" x1="3328" />
            <wire x2="4352" y1="2448" y2="2448" x1="3872" />
            <wire x2="4720" y1="2448" y2="2448" x1="4352" />
            <wire x2="5120" y1="2448" y2="2448" x1="4720" />
            <wire x2="5120" y1="2448" y2="2464" x1="5120" />
            <wire x2="4720" y1="2448" y2="2800" x1="4720" />
            <wire x2="5120" y1="2800" y2="2800" x1="4720" />
            <wire x2="5120" y1="2800" y2="2816" x1="5120" />
            <wire x2="4720" y1="2800" y2="3168" x1="4720" />
            <wire x2="5120" y1="3168" y2="3168" x1="4720" />
            <wire x2="5120" y1="3168" y2="3184" x1="5120" />
            <wire x2="4720" y1="3168" y2="3584" x1="4720" />
            <wire x2="5120" y1="3584" y2="3584" x1="4720" />
            <wire x2="4720" y1="3584" y2="3968" x1="4720" />
            <wire x2="5120" y1="3968" y2="3968" x1="4720" />
            <wire x2="5120" y1="3968" y2="4016" x1="5120" />
            <wire x2="4720" y1="3968" y2="4400" x1="4720" />
            <wire x2="4720" y1="4400" y2="4832" x1="4720" />
            <wire x2="4720" y1="4832" y2="5360" x1="4720" />
            <wire x2="5120" y1="4832" y2="4832" x1="4720" />
            <wire x2="5120" y1="4832" y2="4864" x1="5120" />
            <wire x2="5280" y1="4832" y2="4832" x1="5120" />
            <wire x2="5280" y1="4832" y2="5360" x1="5280" />
            <wire x2="5120" y1="4400" y2="4400" x1="4720" />
            <wire x2="5120" y1="4400" y2="4448" x1="5120" />
            <wire x2="208" y1="2448" y2="3008" x1="208" />
            <wire x2="400" y1="3008" y2="3008" x1="208" />
            <wire x2="768" y1="2032" y2="2032" x1="752" />
            <wire x2="752" y1="2032" y2="2448" x1="752" />
            <wire x2="832" y1="5344" y2="5344" x1="784" />
            <wire x2="1248" y1="4832" y2="4832" x1="832" />
            <wire x2="1248" y1="4832" y2="5344" x1="1248" />
            <wire x2="1680" y1="4832" y2="4832" x1="1248" />
            <wire x2="1680" y1="4832" y2="5344" x1="1680" />
            <wire x2="2160" y1="4832" y2="4832" x1="1680" />
            <wire x2="2160" y1="4832" y2="5344" x1="2160" />
            <wire x2="2720" y1="4832" y2="4832" x1="2160" />
            <wire x2="3232" y1="4832" y2="4832" x1="2720" />
            <wire x2="3232" y1="4832" y2="5344" x1="3232" />
            <wire x2="3680" y1="4832" y2="4832" x1="3232" />
            <wire x2="3680" y1="4832" y2="5360" x1="3680" />
            <wire x2="4288" y1="4832" y2="4832" x1="3680" />
            <wire x2="4720" y1="4832" y2="4832" x1="4288" />
            <wire x2="4288" y1="4832" y2="5360" x1="4288" />
            <wire x2="2720" y1="4832" y2="5344" x1="2720" />
            <wire x2="832" y1="4832" y2="5344" x1="832" />
            <wire x2="1248" y1="2032" y2="2032" x1="1216" />
            <wire x2="1216" y1="2032" y2="2448" x1="1216" />
            <wire x2="1248" y1="5344" y2="5344" x1="1232" />
            <wire x2="1680" y1="5344" y2="5344" x1="1664" />
            <wire x2="1760" y1="2032" y2="2032" x1="1728" />
            <wire x2="1728" y1="2032" y2="2448" x1="1728" />
            <wire x2="2160" y1="5344" y2="5344" x1="2144" />
            <wire x2="2272" y1="2016" y2="2016" x1="2256" />
            <wire x2="2256" y1="2016" y2="2448" x1="2256" />
            <wire x2="2720" y1="5344" y2="5344" x1="2704" />
            <wire x2="2800" y1="2016" y2="2016" x1="2768" />
            <wire x2="2768" y1="2016" y2="2448" x1="2768" />
            <wire x2="3232" y1="5344" y2="5344" x1="3216" />
            <wire x2="3360" y1="2016" y2="2016" x1="3328" />
            <wire x2="3328" y1="2016" y2="2448" x1="3328" />
            <wire x2="3888" y1="2016" y2="2016" x1="3872" />
            <wire x2="3872" y1="2016" y2="2448" x1="3872" />
            <wire x2="4288" y1="5360" y2="5360" x1="4240" />
            <wire x2="4368" y1="2016" y2="2016" x1="4352" />
            <wire x2="4352" y1="2016" y2="2448" x1="4352" />
            <wire x2="4736" y1="2016" y2="2016" x1="4720" />
            <wire x2="4720" y1="2016" y2="2448" x1="4720" />
            <wire x2="5280" y1="5360" y2="5360" x1="5184" />
        </branch>
        <branch name="O">
            <wire x2="112" y1="3264" y2="5344" x1="112" />
            <wire x2="528" y1="5344" y2="5344" x1="112" />
            <wire x2="448" y1="3264" y2="3264" x1="112" />
        </branch>
        <text style="fontsize:150;fontname:Arial" x="1472" y="3712">10^8/ 2^27 ~= 1 Hz</text>
        <text style="fontsize:150;fontname:Arial" x="1448" y="3536">100 MHz = 10^8 Hz ~= 2^27</text>
        <iomarker fontsize="28" x="448" y="3264" name="O" orien="R0" />
    </sheet>
</drawing>