<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_103" />
        <signal name="XLXN_106" />
        <signal name="Q0" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="clear" />
        <signal name="clock599" />
        <signal name="S" />
        <signal name="XLXN_47" />
        <signal name="XLXN_60" />
        <signal name="XLXN_118" />
        <signal name="XLXN_76" />
        <signal name="XLXN_54" />
        <signal name="XLXN_121" />
        <signal name="XLXN_79" />
        <signal name="XLXN_94" />
        <signal name="clock" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="S" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="muxf7">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-256" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-224" y2="-96" x1="256" />
            <line x2="256" y1="-256" y2="-224" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-224" y2="-224" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_94" name="T" />
            <blockpin signalname="clock599" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin signalname="XLXN_47" name="Qb" />
        </block>
        <block symbolname="muxf7" name="XLXI_8">
            <blockpin signalname="Q0" name="I0" />
            <blockpin signalname="XLXN_47" name="I1" />
            <blockpin signalname="S" name="S" />
            <blockpin signalname="XLXN_79" name="O" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_79" name="T" />
            <blockpin signalname="clock599" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin signalname="XLXN_54" name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_76" name="T" />
            <blockpin signalname="clock599" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="muxf7" name="XLXI_9">
            <blockpin signalname="Q1" name="I0" />
            <blockpin signalname="XLXN_54" name="I1" />
            <blockpin signalname="S" name="S" />
            <blockpin signalname="XLXN_60" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="XLXN_79" name="I0" />
            <blockpin signalname="XLXN_60" name="I1" />
            <blockpin signalname="XLXN_76" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_7">
            <blockpin signalname="XLXN_94" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_18">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="clock599" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <text style="fontsize:100;fontname:Arial" x="1024" y="420">SYNC_UP_DOWN_COUNTER</text>
        <instance x="1104" y="1296" name="XLXI_1" orien="R0">
        </instance>
        <branch name="Q0">
            <wire x2="1392" y1="1200" y2="1200" x1="1360" />
            <wire x2="1392" y1="1200" y2="1424" x1="1392" />
            <wire x2="1392" y1="816" y2="1200" x1="1392" />
        </branch>
        <branch name="Q1">
            <wire x2="2048" y1="1200" y2="1200" x1="1984" />
            <wire x2="2048" y1="1200" y2="1424" x1="2048" />
            <wire x2="2192" y1="1424" y2="1424" x1="2048" />
            <wire x2="2048" y1="816" y2="1200" x1="2048" />
        </branch>
        <branch name="Q2">
            <wire x2="3136" y1="1200" y2="1200" x1="3120" />
            <wire x2="3136" y1="816" y2="1200" x1="3136" />
        </branch>
        <branch name="clear">
            <wire x2="752" y1="1088" y2="1088" x1="448" />
            <wire x2="1200" y1="1088" y2="1088" x1="752" />
            <wire x2="1200" y1="1088" y2="1104" x1="1200" />
            <wire x2="1824" y1="1088" y2="1088" x1="1200" />
            <wire x2="1824" y1="1088" y2="1104" x1="1824" />
            <wire x2="2960" y1="1088" y2="1088" x1="1824" />
            <wire x2="2960" y1="1088" y2="1104" x1="2960" />
            <wire x2="752" y1="1088" y2="1168" x1="752" />
        </branch>
        <branch name="clock599">
            <wire x2="1072" y1="1264" y2="1264" x1="864" />
            <wire x2="1104" y1="1264" y2="1264" x1="1072" />
            <wire x2="1072" y1="1264" y2="1360" x1="1072" />
            <wire x2="1504" y1="1360" y2="1360" x1="1072" />
            <wire x2="2656" y1="1360" y2="1360" x1="1504" />
            <wire x2="1504" y1="1264" y2="1360" x1="1504" />
            <wire x2="1728" y1="1264" y2="1264" x1="1504" />
            <wire x2="2656" y1="1264" y2="1360" x1="2656" />
            <wire x2="2864" y1="1264" y2="1264" x1="2656" />
        </branch>
        <branch name="S">
            <wire x2="1344" y1="1616" y2="1616" x1="416" />
            <wire x2="1392" y1="1616" y2="1616" x1="1344" />
            <wire x2="1344" y1="1616" y2="1712" x1="1344" />
            <wire x2="1824" y1="1712" y2="1712" x1="1344" />
            <wire x2="1824" y1="1616" y2="1712" x1="1824" />
            <wire x2="2192" y1="1616" y2="1616" x1="1824" />
        </branch>
        <instance x="1392" y="1648" name="XLXI_8" orien="R0" />
        <branch name="XLXN_47">
            <wire x2="1360" y1="1264" y2="1552" x1="1360" />
            <wire x2="1392" y1="1552" y2="1552" x1="1360" />
        </branch>
        <instance x="1728" y="1296" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_60">
            <wire x2="2560" y1="1520" y2="1520" x1="2512" />
        </branch>
        <instance x="2864" y="1296" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_76">
            <wire x2="2848" y1="1552" y2="1552" x1="2816" />
            <wire x2="2864" y1="1200" y2="1200" x1="2848" />
            <wire x2="2848" y1="1200" y2="1552" x1="2848" />
        </branch>
        <instance x="2192" y="1648" name="XLXI_9" orien="R0" />
        <branch name="XLXN_54">
            <wire x2="2000" y1="1264" y2="1264" x1="1984" />
            <wire x2="2000" y1="1264" y2="1552" x1="2000" />
            <wire x2="2192" y1="1552" y2="1552" x1="2000" />
        </branch>
        <instance x="2560" y="1648" name="XLXI_10" orien="R0" />
        <branch name="XLXN_79">
            <wire x2="1728" y1="1520" y2="1520" x1="1712" />
            <wire x2="1728" y1="1520" y2="1760" x1="1728" />
            <wire x2="2512" y1="1760" y2="1760" x1="1728" />
            <wire x2="1728" y1="1200" y2="1520" x1="1728" />
            <wire x2="2560" y1="1584" y2="1584" x1="2512" />
            <wire x2="2512" y1="1584" y2="1760" x1="2512" />
        </branch>
        <rect width="2604" x="632" y="952" height="948" />
        <branch name="XLXN_94">
            <wire x2="1104" y1="1200" y2="1200" x1="1088" />
        </branch>
        <instance x="1088" y="1264" name="XLXI_7" orien="R270" />
        <instance x="640" y="1344" name="XLXI_18" orien="R0">
        </instance>
        <branch name="clock">
            <wire x2="624" y1="1264" y2="1264" x1="448" />
            <wire x2="640" y1="1264" y2="1264" x1="624" />
        </branch>
        <iomarker fontsize="28" x="1392" y="816" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="2048" y="816" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="3136" y="816" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="448" y="1088" name="clear" orien="R180" />
        <iomarker fontsize="28" x="448" y="1264" name="clock" orien="R180" />
        <iomarker fontsize="28" x="416" y="1616" name="S" orien="R180" />
    </sheet>
</drawing>