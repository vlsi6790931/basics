<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="wire" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="clear" />
        <signal name="I" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="Q" />
        <signal name="clock" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="I" />
        <port polarity="Output" name="Q" />
        <port polarity="Input" name="clock" />
        <blockdef name="DFlipFLop">
            <timestamp>2023-7-25T19:26:20</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="DFlipFLop" name="XLXI_1">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="I" name="DATA" />
            <blockpin signalname="XLXN_9" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_2">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_9" name="DATA" />
            <blockpin signalname="XLXN_10" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_4">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_11" name="DATA" />
            <blockpin signalname="Q" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_3">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_10" name="DATA" />
            <blockpin signalname="XLXN_11" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_5">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="wire" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="672" y="928" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1152" y="928" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2144" y="928" name="XLXI_4" orien="R0">
        </instance>
        <branch name="wire">
            <wire x2="608" y1="656" y2="656" x1="432" />
            <wire x2="608" y1="656" y2="768" x1="608" />
            <wire x2="672" y1="768" y2="768" x1="608" />
            <wire x2="1136" y1="656" y2="656" x1="608" />
            <wire x2="1136" y1="656" y2="768" x1="1136" />
            <wire x2="1152" y1="768" y2="768" x1="1136" />
            <wire x2="1616" y1="656" y2="656" x1="1136" />
            <wire x2="2112" y1="656" y2="656" x1="1616" />
            <wire x2="2112" y1="656" y2="768" x1="2112" />
            <wire x2="2144" y1="768" y2="768" x1="2112" />
            <wire x2="1616" y1="656" y2="768" x1="1616" />
            <wire x2="1632" y1="768" y2="768" x1="1616" />
        </branch>
        <instance x="1632" y="928" name="XLXI_3" orien="R0">
        </instance>
        <branch name="I">
            <wire x2="240" y1="864" y2="864" x1="160" />
            <wire x2="240" y1="864" y2="896" x1="240" />
            <wire x2="672" y1="896" y2="896" x1="240" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="1088" y1="768" y2="768" x1="1056" />
            <wire x2="1088" y1="768" y2="896" x1="1088" />
            <wire x2="1152" y1="896" y2="896" x1="1088" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="1568" y1="768" y2="768" x1="1536" />
            <wire x2="1568" y1="768" y2="896" x1="1568" />
            <wire x2="1632" y1="896" y2="896" x1="1568" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="2080" y1="768" y2="768" x1="2016" />
            <wire x2="2080" y1="768" y2="896" x1="2080" />
            <wire x2="2144" y1="896" y2="896" x1="2080" />
        </branch>
        <branch name="Q">
            <wire x2="2560" y1="768" y2="768" x1="2528" />
        </branch>
        <instance x="512" y="880" name="XLXI_5" orien="R270">
        </instance>
        <iomarker fontsize="28" x="2560" y="768" name="Q" orien="R0" />
        <iomarker fontsize="28" x="208" y="1008" name="clear" orien="R180" />
        <branch name="clear">
            <wire x2="320" y1="1008" y2="1008" x1="208" />
            <wire x2="336" y1="1008" y2="1008" x1="320" />
            <wire x2="608" y1="1008" y2="1008" x1="336" />
            <wire x2="1120" y1="1008" y2="1008" x1="608" />
            <wire x2="1600" y1="1008" y2="1008" x1="1120" />
            <wire x2="2112" y1="1008" y2="1008" x1="1600" />
            <wire x2="336" y1="768" y2="768" x1="320" />
            <wire x2="320" y1="768" y2="1008" x1="320" />
            <wire x2="608" y1="832" y2="1008" x1="608" />
            <wire x2="672" y1="832" y2="832" x1="608" />
            <wire x2="1120" y1="832" y2="1008" x1="1120" />
            <wire x2="1152" y1="832" y2="832" x1="1120" />
            <wire x2="1632" y1="832" y2="832" x1="1600" />
            <wire x2="1600" y1="832" y2="1008" x1="1600" />
            <wire x2="2144" y1="832" y2="832" x1="2112" />
            <wire x2="2112" y1="832" y2="1008" x1="2112" />
        </branch>
        <text style="fontsize:150;fontname:Arial" x="1172" y="284">SISO</text>
        <branch name="clock">
            <wire x2="432" y1="944" y2="944" x1="208" />
            <wire x2="432" y1="880" y2="896" x1="432" />
            <wire x2="432" y1="896" y2="944" x1="432" />
        </branch>
        <iomarker fontsize="28" x="208" y="944" name="clock" orien="R180" />
        <iomarker fontsize="28" x="160" y="864" name="I" orien="R180" />
    </sheet>
</drawing>