<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="XLXN_57" />
        <signal name="XLXN_58" />
        <signal name="wire" />
        <signal name="XLXN_61" />
        <signal name="XLXN_62" />
        <signal name="XLXN_63" />
        <signal name="XLXN_64" />
        <signal name="clear" />
        <signal name="Q1" />
        <signal name="Q0" />
        <signal name="Q2" />
        <signal name="XLXN_11" />
        <signal name="preset" />
        <signal name="Q3" />
        <signal name="XLXN_72" />
        <signal name="clock" />
        <signal name="XLXN_88" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="preset" />
        <port polarity="Output" name="Q3" />
        <port polarity="Input" name="clock" />
        <blockdef name="DFlipFLop">
            <timestamp>2023-7-25T19:26:20</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="DFlipFLop" name="XLXI_1">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_11" name="DATA" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_2">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q0" name="DATA" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_3">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q1" name="DATA" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_4">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q2" name="DATA" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="preset" name="I0" />
            <blockpin signalname="Q3" name="I1" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_6">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="wire" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <text style="fontsize:150;fontname:Arial" x="1140" y="452">RING_COUNTER</text>
        <instance x="1120" y="1488" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1680" y="1488" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2240" y="1488" name="XLXI_3" orien="R0">
        </instance>
        <instance x="2800" y="1472" name="XLXI_4" orien="R0">
        </instance>
        <branch name="wire">
            <wire x2="1056" y1="1200" y2="1200" x1="432" />
            <wire x2="1664" y1="1200" y2="1200" x1="1056" />
            <wire x2="1664" y1="1200" y2="1328" x1="1664" />
            <wire x2="1680" y1="1328" y2="1328" x1="1664" />
            <wire x2="2224" y1="1200" y2="1200" x1="1664" />
            <wire x2="2224" y1="1200" y2="1328" x1="2224" />
            <wire x2="2240" y1="1328" y2="1328" x1="2224" />
            <wire x2="2704" y1="1200" y2="1200" x1="2224" />
            <wire x2="2704" y1="1200" y2="1312" x1="2704" />
            <wire x2="2800" y1="1312" y2="1312" x1="2704" />
            <wire x2="1056" y1="1200" y2="1328" x1="1056" />
            <wire x2="1120" y1="1328" y2="1328" x1="1056" />
        </branch>
        <branch name="clear">
            <wire x2="320" y1="1648" y2="1648" x1="176" />
            <wire x2="1040" y1="1648" y2="1648" x1="320" />
            <wire x2="336" y1="1312" y2="1312" x1="320" />
            <wire x2="320" y1="1312" y2="1648" x1="320" />
            <wire x2="1120" y1="1392" y2="1392" x1="1040" />
            <wire x2="1040" y1="1392" y2="1600" x1="1040" />
            <wire x2="1664" y1="1600" y2="1600" x1="1040" />
            <wire x2="2208" y1="1600" y2="1600" x1="1664" />
            <wire x2="2736" y1="1600" y2="1600" x1="2208" />
            <wire x2="1040" y1="1600" y2="1648" x1="1040" />
            <wire x2="1680" y1="1392" y2="1392" x1="1664" />
            <wire x2="1664" y1="1392" y2="1600" x1="1664" />
            <wire x2="2240" y1="1392" y2="1392" x1="2208" />
            <wire x2="2208" y1="1392" y2="1600" x1="2208" />
            <wire x2="2800" y1="1376" y2="1376" x1="2736" />
            <wire x2="2736" y1="1376" y2="1600" x1="2736" />
        </branch>
        <branch name="Q1">
            <wire x2="2144" y1="1328" y2="1328" x1="2064" />
            <wire x2="2144" y1="1328" y2="1456" x1="2144" />
            <wire x2="2240" y1="1456" y2="1456" x1="2144" />
            <wire x2="2144" y1="1456" y2="1824" x1="2144" />
        </branch>
        <branch name="Q0">
            <wire x2="1584" y1="1328" y2="1328" x1="1504" />
            <wire x2="1584" y1="1328" y2="1456" x1="1584" />
            <wire x2="1680" y1="1456" y2="1456" x1="1584" />
            <wire x2="1584" y1="1456" y2="1824" x1="1584" />
        </branch>
        <branch name="Q2">
            <wire x2="2672" y1="1328" y2="1328" x1="2624" />
            <wire x2="2672" y1="1328" y2="1440" x1="2672" />
            <wire x2="2800" y1="1440" y2="1440" x1="2672" />
            <wire x2="2672" y1="1440" y2="1824" x1="2672" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="1120" y1="1456" y2="1456" x1="1072" />
        </branch>
        <instance x="816" y="1552" name="XLXI_5" orien="R0" />
        <branch name="preset">
            <wire x2="800" y1="1568" y2="1568" x1="192" />
            <wire x2="816" y1="1488" y2="1488" x1="800" />
            <wire x2="800" y1="1488" y2="1568" x1="800" />
        </branch>
        <branch name="Q3">
            <wire x2="3248" y1="1104" y2="1104" x1="784" />
            <wire x2="3248" y1="1104" y2="1312" x1="3248" />
            <wire x2="3248" y1="1312" y2="1824" x1="3248" />
            <wire x2="784" y1="1104" y2="1424" x1="784" />
            <wire x2="816" y1="1424" y2="1424" x1="784" />
            <wire x2="3248" y1="1312" y2="1312" x1="3184" />
        </branch>
        <instance x="512" y="1424" name="XLXI_6" orien="R270">
        </instance>
        <branch name="clock">
            <wire x2="432" y1="1472" y2="1472" x1="176" />
            <wire x2="432" y1="1424" y2="1440" x1="432" />
            <wire x2="432" y1="1440" y2="1472" x1="432" />
        </branch>
        <iomarker fontsize="28" x="1584" y="1824" name="Q0" orien="R90" />
        <iomarker fontsize="28" x="2144" y="1824" name="Q1" orien="R90" />
        <iomarker fontsize="28" x="2672" y="1824" name="Q2" orien="R90" />
        <iomarker fontsize="28" x="3248" y="1824" name="Q3" orien="R90" />
        <iomarker fontsize="28" x="176" y="1648" name="clear" orien="R180" />
        <iomarker fontsize="28" x="192" y="1568" name="preset" orien="R180" />
        <iomarker fontsize="28" x="176" y="1472" name="clock" orien="R180" />
        <rect width="3136" x="244" y="1000" height="708" />
    </sheet>
</drawing>