
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name schematic-seq -dir "C:/Users/salty/Desktop/vlsi/sequential/schematic-seq/planAhead_run_2" -part xc7a100tcsg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "JohnsonCounter.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {JKMasterSlave.vf}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {TFlipFlop.vf}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {OneSecTimer.vf}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {DFlipFLop.vf}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {JohnsonCounter.vf}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top JohnsonCounter $srcset
add_files [list {JohnsonCounter.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc7a100tcsg324-3
