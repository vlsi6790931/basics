<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_155" />
        <signal name="XLXN_156" />
        <signal name="XLXN_160" />
        <signal name="Q0" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="clear" />
        <signal name="S" />
        <signal name="XLXN_47" />
        <signal name="XLXN_169" />
        <signal name="XLXN_170" />
        <signal name="XLXN_27" />
        <signal name="XLXN_53" />
        <signal name="XLXN_54" />
        <signal name="XLXN_174" />
        <signal name="XLXN_58" />
        <signal name="XLXN_176" />
        <signal name="clock" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="S" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="muxf7">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-256" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-224" y2="-96" x1="256" />
            <line x2="256" y1="-256" y2="-224" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-224" y2="-224" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_27" name="T" />
            <blockpin signalname="XLXN_176" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin signalname="XLXN_47" name="Qb" />
        </block>
        <block symbolname="muxf7" name="XLXI_8">
            <blockpin signalname="Q0" name="I0" />
            <blockpin signalname="XLXN_47" name="I1" />
            <blockpin signalname="S" name="S" />
            <blockpin signalname="XLXN_53" name="O" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_27" name="T" />
            <blockpin signalname="XLXN_53" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin signalname="XLXN_54" name="Qb" />
        </block>
        <block symbolname="muxf7" name="XLXI_9">
            <blockpin signalname="Q1" name="I0" />
            <blockpin signalname="XLXN_54" name="I1" />
            <blockpin signalname="S" name="S" />
            <blockpin signalname="XLXN_58" name="O" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_27" name="T" />
            <blockpin signalname="XLXN_58" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="vcc" name="XLXI_7">
            <blockpin signalname="XLXN_27" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_28">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="XLXN_176" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <text style="fontsize:100;fontname:Arial" x="936" y="204">ASYNC_UP_DOWN_COUNTER</text>
        <instance x="800" y="976" name="XLXI_1" orien="R0">
        </instance>
        <branch name="Q0">
            <wire x2="1088" y1="880" y2="880" x1="1056" />
            <wire x2="1088" y1="880" y2="1104" x1="1088" />
            <wire x2="1088" y1="496" y2="880" x1="1088" />
        </branch>
        <branch name="Q1">
            <wire x2="1744" y1="880" y2="880" x1="1680" />
            <wire x2="1744" y1="880" y2="1104" x1="1744" />
            <wire x2="1808" y1="1104" y2="1104" x1="1744" />
            <wire x2="1744" y1="496" y2="880" x1="1744" />
        </branch>
        <branch name="Q2">
            <wire x2="2432" y1="896" y2="896" x1="2416" />
            <wire x2="2432" y1="496" y2="896" x1="2432" />
        </branch>
        <branch name="clear">
            <wire x2="576" y1="768" y2="768" x1="304" />
            <wire x2="896" y1="768" y2="768" x1="576" />
            <wire x2="896" y1="768" y2="784" x1="896" />
            <wire x2="1520" y1="768" y2="768" x1="896" />
            <wire x2="2256" y1="768" y2="768" x1="1520" />
            <wire x2="2256" y1="768" y2="800" x1="2256" />
            <wire x2="1520" y1="768" y2="784" x1="1520" />
            <wire x2="576" y1="768" y2="848" x1="576" />
        </branch>
        <branch name="S">
            <wire x2="1040" y1="1296" y2="1296" x1="576" />
            <wire x2="1088" y1="1296" y2="1296" x1="1040" />
            <wire x2="1040" y1="1296" y2="1392" x1="1040" />
            <wire x2="1664" y1="1392" y2="1392" x1="1040" />
            <wire x2="1664" y1="1296" y2="1392" x1="1664" />
            <wire x2="1808" y1="1296" y2="1296" x1="1664" />
        </branch>
        <instance x="1088" y="1328" name="XLXI_8" orien="R0" />
        <branch name="XLXN_47">
            <wire x2="1056" y1="944" y2="1232" x1="1056" />
            <wire x2="1088" y1="1232" y2="1232" x1="1056" />
        </branch>
        <instance x="1424" y="976" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_27">
            <wire x2="800" y1="880" y2="880" x1="784" />
            <wire x2="784" y1="880" y2="1024" x1="784" />
            <wire x2="1360" y1="1024" y2="1024" x1="784" />
            <wire x2="2080" y1="1024" y2="1024" x1="1360" />
            <wire x2="1424" y1="880" y2="880" x1="1360" />
            <wire x2="1360" y1="880" y2="1024" x1="1360" />
            <wire x2="2160" y1="896" y2="896" x1="2080" />
            <wire x2="2080" y1="896" y2="1024" x1="2080" />
        </branch>
        <branch name="XLXN_53">
            <wire x2="1424" y1="1200" y2="1200" x1="1408" />
            <wire x2="1424" y1="944" y2="1200" x1="1424" />
        </branch>
        <instance x="1808" y="1328" name="XLXI_9" orien="R0" />
        <branch name="XLXN_54">
            <wire x2="1696" y1="944" y2="944" x1="1680" />
            <wire x2="1696" y1="944" y2="1232" x1="1696" />
            <wire x2="1808" y1="1232" y2="1232" x1="1696" />
        </branch>
        <instance x="2160" y="992" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_58">
            <wire x2="2144" y1="1200" y2="1200" x1="2128" />
            <wire x2="2160" y1="960" y2="960" x1="2144" />
            <wire x2="2144" y1="960" y2="1200" x1="2144" />
        </branch>
        <instance x="784" y="944" name="XLXI_7" orien="R270" />
        <rect width="2096" x="420" y="636" height="848" />
        <iomarker fontsize="28" x="1088" y="496" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="1744" y="496" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="2432" y="496" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="576" y="1296" name="S" orien="R180" />
        <iomarker fontsize="28" x="304" y="768" name="clear" orien="R180" />
        <instance x="464" y="1024" name="XLXI_28" orien="R0">
        </instance>
        <branch name="XLXN_176">
            <wire x2="800" y1="944" y2="944" x1="688" />
        </branch>
        <branch name="clock">
            <wire x2="448" y1="944" y2="944" x1="320" />
            <wire x2="464" y1="944" y2="944" x1="448" />
        </branch>
        <iomarker fontsize="28" x="320" y="944" name="clock" orien="R180" />
    </sheet>
</drawing>