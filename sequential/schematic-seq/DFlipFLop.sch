<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Q" />
        <signal name="Qb" />
        <signal name="clock" />
        <signal name="clear" />
        <signal name="XLXN_32" />
        <signal name="DATA" />
        <port polarity="Output" name="Q" />
        <port polarity="Output" name="Qb" />
        <port polarity="Input" name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="DATA" />
        <blockdef name="JKMasterSlave">
            <timestamp>2023-7-25T18:46:50</timestamp>
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="112" y1="-192" y2="-256" x1="112" />
            <line x2="320" y1="-48" y2="-48" x1="256" />
            <rect width="192" x="64" y="-192" height="192" />
            <line x2="320" y1="-128" y2="-128" x1="256" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="JKMasterSlave" name="XLXI_1">
            <blockpin signalname="clock" name="enable" />
            <blockpin signalname="DATA" name="J" />
            <blockpin signalname="XLXN_32" name="K" />
            <blockpin signalname="clear" name="c" />
            <blockpin signalname="Qb" name="Qb" />
            <blockpin signalname="Q" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_4">
            <blockpin signalname="DATA" name="I" />
            <blockpin signalname="XLXN_32" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <branch name="clock">
            <wire x2="736" y1="912" y2="912" x1="336" />
            <wire x2="752" y1="832" y2="832" x1="736" />
            <wire x2="736" y1="832" y2="912" x1="736" />
        </branch>
        <branch name="clear">
            <wire x2="864" y1="480" y2="608" x1="864" />
        </branch>
        <branch name="Q">
            <wire x2="1088" y1="736" y2="736" x1="1072" />
            <wire x2="1424" y1="736" y2="736" x1="1088" />
        </branch>
        <instance x="752" y="864" name="XLXI_1" orien="R0">
        </instance>
        <branch name="Qb">
            <wire x2="1088" y1="816" y2="816" x1="1072" />
            <wire x2="1408" y1="816" y2="816" x1="1088" />
        </branch>
        <rect width="864" x="464" y="568" height="424" />
        <iomarker fontsize="28" x="864" y="480" name="clear" orien="R270" />
        <branch name="XLXN_32">
            <wire x2="752" y1="768" y2="768" x1="720" />
        </branch>
        <instance x="496" y="800" name="XLXI_4" orien="R0" />
        <branch name="DATA">
            <wire x2="496" y1="704" y2="704" x1="352" />
            <wire x2="752" y1="704" y2="704" x1="496" />
            <wire x2="496" y1="704" y2="768" x1="496" />
        </branch>
        <iomarker fontsize="28" x="352" y="704" name="DATA" orien="R180" />
        <iomarker fontsize="28" x="336" y="912" name="clock" orien="R180" />
        <text style="fontsize:100;fontname:Arial" x="592" y="252">D_FLIP_FLOP</text>
        <iomarker fontsize="28" x="1424" y="736" name="Q" orien="R0" />
        <iomarker fontsize="28" x="1408" y="816" name="Qb" orien="R0" />
    </sheet>
</drawing>