<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clear" />
        <signal name="clock499">
        </signal>
        <signal name="XLXN_34" />
        <signal name="Q1" />
        <signal name="Q0" />
        <signal name="Q2" />
        <signal name="XLXN_43" />
        <signal name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_43" name="T" />
            <blockpin signalname="clock499" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="Q0" name="T" />
            <blockpin signalname="clock499" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_34" name="T" />
            <blockpin signalname="clock499" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="Q1" name="I0" />
            <blockpin signalname="Q0" name="I1" />
            <blockpin signalname="XLXN_34" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_6">
            <blockpin signalname="XLXN_43" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_7">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="clock499" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <instance x="688" y="1040" name="XLXI_2" orien="R0">
        </instance>
        <branch name="clear">
            <wire x2="480" y1="832" y2="832" x1="320" />
            <wire x2="784" y1="832" y2="832" x1="480" />
            <wire x2="784" y1="832" y2="848" x1="784" />
            <wire x2="1120" y1="832" y2="832" x1="784" />
            <wire x2="1120" y1="832" y2="848" x1="1120" />
            <wire x2="1648" y1="832" y2="832" x1="1120" />
            <wire x2="1648" y1="832" y2="848" x1="1648" />
            <wire x2="480" y1="832" y2="912" x1="480" />
        </branch>
        <instance x="1024" y="1040" name="XLXI_3" orien="R0">
        </instance>
        <branch name="clock499">
            <wire x2="672" y1="1008" y2="1008" x1="592" />
            <wire x2="672" y1="1008" y2="1088" x1="672" />
            <wire x2="1024" y1="1088" y2="1088" x1="672" />
            <wire x2="1552" y1="1088" y2="1088" x1="1024" />
            <wire x2="688" y1="1008" y2="1008" x1="672" />
            <wire x2="1024" y1="1008" y2="1088" x1="1024" />
            <wire x2="1552" y1="1008" y2="1088" x1="1552" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="1552" y1="720" y2="944" x1="1552" />
        </branch>
        <branch name="Q1">
            <wire x2="1296" y1="944" y2="944" x1="1280" />
            <wire x2="1360" y1="944" y2="944" x1="1296" />
            <wire x2="1360" y1="944" y2="1184" x1="1360" />
            <wire x2="1296" y1="752" y2="944" x1="1296" />
        </branch>
        <instance x="1296" y="816" name="XLXI_5" orien="R0" />
        <branch name="Q0">
            <wire x2="960" y1="944" y2="944" x1="944" />
            <wire x2="992" y1="944" y2="944" x1="960" />
            <wire x2="1024" y1="944" y2="944" x1="992" />
            <wire x2="992" y1="944" y2="1184" x1="992" />
            <wire x2="1296" y1="688" y2="688" x1="960" />
            <wire x2="960" y1="688" y2="944" x1="960" />
        </branch>
        <branch name="Q2">
            <wire x2="1904" y1="944" y2="944" x1="1808" />
            <wire x2="1904" y1="944" y2="1184" x1="1904" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="688" y1="944" y2="944" x1="672" />
        </branch>
        <iomarker fontsize="28" x="992" y="1184" name="Q0" orien="R90" />
        <iomarker fontsize="28" x="1904" y="1184" name="Q2" orien="R90" />
        <instance x="1552" y="1040" name="XLXI_1" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1360" y="1184" name="Q1" orien="R90" />
        <text style="fontsize:150;fontname:Arial" x="488" y="424">SYNC_UP_COUNTER</text>
        <instance x="672" y="1008" name="XLXI_6" orien="R270" />
        <iomarker fontsize="28" x="320" y="832" name="clear" orien="R180" />
        <instance x="368" y="1088" name="XLXI_7" orien="R0">
        </instance>
        <branch name="clock">
            <wire x2="352" y1="1008" y2="1008" x1="320" />
            <wire x2="368" y1="1008" y2="1008" x1="352" />
        </branch>
        <iomarker fontsize="28" x="320" y="1008" name="clock" orien="R180" />
    </sheet>
</drawing>