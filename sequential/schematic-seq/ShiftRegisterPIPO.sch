<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="wire">
        </signal>
        <signal name="XLXN_36" />
        <signal name="clear" />
        <signal name="data(3)" />
        <signal name="data(2)" />
        <signal name="data(1)" />
        <signal name="data(0)" />
        <signal name="Q0" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="Q3" />
        <signal name="data(3:0)" />
        <signal name="XLXN_47" />
        <signal name="clock" />
        <signal name="XLXN_51" />
        <signal name="XLXN_52" />
        <signal name="XLXN_54" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q3" />
        <port polarity="Input" name="data(3:0)" />
        <port polarity="Input" name="clock" />
        <blockdef name="DFlipFLop">
            <timestamp>2023-7-25T19:26:20</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="DFlipFLop" name="XLXI_3">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="data(3)" name="DATA" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_4">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="data(2)" name="DATA" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_5">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="data(1)" name="DATA" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_6">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="data(0)" name="DATA" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_7">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="wire" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <text style="fontsize:150;fontname:Arial" x="1232" y="192">PIPO</text>
        <instance x="640" y="992" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1168" y="976" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1696" y="960" name="XLXI_5" orien="R0">
        </instance>
        <instance x="2240" y="960" name="XLXI_6" orien="R0">
        </instance>
        <branch name="wire">
            <wire x2="624" y1="688" y2="688" x1="464" />
            <wire x2="1072" y1="688" y2="688" x1="624" />
            <wire x2="1680" y1="688" y2="688" x1="1072" />
            <wire x2="1680" y1="688" y2="800" x1="1680" />
            <wire x2="1696" y1="800" y2="800" x1="1680" />
            <wire x2="2144" y1="688" y2="688" x1="1680" />
            <wire x2="2144" y1="688" y2="800" x1="2144" />
            <wire x2="2240" y1="800" y2="800" x1="2144" />
            <wire x2="1072" y1="688" y2="816" x1="1072" />
            <wire x2="1168" y1="816" y2="816" x1="1072" />
            <wire x2="624" y1="688" y2="832" x1="624" />
            <wire x2="640" y1="832" y2="832" x1="624" />
            <wire x2="464" y1="688" y2="704" x1="464" />
        </branch>
        <branch name="clear">
            <wire x2="368" y1="1040" y2="1040" x1="272" />
            <wire x2="400" y1="1040" y2="1040" x1="368" />
            <wire x2="560" y1="1040" y2="1040" x1="400" />
            <wire x2="1072" y1="1040" y2="1040" x1="560" />
            <wire x2="1616" y1="1040" y2="1040" x1="1072" />
            <wire x2="2144" y1="1040" y2="1040" x1="1616" />
            <wire x2="368" y1="816" y2="1040" x1="368" />
            <wire x2="640" y1="896" y2="896" x1="560" />
            <wire x2="560" y1="896" y2="1040" x1="560" />
            <wire x2="1072" y1="880" y2="1040" x1="1072" />
            <wire x2="1168" y1="880" y2="880" x1="1072" />
            <wire x2="1616" y1="864" y2="1040" x1="1616" />
            <wire x2="1696" y1="864" y2="864" x1="1616" />
            <wire x2="2144" y1="864" y2="1040" x1="2144" />
            <wire x2="2240" y1="864" y2="864" x1="2144" />
        </branch>
        <branch name="data(3)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="1136" type="branch" />
            <wire x2="640" y1="960" y2="960" x1="624" />
            <wire x2="624" y1="960" y2="1136" x1="624" />
        </branch>
        <branch name="data(2)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1152" y="1152" type="branch" />
            <wire x2="1168" y1="944" y2="944" x1="1152" />
            <wire x2="1152" y1="944" y2="1152" x1="1152" />
        </branch>
        <branch name="data(1)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1680" y="1120" type="branch" />
            <wire x2="1696" y1="928" y2="928" x1="1680" />
            <wire x2="1680" y1="928" y2="1120" x1="1680" />
        </branch>
        <branch name="data(0)">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2224" y="1104" type="branch" />
            <wire x2="2240" y1="928" y2="928" x1="2224" />
            <wire x2="2224" y1="928" y2="1104" x1="2224" />
        </branch>
        <branch name="Q0">
            <wire x2="2640" y1="800" y2="800" x1="2624" />
            <wire x2="2640" y1="576" y2="800" x1="2640" />
        </branch>
        <branch name="Q1">
            <wire x2="2096" y1="800" y2="800" x1="2080" />
            <wire x2="2096" y1="576" y2="800" x1="2096" />
        </branch>
        <branch name="Q2">
            <wire x2="1568" y1="816" y2="816" x1="1552" />
            <wire x2="1568" y1="560" y2="816" x1="1568" />
        </branch>
        <branch name="Q3">
            <wire x2="1040" y1="832" y2="832" x1="1024" />
            <wire x2="1040" y1="560" y2="832" x1="1040" />
        </branch>
        <branch name="data(3:0)">
            <wire x2="1216" y1="1472" y2="1472" x1="784" />
        </branch>
        <iomarker fontsize="28" x="784" y="1472" name="data(3:0)" orien="R180" />
        <iomarker fontsize="28" x="2640" y="576" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="2096" y="576" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="1568" y="560" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="1040" y="560" name="Q3" orien="R270" />
        <iomarker fontsize="28" x="272" y="1040" name="clear" orien="R180" />
        <branch name="clock">
            <wire x2="464" y1="944" y2="944" x1="272" />
            <wire x2="464" y1="928" y2="944" x1="464" />
        </branch>
        <instance x="544" y="928" name="XLXI_7" orien="R270">
        </instance>
        <iomarker fontsize="28" x="272" y="944" name="clock" orien="R180" />
    </sheet>
</drawing>