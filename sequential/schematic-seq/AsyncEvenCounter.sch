<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clear" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="Q3" />
        <signal name="Q0" />
        <signal name="XLXN_10" />
        <signal name="XLXN_38" />
        <signal name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q3" />
        <port polarity="Output" name="Q0" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:37:0</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_10" name="T" />
            <blockpin signalname="Q2" name="clock" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_10" name="T" />
            <blockpin signalname="XLXN_38" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_10" name="T" />
            <blockpin signalname="Q1" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="gnd" name="XLXI_18">
            <blockpin signalname="Q0" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_7">
            <blockpin signalname="XLXN_10" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_19">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="XLXN_38" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <text style="fontsize:100;fontname:Arial" x="372" y="276">ASYNC_EVEN_COUNTER</text>
        <instance x="1296" y="944" name="XLXI_3" orien="R0">
        </instance>
        <instance x="528" y="944" name="XLXI_1" orien="R0">
        </instance>
        <instance x="912" y="944" name="XLXI_2" orien="R0">
        </instance>
        <branch name="clear">
            <wire x2="320" y1="736" y2="736" x1="176" />
            <wire x2="320" y1="736" y2="816" x1="320" />
            <wire x2="624" y1="736" y2="736" x1="320" />
            <wire x2="624" y1="736" y2="752" x1="624" />
            <wire x2="1008" y1="736" y2="736" x1="624" />
            <wire x2="1392" y1="736" y2="736" x1="1008" />
            <wire x2="1392" y1="736" y2="752" x1="1392" />
            <wire x2="1008" y1="736" y2="752" x1="1008" />
        </branch>
        <branch name="Q1">
            <wire x2="816" y1="848" y2="848" x1="784" />
            <wire x2="816" y1="848" y2="912" x1="816" />
            <wire x2="912" y1="912" y2="912" x1="816" />
            <wire x2="816" y1="576" y2="848" x1="816" />
        </branch>
        <branch name="Q2">
            <wire x2="1216" y1="848" y2="848" x1="1168" />
            <wire x2="1216" y1="848" y2="912" x1="1216" />
            <wire x2="1296" y1="912" y2="912" x1="1216" />
            <wire x2="1216" y1="576" y2="848" x1="1216" />
        </branch>
        <branch name="Q3">
            <wire x2="1568" y1="848" y2="848" x1="1552" />
            <wire x2="1568" y1="576" y2="848" x1="1568" />
        </branch>
        <branch name="Q0">
            <wire x2="464" y1="576" y2="592" x1="464" />
        </branch>
        <instance x="528" y="720" name="XLXI_18" orien="M0" />
        <branch name="XLXN_10">
            <wire x2="528" y1="848" y2="848" x1="480" />
            <wire x2="480" y1="848" y2="976" x1="480" />
            <wire x2="880" y1="976" y2="976" x1="480" />
            <wire x2="1280" y1="976" y2="976" x1="880" />
            <wire x2="912" y1="848" y2="848" x1="880" />
            <wire x2="880" y1="848" y2="976" x1="880" />
            <wire x2="1280" y1="848" y2="976" x1="1280" />
            <wire x2="1296" y1="848" y2="848" x1="1280" />
        </branch>
        <instance x="544" y="848" name="XLXI_7" orien="M0" />
        <branch name="XLXN_38">
            <wire x2="528" y1="912" y2="912" x1="432" />
        </branch>
        <branch name="clock">
            <wire x2="208" y1="912" y2="912" x1="176" />
        </branch>
        <iomarker fontsize="28" x="1568" y="576" name="Q3" orien="R270" />
        <iomarker fontsize="28" x="1216" y="576" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="816" y="576" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="464" y="576" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="176" y="912" name="clock" orien="R180" />
        <rect width="1336" x="240" y="648" height="376" />
        <iomarker fontsize="28" x="176" y="736" name="clear" orien="R180" />
        <instance x="208" y="992" name="XLXI_19" orien="R0">
        </instance>
    </sheet>
</drawing>