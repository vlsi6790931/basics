<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clear" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="Q3" />
        <signal name="Q0" />
        <signal name="XLXN_18" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q3" />
        <port polarity="Output" name="Q0" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_10" name="T" />
            <blockpin signalname="Q2" name="clock" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_10" name="T" />
            <blockpin signalname="XLXN_11" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_10" name="T" />
            <blockpin signalname="Q1" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="vcc" name="XLXI_16">
            <blockpin signalname="Q0" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_7">
            <blockpin signalname="XLXN_10" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_17">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="XLXN_11" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="1280" y="976" name="XLXI_1" orien="R0">
        </instance>
        <instance x="512" y="976" name="XLXI_2" orien="R0">
        </instance>
        <instance x="896" y="976" name="XLXI_3" orien="R0">
        </instance>
        <branch name="clear">
            <wire x2="288" y1="752" y2="752" x1="144" />
            <wire x2="608" y1="752" y2="752" x1="288" />
            <wire x2="992" y1="752" y2="752" x1="608" />
            <wire x2="1376" y1="752" y2="752" x1="992" />
            <wire x2="1376" y1="752" y2="784" x1="1376" />
            <wire x2="992" y1="752" y2="784" x1="992" />
            <wire x2="608" y1="752" y2="784" x1="608" />
            <wire x2="288" y1="752" y2="848" x1="288" />
        </branch>
        <branch name="Q1">
            <wire x2="800" y1="880" y2="880" x1="768" />
            <wire x2="800" y1="880" y2="944" x1="800" />
            <wire x2="896" y1="944" y2="944" x1="800" />
            <wire x2="800" y1="608" y2="880" x1="800" />
        </branch>
        <branch name="Q2">
            <wire x2="1200" y1="880" y2="880" x1="1152" />
            <wire x2="1200" y1="880" y2="944" x1="1200" />
            <wire x2="1280" y1="944" y2="944" x1="1200" />
            <wire x2="1200" y1="608" y2="880" x1="1200" />
        </branch>
        <branch name="Q3">
            <wire x2="1552" y1="880" y2="880" x1="1536" />
            <wire x2="1552" y1="608" y2="880" x1="1552" />
        </branch>
        <rect width="1412" x="196" y="676" height="376" />
        <branch name="Q0">
            <wire x2="416" y1="608" y2="640" x1="416" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="480" y1="864" y2="880" x1="480" />
            <wire x2="512" y1="880" y2="880" x1="480" />
            <wire x2="480" y1="880" y2="1008" x1="480" />
            <wire x2="864" y1="1008" y2="1008" x1="480" />
            <wire x2="1264" y1="1008" y2="1008" x1="864" />
            <wire x2="896" y1="880" y2="880" x1="864" />
            <wire x2="864" y1="880" y2="1008" x1="864" />
            <wire x2="1264" y1="880" y2="1008" x1="1264" />
            <wire x2="1280" y1="880" y2="880" x1="1264" />
        </branch>
        <instance x="352" y="640" name="XLXI_16" orien="M180" />
        <text style="fontsize:100;fontname:Arial" x="364" y="256">ASYNC_ODD_COUNTER</text>
        <instance x="544" y="864" name="XLXI_7" orien="M0" />
        <instance x="176" y="1024" name="XLXI_17" orien="R0">
        </instance>
        <branch name="XLXN_11">
            <wire x2="512" y1="944" y2="944" x1="400" />
        </branch>
        <branch name="clock">
            <wire x2="176" y1="944" y2="944" x1="144" />
        </branch>
        <iomarker fontsize="28" x="1552" y="608" name="Q3" orien="R270" />
        <iomarker fontsize="28" x="1200" y="608" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="800" y="608" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="416" y="608" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="144" y="944" name="clock" orien="R180" />
        <iomarker fontsize="28" x="144" y="752" name="clear" orien="R180" />
    </sheet>
</drawing>