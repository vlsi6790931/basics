<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="clear" />
        <signal name="Q1" />
        <signal name="Q0" />
        <signal name="T" />
        <signal name="Q2" />
        <signal name="XLXN_3" />
        <signal name="clock" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="T" name="T" />
            <blockpin signalname="Q1" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="T" name="T" />
            <blockpin signalname="XLXN_3" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="T" name="T" />
            <blockpin signalname="Q0" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="vcc" name="XLXI_5">
            <blockpin signalname="T" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_6">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="XLXN_3" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="1312" y="768" name="XLXI_1" orien="R0">
        </instance>
        <instance x="608" y="768" name="XLXI_2" orien="R0">
        </instance>
        <instance x="976" y="768" name="XLXI_3" orien="R0">
        </instance>
        <branch name="clear">
            <wire x2="416" y1="496" y2="496" x1="224" />
            <wire x2="704" y1="496" y2="496" x1="416" />
            <wire x2="704" y1="496" y2="576" x1="704" />
            <wire x2="1072" y1="496" y2="496" x1="704" />
            <wire x2="1072" y1="496" y2="576" x1="1072" />
            <wire x2="1408" y1="496" y2="496" x1="1072" />
            <wire x2="1408" y1="496" y2="576" x1="1408" />
            <wire x2="416" y1="496" y2="640" x1="416" />
        </branch>
        <branch name="Q1">
            <wire x2="1264" y1="672" y2="672" x1="1232" />
            <wire x2="1264" y1="672" y2="736" x1="1264" />
            <wire x2="1312" y1="736" y2="736" x1="1264" />
            <wire x2="1264" y1="736" y2="896" x1="1264" />
        </branch>
        <branch name="Q0">
            <wire x2="912" y1="672" y2="672" x1="864" />
            <wire x2="912" y1="672" y2="736" x1="912" />
            <wire x2="976" y1="736" y2="736" x1="912" />
            <wire x2="912" y1="736" y2="896" x1="912" />
        </branch>
        <branch name="T">
            <wire x2="608" y1="576" y2="576" x1="592" />
            <wire x2="608" y1="576" y2="672" x1="608" />
            <wire x2="976" y1="576" y2="576" x1="608" />
            <wire x2="976" y1="576" y2="672" x1="976" />
            <wire x2="1312" y1="576" y2="576" x1="976" />
            <wire x2="1312" y1="576" y2="672" x1="1312" />
        </branch>
        <branch name="Q2">
            <wire x2="1584" y1="672" y2="672" x1="1568" />
            <wire x2="1600" y1="672" y2="672" x1="1584" />
            <wire x2="1600" y1="672" y2="896" x1="1600" />
        </branch>
        <instance x="592" y="640" name="XLXI_5" orien="R270" />
        <instance x="304" y="816" name="XLXI_6" orien="R0">
        </instance>
        <branch name="XLXN_3">
            <wire x2="608" y1="736" y2="736" x1="528" />
        </branch>
        <branch name="clock">
            <wire x2="288" y1="736" y2="736" x1="224" />
            <wire x2="304" y1="736" y2="736" x1="288" />
        </branch>
        <iomarker fontsize="28" x="1264" y="896" name="Q1" orien="R90" />
        <iomarker fontsize="28" x="1600" y="896" name="Q2" orien="R90" />
        <iomarker fontsize="28" x="912" y="896" name="Q0" orien="R90" />
        <iomarker fontsize="28" x="224" y="496" name="clear" orien="R180" />
        <iomarker fontsize="28" x="224" y="736" name="clock" orien="R180" />
        <rect width="1356" x="300" y="436" height="404" />
        <text style="fontsize:100;fontname:Arial" x="336" y="224">ASYNC_UP_COUNTER</text>
    </sheet>
</drawing>