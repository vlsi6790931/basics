<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="Q0" />
        <signal name="XLXN_3" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="clear" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="clock" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="clear" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:37:0</timestamp>
            <rect width="96" x="80" y="-176" height="96" />
            <line x2="16" y1="-128" y2="-128" x1="80" />
            <line x2="240" y1="-128" y2="-128" x1="176" />
            <line x2="128" y1="-224" y2="-172" x1="128" />
        </blockdef>
        <block symbolname="TFlipFlop" name="XLXI_1">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_27" name="T" />
            <blockpin signalname="XLXN_28" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin signalname="XLXN_3" name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_27" name="T" />
            <blockpin signalname="XLXN_3" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin signalname="XLXN_1" name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="XLXN_27" name="T" />
            <blockpin signalname="XLXN_1" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="vcc" name="XLXI_7">
            <blockpin signalname="XLXN_27" name="P" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_8">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="XLXN_28" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="576" y="880" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_1">
            <wire x2="1280" y1="848" y2="848" x1="1168" />
        </branch>
        <branch name="Q0">
            <wire x2="848" y1="784" y2="784" x1="832" />
            <wire x2="864" y1="784" y2="784" x1="848" />
            <wire x2="864" y1="480" y2="784" x1="864" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="912" y1="848" y2="848" x1="832" />
        </branch>
        <instance x="912" y="880" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1280" y="880" name="XLXI_3" orien="R0">
        </instance>
        <branch name="Q1">
            <wire x2="1184" y1="784" y2="784" x1="1168" />
            <wire x2="1216" y1="784" y2="784" x1="1184" />
            <wire x2="1216" y1="480" y2="784" x1="1216" />
        </branch>
        <branch name="Q2">
            <wire x2="1552" y1="784" y2="784" x1="1536" />
            <wire x2="1568" y1="784" y2="784" x1="1552" />
            <wire x2="1568" y1="480" y2="784" x1="1568" />
        </branch>
        <branch name="clear">
            <wire x2="336" y1="672" y2="672" x1="176" />
            <wire x2="672" y1="672" y2="672" x1="336" />
            <wire x2="1008" y1="672" y2="672" x1="672" />
            <wire x2="1008" y1="672" y2="688" x1="1008" />
            <wire x2="1376" y1="672" y2="672" x1="1008" />
            <wire x2="1376" y1="672" y2="688" x1="1376" />
            <wire x2="672" y1="672" y2="688" x1="672" />
            <wire x2="336" y1="672" y2="816" x1="336" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="560" y1="784" y2="784" x1="512" />
            <wire x2="576" y1="784" y2="784" x1="560" />
            <wire x2="560" y1="784" y2="960" x1="560" />
            <wire x2="912" y1="960" y2="960" x1="560" />
            <wire x2="1264" y1="960" y2="960" x1="912" />
            <wire x2="912" y1="784" y2="960" x1="912" />
            <wire x2="1264" y1="784" y2="960" x1="1264" />
            <wire x2="1280" y1="784" y2="784" x1="1264" />
        </branch>
        <text style="fontsize:100;fontname:Arial" x="300" y="224">ASYNC_DOWN_COUNTER</text>
        <instance x="208" y="1040" name="XLXI_8" orien="R0">
        </instance>
        <branch name="XLXN_28">
            <wire x2="512" y1="912" y2="912" x1="448" />
            <wire x2="512" y1="848" y2="912" x1="512" />
            <wire x2="576" y1="848" y2="848" x1="512" />
        </branch>
        <branch name="clock">
            <wire x2="224" y1="912" y2="912" x1="192" />
        </branch>
        <iomarker fontsize="28" x="1216" y="480" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="864" y="480" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="1568" y="480" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="192" y="912" name="clock" orien="R180" />
        <iomarker fontsize="28" x="176" y="672" name="clear" orien="R180" />
        <instance x="512" y="848" name="XLXI_7" orien="R270" />
        <rect width="1404" x="240" y="540" height="600" />
    </sheet>
</drawing>