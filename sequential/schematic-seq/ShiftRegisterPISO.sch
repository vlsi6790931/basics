<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_57" />
        <signal name="XLXN_71" />
        <signal name="XLXN_73" />
        <signal name="XLXN_75" />
        <signal name="XLXN_77" />
        <signal name="XLXN_81" />
        <signal name="XLXN_82" />
        <signal name="XLXN_83" />
        <signal name="XLXN_85" />
        <signal name="XLXN_97" />
        <signal name="XLXN_99" />
        <signal name="XLXN_101" />
        <signal name="XLXN_103" />
        <signal name="XLXN_107" />
        <signal name="XLXN_108" />
        <signal name="XLXN_109" />
        <signal name="XLXN_110" />
        <signal name="load(2)" />
        <signal name="load(3)" />
        <signal name="load(0)" />
        <signal name="load(1)" />
        <signal name="XLXN_41" />
        <signal name="XLXN_44" />
        <signal name="O" />
        <signal name="XLXN_48" />
        <signal name="sel" />
        <signal name="XLXN_43" />
        <signal name="load(3:0)" />
        <signal name="XLXN_123" />
        <signal name="output3" />
        <signal name="XLXN_125" />
        <signal name="output2" />
        <signal name="XLXN_127" />
        <signal name="output1" />
        <signal name="XLXN_129" />
        <signal name="output0" />
        <signal name="wire" />
        <signal name="clock" />
        <signal name="XLXN_133" />
        <signal name="XLXN_134" />
        <signal name="XLXN_135" />
        <signal name="XLXN_136" />
        <signal name="clear" />
        <port polarity="Output" name="O" />
        <port polarity="Input" name="sel" />
        <port polarity="Input" name="load(3:0)" />
        <port polarity="Input" name="clock" />
        <port polarity="Input" name="clear" />
        <blockdef name="DFlipFLop">
            <timestamp>2023-7-25T19:26:20</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="muxf8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-256" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-224" y2="-96" x1="256" />
            <line x2="256" y1="-256" y2="-224" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-224" y2="-224" x1="0" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="DFlipFLop" name="XLXI_2">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="output1" name="DATA" />
            <blockpin signalname="XLXN_43" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_3">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="output2" name="DATA" />
            <blockpin signalname="XLXN_44" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_1">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="output0" name="DATA" />
            <blockpin signalname="XLXN_41" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="DFlipFLop" name="XLXI_4">
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="output3" name="DATA" />
            <blockpin signalname="O" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="muxf8" name="XLXI_33">
            <blockpin signalname="load(0)" name="I0" />
            <blockpin signalname="XLXN_48" name="I1" />
            <blockpin signalname="sel" name="S" />
            <blockpin signalname="output0" name="O" />
        </block>
        <block symbolname="muxf8" name="XLXI_34">
            <blockpin signalname="load(1)" name="I0" />
            <blockpin signalname="XLXN_41" name="I1" />
            <blockpin signalname="sel" name="S" />
            <blockpin signalname="output1" name="O" />
        </block>
        <block symbolname="muxf8" name="XLXI_36">
            <blockpin signalname="load(3)" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="sel" name="S" />
            <blockpin signalname="output3" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_37">
            <blockpin signalname="XLXN_48" name="G" />
        </block>
        <block symbolname="muxf8" name="XLXI_35">
            <blockpin signalname="load(2)" name="I0" />
            <blockpin signalname="XLXN_43" name="I1" />
            <blockpin signalname="sel" name="S" />
            <blockpin signalname="output2" name="O" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_40">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="wire" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="load(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="1184" type="branch" />
            <wire x2="1936" y1="1184" y2="1184" x1="1888" />
        </branch>
        <branch name="load(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2432" y="1184" type="branch" />
            <wire x2="2464" y1="1184" y2="1184" x1="2432" />
        </branch>
        <branch name="load(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="736" y="1184" type="branch" />
            <wire x2="768" y1="1184" y2="1184" x1="736" />
        </branch>
        <instance x="1584" y="1936" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2176" y="1936" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1104" y="1920" name="XLXI_1" orien="R0">
        </instance>
        <branch name="load(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1312" y="1168" type="branch" />
            <wire x2="1344" y1="1168" y2="1168" x1="1312" />
        </branch>
        <branch name="XLXN_41">
            <wire x2="1344" y1="1296" y2="1296" x1="1280" />
            <wire x2="1280" y1="1296" y2="1552" x1="1280" />
            <wire x2="1488" y1="1552" y2="1552" x1="1280" />
            <wire x2="1488" y1="1552" y2="1760" x1="1488" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="2464" y1="1312" y2="1312" x1="2400" />
            <wire x2="2400" y1="1312" y2="1584" x1="2400" />
            <wire x2="2624" y1="1584" y2="1584" x1="2400" />
            <wire x2="2624" y1="1584" y2="1776" x1="2624" />
            <wire x2="2624" y1="1776" y2="1776" x1="2560" />
        </branch>
        <instance x="2752" y="1920" name="XLXI_4" orien="R0">
        </instance>
        <branch name="O">
            <wire x2="3168" y1="1760" y2="1760" x1="3136" />
        </branch>
        <instance x="768" y="1408" name="XLXI_33" orien="R0" />
        <instance x="1344" y="1392" name="XLXI_34" orien="R0" />
        <instance x="2464" y="1408" name="XLXI_36" orien="R0" />
        <branch name="XLXN_48">
            <wire x2="768" y1="1312" y2="1312" x1="736" />
        </branch>
        <instance x="608" y="1248" name="XLXI_37" orien="R90" />
        <branch name="sel">
            <wire x2="768" y1="1424" y2="1424" x1="464" />
            <wire x2="1344" y1="1424" y2="1424" x1="768" />
            <wire x2="1936" y1="1424" y2="1424" x1="1344" />
            <wire x2="2464" y1="1424" y2="1424" x1="1936" />
            <wire x2="768" y1="1376" y2="1424" x1="768" />
            <wire x2="1344" y1="1360" y2="1424" x1="1344" />
            <wire x2="1936" y1="1376" y2="1424" x1="1936" />
            <wire x2="2464" y1="1376" y2="1424" x1="2464" />
        </branch>
        <instance x="1936" y="1408" name="XLXI_35" orien="R0" />
        <branch name="XLXN_43">
            <wire x2="1936" y1="1312" y2="1312" x1="1872" />
            <wire x2="1872" y1="1312" y2="1536" x1="1872" />
            <wire x2="1984" y1="1536" y2="1536" x1="1872" />
            <wire x2="1984" y1="1536" y2="1776" x1="1984" />
            <wire x2="1984" y1="1776" y2="1776" x1="1968" />
        </branch>
        <branch name="load(3:0)">
            <wire x2="1056" y1="976" y2="976" x1="880" />
        </branch>
        <branch name="output3">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2752" y="1584" type="branch" />
            <wire x2="2720" y1="1584" y2="1888" x1="2720" />
            <wire x2="2752" y1="1888" y2="1888" x1="2720" />
            <wire x2="2752" y1="1584" y2="1584" x1="2720" />
            <wire x2="2832" y1="1584" y2="1584" x1="2752" />
            <wire x2="2832" y1="1280" y2="1280" x1="2784" />
            <wire x2="2832" y1="1280" y2="1584" x1="2832" />
        </branch>
        <branch name="output2">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2192" y="1536" type="branch" />
            <wire x2="2064" y1="1536" y2="1904" x1="2064" />
            <wire x2="2176" y1="1904" y2="1904" x1="2064" />
            <wire x2="2192" y1="1536" y2="1536" x1="2064" />
            <wire x2="2304" y1="1536" y2="1536" x1="2192" />
            <wire x2="2304" y1="1280" y2="1280" x1="2256" />
            <wire x2="2304" y1="1280" y2="1536" x1="2304" />
        </branch>
        <branch name="output1">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="1552" type="branch" />
            <wire x2="1520" y1="1552" y2="1904" x1="1520" />
            <wire x2="1584" y1="1904" y2="1904" x1="1520" />
            <wire x2="1600" y1="1552" y2="1552" x1="1520" />
            <wire x2="1728" y1="1552" y2="1552" x1="1600" />
            <wire x2="1728" y1="1264" y2="1264" x1="1664" />
            <wire x2="1728" y1="1264" y2="1552" x1="1728" />
        </branch>
        <branch name="output0">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1056" y="1552" type="branch" />
            <wire x2="1008" y1="1552" y2="1888" x1="1008" />
            <wire x2="1104" y1="1888" y2="1888" x1="1008" />
            <wire x2="1056" y1="1552" y2="1552" x1="1008" />
            <wire x2="1152" y1="1552" y2="1552" x1="1056" />
            <wire x2="1152" y1="1280" y2="1280" x1="1088" />
            <wire x2="1152" y1="1280" y2="1552" x1="1152" />
        </branch>
        <branch name="wire">
            <wire x2="1088" y1="1632" y2="1632" x1="848" />
            <wire x2="1568" y1="1632" y2="1632" x1="1088" />
            <wire x2="1568" y1="1632" y2="1776" x1="1568" />
            <wire x2="1584" y1="1776" y2="1776" x1="1568" />
            <wire x2="2144" y1="1632" y2="1632" x1="1568" />
            <wire x2="2144" y1="1632" y2="1776" x1="2144" />
            <wire x2="2176" y1="1776" y2="1776" x1="2144" />
            <wire x2="2672" y1="1632" y2="1632" x1="2144" />
            <wire x2="2672" y1="1632" y2="1760" x1="2672" />
            <wire x2="2752" y1="1760" y2="1760" x1="2672" />
            <wire x2="1088" y1="1632" y2="1760" x1="1088" />
            <wire x2="1104" y1="1760" y2="1760" x1="1088" />
            <wire x2="848" y1="1632" y2="1648" x1="848" />
        </branch>
        <instance x="928" y="1872" name="XLXI_40" orien="R270">
        </instance>
        <branch name="clock">
            <wire x2="848" y1="1888" y2="1888" x1="480" />
            <wire x2="848" y1="1872" y2="1888" x1="848" />
        </branch>
        <branch name="clear">
            <wire x2="736" y1="2048" y2="2048" x1="480" />
            <wire x2="1072" y1="2048" y2="2048" x1="736" />
            <wire x2="1568" y1="2048" y2="2048" x1="1072" />
            <wire x2="2144" y1="2048" y2="2048" x1="1568" />
            <wire x2="2624" y1="2048" y2="2048" x1="2144" />
            <wire x2="752" y1="1760" y2="1760" x1="736" />
            <wire x2="736" y1="1760" y2="2048" x1="736" />
            <wire x2="1104" y1="1824" y2="1824" x1="1072" />
            <wire x2="1072" y1="1824" y2="2048" x1="1072" />
            <wire x2="1584" y1="1840" y2="1840" x1="1568" />
            <wire x2="1568" y1="1840" y2="2048" x1="1568" />
            <wire x2="2176" y1="1840" y2="1840" x1="2144" />
            <wire x2="2144" y1="1840" y2="2048" x1="2144" />
            <wire x2="2624" y1="1824" y2="2048" x1="2624" />
            <wire x2="2752" y1="1824" y2="1824" x1="2624" />
        </branch>
        <iomarker fontsize="28" x="3168" y="1760" name="O" orien="R0" />
        <iomarker fontsize="28" x="880" y="976" name="load(3:0)" orien="R180" />
        <iomarker fontsize="28" x="480" y="1888" name="clock" orien="R180" />
        <iomarker fontsize="28" x="480" y="2048" name="clear" orien="R180" />
        <iomarker fontsize="28" x="464" y="1424" name="sel" orien="R180" />
        <text style="fontsize:150;fontname:Arial" x="1588" y="516">PISO</text>
    </sheet>
</drawing>