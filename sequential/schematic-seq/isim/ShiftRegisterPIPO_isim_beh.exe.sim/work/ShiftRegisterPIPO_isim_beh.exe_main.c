/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    unisims_ver_m_00000000003510477262_0709700939_init();
    unisims_ver_m_00000000002549801008_1565138397_init();
    unisims_ver_m_00000000002573598747_3342441346_init();
    unisims_ver_m_00000000000236260522_2449448540_init();
    work_m_00000000004217895895_0772305181_init();
    work_m_00000000002218174817_3346509026_init();
    work_m_00000000000854388045_3259188816_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000000854388045_3259188816");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
