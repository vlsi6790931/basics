/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    unisims_ver_m_00000000003510477262_0709700939_init();
    unisims_ver_m_00000000002549801008_1565138397_init();
    unisims_ver_m_00000000002573598747_3342441346_init();
    unisims_ver_m_00000000000236260522_2449448540_init();
    work_m_00000000004217895895_0256367448_init();
    work_m_00000000003168144409_0298517046_init();
    unisims_ver_m_00000000003927721830_1593237687_init();
    unisims_ver_m_00000000001844249156_3898728092_init();
    work_m_00000000001505973063_2695393351_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000001505973063_2695393351");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
