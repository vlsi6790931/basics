<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_8" />
        <signal name="Q0" />
        <signal name="XLXN_100" />
        <signal name="XLXN_15" />
        <signal name="XLXN_102" />
        <signal name="Q2" />
        <signal name="XLXN_104" />
        <signal name="wire">
        </signal>
        <signal name="XLXN_47" />
        <signal name="w" />
        <signal name="clear" />
        <signal name="XLXN_97" />
        <signal name="Q3" />
        <signal name="XLXN_111" />
        <signal name="Q1" />
        <signal name="clock" />
        <signal name="XLXN_115">
        </signal>
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q3" />
        <port polarity="Output" name="Q1" />
        <port polarity="Input" name="clock" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="nor2b1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="32" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="44" cy="-64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="OneSecTimer">
            <timestamp>2023-7-28T11:45:3</timestamp>
            <rect width="96" x="64" y="-128" height="96" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="224" y1="-80" y2="-80" x1="160" />
            <line x2="112" y1="-176" y2="-124" x1="112" />
        </blockdef>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="Q0" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_9">
            <blockpin signalname="w" name="clear" />
            <blockpin signalname="XLXN_47" name="T" />
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_10">
            <blockpin signalname="w" name="clear" />
            <blockpin signalname="Q0" name="T" />
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_11">
            <blockpin signalname="w" name="clear" />
            <blockpin signalname="XLXN_8" name="T" />
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_12">
            <blockpin signalname="w" name="clear" />
            <blockpin signalname="XLXN_15" name="T" />
            <blockpin signalname="wire" name="clock" />
            <blockpin signalname="Q3" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="XLXN_8" name="I0" />
            <blockpin signalname="Q2" name="I1" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_19">
            <blockpin signalname="XLXN_47" name="P" />
        </block>
        <block symbolname="nor2b1" name="XLXI_35">
            <blockpin signalname="clear" name="I0" />
            <blockpin signalname="XLXN_97" name="I1" />
            <blockpin signalname="w" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_36">
            <blockpin signalname="Q0" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="Q3" name="I2" />
            <blockpin signalname="XLXN_97" name="O" />
        </block>
        <block symbolname="OneSecTimer" name="XLXI_47">
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="wire" name="O" />
            <blockpin signalname="clear" name="clear" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="2720" height="1760">
        <branch name="XLXN_8">
            <wire x2="1536" y1="816" y2="816" x1="1472" />
            <wire x2="1536" y1="816" y2="1136" x1="1536" />
            <wire x2="1552" y1="1136" y2="1136" x1="1536" />
            <wire x2="1856" y1="816" y2="816" x1="1536" />
        </branch>
        <instance x="1216" y="720" name="XLXI_5" orien="M180" />
        <branch name="Q0">
            <wire x2="1120" y1="544" y2="544" x1="608" />
            <wire x2="1120" y1="544" y2="784" x1="1120" />
            <wire x2="1120" y1="784" y2="1136" x1="1120" />
            <wire x2="1200" y1="1136" y2="1136" x1="1120" />
            <wire x2="1216" y1="784" y2="784" x1="1120" />
            <wire x2="608" y1="544" y2="656" x1="608" />
            <wire x2="1120" y1="1136" y2="1136" x1="1104" />
            <wire x2="1120" y1="448" y2="544" x1="1120" />
        </branch>
        <instance x="848" y="1232" name="XLXI_9" orien="R0">
        </instance>
        <instance x="1200" y="1232" name="XLXI_10" orien="R0">
        </instance>
        <instance x="1552" y="1232" name="XLXI_11" orien="R0">
        </instance>
        <instance x="1952" y="1232" name="XLXI_12" orien="R0">
        </instance>
        <branch name="XLXN_15">
            <wire x2="2128" y1="960" y2="960" x1="1936" />
            <wire x2="1936" y1="960" y2="1136" x1="1936" />
            <wire x2="1952" y1="1136" y2="1136" x1="1936" />
            <wire x2="2128" y1="848" y2="848" x1="2112" />
            <wire x2="2128" y1="848" y2="960" x1="2128" />
        </branch>
        <branch name="Q2">
            <wire x2="1824" y1="1136" y2="1136" x1="1808" />
            <wire x2="1824" y1="464" y2="880" x1="1824" />
            <wire x2="1824" y1="880" y2="1136" x1="1824" />
            <wire x2="1856" y1="880" y2="880" x1="1824" />
        </branch>
        <instance x="1856" y="752" name="XLXI_6" orien="M180" />
        <branch name="XLXN_47">
            <wire x2="848" y1="1136" y2="1136" x1="816" />
        </branch>
        <instance x="816" y="1200" name="XLXI_19" orien="R270" />
        <branch name="w">
            <wire x2="944" y1="960" y2="960" x1="928" />
            <wire x2="944" y1="960" y2="1024" x1="944" />
            <wire x2="944" y1="1024" y2="1040" x1="944" />
            <wire x2="1296" y1="1024" y2="1024" x1="944" />
            <wire x2="1296" y1="1024" y2="1040" x1="1296" />
            <wire x2="1648" y1="1024" y2="1024" x1="1296" />
            <wire x2="1648" y1="1024" y2="1040" x1="1648" />
            <wire x2="2048" y1="1024" y2="1024" x1="1648" />
            <wire x2="2048" y1="1024" y2="1040" x1="2048" />
        </branch>
        <branch name="clear">
            <wire x2="512" y1="992" y2="992" x1="368" />
            <wire x2="656" y1="992" y2="992" x1="512" />
            <wire x2="672" y1="992" y2="992" x1="656" />
            <wire x2="512" y1="992" y2="1072" x1="512" />
            <wire x2="512" y1="1072" y2="1088" x1="512" />
            <wire x2="512" y1="1088" y2="1104" x1="512" />
        </branch>
        <branch name="XLXN_97">
            <wire x2="672" y1="912" y2="928" x1="672" />
        </branch>
        <instance x="672" y="1056" name="XLXI_35" orien="R0" />
        <branch name="Q3">
            <wire x2="2208" y1="656" y2="656" x1="736" />
            <wire x2="2208" y1="656" y2="1136" x1="2208" />
            <wire x2="2208" y1="464" y2="656" x1="2208" />
        </branch>
        <branch name="Q1">
            <wire x2="672" y1="608" y2="656" x1="672" />
            <wire x2="1472" y1="608" y2="608" x1="672" />
            <wire x2="1472" y1="608" y2="944" x1="1472" />
            <wire x2="1472" y1="944" y2="1136" x1="1472" />
            <wire x2="1216" y1="848" y2="944" x1="1216" />
            <wire x2="1472" y1="944" y2="944" x1="1216" />
            <wire x2="1472" y1="1136" y2="1136" x1="1456" />
            <wire x2="1472" y1="448" y2="608" x1="1472" />
        </branch>
        <text style="fontsize:100;fontname:Arial" x="716" y="176">SYNC_DECADE_COUNTER</text>
        <instance x="544" y="656" name="XLXI_36" orien="R90" />
        <iomarker fontsize="28" x="1120" y="448" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="1472" y="448" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="1824" y="464" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="2208" y="464" name="Q3" orien="R270" />
        <branch name="wire">
            <wire x2="640" y1="1200" y2="1200" x1="624" />
            <wire x2="832" y1="1200" y2="1200" x1="640" />
            <wire x2="848" y1="1200" y2="1200" x1="832" />
            <wire x2="832" y1="1200" y2="1296" x1="832" />
            <wire x2="1200" y1="1296" y2="1296" x1="832" />
            <wire x2="1552" y1="1296" y2="1296" x1="1200" />
            <wire x2="1936" y1="1296" y2="1296" x1="1552" />
            <wire x2="1200" y1="1200" y2="1296" x1="1200" />
            <wire x2="1552" y1="1200" y2="1296" x1="1552" />
            <wire x2="1952" y1="1200" y2="1200" x1="1936" />
            <wire x2="1936" y1="1200" y2="1296" x1="1936" />
        </branch>
        <instance x="400" y="1280" name="XLXI_47" orien="R0">
        </instance>
        <branch name="clock">
            <wire x2="400" y1="1200" y2="1200" x1="368" />
        </branch>
        <iomarker fontsize="28" x="368" y="1200" name="clock" orien="R180" />
        <iomarker fontsize="28" x="368" y="992" name="clear" orien="R180" />
    </sheet>
</drawing>