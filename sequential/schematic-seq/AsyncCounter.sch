<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clear" />
        <signal name="Q1" />
        <signal name="Q0" />
        <signal name="T" />
        <signal name="clock" />
        <signal name="XLXN_40" />
        <signal name="Q2" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <port polarity="Input" name="clear" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q0" />
        <port polarity="Input" name="clock" />
        <port polarity="Output" name="Q2" />
        <blockdef name="TFlipFlop">
            <timestamp>2023-7-26T9:42:53</timestamp>
            <rect width="128" x="64" y="-128" height="128" />
            <line x2="96" y1="-128" y2="-192" x1="96" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="256" y1="-96" y2="-96" x1="192" />
            <line x2="256" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="vcc" name="XLXI_5">
            <blockpin signalname="T" name="P" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_4">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="T" name="T" />
            <blockpin signalname="Q1" name="clock" />
            <blockpin signalname="Q2" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_2">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="T" name="T" />
            <blockpin signalname="clock" name="clock" />
            <blockpin signalname="Q0" name="Q" />
            <blockpin name="Qb" />
        </block>
        <block symbolname="TFlipFlop" name="XLXI_3">
            <blockpin signalname="clear" name="clear" />
            <blockpin signalname="T" name="T" />
            <blockpin signalname="Q0" name="clock" />
            <blockpin signalname="Q1" name="Q" />
            <blockpin name="Qb" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1760" height="1360">
        <instance x="1104" y="752" name="XLXI_4" orien="R0">
        </instance>
        <instance x="400" y="752" name="XLXI_2" orien="R0">
        </instance>
        <instance x="768" y="752" name="XLXI_3" orien="R0">
        </instance>
        <branch name="clear">
            <wire x2="496" y1="480" y2="480" x1="336" />
            <wire x2="496" y1="480" y2="560" x1="496" />
            <wire x2="864" y1="480" y2="480" x1="496" />
            <wire x2="864" y1="480" y2="560" x1="864" />
            <wire x2="1200" y1="480" y2="480" x1="864" />
            <wire x2="1200" y1="480" y2="560" x1="1200" />
        </branch>
        <branch name="Q1">
            <wire x2="1056" y1="656" y2="656" x1="1024" />
            <wire x2="1056" y1="656" y2="720" x1="1056" />
            <wire x2="1104" y1="720" y2="720" x1="1056" />
            <wire x2="1056" y1="720" y2="880" x1="1056" />
        </branch>
        <branch name="Q0">
            <wire x2="704" y1="656" y2="656" x1="656" />
            <wire x2="704" y1="656" y2="720" x1="704" />
            <wire x2="768" y1="720" y2="720" x1="704" />
            <wire x2="704" y1="720" y2="880" x1="704" />
        </branch>
        <branch name="T">
            <wire x2="400" y1="560" y2="560" x1="240" />
            <wire x2="400" y1="560" y2="656" x1="400" />
            <wire x2="768" y1="560" y2="560" x1="400" />
            <wire x2="768" y1="560" y2="656" x1="768" />
            <wire x2="1104" y1="560" y2="560" x1="768" />
            <wire x2="1104" y1="560" y2="656" x1="1104" />
        </branch>
        <iomarker fontsize="28" x="336" y="480" name="clear" orien="R180" />
        <branch name="clock">
            <wire x2="400" y1="720" y2="720" x1="368" />
        </branch>
        <iomarker fontsize="28" x="368" y="720" name="clock" orien="R180" />
        <instance x="304" y="560" name="XLXI_5" orien="R180" />
        <branch name="Q2">
            <wire x2="1376" y1="656" y2="656" x1="1360" />
            <wire x2="1392" y1="656" y2="656" x1="1376" />
            <wire x2="1392" y1="656" y2="880" x1="1392" />
        </branch>
        <iomarker fontsize="28" x="1056" y="880" name="Q1" orien="R90" />
        <iomarker fontsize="28" x="1392" y="880" name="Q2" orien="R90" />
        <iomarker fontsize="28" x="704" y="880" name="Q0" orien="R90" />
        <text style="fontsize:150;fontname:Arial" x="212" y="284">Async_Up_Counter</text>
    </sheet>
</drawing>