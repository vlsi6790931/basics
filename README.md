# BASICS OF VLSI

## Why ?

This repo contains schematics and Verilog code for logging my learning and projects from my foundation-level program at Pine Training Academy.

## File Structure

```
    .
    └── basics
        ├── combinational
        │   ├── schematic-combi
        │   └── verilog-combi
        ├── sequential
        :   ├── schematic-seq
            └── verilog-seq

```

<!--- 
- [ ] [Set up project integrations](https://gitlab.com/vlsi6790931/basics/-/settings/integrations)
--->
